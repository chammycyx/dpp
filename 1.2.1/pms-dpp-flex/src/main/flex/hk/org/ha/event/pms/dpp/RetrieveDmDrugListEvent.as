package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmDrugListEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		
		private var _event:Event;
		
		public function RetrieveDmDrugListEvent(itemCode:String, event:Event=null):void {
			super();
			_itemCode = itemCode;
			_event = event;
		}
		
		public function get itemCode():String 
		{
			return _itemCode;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}