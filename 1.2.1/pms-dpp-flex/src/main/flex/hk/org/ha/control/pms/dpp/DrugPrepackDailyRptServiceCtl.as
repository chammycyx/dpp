package hk.org.ha.control.pms.dpp {
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dpp.RetrieveDrugPrepackDailyRptListEvent;
	import hk.org.ha.model.pms.dpp.biz.DrugPrepackDailyRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("drugPrepackDailyRptServiceCtl", restrict="false")]
	public class DrugPrepackDailyRptServiceCtl {
		
		[In]
		public var drugPrepackDailyRptService:DrugPrepackDailyRptServiceBean;
		
		private var event:Event;
		
		[Observer]
		public function retrieveDrugPrepackDailyRptList(evt:RetrieveDrugPrepackDailyRptListEvent):void {
			event = evt.event;
			drugPrepackDailyRptService.retrieveDrugPrepackDailyRptList(evt.drugPrepackDailyRptCriteria, retrieveDrugPrepackDailyRptListResult);
		}
		
		public function retrieveDrugPrepackDailyRptListResult(evt:TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
	}
}