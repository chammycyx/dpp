package hk.org.ha.event.pms.dpp
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ResetDrugItemInfoEvent extends AbstractTideEvent {
		
		public function ResetDrugItemInfoEvent():void {
			super();
		}
	}
}