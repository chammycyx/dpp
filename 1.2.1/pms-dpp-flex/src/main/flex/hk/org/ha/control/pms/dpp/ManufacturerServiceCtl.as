package hk.org.ha.control.pms.dpp {
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dpp.RetrieveManufacturerByManufCodeEvent;
	import hk.org.ha.model.pms.dpp.biz.ManufacturerServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("manufacturerServiceCtl", restrict="false")]
	public class ManufacturerServiceCtl {

		[In]
		public var manufacturerService:ManufacturerServiceBean;
		
		private var event:Event;
		
		[Observer]
		public function retrieveManufacturerByManufCode(evt:RetrieveManufacturerByManufCodeEvent):void {
			event = evt.event;
			manufacturerService.retrieveManufacturerByManufCode(evt.manufCode, retrieveManufacturerByManufCodeResult);
		}
		
		public function retrieveManufacturerByManufCodeResult(evt:TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
	}
}