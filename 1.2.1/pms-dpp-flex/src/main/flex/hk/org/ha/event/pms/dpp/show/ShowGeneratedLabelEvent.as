package hk.org.ha.event.pms.dpp.show 
{
	import flash.events.Event;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowGeneratedLabelEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		public function ShowGeneratedLabelEvent(event:Event=null):void 
		{
			super();
			_event = event;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
}

