package hk.org.ha.event.pms.dpp.show
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPrepackRecordCancelReasonPopupEvent extends AbstractTideEvent
	{
		public function ShowPrepackRecordCancelReasonPopupEvent()
		{
			super();
		}
	}
}