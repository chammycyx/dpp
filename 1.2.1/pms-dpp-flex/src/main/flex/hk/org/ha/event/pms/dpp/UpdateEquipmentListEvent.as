package hk.org.ha.event.pms.dpp {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateEquipmentListEvent extends AbstractTideEvent {
		
		private var _equipmentList:ListCollectionView;
		
		public function UpdateEquipmentListEvent(equipmentList:ListCollectionView):void {
			super();
			_equipmentList = equipmentList;
		}
		
		public function get equipmentList():ListCollectionView {
			return _equipmentList;
		}
	}
}