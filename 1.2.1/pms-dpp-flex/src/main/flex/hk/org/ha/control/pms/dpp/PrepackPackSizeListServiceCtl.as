package hk.org.ha.control.pms.dpp {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dpp.RetrievePrepackPackSizeListEvent;
	import hk.org.ha.model.pms.dpp.biz.PrepackPackSizeListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("prepackPackSizeListServiceCtl", restrict="true")]
	public class PrepackPackSizeListServiceCtl {
		
		[In]
		public var prepackPackSizeListService:PrepackPackSizeListServiceBean;
		
		[In]
		public var ctx:Context;
		
		private var event:Event;
		
		[Observer]
		public function retrievePrepackPackSizeListByDrugItemInfoId(evt:RetrievePrepackPackSizeListEvent):void {
			event = evt.event;
			ctx.prepackPackSizeList = null;
			prepackPackSizeListService.retrievePrepackPackSizeListByDrugItemInfoId(evt.drugItemInfoId, retrievePrepackPackSizeListByDrugItemInfoIdResult);
		}
		
		public function retrievePrepackPackSizeListByDrugItemInfoIdResult(evt:TideResultEvent):void {
			if(event != null) {
				evt.context.dispatchEvent(event);
			}
		}
	}
}