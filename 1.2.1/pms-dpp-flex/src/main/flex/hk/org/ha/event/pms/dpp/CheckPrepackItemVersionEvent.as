package hk.org.ha.event.pms.dpp
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;
	
	public class CheckPrepackItemVersionEvent extends AbstractTideEvent {
				
		private var _prepackRecord:PrepackRecord;
		private var _screenName:String;
		
		public function CheckPrepackItemVersionEvent(prepackRecord:PrepackRecord, screenName:String):void {
			super();
			_prepackRecord = prepackRecord;
			_screenName = screenName;
		}
		
		public function get prepackRecord():PrepackRecord {
			return _prepackRecord;
		}
		
		public function get screenName():String {
			return _screenName;
		}
	}
}