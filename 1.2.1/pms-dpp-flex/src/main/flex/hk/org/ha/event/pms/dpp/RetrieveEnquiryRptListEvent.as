package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dpp.vo.EnquiryRptCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveEnquiryRptListEvent extends AbstractTideEvent {
		
		private var _enquiryRpt:EnquiryRptCriteria;
		
		private var _event:Event;
		
		public function RetrieveEnquiryRptListEvent(enquiryRpt:EnquiryRptCriteria, event:Event=null):void {
			super();
			_enquiryRpt = enquiryRpt;
			_event = event;
		}
		
		public function get enquiryRptCriteria():EnquiryRptCriteria {
			return _enquiryRpt;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}