package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dpp.persistence.DrugItemInfo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateDrugItemInfoEvent extends AbstractTideEvent {
		
		private var _drugItemInfo:DrugItemInfo;
		
		private var _event:Event;
		
		public function UpdateDrugItemInfoEvent(drugItemInfo:DrugItemInfo, event:Event=null):void {
			super();
			_drugItemInfo = drugItemInfo;
			_event = event;
		}
		
		public function get drugItemInfo():DrugItemInfo {
			return _drugItemInfo;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}