package hk.org.ha.control.pms.dpp {
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dpp.RetrieveAuditRptListEvent;
	import hk.org.ha.model.pms.dpp.biz.AuditRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("auditRptServiceCtl", restrict="false")]
	public class AuditRptServiceCtl {
		
		[In]
		public var auditRptService:AuditRptServiceBean;
		
		private var event:Event;
		
		[Observer]
		public function retrieveAuditRptList(evt:RetrieveAuditRptListEvent):void {
			event = evt.event;
			auditRptService.retrieveAuditRptList(evt.auditRptCriteria, retrieveAuditRptListResult);
		}
		
		public function retrieveAuditRptListResult(evt:TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
	}
}