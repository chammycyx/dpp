package hk.org.ha.event.pms.dpp
{
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshPrepackRecordListEvent extends AbstractTideEvent
	{
		private var _event:Event;
		
		public function RefreshPrepackRecordListEvent(event:Event=null)	{
			super();
			_event = event;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}