package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveManufacturerByManufCodeEvent extends AbstractTideEvent {
		
		private var _event:Event;
		
		private var _manufCode:String;
		
		public function RetrieveManufacturerByManufCodeEvent(manufCode:String, event:Event=null):void {
			super();
			_event = event;
			_manufCode = manufCode;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get manufCode():String {
			return _manufCode;
		}
	}
}