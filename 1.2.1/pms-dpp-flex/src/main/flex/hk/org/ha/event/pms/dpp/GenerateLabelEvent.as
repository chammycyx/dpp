package hk.org.ha.event.pms.dpp {
	
	import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class GenerateLabelEvent extends AbstractTideEvent 
	{
		private var _id:Number;
		
		public function GenerateLabelEvent(id:Number):void 
		{
			super();
			_id = id;
		}
		
		public function get id():Number {
			return _id;
		}
	}
}