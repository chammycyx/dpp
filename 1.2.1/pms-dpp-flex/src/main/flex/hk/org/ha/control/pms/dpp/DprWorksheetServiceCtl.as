package hk.org.ha.control.pms.dpp {
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dpp.CheckPrepackItemVersionEvent;
	import hk.org.ha.event.pms.dpp.PrepareDprWorksheetEvent;
	import hk.org.ha.event.pms.dpp.ShowPrepackItemPrintPopupEvent;
	import hk.org.ha.event.pms.dpp.ShowVersionCheckFailureMessageEvent;
	import hk.org.ha.event.pms.dpp.show.ShowDprWorksheetEvent;
	import hk.org.ha.model.pms.dpp.biz.DprWorksheetServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("dprWorksheetServiceCtl", restrict="true")]
	public class DprWorksheetServiceCtl {
		
		[In]
		public var dprWorksheetService:DprWorksheetServiceBean;
		
		private var checkPrepackItemVersionEvent:CheckPrepackItemVersionEvent;
				
		[Observer]
		public function checkPrepackItemVersion(evt:CheckPrepackItemVersionEvent):void
		{
			checkPrepackItemVersionEvent = evt;
			dprWorksheetService.checkPrepackItemVersion(evt.prepackRecord, checkPrepackItemVersionResult);
		}
		
		private function checkPrepackItemVersionResult(evt:TideResultEvent):void
		{
			if (evt.result as String != null)
			{
				evt.context.dispatchEvent(new ShowVersionCheckFailureMessageEvent(evt.result as String));
			}
			else
			{
				evt.context.dispatchEvent(new ShowPrepackItemPrintPopupEvent(checkPrepackItemVersionEvent.screenName));
			}	
		}
		
		[Observer]
		public function prepareDprWorksheet(evt:PrepareDprWorksheetEvent):void {
			dprWorksheetService.prepareDprWorksheet(evt.id, prepareDprWorksheetResult);
		}
		
		public function prepareDprWorksheetResult(evt:TideResultEvent):void {
				evt.context.dispatchEvent(new ShowDprWorksheetEvent());
		}
		
	}
}