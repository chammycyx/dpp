package hk.org.ha.event.pms.dpp {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateDiscrepancyListEvent extends AbstractTideEvent {
		
		private var _discrepancyList:ListCollectionView;
		
		public function UpdateDiscrepancyListEvent(discrepancyList:ListCollectionView):void {
			super();
			_discrepancyList = discrepancyList;
		}
		
		public function get discrepancyList():ListCollectionView {
			return _discrepancyList;
		}
	}
}