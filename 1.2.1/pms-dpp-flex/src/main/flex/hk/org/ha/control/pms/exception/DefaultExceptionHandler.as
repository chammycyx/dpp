package hk.org.ha.control.pms.exception {

    import hk.org.ha.event.pms.exception.show.ShowExceptionPopupEvent;
    
    import mx.messaging.messages.ErrorMessage;
    
    import org.granite.tide.BaseContext;
    import org.granite.tide.IExceptionHandler;

    public class DefaultExceptionHandler implements IExceptionHandler 
	{
        
        public function accepts(emsg:ErrorMessage):Boolean 
		{
            return true;
        }

        public function handle(context:BaseContext, emsg:ErrorMessage):void 
		{
   			context.dispatchEvent(new ShowExceptionPopupEvent(emsg.faultCode, emsg.faultString, emsg.faultDetail));
        }
        
    }
}
