package hk.org.ha.event.pms.dpp {
	import org.granite.tide.events.AbstractTideEvent;
	import flash.events.Event;
	
	public class RefreshEnquiryRptItemEvent extends AbstractTideEvent {
		
		private var _event:Event;
		
		public function RefreshEnquiryRptItemEvent(event:Event=null):void {
			super();
			_event = event;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}