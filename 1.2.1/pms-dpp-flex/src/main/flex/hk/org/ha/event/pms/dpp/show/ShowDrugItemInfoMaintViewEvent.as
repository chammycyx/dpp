package hk.org.ha.event.pms.dpp.show
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDrugItemInfoMaintViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowDrugItemInfoMaintViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}