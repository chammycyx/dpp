package hk.org.ha.event.pms.dpp {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateActionTakenListEvent extends AbstractTideEvent {
		
		private var _actionTakenList:ListCollectionView;
		
		public function UpdateActionTakenListEvent(actionTakenList:ListCollectionView):void {
			super();
			_actionTakenList = actionTakenList;
		}
		
		public function get actionTakenList():ListCollectionView {
			return _actionTakenList;
		}
	}
}