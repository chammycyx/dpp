package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dpp.vo.DrugPrepackDailyRptCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDrugPrepackDailyRptListEvent extends AbstractTideEvent {
		
		private var _drugPrepackDailyRptCriteria:DrugPrepackDailyRptCriteria;
		
		private var _event:Event;
		
		public function RetrieveDrugPrepackDailyRptListEvent(drugPrepackDailyRptCriteria:DrugPrepackDailyRptCriteria, event:Event=null):void {
			super();
			_drugPrepackDailyRptCriteria = drugPrepackDailyRptCriteria;
			_event = event;
		}
		
		public function get drugPrepackDailyRptCriteria():DrugPrepackDailyRptCriteria {
			return _drugPrepackDailyRptCriteria;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}