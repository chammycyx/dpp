package hk.org.ha.event.pms.dpp {
	
	import mx.collections.ListCollectionView;
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateDiscrepancyDetailsListEvent extends AbstractTideEvent {
		
		private var _event:Event;
		private var _discrepancyList:ListCollectionView;
		private var _prepackRecordId:Number;
		
		public function UpdateDiscrepancyDetailsListEvent(prepackRecordId:Number, discrepancyList:ListCollectionView, event:Event=null):void {
			super();
			_event = event;
			_discrepancyList = discrepancyList;
			_prepackRecordId = prepackRecordId;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get prepackRecordId():Number {
			return _prepackRecordId;
		}
		
		public function get discrepancyList():ListCollectionView {
			return _discrepancyList;
		}
	}
}