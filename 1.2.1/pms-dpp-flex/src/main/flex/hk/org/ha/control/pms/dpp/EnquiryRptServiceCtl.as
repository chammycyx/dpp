package hk.org.ha.control.pms.dpp {
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dpp.RefreshEnquiryRptViewEvent;
	import hk.org.ha.event.pms.dpp.RetrieveEnquiryRptListEvent;
	import hk.org.ha.event.pms.dpp.RetrieveManufacturerByManufCodeEvent;
	import hk.org.ha.model.pms.dpp.biz.ContainerListServiceBean;
	import hk.org.ha.model.pms.dpp.biz.EnquiryRptServiceBean;
	import hk.org.ha.model.pms.dpp.biz.EquipmentListServiceBean;
	import hk.org.ha.model.pms.dpp.biz.ManufacturerListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("enquiryRptServiceCtl", restrict="false")]
	public class EnquiryRptServiceCtl {
		
		[In]
		public var enquiryRptService:EnquiryRptServiceBean;
		
		[In]
		public var containerListService:ContainerListServiceBean;
		
		[In]
		public var equipmentListService:EquipmentListServiceBean;
		
		[In]
		public var manufacturerListService:ManufacturerListServiceBean;
		
		private var event:Event;
		
		[Observer]
		public function refreshEnquiryRptView(evt:RefreshEnquiryRptViewEvent):void {
			event = evt.event;
			containerListService.retrieveContainerList();
			equipmentListService.retrieveEquipmentList();
			manufacturerListService.retrieveManufacturerList();
		}
		
		[Observer]
		public function retrieveEnquiryRptList(evt:RetrieveEnquiryRptListEvent):void {
			event = evt.event;
			enquiryRptService.retrieveEnquiryRptList(evt.enquiryRptCriteria, retrieveEnquiryRptListResult);
		}
		
		public function retrieveEnquiryRptListResult(evt:TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
	}
}