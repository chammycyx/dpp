package hk.org.ha.event.pms.dpp {
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateContainerListEvent extends AbstractTideEvent {
		
		private var _containerList:ListCollectionView;
		
		public function UpdateContainerListEvent(containerList:ListCollectionView):void {
			super();
			_containerList = containerList;
		}
		
		public function get containerList():ListCollectionView {
			return _containerList;
		}
	}
}