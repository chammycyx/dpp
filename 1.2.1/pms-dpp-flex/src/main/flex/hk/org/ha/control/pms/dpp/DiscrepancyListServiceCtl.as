package hk.org.ha.control.pms.dpp {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dpp.RetrieveDiscrepancyDetailsListEvent;
	import hk.org.ha.event.pms.dpp.RetrieveDiscrepancyListEvent;
	import hk.org.ha.event.pms.dpp.UpdateDiscrepancyListEvent;
	import hk.org.ha.event.pms.dpp.UpdateDiscrepancyDetailsListEvent
	import hk.org.ha.model.pms.dpp.biz.DiscrepancyListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("discrepancyListServiceCtl", restrict="true")]
	public class DiscrepancyListServiceCtl {
		
		[In]
		public var discrepancyListService:DiscrepancyListServiceBean;
		
		private var event:Event;
		
		[Observer]
		public function retrieveDiscrepancyList(evt:RetrieveDiscrepancyListEvent):void {
			discrepancyListService.retrieveDiscrepancyList();
		}
		
		[Observer]
		public function updateDiscrepancyList(evt:UpdateDiscrepancyListEvent):void {
			discrepancyListService.updateDiscrepancyList(evt.discrepancyList);
		}
		
		[Observer]
		public function retrieveDiscrepancyDetailsList(evt:RetrieveDiscrepancyDetailsListEvent):void {
			event = evt.event;
			discrepancyListService.retrieveDiscrepancyDetailsList(evt.prepackRecordVerId, retrieveDiscrepancyDetailsListResult);
		}
		
		public function retrieveDiscrepancyDetailsListResult(evt:TideResultEvent):void {
			if ( event != null) {
				evt.context.dispatchEvent(event);
			}
		}
		
		[Observer]
		public function updateDiscrepancyDetailsList(evt:UpdateDiscrepancyDetailsListEvent):void {
			event = evt.event;
			discrepancyListService.updateDiscrepancyDetailsList(evt.prepackRecordId, evt.discrepancyList, updateDiscrepancyDetailsListResult);
		}
		
		public function updateDiscrepancyDetailsListResult(evt:TideResultEvent):void {
			if(event != null) {
				evt.context.dispatchEvent(event);
			}
		}
	}
	
}