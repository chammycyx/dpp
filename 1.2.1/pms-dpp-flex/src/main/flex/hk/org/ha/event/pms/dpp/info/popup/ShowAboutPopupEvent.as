package hk.org.ha.event.pms.dpp.info.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowAboutPopupEvent extends AbstractTideEvent 
	{				
		public function ShowAboutPopupEvent():void 
		{
			super();
		}		
	}
}