package hk.org.ha.control.pms.exception {
		
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.ShowSystemMessagePopupEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	
	import mx.messaging.messages.ErrorMessage;
	
	import org.granite.tide.BaseContext;
	import org.granite.tide.IExceptionHandler;
	
	public class PrintingExceptionHandler implements IExceptionHandler {

		public static const PRINTING_EXCEPTION:String = "PrintingException";
		
		public function accepts(emsg:ErrorMessage):Boolean {
			return emsg.faultCode == PRINTING_EXCEPTION;
		}
		
		public function handle(context:BaseContext, emsg:ErrorMessage):void {
//			trace("Error " + emsg.extendedData["message"]);
//			evt.errorString = sysMsgMap.getMessage(emsg.extendedData["messageCode"], emsg.extendedData["messageParam"]); 
//			context.dispatchEvent(evt);
			var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
			msgProp.messageCode = "0078";
			msgProp.messageWinHeight = 200;				
			msgProp.setOkButtonOnly = true;
			msgProp.messageParams = new Array(emsg.extendedData["message"]);
			context.dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
		}
	}
}