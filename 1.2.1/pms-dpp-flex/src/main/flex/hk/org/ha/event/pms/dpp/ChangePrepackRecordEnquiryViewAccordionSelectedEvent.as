package hk.org.ha.event.pms.dpp
{
	import org.granite.tide.events.AbstractTideEvent;
	import flash.events.Event;
	
	public class ChangePrepackRecordEnquiryViewAccordionSelectedEvent extends AbstractTideEvent {
		
		private var _event:Event;
		
		private var _index:int;
		
		public function ChangePrepackRecordEnquiryViewAccordionSelectedEvent(index:int, event:Event=null):void {
			super();
			_index = index;
			_event = event;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get index():int {
			return _index;
		}
	}
}