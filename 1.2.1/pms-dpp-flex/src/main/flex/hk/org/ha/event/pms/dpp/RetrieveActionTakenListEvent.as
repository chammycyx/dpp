package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveActionTakenListEvent extends AbstractTideEvent {
		
		public function RetrieveActionTakenListEvent(event:Event=null):void {
			super();
		}
	}
}