package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveTargetAccessRightEvent extends AbstractTideEvent 
	{
		private var _targetFunc:String;
		
		private var _refreshAccessRightFuncName:String;
		
		private var _event:Event;
		
		public function RetrieveTargetAccessRightEvent(targetFunc:String, refreshAccessRightFuncName:String, event:Event=null):void {
			super();
			_targetFunc = targetFunc;
			_refreshAccessRightFuncName = refreshAccessRightFuncName;
			_event = event;
		}
		
		public function get targetFunc():String
		{
			return _targetFunc;
		}
		
		public function get refreshAccessRightFuncName():String
		{
			return _refreshAccessRightFuncName;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}