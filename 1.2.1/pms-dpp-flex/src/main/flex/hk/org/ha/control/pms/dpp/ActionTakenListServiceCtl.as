package hk.org.ha.control.pms.dpp {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dpp.RetrieveActionTakenListEvent;
	import hk.org.ha.event.pms.dpp.UpdateActionTakenListEvent;
	import hk.org.ha.event.pms.dpp.RetrieveActionTakenDetailsListEvent;
	import hk.org.ha.event.pms.dpp.UpdateActionTakenDetailsListEvent;
	import hk.org.ha.model.pms.dpp.biz.ActionTakenListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("actionTakenListServiceCtl", restrict="true")]
	public class ActionTakenListServiceCtl {
		
		[In]
		public var actionTakenListService:ActionTakenListServiceBean;
		
		private var event:Event;
		
		[Observer]
		public function retrieveActionTakenList(evt:RetrieveActionTakenListEvent):void {
			actionTakenListService.retrieveActionTakenList();
		}
		
		[Observer]
		public function updateActionTakenList(evt:UpdateActionTakenListEvent):void {
			actionTakenListService.updateActionTakenList(evt.actionTakenList);
		}
		
		[Observer]
		public function retrieveActionTakenDetailsListEvent(evt:RetrieveActionTakenDetailsListEvent):void {
			event = evt.event;
			actionTakenListService.retrieveActionTakenDetailsList(evt.prepackRecordVerId, retrieveActionTakenDetailsListEventResult);
		}
		
		public function retrieveActionTakenDetailsListEventResult(evt:TideResultEvent):void {
			if ( event != null) {
				evt.context.dispatchEvent(event);
			}
		}
		
		[Observer]
		public function updateActionTakenDetailsList(evt:UpdateActionTakenDetailsListEvent):void {
			event = evt.event;
			actionTakenListService.updateActionTakenDetailsList(evt.prepackRecordId, evt.actionTakenList, updateActionTakenDetailsListResult);
		}
		
		public function updateActionTakenDetailsListResult(evt:TideResultEvent):void {
			if(event != null) {
				evt.context.dispatchEvent(event);
			}
		}
	}
	
}