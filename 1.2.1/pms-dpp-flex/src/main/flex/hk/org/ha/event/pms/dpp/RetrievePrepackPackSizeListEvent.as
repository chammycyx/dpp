package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePrepackPackSizeListEvent extends AbstractTideEvent {
		
		private var _event:Event;
		
		private var _drugItemInfoId:Number;
		
		public function RetrievePrepackPackSizeListEvent(drugItemInfoId:Number, event:Event=null) {
			super();
			_event = event;
			_drugItemInfoId = drugItemInfoId;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get drugItemInfoId():Number {
			return _drugItemInfoId;
		}
	}
}