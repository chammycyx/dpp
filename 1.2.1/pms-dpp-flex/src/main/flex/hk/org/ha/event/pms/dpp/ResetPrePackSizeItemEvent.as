package hk.org.ha.event.pms.dpp
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ResetPrePackSizeItemEvent extends AbstractTideEvent {
		
		public function ResetPrePackSizeItemEvent():void {
			super();
		}
	}
}