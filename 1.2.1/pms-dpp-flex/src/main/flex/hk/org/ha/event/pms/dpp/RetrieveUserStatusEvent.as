package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveUserStatusEvent extends AbstractTideEvent 
	{
		private var _corpId:String;
		
		private var _callBackFuncName:String;
		
		private var _event:Event;
		
		public function RetrieveUserStatusEvent(corpId:String, callBackFuncName:String, event:Event=null):void {
			super();
			_corpId = corpId;
			_callBackFuncName = callBackFuncName;
			_event = event;
		}
		
		public function get corpId():String 
		{
			return _corpId;
		}
		
		public function get callBackFuncName():String
		{
			return _callBackFuncName;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}