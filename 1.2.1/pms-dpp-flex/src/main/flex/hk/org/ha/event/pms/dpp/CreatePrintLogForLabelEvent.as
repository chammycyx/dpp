package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CreatePrintLogForLabelEvent extends AbstractTideEvent {
		
		private var _event:Event;
		
		private var _prepackRecordVerId:Number;
		
		private var _qty:Number;
		
		public function CreatePrintLogForLabelEvent(prepackRecordVerId:Number, qty:Number,event:Event=null):void {
			super();
			_event = event;
			_prepackRecordVerId = prepackRecordVerId;
			_qty = qty;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get prepackRecordVerId():Number {
			return _prepackRecordVerId;
		}
		
		public function get qty():Number {
			return _qty;
		}
	}
}