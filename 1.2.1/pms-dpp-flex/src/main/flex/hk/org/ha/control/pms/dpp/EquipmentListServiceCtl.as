package hk.org.ha.control.pms.dpp {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dpp.RetrieveEquipmentListEvent;
	import hk.org.ha.event.pms.dpp.UpdateEquipmentListEvent;
	import hk.org.ha.model.pms.dpp.biz.EquipmentListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	[Bindable]
	[Name("equipmentListServiceCtl", restrict="true")]
	public class EquipmentListServiceCtl {
		
		[In]
		public var equipmentListService:EquipmentListServiceBean;
		
		[Observer]
		public function retrieveEquipmentList(evt:RetrieveEquipmentListEvent):void {
			equipmentListService.retrieveEquipmentList();
		}
		
		[Observer]
		public function updateEquipmentList(evt:UpdateEquipmentListEvent):void {
			equipmentListService.updateEquipmentList(evt.equipmentList);
		}
	}
}