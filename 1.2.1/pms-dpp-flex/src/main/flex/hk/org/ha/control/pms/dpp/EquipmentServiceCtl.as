package hk.org.ha.control.pms.dpp {
	
	import flash.events.Event;

	import hk.org.ha.event.pms.dpp.RetrieveEquipmentByPrepackRecordVerIdEvent;
	import hk.org.ha.model.pms.dpp.biz.EquipmentServiceBean;
	
	import hk.org.ha.view.pms.dpp.prepack.PrepackRecordEnqView;
	import hk.org.ha.view.pms.dpp.prepack.ActualPrepackInfoAddView;
	
	import org.granite.tide.events.TideResultEvent;

	[Bindable]
	[Name("equipmentServiceCtl", restrict="true")]
	public class EquipmentServiceCtl {
		
		[In]
		public var equipmentService:EquipmentServiceBean;
		
		[In]
		public var prepackRecordEnqView:PrepackRecordEnqView;
		
		[In]
		public var actualPrepackInfoAddView:ActualPrepackInfoAddView;
		
		private var event:Event;
		
		private var screenName:String;
		
		[Observer]
		public function RetrieveEquipmentByPrepackRecordVerId(evt:RetrieveEquipmentByPrepackRecordVerIdEvent):void {
			event = evt.event;
			screenName = evt.screenName;
			equipmentService.retrieveEquipmentByPrepackRecordVerId(evt.prepackRecordVerId, RetrieveEquipmentByPrepackRecordVerIdResult);
		}
		
		public function RetrieveEquipmentByPrepackRecordVerIdResult(evt:TideResultEvent):void {
			if (screenName == "prepackRecordEnqView") {
				prepackRecordEnqView.addDeletedEquipment();
			} else if (screenName == "actualPrepackInfoAddView") {
				actualPrepackInfoAddView.addDeletedEquipment();
			}
		}
	}
}