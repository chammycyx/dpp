package hk.org.ha.control.pms.dpp {
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dpp.RetrieveDiscrepancyActionTakenRptListEvent;
	import hk.org.ha.model.pms.dpp.biz.DiscrepancyActionTakenRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("discrepancyActionTakenRptServiceCtl", restrict="false")]
	public class DiscrepancyActionTakenRptServiceCtl {
		
		[In]
		public var discrepancyActionTakenRptService:DiscrepancyActionTakenRptServiceBean;
		
		private var event:Event;
		
		[Observer]
		public function retrieveDiscrepancyActionTakenRptList(evt:RetrieveDiscrepancyActionTakenRptListEvent):void {
			event = evt.event;
			discrepancyActionTakenRptService.retrieveDiscrepancyActionTakenRptList(evt.discrepancyActionTakenRptCriteria, retrieveDiscrepancyActionTakenRptListResult);
		}
		
		public function retrieveDiscrepancyActionTakenRptListResult(evt:TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
	}
}