package hk.org.ha.event.pms.dpp.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDprWorksheetEvent extends AbstractTideEvent {
		
		public function ShowDprWorksheetEvent():void {
			super();
		}
	}
}