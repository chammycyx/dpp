package hk.org.ha.control.pms.dpp {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dpp.RetrieveContainerListEvent;
	import hk.org.ha.event.pms.dpp.UpdateContainerListEvent;
	import hk.org.ha.model.pms.dpp.biz.ContainerListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	[Bindable]
	[Name("containerListServiceCtl", restrict="true")]
	public class ContainerListServiceCtl {
		
		[In]
		public var containerListService:ContainerListServiceBean;
		
		[Observer]
		public function retrieveContainerList(evt:RetrieveContainerListEvent):void {
			containerListService.retrieveContainerList();
		}
		
		[Observer]
		public function updateContainerList(evt:UpdateContainerListEvent):void {
			containerListService.updateContainerList(evt.containerList);
		}
	}
	
}