package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveContainerListEvent extends AbstractTideEvent {
		
		public function RetrieveContainerListEvent(event:Event=null):void {
			super();
		}
	}
}