package hk.org.ha.event.pms.dpp
{
	import org.granite.tide.events.AbstractTideEvent;
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;
	
	public class RetrieveManufacturerByManufCodeResultEvent extends AbstractTideEvent {
		
		private var _event:Event;
		
		private var _screenName:String;
		
		public function RetrieveManufacturerByManufCodeResultEvent(screenName:String, event:Event=null):void {
			super();
			_event = event;
			_screenName = screenName;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get screenName():String {
			return _screenName;
		}
	}
}