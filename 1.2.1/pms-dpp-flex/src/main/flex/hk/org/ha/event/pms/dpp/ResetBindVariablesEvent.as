package hk.org.ha.event.pms.dpp {
	
	import org.granite.tide.events.AbstractTideEvent;
	import flash.events.Event;
	
	public class ResetBindVariablesEvent extends AbstractTideEvent {
		
		private var _event:Event;
		
		public function ResetBindVariablesEvent(event:Event=null):void {
			super();
			_event = event;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}