package hk.org.ha.event.pms.dpp.show
{
	import org.granite.tide.events.AbstractTideEvent;

	public class ShowItemCodeLookupPopupEvent extends AbstractTideEvent
	{
		private var _screenName:String;
		
		public function ShowItemCodeLookupPopupEvent(screenName:String)
		{
			 super();
			 _screenName = screenName;
		}
		
		public function get screenName():String {
			return _screenName;
		}
	}
}