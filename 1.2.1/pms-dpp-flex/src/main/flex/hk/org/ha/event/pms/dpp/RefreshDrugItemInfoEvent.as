package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshDrugItemInfoEvent extends AbstractTideEvent {
		
		private var _event:Event;
		private var _screenName:String;
		
		public function RefreshDrugItemInfoEvent(screenName:String, event:Event=null) {
			super();
			_event = event;
			_screenName = screenName;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get screenName():String {
			return _screenName;
		}
	}
}