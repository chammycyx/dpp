package hk.org.ha.control.pms.dpp {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dpp.GenerateLabelEvent;
	import hk.org.ha.event.pms.dpp.PrintLabelEvent;
	import hk.org.ha.event.pms.dpp.show.ShowGeneratedLabelEvent;
	import hk.org.ha.model.pms.dpp.biz.PrintLabelServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("printLabelServiceCtl", restrict="false")]
	public class PrintLabelServiceCtl
	{
		
		[In]
		public var printLabelService:PrintLabelServiceBean;
		
		private var printLabelEvent:PrintLabelEvent;
		
		[Observer]
		public function printLabel(evt:GenerateLabelEvent):void
		{
			printLabelService.retrievePrepackRecord(evt.id, printLabelResult);
		}
		
		public function printLabelResult(evt:TideResultEvent):void{
			dispatchEvent(new ShowGeneratedLabelEvent());
		}
		
		[Observer]
		public function printOutLabel(evt:PrintLabelEvent):void {
			printLabelEvent = evt;
			printLabelService.print(evt.prepackRecord, evt.printQty, evt.isSample, printOutLabelResult);
		}
		
		public function printOutLabelResult(evt:TideResultEvent):void{
			if (evt.result as String != null)
			{
				printLabelEvent.prepackItemLabelView.showSystemMessage(evt.result as String);
				return;				
			}
			
			if( printLabelEvent.event != null ) {
				evt.context.dispatchEvent(printLabelEvent.event);
			}
		}
		
	}	
}