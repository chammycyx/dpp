package hk.org.ha.control.pms.dpp {
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	
	import hk.org.ha.event.pms.dpp.AddPrepackRecordEvent;
	import hk.org.ha.event.pms.dpp.CancelPrepackRecordEvent;
	import hk.org.ha.event.pms.dpp.CreatePrepackRecordEvent;
	import hk.org.ha.event.pms.dpp.RetrievePrepackRecordByIdEvent;
	import hk.org.ha.event.pms.dpp.RetrievePrepackRecordByLotNoEvent;
	import hk.org.ha.event.pms.dpp.RetrievePrepackPackSizeListEvent
	import hk.org.ha.event.pms.dpp.RetrieveDrugItemInfoEvent;
	import hk.org.ha.event.pms.dpp.RetrievePrepackRecordVersionEvent;
	
	import hk.org.ha.model.pms.dpp.biz.PrepackRecordServiceBean;
	
	import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;
	import hk.org.ha.model.pms.dpp.persistence.DrugItemInfo;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;

	import hk.org.ha.view.pms.dpp.prepack.popup.CancelPrepackRecordPopup;
	
	[Bindable]
	[Name("prepackRecordServiceCtl", restrict="true")]
	public class PrepackRecordServiceCtl {
		
		[In]
		public var prepackRecordService:PrepackRecordServiceBean;
		
		private var event:Event;
		
		[In]
		public var prepackRecord:PrepackRecord;
		
		[In]
		public var cancelPrepackRecordPopup:CancelPrepackRecordPopup;
		
		[In]
		public var drugItemInfo:DrugItemInfo;
		
		[In]
		public var ctx:Context;
		
		[Observer]
		public function addPrepackRecord(evt:AddPrepackRecordEvent):void {
			event = evt.event;
			prepackRecordService.addPrepackRecord(addPrepackRecordResult);
		}
		
		public function addPrepackRecordResult(evt:TideResultEvent):void {
			if (event != null) {
				evt.context.dispatchEvent(event);
			}
		}
		
		[Observer]
		public function createPrepackRecord(evt:CreatePrepackRecordEvent):void {
			event = evt.event;
			prepackRecordService.createPrepackRecord(evt.prepackRecord, createPrepackRecordResult);
		}
		
		public function createPrepackRecordResult(evt:TideResultEvent):void {
			if (event != null) {
				evt.context.dispatchEvent(event);
			}
		}
		
		[Observer]
		public function cancelPrepackRecord(evt:CancelPrepackRecordEvent):void {
			event = evt.event;
			prepackRecordService.cancelPrepackRecord(evt.prepackRecord, cancelPrepackRecordResult);
		}
		
		public function cancelPrepackRecordResult(evt:TideResultEvent):void {
			cancelPrepackRecordPopup.closeWin();
			if (event != null) {
				evt.context.dispatchEvent(event);
			}
		}
		
		[Observer]
		public function retrievePrepackRecordById(evt:RetrievePrepackRecordByIdEvent):void {
			event = evt.event;
			prepackRecordService.retrievePrepackRecordById(evt.prepackRecordId, retrievePrepackRecordByIdResult);
		}
		
		public function retrievePrepackRecordByIdResult(evt:TideResultEvent):void {
			if ( event != null) {
			evt.context.dispatchEvent(event);
			}
		}
		
		[Observer]
		public function retrievePrepackRecordByLotNo(evt:RetrievePrepackRecordByLotNoEvent):void {
			event = evt.event;
			
			// Reset data provider for DDU & Manufacturer
			if ( drugItemInfo != null && drugItemInfo.dmDrug != null ) {
				drugItemInfo.dmDrug.dmDrugManufacturerList = null;
				if ( drugItemInfo.dmDrug.dmForm != null ) {
					drugItemInfo.dmDrug.dmForm.dmFormDosageUnitMappingList = null;
				}
				ctx.prepackPackSizeList = null;
			}
			
			prepackRecordService.retrievePrepackRecordByNotNo(evt.lotNo, retrievePrepackRecordByLotNoResult);
		}
		
		public function retrievePrepackRecordByLotNoResult(evt:TideResultEvent):void {
			if ( prepackRecord != null ) {
				evt.context.dispatchEvent(
					new RetrieveDrugItemInfoEvent(prepackRecord.prepackRecordVer.drugItemInfoVer.drugItemInfo.itemCode,
						new RetrievePrepackPackSizeListEvent(prepackRecord.prepackRecordVer.drugItemInfoVer.drugItemInfo.id,
							event)
					)
				);
			} else {
				showSystemMessage("0069");
			}
		}
		
		[Observer]
		public function retrievePrepackRecordVersion(evt:RetrievePrepackRecordVersionEvent):void {
			event = evt.event;
			prepackRecordService.retrievePrepackRecordVersion(evt.id, retrievePrepackRecordVersionResult)
		}

		public function retrievePrepackRecordVersionResult(evt:TideResultEvent):void {
			if ( event != null) {
				evt.context.dispatchEvent(event);
			}
		}
		
		private function showSystemMessage(errorCode:String, okFunc:Function=null, yesFunc:Function=null):void {
			var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
			msgProp.messageCode = errorCode;  
			msgProp.messageWinHeight = 200;
			
			if(okFunc != null) {
				msgProp.setOkCancelButton = true;
				msgProp.okHandler = okFunc;
			} else if(yesFunc != null) {
				msgProp.setYesNoButton = true;
				msgProp.yesHandler = yesFunc;
			} else {
				msgProp.setOkButtonOnly = true;
			}
			
			dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
		}		
	}
}