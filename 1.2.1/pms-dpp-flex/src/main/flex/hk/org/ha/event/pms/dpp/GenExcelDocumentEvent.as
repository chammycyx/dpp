package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class GenExcelDocumentEvent extends AbstractTideEvent {
		
		private var _reportKey:String;
		
		private var _reportPath:String;
		
		private var _reportFileName:String;
		
		private var _event:Event;
		
		public function GenExcelDocumentEvent(reportPath:String, reportKey:String, reportFileName:String, event:Event=null):void {
			super();
			_reportKey = reportKey;
			_reportPath = reportPath;
			_reportFileName = reportFileName;
			_event = event;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get reportKey():String {
			return _reportKey;
		}
		
		public function get reportPath():String {
			return _reportPath;
		}
		
		public function get reportFileName():String {
			return _reportFileName;
		}
	}
}