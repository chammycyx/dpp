package hk.org.ha.control.pms.dpp {
	
	import flash.events.Event;
	import flash.external.ExternalInterface;
	
	import hk.org.ha.event.pms.dpp.GenExcelDocumentEvent;
	import hk.org.ha.model.pms.dpp.biz.ReportServiceBean;
	
	import mx.formatters.DateFormatter;
	import mx.messaging.config.ServerConfig;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("reportServiceCtl", restrict="false")]
	public class ReportServiceCtl
	{		 
		[In]
		public var reportService:ReportServiceBean;
		
		[In]
		public var reportWindowsProperties:String;
		
		private var event:Event;	
		
		private var winName:String;


		[Observer]
		public function genExcelDocument(evt:GenExcelDocumentEvent):void
		{
			reportService.setReport(evt.reportPath, evt.reportKey, evt.reportFileName, genExcelDocumentResult);
		}
		
		public function genExcelDocumentResult(evt:TideResultEvent):void
		{
			
			var url:String = "genRpt.seam";
			
			var fr:DateFormatter = new DateFormatter();
			fr.formatString = "YYYYMMDDJJNNSS";
			var winName:String ="excel_win_" + fr.format(new Date());
			
			var argStr:String = reportWindowsProperties;
			
			if (ServerConfig.getChannel("my-graniteamf").uri.search("{server.name}") == -1) {
				url = ServerConfig.getChannel("my-graniteamf").uri.replace("graniteamf/amf", "") + url;
			}				
			
			ExternalInterface.call("window.open", url, winName, argStr );  
		}	
	}
}