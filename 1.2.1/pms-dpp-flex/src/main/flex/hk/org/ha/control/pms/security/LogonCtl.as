package hk.org.ha.control.pms.security {
	
	import flash.external.ExternalInterface;
	
	import hk.org.ha.event.pms.security.LogoffEvent;
	import hk.org.ha.event.pms.security.DebugLogonEvent;
	import hk.org.ha.event.pms.security.PostLogonEvent;
	import hk.org.ha.event.pms.security.SaveLogonReasonEvent;
	import hk.org.ha.event.pms.security.show.ShowLogonReasonPopupEvent;
	import hk.org.ha.event.pms.main.show.ShowStartupViewEvent;
	import hk.org.ha.model.pms.biz.security.AccessReasonServiceBean;
	import hk.org.ha.model.pms.biz.security.PostLogonServiceBean;
	import hk.org.ha.fmk.pms.security.UamInfo;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.security.Identity;
	
	[Bindable]
	[Name("logonCtl")]
	public class LogonCtl 
	{
		[In]
		public var postLogonService:PostLogonServiceBean;
		
		[In]
		public var accessReasonService:AccessReasonServiceBean;
		
		[In]
		public var identity:Identity; 
		
		[In]
		public var uamInfo:UamInfo;
				
		[Observer]
		public function debugLogon(evt:DebugLogonEvent):void 
		{
			identity.username = evt.username;
			identity.password = evt.password;
			identity.login(debugLogonResult);
		}
		
		private function debugLogonResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new PostLogonEvent());
		}
		
		[Observer]
		public function logoff(evt:LogoffEvent):void 
		{	
			ExternalInterface.call("logoff");
		}
		
		[Observer]
		public function postLogon(evt:PostLogonEvent):void {
			postLogonService.postLogon(postLogonResult);
		}
		
		private function postLogonResult(evt:TideResultEvent):void {
			if (uamInfo.userRole.split("\.")[1].toString().search("ITD") > 0) {
				evt.context.dispatchEvent(new ShowLogonReasonPopupEvent());
			}
			else {
				evt.context.dispatchEvent(new ShowStartupViewEvent());
			}
		}
		
		[Observer]
		public function saveLogonReason(evt:SaveLogonReasonEvent):void {
			accessReasonService.saveLogonReason(evt.reason, saveLogonReasonResult);
		}
		
		private function saveLogonReasonResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new ShowStartupViewEvent());
		}
	}
}