package hk.org.ha.event.pms.dpp.show
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowAuditRptViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowAuditRptViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}