package hk.org.ha.event.pms.dpp {
	import org.granite.tide.events.AbstractTideEvent;
	import flash.events.Event;
	
	public class RefreshEnquiryRptViewEvent extends AbstractTideEvent {
		
		private var _event:Event;
		
		public function RefreshEnquiryRptViewEvent(event:Event=null):void {
			super();
			_event = event;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}