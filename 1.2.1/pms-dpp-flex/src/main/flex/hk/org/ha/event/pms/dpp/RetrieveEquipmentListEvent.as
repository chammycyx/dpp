package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveEquipmentListEvent extends AbstractTideEvent {
		
		public function RetrieveEquipmentListEvent(event:Event=null):void {
			super();
		}
	}
}