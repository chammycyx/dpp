package hk.org.ha.control.pms.dpp{
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dpp.RetrieveDmDrugListEvent;
	import hk.org.ha.model.pms.dpp.biz.DmDrugListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("dmDrugListServiceCtl", restrict="true")]
	public class DmDrugListServiceCtl {
		
		[In]
		public var dmDrugListService:DmDrugListServiceBean;
		
		private var event:Event;
		
		[In]
		public var ctx:Context;
		
		[Observer]
		public function retrieveDmDrugList(evt:RetrieveDmDrugListEvent):void {
			event = evt.event;
			ctx.dmDrugList = null;
			dmDrugListService.retrieveDmDrugListLikeItemCode(evt.itemCode, retrieveDmDrugListResult);
		}
		
		public function retrieveDmDrugListResult(evt: TideResultEvent):void {
			if( event != null ) {
				evt.context.dispatchEvent(event);
			}
		}
		
	}
}