package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveActionTakenDetailsListEvent extends AbstractTideEvent {
		
		private var _event:Event;
		
		private var _prepackRecordVerId:Number;
		
		public function RetrieveActionTakenDetailsListEvent(prepackRecordVerId:Number, event:Event=null):void {
			super();
			_event = event;
			_prepackRecordVerId = prepackRecordVerId;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get prepackRecordVerId():Number {
			return _prepackRecordVerId;
		}
	}
}