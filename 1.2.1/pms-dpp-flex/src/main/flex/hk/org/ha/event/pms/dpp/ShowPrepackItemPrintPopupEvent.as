package hk.org.ha.event.pms.dpp
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowPrepackItemPrintPopupEvent extends AbstractTideEvent {
		
		private var _screenName:String;
		
		public function ShowPrepackItemPrintPopupEvent(screenName:String):void {
			super();
			_screenName = screenName;
		}
		
		public function get screenName():String {
			return _screenName;
		}
	}
}