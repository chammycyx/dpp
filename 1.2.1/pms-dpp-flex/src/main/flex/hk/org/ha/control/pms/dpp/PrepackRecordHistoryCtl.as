package hk.org.ha.control.pms.dpp {
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.event.pms.dpp.RetrievePrepackRecordHistoryEvent;
	
	import hk.org.ha.model.pms.dpp.biz.PrepackRecordHistoryServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("prepackRecordHistoryCtl", restrict="true")]
	public class PrepackRecordHistoryCtl {
		
		[In]
		public var prepackRecordHistoryService:PrepackRecordHistoryServiceBean;
		
		private var event:Event;
		
		[Observer]
		public function retrievePrepackRecordHistory(evt:RetrievePrepackRecordHistoryEvent):void {
			event = evt.event;
			prepackRecordHistoryService.retrievePrepackRecordHistory(evt.prepackRecordId, retrievePrepackRecordHistoryResult);
		}
		
		public function retrievePrepackRecordHistoryResult(evt:TideResultEvent):void {
			if(event != null) {
				evt.context.dispatchEvent(event);
			}
		}
	}
}