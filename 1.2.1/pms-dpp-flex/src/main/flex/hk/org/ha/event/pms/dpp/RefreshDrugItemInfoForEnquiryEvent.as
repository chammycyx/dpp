package hk.org.ha.event.pms.dpp {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshDrugItemInfoForEnquiryEvent extends AbstractTideEvent {
		
		public function RefreshDrugItemInfoForEnquiryEvent() {
			super();
		}
	}
}