package hk.org.ha.event.pms.dpp {
	
	import mx.collections.ListCollectionView;
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateActionTakenDetailsListEvent extends AbstractTideEvent {
		
		private var _event:Event;
		private var _actionTakenList:ListCollectionView;
		private var _prepackRecordId:Number;
		
		public function UpdateActionTakenDetailsListEvent(prepackRecordId:Number, actionTakenList:ListCollectionView, event:Event=null):void {
			super();
			_event = event;
			_actionTakenList = actionTakenList;
			_prepackRecordId = prepackRecordId;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get prepackRecordId():Number {
			return _prepackRecordId;
		}
		
		public function get actionTakenList():ListCollectionView {
			return _actionTakenList;
		}
	}
}