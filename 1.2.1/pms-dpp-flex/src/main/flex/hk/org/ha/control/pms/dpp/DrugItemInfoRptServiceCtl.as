package hk.org.ha.control.pms.dpp {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dpp.GenExcelDocumentEvent;
	import hk.org.ha.event.pms.dpp.RetrieveDrugItemInfoRptEvent;
	import hk.org.ha.model.pms.dpp.biz.DrugItemInfoRptServiceBean;
	import hk.org.ha.fmk.pms.security.UamInfo;
	import mx.collections.ArrayCollection;
	import mx.formatters.DateFormatter;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("drugItemInfoRptServiceCtl", restrict="true")]
	public class DrugItemInfoRptServiceCtl {
		
		[In]
		public var drugItemInfoRptService:DrugItemInfoRptServiceBean;
		
		[In] [Bindable]
		public var uamInfo:UamInfo;
		
		private var event:Event;
		
		[Observer]
		public function retrieveDrugItemInfoRpt(evt:RetrieveDrugItemInfoRptEvent):void 
		{
			drugItemInfoRptService.retrieveDrugItemInfoRpt(retrieveDrugItemInfoRptResult);
		}
		
		public function retrieveDrugItemInfoRptResult(evt:TideResultEvent):void {
			var today:Date = new Date();
			var dateFormatter:DateFormatter = new DateFormatter;
			dateFormatter.formatString = "YYYYMMDDJJNNSS";
			
			evt.context.dispatchEvent(
				new GenExcelDocumentEvent("/excelTemplate/drugItemInfoRpt.xhtml", "DrugItemInfoReport", "DrugItemInfoReport"+"_"+uamInfo.hospital+"_"+uamInfo.workstore+"_"+dateFormatter.format(today))
			);
		}
		
		
	}
	
}