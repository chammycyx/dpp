package hk.org.ha.event.pms.dpp
{
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class ShowVersionCheckFailureMessageEvent extends AbstractTideEvent {
		
		private var _messageCode:String;
		
		public function ShowVersionCheckFailureMessageEvent(messageCode:String):void {
			super();
			_messageCode = messageCode;
		}
		
		public function get messageCode():String {
			return _messageCode;
		}
	}
}