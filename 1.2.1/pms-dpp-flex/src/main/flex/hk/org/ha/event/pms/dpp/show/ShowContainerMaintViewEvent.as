package hk.org.ha.event.pms.dpp.show
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowContainerMaintViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowContainerMaintViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}