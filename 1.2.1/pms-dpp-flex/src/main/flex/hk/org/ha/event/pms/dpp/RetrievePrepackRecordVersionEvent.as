package hk.org.ha.event.pms.dpp
{
	import org.granite.tide.events.AbstractTideEvent;
	import flash.events.Event;
	
	public class RetrievePrepackRecordVersionEvent extends AbstractTideEvent {
		
		private var _event:Event;
		
		private var _id:int;
		
		public function RetrievePrepackRecordVersionEvent(id:int, event:Event=null):void {
			super();
			_event = event;
			_id = id;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get id():int {
			return _id;
		}
	}
}