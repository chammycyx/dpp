package hk.org.ha.control.pms.dpp {
	
	import flash.events.Event;

	import hk.org.ha.model.pms.dpp.biz.ContainerServiceBean;
	import hk.org.ha.event.pms.dpp.RetrieveContainerByPrepackRecordVerIdEvent;
	
	import hk.org.ha.view.pms.dpp.prepack.PrepackRecordEnqView;
	import hk.org.ha.view.pms.dpp.prepack.ActualPrepackInfoAddView;
	
	import org.granite.tide.events.TideResultEvent;

	[Bindable]
	[Name("containerServiceCtl", restrict="true")]
	public class ContainerServiceCtl {
		
		[In]
		public var containerService:ContainerServiceBean;
		
		[In]
		public var prepackRecordEnqView:PrepackRecordEnqView;
		
		[In]
		public var actualPrepackInfoAddView:ActualPrepackInfoAddView;
		
		private var event:Event;
		
		private var screenName:String;
		
		[Observer]
		public function RetrieveContainerByPrepackRecordVerId(evt:RetrieveContainerByPrepackRecordVerIdEvent):void {
			event = evt.event;
			screenName = evt.screenName;
			containerService.retrieveContainerByPrepackRecordVerId(evt.prepackRecordVerId, RetrieveContainerByPrepackRecordVerIdResult);
		}
		
		public function RetrieveContainerByPrepackRecordVerIdResult(evt:TideResultEvent):void {
			if (screenName == "prepackRecordEnqView") {
				prepackRecordEnqView.addDeletedContainer();
			} else if (screenName == "actualPrepackInfoAddView") {
				actualPrepackInfoAddView.addDeletedContainer();
			}
		}
	}
	
}