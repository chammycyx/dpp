package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ClearDrugItemInfoEvent extends AbstractTideEvent {
		
		private var _event:Event;
		
		public function ClearDrugItemInfoEvent(event:Event=null) {
			super();
			_event = event;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}