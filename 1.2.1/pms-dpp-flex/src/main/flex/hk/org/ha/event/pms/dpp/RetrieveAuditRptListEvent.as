package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dpp.vo.AuditRptCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveAuditRptListEvent extends AbstractTideEvent {
		
		private var _auditRptCriteria:AuditRptCriteria;
		
		private var _event:Event;
		
		public function RetrieveAuditRptListEvent(auditRptCriteria:AuditRptCriteria, event:Event=null):void {
			super();
			_auditRptCriteria = auditRptCriteria;
			_event = event;
		}
		
		public function get auditRptCriteria():AuditRptCriteria {
			return _auditRptCriteria;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}