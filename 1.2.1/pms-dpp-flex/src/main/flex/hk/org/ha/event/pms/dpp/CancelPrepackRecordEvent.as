package hk.org.ha.event.pms.dpp
{
	import org.granite.tide.events.AbstractTideEvent;
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;
	
	public class CancelPrepackRecordEvent extends AbstractTideEvent {
		
		private var _event:Event;
		
		private var _prepackRecord:PrepackRecord;
		
		public function CancelPrepackRecordEvent(prepackRecord:PrepackRecord, event:Event=null):void {
			super();
			_event = event;
			_prepackRecord = prepackRecord;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get prepackRecord():PrepackRecord {
			return _prepackRecord;
		}
	}
}