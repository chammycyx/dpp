package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class PrepareDprWorksheetEvent extends AbstractTideEvent {
		
		private var _id:Number;
		
		private var _event:Event;
		
		public function PrepareDprWorksheetEvent(id:Number, event:Event=null):void {
			super();
			_id = id;
			_event = event;
		}
		
		public function get id():Number {
			return _id;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}