package hk.org.ha.event.pms.dpp
{
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import hk.org.ha.model.pms.dpp.vo.PrepackRecordSearchCriteria;
	
	public class RetrievePrepackRecordListEvent extends AbstractTideEvent {
		
		private var _event:Event;
		
		private var _prepackRecordSearchCriteria:PrepackRecordSearchCriteria;
		
		public function RetrievePrepackRecordListEvent(prepackRecordSearchCriteria:PrepackRecordSearchCriteria, event:Event=null) {
			super();
			_event = event;
			_prepackRecordSearchCriteria = prepackRecordSearchCriteria;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get prepackRecordSearchCriteria():PrepackRecordSearchCriteria {
			return _prepackRecordSearchCriteria;
		}
	}
}