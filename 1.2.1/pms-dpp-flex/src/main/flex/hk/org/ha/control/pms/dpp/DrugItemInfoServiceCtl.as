package hk.org.ha.control.pms.dpp {
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dpp.RetrieveDrugItemInfoEvent;
	import hk.org.ha.event.pms.dpp.RetrieveDrugItemInfoForEnquiryEvent;
	import hk.org.ha.event.pms.dpp.UpdateDrugItemInfoEvent;
	import hk.org.ha.event.pms.dpp.ResetDrugItemInfoForEnquiryEvent;
	import hk.org.ha.event.pms.dpp.ResetDrugItemInfoEvent;
	import hk.org.ha.model.pms.dpp.biz.DrugItemInfoServiceBean;
	import hk.org.ha.model.pms.dpp.persistence.DrugItemInfo;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("drugItemInfoServiceCtl", restrict="true")]
	public class DrugItemInfoServiceCtl {
		
		[In]
		public var drugItemInfoService:DrugItemInfoServiceBean;
		
		[In]
		public var drugItemInfo:DrugItemInfo;
				
		[In]
		public var ctx:Context;
		
		private var event:Event;
		
		[Observer]
		public function retrieveDrugItemInfo(evt:RetrieveDrugItemInfoEvent):void {
			event = evt.event;
			drugItemInfoService.retrieveDrugItemInfo(evt.itemCode, retrieveDrugItemInfoResult);
		}
		
		public function retrieveDrugItemInfoResult(evt:TideResultEvent):void {
			if(event != null) {
				evt.context.dispatchEvent(event);
			}
		}
		
		[Observer]
		public function updateDrugItemInfo(evt:UpdateDrugItemInfoEvent):void {
			event = evt.event;
			drugItemInfoService.updateDrugItemInfo(evt.drugItemInfo, updateDrugItemInfoResult);
		}
		
		public function updateDrugItemInfoResult(evt:TideResultEvent):void {
			if(event != null) {
				evt.context.dispatchEvent(event);
			}
		}
		
		[Observer]
		public function retrieveDrugItemInfoForEnquiry(evt:RetrieveDrugItemInfoForEnquiryEvent):void {
			event = evt.event;
			drugItemInfoService.retrieveDrugItemInfoForEnquiry(evt.itemCode, retrieveDrugItemInfoForEnquiryResult);
		}
		
		public function retrieveDrugItemInfoForEnquiryResult(evt:TideResultEvent):void {
			if(event != null) {
				evt.context.dispatchEvent(event);
			}
		}
		
		[Observer]
		public function resetDrugItemInfoForEnquiry(evt:ResetDrugItemInfoForEnquiryEvent):void {
			drugItemInfoService.resetDrugItemInfoForEnquiry();
		}
		
		[Observer]
		public function resetDrugItemInfo(evt:ResetDrugItemInfoEvent):void {
			drugItemInfoService.resetDrugItemInfo();
		}
	}
	
}