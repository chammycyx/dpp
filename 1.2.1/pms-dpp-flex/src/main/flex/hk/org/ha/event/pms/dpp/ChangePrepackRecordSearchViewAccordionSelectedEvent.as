package hk.org.ha.event.pms.dpp
{
	import org.granite.tide.events.AbstractTideEvent;
	import flash.events.Event;
	
	public class ChangePrepackRecordSearchViewAccordionSelectedEvent extends AbstractTideEvent {
		
		private var _event:Event;
		
		private var _selectedIndex:int;
		
		public function ChangePrepackRecordSearchViewAccordionSelectedEvent(selectedIndex:int, event:Event=null):void {
			super();
			_event = event;
			_selectedIndex = selectedIndex;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get selectedIndex():int {
			return _selectedIndex;
		}
	}
}