package hk.org.ha.event.pms.dpp
{
	import org.granite.tide.events.AbstractTideEvent;
	import flash.events.Event;
	
	public class RetrievePrepackRecordByLotNoEvent extends AbstractTideEvent {
		
		private var _event:Event;
		
		private var _lotNo:String;
		
		public function RetrievePrepackRecordByLotNoEvent(lotNo:String, event:Event=null):void {
			super();
			_event = event;
			_lotNo = lotNo;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get lotNo():String {
			return _lotNo;
		}
	}
}