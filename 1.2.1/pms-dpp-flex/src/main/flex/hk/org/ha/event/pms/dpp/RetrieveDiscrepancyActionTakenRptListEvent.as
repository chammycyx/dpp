package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dpp.vo.DiscrepancyActionTakenRptCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDiscrepancyActionTakenRptListEvent extends AbstractTideEvent {
		
		private var _discrepancyActionTakenRptCriteria:DiscrepancyActionTakenRptCriteria;
		
		private var _event:Event;
		
		public function RetrieveDiscrepancyActionTakenRptListEvent(discrepancyActionTakenRptCriteria:DiscrepancyActionTakenRptCriteria, event:Event=null):void {
			super();
			_discrepancyActionTakenRptCriteria = discrepancyActionTakenRptCriteria;
			_event = event;
		}
		
		public function get discrepancyActionTakenRptCriteria():DiscrepancyActionTakenRptCriteria {
			return _discrepancyActionTakenRptCriteria;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}