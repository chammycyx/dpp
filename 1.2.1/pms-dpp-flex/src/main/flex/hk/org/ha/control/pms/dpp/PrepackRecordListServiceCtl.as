package hk.org.ha.control.pms.dpp {
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.event.pms.dpp.AddPrepackRecordEvent;
	import hk.org.ha.event.pms.dpp.CancelPrepackRecordEvent;
	import hk.org.ha.event.pms.dpp.RetrievePrepackRecordListEvent;
	
	import hk.org.ha.model.pms.dpp.biz.PrepackRecordListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("prepackRecordListServiceCtl", restrict="true")]
	public class PrepackRecordListServiceCtl {
		
		[In]
		public var prepackRecordListService:PrepackRecordListServiceBean;
		
		private var event:Event;
		
		[In]
		public var ctx:Context;
		
		[Observer]
		public function retrievePrepackRecordListBySearchCriteria(evt:RetrievePrepackRecordListEvent):void {
			event = evt.event;
			ctx.prepackRecordList = null;
			prepackRecordListService.retrievePrepackRecordListBySearchCriteria(evt.prepackRecordSearchCriteria, retrievePrepackRecordListBySearchCriteriaResult);
		}
		
		public function retrievePrepackRecordListBySearchCriteriaResult(evt:TideResultEvent):void {
			if(event != null) {
				evt.context.dispatchEvent(event);
			}
		}
	}
}