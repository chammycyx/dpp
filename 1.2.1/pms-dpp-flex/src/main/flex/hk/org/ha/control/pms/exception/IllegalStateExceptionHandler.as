package hk.org.ha.control.pms.exception {

    import mx.controls.Alert;
    import mx.messaging.messages.ErrorMessage;
    
    import org.granite.tide.BaseContext;
    import org.granite.tide.IExceptionHandler;

    public class IllegalStateExceptionHandler implements IExceptionHandler 
	{
        
        public static const ILLEGAL_STATE:String = "Persistence.IllegalState"; 
        
        
        public function accepts(emsg:ErrorMessage):Boolean 
		{
            return emsg.faultCode == ILLEGAL_STATE;
        }

        public function handle(context:BaseContext, emsg:ErrorMessage):void 
		{
            Alert.show(emsg.faultString);
			
        }
    }
}
