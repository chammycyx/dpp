package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;
	import hk.org.ha.view.pms.dpp.prepack.PrepackItemLabelView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class PrintLabelEvent extends AbstractTideEvent {
		
		private var _prepackItemPrintPopup:PrepackItemLabelView;
		private var _prepackRecord:PrepackRecord;
		private var _printQty:int;
		private var _isSample:Boolean;
		private var _event:Event;
		
		public function PrintLabelEvent(prepackItemLabelView:PrepackItemLabelView, prepackRecord:PrepackRecord, 
										printQty:int, isSample:Boolean, event:Event=null):void {
			super();
			_prepackItemPrintPopup = prepackItemLabelView;
			_prepackRecord = prepackRecord;
			_printQty = printQty;
			_isSample = isSample;
			_event = event;
		}
		
		public function get prepackItemLabelView():PrepackItemLabelView
		{
			return _prepackItemPrintPopup;
		}
		
		public function get prepackRecord():PrepackRecord
		{
			return _prepackRecord;
		}
		
		public function get printQty():int {
			return _printQty;
		}
		
		public function get isSample():Boolean {
			return _isSample;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}