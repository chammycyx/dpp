package hk.org.ha.control.pms.dpp {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dpp.biz.UserAuthenticationServiceBean;
	
	import hk.org.ha.view.pms.dpp.prepack.ActualPrepackInfoAddView;
	import hk.org.ha.view.pms.dpp.prepack.PrepackRecordEnqView;
	
	import hk.org.ha.event.pms.dpp.RetrieveUserStatusEvent;
	import  hk.org.ha.event.pms.dpp.RetrieveTargetAccessRightEvent;

	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("userStatusServiceCtl", restrict="true")]
	public class UserStatusServiceCtl {
		
		[In]
		public var userAuthenticationService:UserAuthenticationServiceBean;
		
		private var event:Event;
		
		private var callBackFuncName:String;
		
		private var refreshAccessRightFuncName:String;
		
		[In]
		public var actualPrepackInfoAddView:ActualPrepackInfoAddView;
		
		[In]
		public var prepackRecordEnqView:PrepackRecordEnqView;
		
		[Observer]
		public function retrieveUserStatus(evt:RetrieveUserStatusEvent):void {
			event = evt.event;
			callBackFuncName = evt.callBackFuncName;
			userAuthenticationService.retrieveUserStatus(evt.corpId, retrieveUserStatusEventResult);
		}
		
		public function retrieveUserStatusEventResult(evt:TideResultEvent):void {
			if (callBackFuncName == "validateUserOfLineClearanceCheckedBy") {
				actualPrepackInfoAddView.validateUserOfLineClearanceCheckedBy();
			} else if (callBackFuncName == "validateUserOfFinalCheckedBy") {
				actualPrepackInfoAddView.validateUserOfFinalCheckedBy();
			} else if (callBackFuncName == "finalUserStatusCheck") {
				actualPrepackInfoAddView.finalUserStatusCheck();
			}
		}
		
		[Observer]
		public function retrieveTargetAccessRight(evt:RetrieveTargetAccessRightEvent):void {
			event = evt.event;
			refreshAccessRightFuncName = evt.refreshAccessRightFuncName;
			userAuthenticationService.retrieveTargetAccessRight(evt.targetFunc, retrieveTargetAccessRightResult);
		}
		
		public function retrieveTargetAccessRightResult(evt:TideResultEvent):void {
			if (refreshAccessRightFuncName == "setActualPrepackAmendSaveAccessRight") {
				actualPrepackInfoAddView.setActualPrepackAmendSaveAccessRight();
			} else if (refreshAccessRightFuncName == "setActualPrepackAmendSaveOwnAccessRight") {
				actualPrepackInfoAddView.setActualPrepackAmendSaveOwnAccessRight();
			} else if (refreshAccessRightFuncName == "setRecordEnqSaveAccessRight") {
				prepackRecordEnqView.setRecordEnqSaveAccessRight();
			} else if (refreshAccessRightFuncName == "setRecordEnqSaveOwnAccessRight") {
				prepackRecordEnqView.setRecordEnqSaveOwnAccessRight();
			} else if (refreshAccessRightFuncName == "setRecordEnqCancelAccessRight") {
				prepackRecordEnqView.prepackRecordSearchView.setRecordEnqCancelAccessRight();
			} else if (refreshAccessRightFuncName == "setRecordEnqCancelOwnAccessRight") {
				prepackRecordEnqView.prepackRecordSearchView.setRecordEnqCancelOwnAccessRight();
			}
			if ( event != null) {
				evt.context.dispatchEvent(event);
			}
		}
		
	}
	
}