package hk.org.ha.event.pms.dpp
{
	import org.granite.tide.events.AbstractTideEvent;
	import flash.events.Event;
	
	public class RetrieveContainerByPrepackRecordVerIdEvent extends AbstractTideEvent {
		
		private var _event:Event;
		
		private var _prepackRecordVerId:int;
		
		private var _screenName:String;
		
		public function RetrieveContainerByPrepackRecordVerIdEvent(prepackRecordVerId:int, screenName:String, event:Event=null):void {
			super();
			_event = event;
			_screenName = screenName;
			_prepackRecordVerId = prepackRecordVerId;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get prepackRecordVerId():int {
			return _prepackRecordVerId;
		}
		
		public function get screenName():String {
			return _screenName;
		}
	}
}