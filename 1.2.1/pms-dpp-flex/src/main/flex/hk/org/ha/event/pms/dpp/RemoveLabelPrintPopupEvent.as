package hk.org.ha.event.pms.dpp {
	
	import hk.org.ha.view.pms.dpp.prepack.popup.PrepackItemPrintPopup;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RemoveLabelPrintPopupEvent extends AbstractTideEvent {
		
		private var _screenName:String;
		private var _popup:PrepackItemPrintPopup;
		
		public function RemoveLabelPrintPopupEvent(screenName:String, popup:PrepackItemPrintPopup){
			_screenName = screenName;
			_popup = popup;
			super();
		}
		
		public function get screenName():String {
			return _screenName;
		}
		
		public function get popup():PrepackItemPrintPopup {
			return _popup;
		}
	}
}