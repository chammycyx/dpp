package hk.org.ha.fmk.pms.flex.components.core
{
	import flash.events.MouseEvent;
	
	import hk.org.ha.fmk.pms.flex.components.message.ShowSystemMessagePopupEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	
	import mx.collections.ArrayCollection;
	import mx.controls.LinkBar;
	import mx.controls.LinkButton;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	public class ExtendToolbar extends LinkBar
	{	
		public var _retrieveFunc:Function = null;
		public var _saveFunc:Function = null;
		public var _saveYesFunc:Function = null;
		public var _saveNoFunc:Function = null;
		public var _clearFunc:Function = null;
		public var _printFunc:Function = null;
		public var _exportFunc:Function = null;
		public var _editFunc:Function = null;
		
		private var operationName:String;
		
		public var retrieveButton:LinkButton;
		public var saveButton:LinkButton;
		public var clearButton:LinkButton;
		public var printButton:LinkButton;
		public var exportButton:LinkButton;
		public var editButton:LinkButton;
		
		private var btnList:ArrayCollection = new ArrayCollection();
		private var btnSeq:Array = new Array("Retrieve", "Save", "Delete", "Clear", "Print", "Reprint", "Export", "Edit");
		
		public function ExtendToolbar() {             
			super();
		}
		
		public function hasInited():Boolean {
			if (this.numChildren == 0) {
				return false;
			} else {
				return true;
			}
		}
		
		public function init():void {
			for each(var btnName:String in btnSeq) {
				for each(var btn:LinkButton in btnList) {
					if (btnName == btn.label) {					
						this.addChild(btn);
						break;
					}
				}
			}
		}
		
		public function set retrieveFunc(retrieveFunc:Function):void {
			if (retrieveButton == null) {
				this._retrieveFunc = retrieveFunc;
				retrieveButton = new LinkButton();
				retrieveButton.label = "Retrieve";
				retrieveButton.tabFocusEnabled = false;
				retrieveButton.focusEnabled = false;
				retrieveButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(retrieveButton);
			}
		}
		
		public function set saveFunc(saveFunc:Function):void {
			if (saveButton == null) {
				this._saveFunc = saveFunc;
				saveButton = new LinkButton();
				saveButton.label = "Save";
				saveButton.tabFocusEnabled = false;
				saveButton.focusEnabled = false;
				saveButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(saveButton);
			}
		}
		
		public function set saveYesFunc(saveYesFunc:Function):void {
			if (saveButton == null) {
				this._saveYesFunc = saveYesFunc;
				saveButton = new LinkButton();
				saveButton.label = "Save";
				saveButton.tabFocusEnabled = false;
				saveButton.focusEnabled = false;
				saveButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(saveButton);
			}
		}
		
		public function set clearFunc(clearFunc:Function):void {
			if (clearButton == null) {
				this._clearFunc = clearFunc;
				clearButton = new LinkButton();
				clearButton.label = "Clear";
				clearButton.tabFocusEnabled = false;
				clearButton.focusEnabled = false;
				clearButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(clearButton);
			}
		}
		
		public function set printFunc(printFunc:Function):void {
			if (printButton == null) {
				this._printFunc = printFunc;
				printButton = new LinkButton();
				printButton.label = "Print";
				printButton.tabFocusEnabled = false;
				printButton.focusEnabled = false;
				printButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(printButton);
			}
		}
		
		public function set exportFunc(exportFunc:Function):void {
			if (exportButton == null) {
				this._exportFunc = exportFunc;
				exportButton = new LinkButton();
				exportButton.label = "Export";
				exportButton.tabFocusEnabled = false;
				exportButton.focusEnabled = false;
				exportButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(exportButton);
			}
		}
		
		public function set editFunc(editFunc:Function):void {
			if (editButton == null) {
				this._editFunc = editFunc;
				editButton = new LinkButton();
				editButton.label = "Edit";
				editButton.tabFocusEnabled = false;
				editButton.focusEnabled = false;
				editButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(editButton);
			}
		}
		
		public function itemClickHandler(evt:MouseEvent):void {
			operationName = "_" + (evt.currentTarget.label).substring(0,1).toLowerCase() + (evt.currentTarget.label).substring(1);
			operationName = operationName.split(" ").join("");

			if (operationName == "_save" || operationName == "_delete") {
				confirmationMessagePopup(operationName);
			} else {
				this[operationName + "Func"]();
			}
		}
		
		private function confirmationMessagePopup(operationName:String):void {
			var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
			if (operationName == "_save") {
				msgProp.displayDesc = "<font size='16'><b>Are you sure you want to save?</b></font>";
				msgProp.messageTitle = "Save Confirmation";
			}
			msgProp.setYesNoButton = true;
			msgProp.setYesDefaultButton = false;
			msgProp.yesHandler = confirmHandler;
			msgProp.noHandler = cancelHandler;
			dispatchEvent(new ShowSystemMessagePopupEvent(msgProp));
		}
	
		private function confirmHandler(evt:MouseEvent):void {
			closePopupHandler(evt);
			if (operationName == "_save") {
				_saveYesFunc();
			}
		}
		
		private function cancelHandler(evt:MouseEvent):void {
			closePopupHandler(evt);
			if (operationName == "_save") {
				if (_saveNoFunc != null) {
					_saveNoFunc();
				}
			} 
		}
		
		private function closePopupHandler(evt:MouseEvent):void {
			var temp:UIComponent = evt.currentTarget as UIComponent; 
			PopUpManager.removePopUp(temp.parentDocument as IFlexDisplayObject);
		}
	}
}