package hk.org.ha.event.pms.dpp
{
	import org.granite.tide.events.AbstractTideEvent;
	import flash.events.Event;
	
	public class RetrievePrepackRecordHistoryEvent extends AbstractTideEvent {
		
		private var _event:Event;
		
		private var _prepackRecordId:Number;
		
		public function RetrievePrepackRecordHistoryEvent(prepackRecordId:int, event:Event=null):void {
			super();
			_event = event;
			_prepackRecordId = prepackRecordId;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get prepackRecordId():int {
			return _prepackRecordId;
		}
	}
}