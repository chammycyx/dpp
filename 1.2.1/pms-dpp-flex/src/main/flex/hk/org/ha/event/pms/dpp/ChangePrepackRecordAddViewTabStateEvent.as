package hk.org.ha.event.pms.dpp
{
	import org.granite.tide.events.AbstractTideEvent;
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;
	
	public class ChangePrepackRecordAddViewTabStateEvent extends AbstractTideEvent {
		
		private var _event:Event;
		
		public function ChangePrepackRecordAddViewTabStateEvent(event:Event=null):void {
			super();
			_event = event;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}