package hk.org.ha.event.pms.dpp {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDiscrepancyListEvent extends AbstractTideEvent {
		
		public function RetrieveDiscrepancyListEvent(event:Event=null):void {
			super();
		}
	}
}