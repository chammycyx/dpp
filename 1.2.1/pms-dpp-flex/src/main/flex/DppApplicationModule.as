package {
	
	import hk.org.ha.control.pms.dpp.ActionTakenListServiceCtl;
	import hk.org.ha.control.pms.dpp.AuditRptServiceCtl;
	import hk.org.ha.control.pms.dpp.ContainerListServiceCtl;
	import hk.org.ha.control.pms.dpp.ContainerServiceCtl;
	import hk.org.ha.control.pms.dpp.DiscrepancyActionTakenRptServiceCtl;
	import hk.org.ha.control.pms.dpp.DiscrepancyListServiceCtl;
	import hk.org.ha.control.pms.dpp.DmDrugListServiceCtl;
	import hk.org.ha.control.pms.dpp.DprWorksheetServiceCtl;
	import hk.org.ha.control.pms.dpp.DrugItemInfoRptServiceCtl;
	import hk.org.ha.control.pms.dpp.DrugItemInfoServiceCtl;
	import hk.org.ha.control.pms.dpp.DrugPrepackDailyRptServiceCtl;
	import hk.org.ha.control.pms.dpp.EnquiryRptServiceCtl;
	import hk.org.ha.control.pms.dpp.EquipmentListServiceCtl;
	import hk.org.ha.control.pms.dpp.EquipmentServiceCtl;
	import hk.org.ha.control.pms.dpp.ManufacturerServiceCtl;
	import hk.org.ha.control.pms.dpp.PrepackPackSizeListServiceCtl;
	import hk.org.ha.control.pms.dpp.PrepackRecordHistoryCtl;
	import hk.org.ha.control.pms.dpp.PrepackRecordListServiceCtl;
	import hk.org.ha.control.pms.dpp.PrepackRecordServiceCtl;
	import hk.org.ha.control.pms.dpp.PrintLabelServiceCtl;
	import hk.org.ha.control.pms.dpp.PrintLogServiceCtl;
	import hk.org.ha.control.pms.dpp.ReportServiceCtl;
	import hk.org.ha.control.pms.dpp.UserStatusServiceCtl;
	import hk.org.ha.control.pms.exception.*;
	import hk.org.ha.control.pms.security.*;
	import hk.org.ha.control.pms.sys.*;
	
	import mx.logging.Log;
	import mx.logging.targets.TraceTarget;
	
	import org.granite.tide.ITideModule;
	import org.granite.tide.Tide;
	import org.granite.tide.validators.ValidatorExceptionHandler;
	import hk.org.ha.fmk.pms.flex.components.window.Window;

	[Bindable]
	public class DppApplicationModule implements ITideModule 
	{
		
		public function init(tide:Tide):void 
		{
			var t:TraceTarget = new TraceTarget();
			t.filters = ["org.granite.*","hk.org.ha.*"];
			Log.addTarget(t);
			
			tide.addExceptionHandler(PrintingExceptionHandler);
			tide.addExceptionHandler(NotLoggedInExceptionHandler);
			tide.addExceptionHandler(AccessDeniedExceptionHandler);
			tide.addExceptionHandler(SecurityExceptionHandler);
			tide.addExceptionHandler(ValidatorExceptionHandler);
			tide.addExceptionHandler(IllegalStateExceptionHandler);
			tide.addExceptionHandler(OptimisticLockExceptionHandler);
			tide.addExceptionHandler(DefaultExceptionHandler);
			
			tide.addComponent("window", Window);
			
			tide.addComponents(
				[	LogonCtl,
					SystemMessageServiceCtl, 
					ActionTakenListServiceCtl,
					ContainerListServiceCtl,
					DiscrepancyListServiceCtl,
					EquipmentListServiceCtl,
					DrugItemInfoServiceCtl,
					PrepackPackSizeListServiceCtl,
					DmDrugListServiceCtl,
					PrepackRecordListServiceCtl,
					PrepackRecordServiceCtl,
					PrintLabelServiceCtl,
					DprWorksheetServiceCtl,
					DrugPrepackDailyRptServiceCtl,
					DiscrepancyActionTakenRptServiceCtl,
					AuditRptServiceCtl,
					EnquiryRptServiceCtl,
					ReportServiceCtl,
					PrintLogServiceCtl,
					UserStatusServiceCtl,
					PrepackRecordHistoryCtl,
					EquipmentServiceCtl,
					ContainerServiceCtl,
					ManufacturerServiceCtl,
					DrugItemInfoRptServiceCtl
				]); 
		}
	}
}
