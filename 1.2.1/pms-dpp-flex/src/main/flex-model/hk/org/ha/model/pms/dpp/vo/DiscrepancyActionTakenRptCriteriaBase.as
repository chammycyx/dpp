/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (DiscrepancyActionTakenRptCriteria.as).
 */

package hk.org.ha.model.pms.dpp.vo {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import hk.org.ha.model.pms.dpp.udt.ReportDateType;
    import org.granite.util.Enum;

    [Bindable]
    public class DiscrepancyActionTakenRptCriteriaBase implements IExternalizable {

        private var _endDate:Date;
        private var _reportDateType:ReportDateType;
        private var _startDate:Date;

        public function set endDate(value:Date):void {
            _endDate = value;
        }
        public function get endDate():Date {
            return _endDate;
        }

        public function set reportDateType(value:ReportDateType):void {
            _reportDateType = value;
        }
        public function get reportDateType():ReportDateType {
            return _reportDateType;
        }

        public function set startDate(value:Date):void {
            _startDate = value;
        }
        public function get startDate():Date {
            return _startDate;
        }

        public function readExternal(input:IDataInput):void {
            _endDate = input.readObject() as Date;
            _reportDateType = Enum.readEnum(input) as ReportDateType;
            _startDate = input.readObject() as Date;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_endDate);
            output.writeObject(_reportDateType);
            output.writeObject(_startDate);
        }
    }
}