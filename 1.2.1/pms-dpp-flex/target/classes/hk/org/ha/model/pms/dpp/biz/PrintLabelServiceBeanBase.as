/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (PrintLabelServiceBean.as).
 */

package hk.org.ha.model.pms.dpp.biz {

    import flash.utils.flash_proxy;
    import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class PrintLabelServiceBeanBase extends Component {

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function print(arg0:PrepackRecord, arg1:int, arg2:Boolean, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("print", arg0, arg1, arg2, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("print", arg0, arg1, arg2, resultHandler);
            else if (resultHandler == null)
                callProperty("print", arg0, arg1, arg2);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrievePrepackRecord(arg0:Number, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrievePrepackRecord", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrievePrepackRecord", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("retrievePrepackRecord", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function generateLabel(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("generateLabel", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("generateLabel", resultHandler);
            else if (resultHandler == null)
                callProperty("generateLabel");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
