/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (PrintLogServiceBean.as).
 */

package hk.org.ha.model.pms.dpp.biz {

    import flash.utils.flash_proxy;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class PrintLogServiceBeanBase extends Component {

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function createPrintLogForSampleLabel(arg0:Number, arg1:Number, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("createPrintLogForSampleLabel", arg0, arg1, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("createPrintLogForSampleLabel", arg0, arg1, resultHandler);
            else if (resultHandler == null)
                callProperty("createPrintLogForSampleLabel", arg0, arg1);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function createPrintLogForLabel(arg0:Number, arg1:Number, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("createPrintLogForLabel", arg0, arg1, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("createPrintLogForLabel", arg0, arg1, resultHandler);
            else if (resultHandler == null)
                callProperty("createPrintLogForLabel", arg0, arg1);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrievePrintLogByPrepackRecordId(arg0:Number, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrievePrintLogByPrepackRecordId", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrievePrintLogByPrepackRecordId", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("retrievePrintLogByPrepackRecordId", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
