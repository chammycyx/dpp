/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (ReportServiceBean.as).
 */

package hk.org.ha.model.pms.dpp.biz {

    import flash.utils.flash_proxy;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class ReportServiceBeanBase extends Component {

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function setReport(arg0:String, arg1:String, arg2:String, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("setReport", arg0, arg1, arg2, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("setReport", arg0, arg1, arg2, resultHandler);
            else if (resultHandler == null)
                callProperty("setReport", arg0, arg1, arg2);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function setReportWindowsProperties(arg0:String, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("setReportWindowsProperties", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("setReportWindowsProperties", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("setReportWindowsProperties", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function generateExcelReport(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("generateExcelReport", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("generateExcelReport", resultHandler);
            else if (resultHandler == null)
                callProperty("generateExcelReport");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
