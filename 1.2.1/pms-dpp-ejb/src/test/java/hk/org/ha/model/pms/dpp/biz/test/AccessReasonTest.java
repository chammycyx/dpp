package hk.org.ha.model.pms.dpp.biz.test;

import hk.org.ha.model.pms.biz.security.AccessReasonServiceLocal;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class AccessReasonTest extends SeamTest {
	
	@Test
	public void testAccessReasonComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
			}
		}.run();
		
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				AccessReasonServiceLocal accessReasonServiceLocal = (AccessReasonServiceLocal)getValue("#{accessReasonService}");
				
				accessReasonServiceLocal.saveLogonReason("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
			}
		}.run();
	}

}
