package hk.org.ha.model.pms.dpp.biz.test;

import java.util.List;
import hk.org.ha.model.pms.dpp.vo.Manufacturer;
import hk.org.ha.model.pms.dpp.biz.ManufacturerListServiceLocal;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class ManufacturerListTest extends SeamTest {
	
	@Test
	public void testManufacturerListComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
			}
		}.run();
		
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				List<Manufacturer> manufacturerList;
				
				ManufacturerListServiceLocal manufacturerListServiceLocal = (ManufacturerListServiceLocal)getValue("#{manufacturerListService}");
				
				manufacturerListServiceLocal.retrieveManufacturerList();
				
				manufacturerList = (List<Manufacturer>)getValue("#{manufacturerList}");
				
				assert manufacturerList != null;
				
				assert manufacturerList.size() > 0;
			}
		}.run();
	}

}
