package hk.org.ha.model.pms.dpp.biz.test;

import hk.org.ha.model.pms.dpp.biz.ActionTakenListServiceLocal;
import hk.org.ha.model.pms.dpp.persistence.ActionTaken;
import hk.org.ha.model.pms.dpp.udt.RecordStatus;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class ActionTakenListTest extends SeamTest {
	
	private final String actionTakenName = "TESTING ACTION TAKEN NAME";
	private final String newActionTakenName = "!T@E#S$T%I^N&G* !A@C#T$I%O^N& !N@A#M$E%";
	
	@Test
	public void testActionTakenListComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
				invokeMethod("#{postLogonService.postLogon}");
			}
		}.run();
		
		// Retrieve, Insert, Update, Remove
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				// Initialize service
				ActionTakenListServiceLocal actionTakenListService = (ActionTakenListServiceLocal)getValue("#{actionTakenListService}");
				
				// Retrieve existing actionTaken list
				actionTakenListService.retrieveActionTakenList();
				List<ActionTaken> actionTakenList = (List<ActionTaken>)getValue("#{actionTakenList}");
				assert actionTakenList != null;
				int totalAmount = actionTakenList.size();
				
				// Insert new actionTaken
				ActionTaken newActionTaken = new ActionTaken();
				newActionTaken.setActionTakenName(actionTakenName);
				newActionTaken.setRecordStatus(RecordStatus.Active);
				actionTakenList.add(newActionTaken);
				
				// Check 1. Id generated after update 2. total record amount added 3. actionTaken name
				// actionTaken list will be retrieved during update method 
				assert newActionTaken.getId() == null;
				actionTakenListService.updateActionTakenList(actionTakenList);
				assert newActionTaken.getId() != null;
				long newId = newActionTaken.getId().longValue();
				actionTakenList = (List<ActionTaken>)getValue("#{actionTakenList}");
				assert actionTakenList.size() == totalAmount + 1;
				newActionTaken = null;
				for(ActionTaken actionTaken : actionTakenList) {
					if(actionTaken.getId().longValue() == newId) {
						newActionTaken = actionTaken;
						break;
					}
				}
				assert newActionTaken.getActionTakenName().equals(actionTakenName);
				
				// Update
				newActionTaken.setActionTakenName(newActionTakenName);
				actionTakenListService.updateActionTakenList(actionTakenList);
				actionTakenList = (List<ActionTaken>)getValue("#{actionTakenList}");
				for(ActionTaken actionTaken : actionTakenList) {
					if(actionTaken.getId().longValue() == newId) {
						newActionTaken = actionTaken;
						break;
					}
				}
				assert !newActionTaken.getActionTakenName().equals(actionTakenName);
				assert newActionTaken.getActionTakenName().equals(newActionTakenName);
				
				// Remove
				newActionTaken.setRecordStatus(RecordStatus.Deleted);
				actionTakenListService.updateActionTakenList(actionTakenList);
				actionTakenList = (List<ActionTaken>)getValue("#{actionTakenList}");
				assert actionTakenList.size() == totalAmount;
				for(ActionTaken actionTaken : actionTakenList) {
					if(actionTaken.getId().longValue() == newId) {
						assert false;
						break;
					}
				}
				
				// Integration
				ActionTaken updateActionTaken = new ActionTaken();
				ActionTaken deleteActionTaken = new ActionTaken();
				ActionTaken insertActionTaken = new ActionTaken();
				updateActionTaken.setActionTakenName(actionTakenName);
				updateActionTaken.setRecordStatus(RecordStatus.Active);
				deleteActionTaken.setActionTakenName(newActionTakenName);
				deleteActionTaken.setRecordStatus(RecordStatus.Active);
				insertActionTaken.setActionTakenName(newActionTakenName);
				insertActionTaken.setRecordStatus(RecordStatus.Active);
				actionTakenList.add(updateActionTaken);
				actionTakenList.add(deleteActionTaken);
				
				actionTakenListService.updateActionTakenList(actionTakenList);
				
				long updateId = updateActionTaken.getId().longValue();
				long deleteId = deleteActionTaken.getId().longValue();
				
				actionTakenList = (List<ActionTaken>)getValue("#{actionTakenList}");
				totalAmount = actionTakenList.size();
				for(ActionTaken actionTaken : actionTakenList) {
					if(actionTaken.getId().longValue() == updateId) {
						actionTaken.setActionTakenName(newActionTakenName);
						continue;
					}
					if(actionTaken.getId().longValue() == deleteId) {
						actionTaken.setRecordStatus(RecordStatus.Deleted);
					}
				}
				actionTakenList.add(insertActionTaken);
				
				actionTakenListService.updateActionTakenList(actionTakenList);
				
				actionTakenList = (List<ActionTaken>)getValue("#{actionTakenList}");
				
				assert actionTakenList.size() == totalAmount;
				assert insertActionTaken.getId() != null;
				updateActionTaken = null;
				for(ActionTaken actionTaken : actionTakenList) {
					if(actionTaken.getId().longValue() == updateId) {
						updateActionTaken = actionTaken;
						continue;
					}
					if(actionTaken.getId().longValue() == deleteId) {
						assert false;
					}
				}
				assert updateActionTaken.getActionTakenName().equals(newActionTakenName);
				
				
				List<ActionTaken> tmpActionTakenList;
				
				// Test Function "updateActionTakenDetailsList", "retrieveActionTakenDetailsList"
				actionTakenListService.retrieveActionTakenDetailsList(1012L);
				tmpActionTakenList = (List<ActionTaken>)getValue("#{actionTakenList}");
				assert actionTakenList != null;
				// Set True to False and False to True
				for (ActionTaken actionTaken: tmpActionTakenList) {
					actionTaken.setEnable(!actionTaken.isEnable());
				}
				actionTakenListService.updateActionTakenDetailsList(1004L, tmpActionTakenList);
				
				// Verify
				actionTakenListService.retrieveActionTakenDetailsList(1012L);
				actionTakenList = (List<ActionTaken>)getValue("#{actionTakenList}");
				assert actionTakenList.size() == tmpActionTakenList.size();
				for (int i=0; i<actionTakenList.size(); i++) {
					for(ActionTaken tmpActionTaken : tmpActionTakenList) {
						if (tmpActionTaken.getId() == ((ActionTaken)actionTakenList.get(i)).getId()) {
							assert ((ActionTaken)actionTakenList.get(i)).isEnable() == tmpActionTaken.isEnable();
						}
					}
				}
			}
		}.run();
	}

}
