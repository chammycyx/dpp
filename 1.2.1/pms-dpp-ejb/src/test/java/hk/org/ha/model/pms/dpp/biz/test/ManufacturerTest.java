package hk.org.ha.model.pms.dpp.biz.test;

import hk.org.ha.model.pms.dpp.biz.ManufacturerServiceLocal;
import hk.org.ha.model.pms.dpp.vo.Manufacturer;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class ManufacturerTest extends SeamTest {
	
	@Test
	public void testManufacturerComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
				invokeMethod("#{postLogonService.postLogon}");
			}
		}.run();
		
		new ComponentTest() {
			protected void testComponents() throws Exception {
				Manufacturer manufacturer;
				
				// Retrieve number of existing records
				ManufacturerServiceLocal manufacturerServiceLocal = (ManufacturerServiceLocal)getValue("#{manufacturerService}");
				
				manufacturerServiceLocal.retrieveManufacturerByManufCode("MEDI");
				
				manufacturer = (Manufacturer)getValue("#{manufacturer}");
				
				assert manufacturer != null;
				
				assert manufacturer.getCompanyName().equals("MEDIPHARMA LTD"); 
			}
		}.run();
	}

}