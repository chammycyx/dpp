package hk.org.ha.model.pms.dpp.biz.test;

import hk.org.ha.model.pms.dpp.biz.ContainerListServiceLocal;
import hk.org.ha.model.pms.dpp.persistence.Container;
import hk.org.ha.model.pms.dpp.udt.RecordStatus;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class ContainerListTest extends SeamTest {
	
	private final String containerName = "TESTING CONTAINER NAME";
	private final String newContainerName = "!T@E#S$T%I^N&G* !C@O#N$T%A^I&N*E(R) !N@A#M$E%";
	
	@Test
	public void testContainerListComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
				invokeMethod("#{postLogonService.postLogon}");
			}
		}.run();
		
		// Retrieve, Insert, Update, Remove
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				// Initialize service
				ContainerListServiceLocal containerListService = (ContainerListServiceLocal)getValue("#{containerListService}");
				
				// Retrieve existing container list
				containerListService.retrieveContainerList();
				List<Container> containerList = (List<Container>)getValue("#{containerList}");
				assert containerList != null;
				int totalAmount = containerList.size();
				
				// Insert new container
				Container newContainer = new Container();
				newContainer.setContainerName(containerName);
				newContainer.setRecordStatus(RecordStatus.Active);
				containerList.add(newContainer);
				
				// Check 1. Id generated after update 2. total record amount added 3. container name
				// container list will be retrieved during update method 
				assert newContainer.getId() == null;
				containerListService.updateContainerList(containerList);
				assert newContainer.getId() != null;
				long newId = newContainer.getId().longValue();
				containerList = (List<Container>)getValue("#{containerList}");
				assert containerList.size() == totalAmount + 1;
				newContainer = null;
				for(Container container : containerList) {
					if(container.getId().longValue() == newId) {
						newContainer = container;
						break;
					}
				}
				assert newContainer.getContainerName().equals(containerName);
				
				// Update
				newContainer.setContainerName(newContainerName);
				containerListService.updateContainerList(containerList);
				containerList = (List<Container>)getValue("#{containerList}");
				for(Container container : containerList) {
					if(container.getId().longValue() == newId) {
						newContainer = container;
						break;
					}
				}
				assert !newContainer.getContainerName().equals(containerName);
				assert newContainer.getContainerName().equals(newContainerName);
				
				// Remove
				newContainer.setRecordStatus(RecordStatus.Deleted);
				containerListService.updateContainerList(containerList);
				containerList = (List<Container>)getValue("#{containerList}");
				assert containerList.size() == totalAmount;
				for(Container container : containerList) {
					if(container.getId().longValue() == newId) {
						assert false;
						break;
					}
				}
				
				// Integration
				Container updateContainer = new Container();
				Container deleteContainer = new Container();
				Container insertContainer = new Container();
				updateContainer.setContainerName(containerName);
				updateContainer.setRecordStatus(RecordStatus.Active);
				deleteContainer.setContainerName(newContainerName);
				deleteContainer.setRecordStatus(RecordStatus.Active);
				insertContainer.setContainerName(newContainerName);
				insertContainer.setRecordStatus(RecordStatus.Active);
				containerList.add(updateContainer);
				containerList.add(deleteContainer);
				
				containerListService.updateContainerList(containerList);
				
				long updateId = updateContainer.getId().longValue();
				long deleteId = deleteContainer.getId().longValue();
				
				containerList = (List<Container>)getValue("#{containerList}");
				totalAmount = containerList.size();
				for(Container container : containerList) {
					if(container.getId().longValue() == updateId) {
						container.setContainerName(newContainerName);
						continue;
					}
					if(container.getId().longValue() == deleteId) {
						container.setRecordStatus(RecordStatus.Deleted);
					}
				}
				containerList.add(insertContainer);
				
				containerListService.updateContainerList(containerList);
				
				containerList = (List<Container>)getValue("#{containerList}");
				
				assert containerList.size() == totalAmount;
				assert insertContainer.getId() != null;
				updateContainer = null;
				for(Container container : containerList) {
					if(container.getId().longValue() == updateId) {
						updateContainer = container;
						continue;
					}
					if(container.getId().longValue() == deleteId) {
						assert false;
					}
				}
				assert updateContainer.getContainerName().equals(newContainerName);
				
			}
		}.run();
	}

}
