package hk.org.ha.model.pms.dpp.biz.test;

import java.math.BigDecimal;
import java.util.List;

import hk.org.ha.model.pms.dpp.biz.PrepackPackSizeListServiceLocal;
import hk.org.ha.model.pms.dpp.persistence.PrepackPackSize;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PrepackPackSizeListTest extends SeamTest {
	
	@Test
	public void testPrepackPackSizeListComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
				invokeMethod("#{accessControl.retrieveUserAccessControl}");
			}
		}.run();
		
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				List<PrepackPackSize> prepackPackSizeList;
				
				PrepackPackSizeListServiceLocal prepackPackSizeListServiceLocal = (PrepackPackSizeListServiceLocal)getValue("#{prepackPackSizeListService}");
				
				prepackPackSizeListServiceLocal.retrievePrepackPackSizeListByDrugItemInfoId(1000L);
				
				prepackPackSizeList = (List<PrepackPackSize>)getValue("#{prepackPackSizeList}");
				
				assert prepackPackSizeList.size() > 0;
				
				for(PrepackPackSize prepackPackSize: prepackPackSizeList) {
					assert prepackPackSize.getPackUnit().equals("TABLET(S)");
					if (prepackPackSize.getId().equals(1000L)) {
						assert prepackPackSize.getPackSize().equals(new BigDecimal(4));
					} else if (prepackPackSize.getId().equals(1001L)) {
						assert prepackPackSize.getPackSize().equals(new BigDecimal(8));
					} else if (prepackPackSize.getId().equals(1002L)) {
						assert prepackPackSize.getPackSize().equals(new BigDecimal(12));
					} else if (prepackPackSize.getId().equals(1014L)) {
						assert prepackPackSize.getPackSize().equals(new BigDecimal(16.25));
					}
				}
			}
		}.run();
	}

}
