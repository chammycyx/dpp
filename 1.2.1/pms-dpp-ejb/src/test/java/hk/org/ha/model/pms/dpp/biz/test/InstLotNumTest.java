package hk.org.ha.model.pms.dpp.biz.test;

import hk.org.ha.model.pms.dpp.biz.InstLotNumServiceLocal;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class InstLotNumTest extends SeamTest {
	
	@Test
	public void testInstLotNumComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
				invokeMethod("#{postLogonService.postLogon}");
			}
		}.run();
		
		// Retrieve
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				Integer nextLotNum;
				
				// Initialize service
				InstLotNumServiceLocal instLotNumService = (InstLotNumServiceLocal)getValue("#{instLotNumService}");
				
				nextLotNum = instLotNumService.newInstLotNum("PMH");
				
				assert nextLotNum.equals(new Integer(1));
				
				nextLotNum = instLotNumService.newInstLotNum("PMH");
				
				assert nextLotNum.equals(new Integer(2));
				
				nextLotNum = instLotNumService.newInstLotNum("PMH");
				
				assert nextLotNum.equals(new Integer(3));
				
				nextLotNum = instLotNumService.newInstLotNum("QEH");
				
				assert nextLotNum.equals(new Integer(1));
				
				nextLotNum = instLotNumService.newInstLotNum("QEH");
				
				assert nextLotNum.equals(new Integer(2));
				
			}
			
		}.run();
		
	}

}