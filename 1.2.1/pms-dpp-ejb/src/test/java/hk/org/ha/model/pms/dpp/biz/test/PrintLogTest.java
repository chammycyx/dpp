package hk.org.ha.model.pms.dpp.biz.test;

import java.util.List;

import hk.org.ha.model.pms.dpp.biz.PrintLogServiceLocal;
import hk.org.ha.model.pms.dpp.biz.PrepackRecordServiceLocal;
import hk.org.ha.model.pms.dpp.persistence.PrintLog;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PrintLogTest extends SeamTest {
	
	@Test
	public void testPrintLogComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
				invokeMethod("#{accessControl.retrieveUserAccessControl}");
			}
		}.run();
		
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				List<PrintLog> printLogList;
				int numExistingRecords;
				
				String oldPrepackRecordVersion, newPrepackRecordVersion;
				
				// For getting prepack record version
				PrepackRecordServiceLocal prepackRecordService = (PrepackRecordServiceLocal)getValue("#{prepackRecordService}");
				
				// Retrieve number of existing records
				PrintLogServiceLocal printLogServiceLocal = (PrintLogServiceLocal)getValue("#{printLogService}");
				
				printLogServiceLocal.retrievePrintLogByPrepackRecordId(1003L);
				
				printLogList = (List<PrintLog>)getValue("#{printLogList}");
				
				assert printLogList != null;
				
				assert printLogList.size() == 3;
				
				numExistingRecords = printLogList.size();
				
				// Insert 1 Print Log record
				printLogServiceLocal.createPrintLogForSampleLabel(1006L, 3);
				
				prepackRecordService.retrievePrepackRecordVersion(1003L);
				
				oldPrepackRecordVersion = (String)getValue("#{prepackRecordVersion}");
				
				// Insert 1 Print Log record
				printLogServiceLocal.createPrintLogForLabel(1003L, 24);
				
				// Verify newly inserted records
				printLogServiceLocal.retrievePrintLogByPrepackRecordId(1003L);
				
				printLogList = (List<PrintLog>)getValue("#{printLogList}");
				
				assert printLogList != null;
				
				if (printLogList.size() != (numExistingRecords + 2)) {
					assert false;
				}
				
				// Verify newly created prepack record
				
				prepackRecordService.retrievePrepackRecordVersion(1003L);
				
				newPrepackRecordVersion = (String)getValue("#{prepackRecordVersion}");
				
				assert Long.parseLong(oldPrepackRecordVersion) < Long.parseLong(newPrepackRecordVersion);
			}
		}.run();
	}

}
