package hk.org.ha.model.pms.dpp.biz.test;

import java.text.SimpleDateFormat;
import java.util.Date;

import hk.org.ha.model.pms.dpp.biz.DiscrepancyActionTakenRptServiceLocal;
import hk.org.ha.model.pms.dpp.udt.ReportDateType;
import hk.org.ha.model.pms.dpp.vo.DiscrepancyActionTakenRptCriteria;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class DiscrepancyActionTakenRptTest extends SeamTest {
	
	@Test
	public void testDiscrepancyActionTakenRptComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
				invokeMethod("#{postLogonService.postLogon}");
			}
		}.run();
		
		// Retrieve
		new ComponentTest() {
			protected void testComponents() throws Exception {
				// Initialize service
				DiscrepancyActionTakenRptServiceLocal discrepancyActionTakenRptService = (DiscrepancyActionTakenRptServiceLocal)getValue("#{discrepancyActionTakenRptService}");
				
				DiscrepancyActionTakenRptCriteria discrepancyActionTakenRptCriteria = new DiscrepancyActionTakenRptCriteria();
				
				discrepancyActionTakenRptCriteria.setReportDateType(ReportDateType.PreparationDate);
				
				String dateString = "20110829";
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				Date startDate = sdf.parse(dateString);
				dateString = "20111023";
				Date endDate = sdf.parse(dateString);
				discrepancyActionTakenRptCriteria.setStartDate(startDate);
				discrepancyActionTakenRptCriteria.setEndDate(endDate);
				
				discrepancyActionTakenRptService.retrieveDiscrepancyActionTakenRptList(discrepancyActionTakenRptCriteria);
				
				assert discrepancyActionTakenRptService.getDiscrepancyActionTakenRptList().size() == 1;
			}
			
		}.run();
		
	}

}
