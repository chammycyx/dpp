package hk.org.ha.model.pms.dpp.biz.test;

import hk.org.ha.model.pms.dpp.biz.DiscrepancyListServiceLocal;
import hk.org.ha.model.pms.dpp.persistence.Discrepancy;
import hk.org.ha.model.pms.dpp.udt.RecordStatus;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class DiscrepancyListTest extends SeamTest {
	
	private final String discrepancyName = "TESTING DISCREPANCY NAME";
	private final String newDiscrepancyName = "!T@E#S$T%I^N&G* !D@I#S$C%R^E&P*A(N)C_Y+ !N@A#M$E%";
	
	@Test
	public void testDiscrepancyListComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
				invokeMethod("#{postLogonService.postLogon}");
			}
		}.run();
		
		// Retrieve, Insert, Update, Remove
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				// Initialize service
				DiscrepancyListServiceLocal discrepancyListService = (DiscrepancyListServiceLocal)getValue("#{discrepancyListService}");
				
				// Retrieve existing discrepancy list
				discrepancyListService.retrieveDiscrepancyList();
				List<Discrepancy> discrepancyList = (List<Discrepancy>)getValue("#{discrepancyList}");
				assert discrepancyList != null;
				int totalAmount = discrepancyList.size();
				
				// Insert new discrepancy
				Discrepancy newDiscrepancy = new Discrepancy();
				newDiscrepancy.setDiscrepancyName(discrepancyName);
				newDiscrepancy.setRecordStatus(RecordStatus.Active);
				discrepancyList.add(newDiscrepancy);
				
				// Check 1. Id generated after update 2. total record amount added 3. discrepancy name
				// discrepancy list will be retrieved during update method 
				assert newDiscrepancy.getId() == null;
				discrepancyListService.updateDiscrepancyList(discrepancyList);
				assert newDiscrepancy.getId() != null;
				long newId = newDiscrepancy.getId().longValue();
				discrepancyList = (List<Discrepancy>)getValue("#{discrepancyList}");
				assert discrepancyList.size() == totalAmount + 1;
				newDiscrepancy = null;
				for(Discrepancy discrepancy : discrepancyList) {
					if(discrepancy.getId().longValue() == newId) {
						newDiscrepancy = discrepancy;
						break;
					}
				}
				assert newDiscrepancy.getDiscrepancyName().equals(discrepancyName);
				
				// Update
				newDiscrepancy.setDiscrepancyName(newDiscrepancyName);
				discrepancyListService.updateDiscrepancyList(discrepancyList);
				discrepancyList = (List<Discrepancy>)getValue("#{discrepancyList}");
				for(Discrepancy discrepancy : discrepancyList) {
					if(discrepancy.getId().longValue() == newId) {
						newDiscrepancy = discrepancy;
						break;
					}
				}
				assert !newDiscrepancy.getDiscrepancyName().equals(discrepancyName);
				assert newDiscrepancy.getDiscrepancyName().equals(newDiscrepancyName);
				
				// Remove
				newDiscrepancy.setRecordStatus(RecordStatus.Deleted);
				discrepancyListService.updateDiscrepancyList(discrepancyList);
				discrepancyList = (List<Discrepancy>)getValue("#{discrepancyList}");
				assert discrepancyList.size() == totalAmount;
				for(Discrepancy discrepancy : discrepancyList) {
					if(discrepancy.getId().longValue() == newId) {
						assert false;
						break;
					}
				}
				
				// Integration
				Discrepancy updateDiscrepancy = new Discrepancy();
				Discrepancy deleteDiscrepancy = new Discrepancy();
				Discrepancy insertDiscrepancy = new Discrepancy();
				updateDiscrepancy.setDiscrepancyName(discrepancyName);
				updateDiscrepancy.setRecordStatus(RecordStatus.Active);
				deleteDiscrepancy.setDiscrepancyName(newDiscrepancyName);
				deleteDiscrepancy.setRecordStatus(RecordStatus.Active);
				insertDiscrepancy.setDiscrepancyName(newDiscrepancyName);
				insertDiscrepancy.setRecordStatus(RecordStatus.Active);
				discrepancyList.add(updateDiscrepancy);
				discrepancyList.add(deleteDiscrepancy);
				
				discrepancyListService.updateDiscrepancyList(discrepancyList);
				
				long updateId = updateDiscrepancy.getId().longValue();
				long deleteId = deleteDiscrepancy.getId().longValue();
				
				discrepancyList = (List<Discrepancy>)getValue("#{discrepancyList}");
				totalAmount = discrepancyList.size();
				for(Discrepancy discrepancy : discrepancyList) {
					if(discrepancy.getId().longValue() == updateId) {
						discrepancy.setDiscrepancyName(newDiscrepancyName);
						continue;
					}
					if(discrepancy.getId().longValue() == deleteId) {
						discrepancy.setRecordStatus(RecordStatus.Deleted);
					}
				}
				discrepancyList.add(insertDiscrepancy);
				
				discrepancyListService.updateDiscrepancyList(discrepancyList);
				
				discrepancyList = (List<Discrepancy>)getValue("#{discrepancyList}");
				
				assert discrepancyList.size() == totalAmount;
				assert insertDiscrepancy.getId() != null;
				updateDiscrepancy = null;
				for(Discrepancy discrepancy : discrepancyList) {
					if(discrepancy.getId().longValue() == updateId) {
						updateDiscrepancy = discrepancy;
						continue;
					}
					if(discrepancy.getId().longValue() == deleteId) {
						assert false;
					}
				}
				assert updateDiscrepancy.getDiscrepancyName().equals(newDiscrepancyName);
				
				
				List<Discrepancy> tmpDiscrepancyList;
				
				// Test Function "updateDiscrepancyDetailsList", "retrieveDiscrepancyDetailsList"
				discrepancyListService.retrieveDiscrepancyDetailsList(1012L);
				tmpDiscrepancyList = (List<Discrepancy>)getValue("#{discrepancyList}");
				assert discrepancyList != null;
				// Set True to False and False to True
				for (Discrepancy discrepancy: tmpDiscrepancyList) {
					discrepancy.setEnable(!discrepancy.isEnable());
				}
				discrepancyListService.updateDiscrepancyDetailsList(1004L, tmpDiscrepancyList);
				
				// Verify
				discrepancyListService.retrieveDiscrepancyDetailsList(1012L);
				discrepancyList = (List<Discrepancy>)getValue("#{discrepancyList}");
				assert discrepancyList.size() == tmpDiscrepancyList.size();
				for (int i=0; i<discrepancyList.size(); i++) {
					for(Discrepancy tmpDiscrepancy : tmpDiscrepancyList) {
						if (tmpDiscrepancy.getId() == ((Discrepancy)discrepancyList.get(i)).getId()) {
							assert ((Discrepancy)discrepancyList.get(i)).isEnable() == tmpDiscrepancy.isEnable();
						}
					}
				}
			}
		}.run();
	}

}
