package hk.org.ha.model.pms.dpp.biz.test;

import java.text.SimpleDateFormat;
import java.util.Date;

import hk.org.ha.model.pms.dpp.biz.DrugPrepackDailyRptServiceLocal;
import hk.org.ha.model.pms.dpp.udt.ReportDateType;
import hk.org.ha.model.pms.dpp.vo.DrugPrepackDailyRptCriteria;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class DrugPrepackDailyRptTest extends SeamTest {
	
	@Test
	public void testDrugPrepackDailyRptComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
				invokeMethod("#{postLogonService.postLogon}");
			}
		}.run();
		
		// Retrieve
		new ComponentTest() {
			protected void testComponents() throws Exception {
				// Initialize service
				DrugPrepackDailyRptServiceLocal drugPrepackDailyRptService = (DrugPrepackDailyRptServiceLocal)getValue("#{drugPrepackDailyRptService}");
				
				DrugPrepackDailyRptCriteria drugPrepackDailyRptCriteria = new DrugPrepackDailyRptCriteria();
				
				drugPrepackDailyRptCriteria.setReportDateType(ReportDateType.PreparationDate);
				
				String dateString = "20110829";
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				Date startDate = sdf.parse(dateString);
				dateString = "20111023";
				Date endDate = sdf.parse(dateString);
				drugPrepackDailyRptCriteria.setStartDate(startDate);
				drugPrepackDailyRptCriteria.setEndDate(endDate);
				
				drugPrepackDailyRptService.retrieveDrugPrepackDailyRptList(drugPrepackDailyRptCriteria);
				
				assert drugPrepackDailyRptService.getDrugPrepackDailyRptList().size() == 5;
			}
			
		}.run();
		
	}

}
