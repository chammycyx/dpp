package hk.org.ha.model.pms.dpp.biz.test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import hk.org.ha.model.pms.dpp.biz.ContainerListServiceLocal;
import hk.org.ha.model.pms.dpp.biz.EquipmentListServiceLocal;
import hk.org.ha.model.pms.dpp.biz.PrepackPackSizeListServiceLocal;
import hk.org.ha.model.pms.dpp.biz.PrepackRecordServiceLocal;
import hk.org.ha.model.pms.dpp.biz.DrugItemInfoServiceLocal;
import hk.org.ha.model.pms.dpp.persistence.Container;
import hk.org.ha.model.pms.dpp.persistence.DrugItemInfo;
import hk.org.ha.model.pms.dpp.persistence.Equipment;
import hk.org.ha.model.pms.dpp.persistence.PrepackPackSize;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;
import hk.org.ha.model.pms.dpp.udt.ActionStatus;
import hk.org.ha.model.pms.dpp.udt.ExpiryMonth;
import hk.org.ha.model.pms.dpp.udt.ExpiryWeek;
import hk.org.ha.model.pms.dpp.udt.LabelSize;
import hk.org.ha.model.pms.dpp.udt.PrepackStatus;
import hk.org.ha.model.pms.dpp.udt.RecordStatus;

//import java.util.Date;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PrepackRecordTest extends SeamTest {
	
	@Test
	public void testPrepackRecordComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
				invokeMethod("#{accessControl.retrieveUserAccessControl}");
			}
		}.run();
		
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				// Test Function "retrievePrepackRecordVersion"
				
				String prepackRecordVersion;
				
				PrepackRecordServiceLocal prepackRecordService = (PrepackRecordServiceLocal)getValue("#{prepackRecordService}");
				
				prepackRecordService.retrievePrepackRecordVersion(1000L);
				
				prepackRecordVersion = (String)getValue("#{prepackRecordVersion}");
				
				assert prepackRecordVersion != null;
				
				assert prepackRecordVersion.equals("1");
				
				//*******************************************************************************
				// Test Function "addPrepackRecord", "createPrepackRecord", "updatePrepackRecord",
				// 						"cancelPrepackRecord" and "retrievePrepackRecordById"
				//*******************************************************************************
				
				// Add a new pre-pack record
				PrepackRecord prepackRecord;
				
				prepackRecordService.addPrepackRecord();
				
				prepackRecord = (PrepackRecord)getValue("#{prepackRecord}");
				
				assert prepackRecord != null;
				
				assert prepackRecord.getId() == null;
				
				assert prepackRecord.getPrepackRecordVer().getAssignExpiryMonth() == ExpiryMonth.TwelveMonths;
				
				assert prepackRecord.getPrepackRecordVer().getAssignExpiryWeek() == ExpiryWeek.ZeroWeeks;
				
				// fill new pre-pack record with data and save
				
				Calendar cal = Calendar.getInstance();
				java.sql.Date today = new java.sql.Date( cal.getTime().getTime());
				
				String manufCode = "MEDI";
				String batchNum = "W123456";
				String ddu = "TABLET(S)";
				Integer originalPackQty = 1;
				Integer originalPackSize = 2;
				Integer originalTotalQty = 3;
				Integer sampleCheckQty = 2;
				Integer provisionPrepackQty = 4;
				Integer provisionPrepackLabelQty = 5;
				BigDecimal provisionRemainQty = new BigDecimal(6);
				Integer actualPrepackQty = 5;
				Integer actualLabelRemainQty = 4;
				Integer actualAddLabelQty = 2;
				BigDecimal actualItemRemainQty = new BigDecimal(1);
				String prepareBy = "PrepareBy";
				String preCheckBy = "PreCheckBy";
				String lineClearCheckBy = "LineClear";
				String prepackBy = "PrepackBy";
				String finalCheckBy = "FinalCheckBy";
				String discrepancyRemark = "DiscrepancyRemark";
				String actionTakenRemark = "ActionTakenRemark";
				String cancelReason = "CancelReason";
				
				prepackRecord.getPrepackRecordVer().setManufCode(manufCode);
				prepackRecord.getPrepackRecordVer().setBatchNum(batchNum);
				prepackRecord.getPrepackRecordVer().setExpiryDate(today);
				prepackRecord.getPrepackRecordVer().setDispenseDosageUnit(ddu);
				prepackRecord.getPrepackRecordVer().setOriginalPackQty(originalPackQty);
				prepackRecord.getPrepackRecordVer().setOriginalPackSize(originalPackSize);
				prepackRecord.getPrepackRecordVer().setOriginalTotalQty(originalTotalQty);
				prepackRecord.getPrepackRecordVer().setPreparationDate(today);
				prepackRecord.getPrepackRecordVer().setAssignExpiryMonth(ExpiryMonth.EighteenMonths);
				prepackRecord.getPrepackRecordVer().setAssignExpiryWeek(ExpiryWeek.FourWeeks);
				prepackRecord.getPrepackRecordVer().setAssignExpiryDate(today);
				prepackRecord.getPrepackRecordVer().setSampleCheckQty(sampleCheckQty);
				prepackRecord.getPrepackRecordVer().setProvisionPrepackQty(provisionPrepackQty);
				prepackRecord.getPrepackRecordVer().setProvisionPrepackLabelQty(provisionPrepackLabelQty);
				prepackRecord.getPrepackRecordVer().setProvisionRemainQty(provisionRemainQty);
				prepackRecord.getPrepackRecordVer().setLabelSize(LabelSize.DppLabel1Size4x2);
				prepackRecord.getPrepackRecordVer().setActualPrepackDate(today);
				prepackRecord.getPrepackRecordVer().setActualPrepackQty(actualPrepackQty);
				prepackRecord.getPrepackRecordVer().setActualLabelRemainQty(actualLabelRemainQty);
				prepackRecord.getPrepackRecordVer().setActualAddLabelQty(actualAddLabelQty);
				prepackRecord.getPrepackRecordVer().setActualItemRemainQty(actualItemRemainQty);
				prepackRecord.getPrepackRecordVer().setPrepareBy(prepareBy);
				prepackRecord.getPrepackRecordVer().setPreCheckBy(preCheckBy);
				prepackRecord.getPrepackRecordVer().setLineClearCheckBy(lineClearCheckBy);
				prepackRecord.getPrepackRecordVer().setPrepackBy(prepackBy);
				prepackRecord.getPrepackRecordVer().setFinalCheckBy(finalCheckBy);
				prepackRecord.getPrepackRecordVer().setDiscrepancyRemark(discrepancyRemark);
				prepackRecord.getPrepackRecordVer().setActionTakenRemark(actionTakenRemark);
				prepackRecord.getPrepackRecordVer().setPrepackStatus(PrepackStatus.New);
				prepackRecord.getPrepackRecordVer().setRecordStatus(RecordStatus.Active);
				prepackRecord.getPrepackRecordVer().setCancelReason(cancelReason);
				prepackRecord.getPrepackRecordVer().setActionStatus(ActionStatus.New);
				
				
				ContainerListServiceLocal containerListService = (ContainerListServiceLocal)getValue("#{containerListService}");
				containerListService.retrieveContainerList();
				List<Container> containerList = (List<Container>)getValue("#{containerList}");
				assert containerList != null;
				prepackRecord.getPrepackRecordVer().setContainer(containerList.get(0));
				
				EquipmentListServiceLocal equipmentListService = (EquipmentListServiceLocal)getValue("#{equipmentListService}");
				equipmentListService.retrieveEquipmentList();
				List<Equipment> equipmentList = (List<Equipment>)getValue("#{equipmentList}");
				assert equipmentList != null;
				prepackRecord.getPrepackRecordVer().setEquipment(equipmentList.get(0));
				
				DrugItemInfoServiceLocal drugItemInfoService = (DrugItemInfoServiceLocal)getValue("#{drugItemInfoService}");
				drugItemInfoService.retrieveDrugItemInfo("PARA01");
				DrugItemInfo drugItemInfo = (DrugItemInfo)getValue("#{drugItemInfo}");
				assert drugItemInfo != null;
				
				prepackRecord.getPrepackRecordVer().setPrepackPackSize(drugItemInfo.getPrepackPackSizeList().get(0));
				prepackRecord.getPrepackRecordVer().setDrugItemInfoVer(drugItemInfo.getDrugItemInfoVer());
				
				prepackRecordService.createPrepackRecord(prepackRecord);
				
				// Verify data
				// Get auto generated id
				prepackRecord = (PrepackRecord)getValue("#{prepackRecord}");
				prepackRecordService.retrievePrepackRecordById(prepackRecord.getId());
				
				prepackRecord = (PrepackRecord)getValue("#{prepackRecord}");
				
				assert prepackRecord.getPrepackRecordVer().getManufCode().equals(manufCode);
				assert prepackRecord.getPrepackRecordVer().getBatchNum().equals(batchNum);
				assert prepackRecord.getPrepackRecordVer().getDispenseDosageUnit().equals(ddu);
				assert prepackRecord.getPrepackRecordVer().getOriginalPackQty().equals(originalPackQty);
				assert prepackRecord.getPrepackRecordVer().getOriginalPackSize().equals(originalPackSize);
				assert prepackRecord.getPrepackRecordVer().getOriginalTotalQty().equals(originalTotalQty);
				assert prepackRecord.getPrepackRecordVer().getPreparationDate().toString().equals(today.toString());
				assert prepackRecord.getPrepackRecordVer().getAssignExpiryMonth() == ExpiryMonth.EighteenMonths;
				assert prepackRecord.getPrepackRecordVer().getAssignExpiryWeek() == ExpiryWeek.FourWeeks;
				assert prepackRecord.getPrepackRecordVer().getAssignExpiryDate().toString().equals(today.toString());
				assert prepackRecord.getPrepackRecordVer().getSampleCheckQty().equals(sampleCheckQty);
				assert prepackRecord.getPrepackRecordVer().getProvisionPrepackQty().equals(provisionPrepackQty);
				assert prepackRecord.getPrepackRecordVer().getProvisionPrepackLabelQty().equals(provisionPrepackLabelQty);
				assert prepackRecord.getPrepackRecordVer().getProvisionRemainQty().equals(provisionRemainQty);
				assert prepackRecord.getPrepackRecordVer().getLabelSize() == LabelSize.DppLabel1Size4x2;
				assert prepackRecord.getPrepackRecordVer().getActualPrepackDate().toString().equals(today.toString());
				assert prepackRecord.getPrepackRecordVer().getActualPrepackQty().equals(actualPrepackQty);
				assert prepackRecord.getPrepackRecordVer().getActualLabelRemainQty().equals(actualLabelRemainQty);
				assert prepackRecord.getPrepackRecordVer().getActualAddLabelQty().equals(actualAddLabelQty);
				assert prepackRecord.getPrepackRecordVer().getActualItemRemainQty().equals(actualItemRemainQty);
				assert prepackRecord.getPrepackRecordVer().getPrepareBy().equals(prepareBy);
				assert prepackRecord.getPrepackRecordVer().getPreCheckBy().equals(preCheckBy);
				assert prepackRecord.getPrepackRecordVer().getLineClearCheckBy().equals(lineClearCheckBy);
				assert prepackRecord.getPrepackRecordVer().getPrepackBy().equals(prepackBy);
				assert prepackRecord.getPrepackRecordVer().getFinalCheckBy().equals(finalCheckBy);
				assert prepackRecord.getPrepackRecordVer().getDiscrepancyRemark().equals(discrepancyRemark);
				assert prepackRecord.getPrepackRecordVer().getActionTakenRemark().equals(actionTakenRemark);
				assert prepackRecord.getPrepackRecordVer().getPrepackStatus() == PrepackStatus.New;
				assert prepackRecord.getPrepackRecordVer().getRecordStatus() == RecordStatus.Active;
				assert prepackRecord.getPrepackRecordVer().getCancelReason().equals(cancelReason);
				assert prepackRecord.getPrepackRecordVer().getActionStatus().equals(ActionStatus.New);
				assert prepackRecord.getPrepackRecordVer().getContainer().getId().equals(containerList.get(0).getId());
				assert prepackRecord.getPrepackRecordVer().getEquipment().getId().equals(equipmentList.get(0).getId());
				assert prepackRecord.getPrepackRecordVer().getDrugItemInfoVer().getId().equals(drugItemInfo.getDrugItemInfoVer().getId());
				assert prepackRecord.getPrepackRecordVer().getPrepackPackSize().getId().equals(drugItemInfo.getPrepackPackSizeList().get(0).getId());

				// Update pre-pack record
				
				manufCode = "CROM";
				batchNum = "SXXXXXXXXXXXXXXXXXXE";
				ddu = "BOTTLE(S)";
				originalPackQty = 12;
				originalPackSize = 13;
				originalTotalQty = 156;
				sampleCheckQty = 4;
				provisionPrepackQty = 9;
				provisionPrepackLabelQty = 10;
				provisionRemainQty = new BigDecimal(9.75);
				actualPrepackQty = 9;
				actualLabelRemainQty = 1;
				actualAddLabelQty = 1;
				actualItemRemainQty = new BigDecimal(1.25);
				prepareBy = "itditd";
				preCheckBy = "itdcoorb";
				lineClearCheckBy = "itdadmin";
				prepackBy = "itdopera";
				finalCheckBy = "itdadmin";
				discrepancyRemark = "SXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXE";
				actionTakenRemark = "SXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXE";
				cancelReason = "SXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXE";
				
				List<PrepackPackSize> prepackPackSizeList;
				PrepackPackSizeListServiceLocal prepackPackSizeListServiceLocal = (PrepackPackSizeListServiceLocal)getValue("#{prepackPackSizeListService}");
				prepackPackSizeListServiceLocal.retrievePrepackPackSizeListByDrugItemInfoId(1000L);
				prepackPackSizeList = (List<PrepackPackSize>)getValue("#{prepackPackSizeList}");
				
				for(PrepackPackSize prepackPackSize: prepackPackSizeList) {
					if (prepackPackSize.getId().equals(1014L)) {
						prepackRecord.getPrepackRecordVer().setPrepackPackSize(prepackPackSize);
						break;
					}
				}
				
				prepackRecord.getPrepackRecordVer().setManufCode(manufCode);
				prepackRecord.getPrepackRecordVer().setBatchNum(batchNum);
				prepackRecord.getPrepackRecordVer().setExpiryDate(today);
				prepackRecord.getPrepackRecordVer().setDispenseDosageUnit(ddu);
				prepackRecord.getPrepackRecordVer().setOriginalPackQty(originalPackQty);
				prepackRecord.getPrepackRecordVer().setOriginalPackSize(originalPackSize);
				prepackRecord.getPrepackRecordVer().setOriginalTotalQty(originalTotalQty);
				prepackRecord.getPrepackRecordVer().setPreparationDate(today);
				prepackRecord.getPrepackRecordVer().setAssignExpiryMonth(ExpiryMonth.NullValue);
				prepackRecord.getPrepackRecordVer().setAssignExpiryWeek(ExpiryWeek.NullValue);
				prepackRecord.getPrepackRecordVer().setAssignExpiryDate(today);
				prepackRecord.getPrepackRecordVer().setSampleCheckQty(sampleCheckQty);
				prepackRecord.getPrepackRecordVer().setProvisionPrepackQty(provisionPrepackQty);
				prepackRecord.getPrepackRecordVer().setProvisionPrepackLabelQty(provisionPrepackLabelQty);
				prepackRecord.getPrepackRecordVer().setProvisionRemainQty(provisionRemainQty);
				prepackRecord.getPrepackRecordVer().setLabelSize(LabelSize.DppLabel2Size4x2);
				prepackRecord.getPrepackRecordVer().setActualPrepackDate(today);
				prepackRecord.getPrepackRecordVer().setActualPrepackQty(actualPrepackQty);
				prepackRecord.getPrepackRecordVer().setActualLabelRemainQty(actualLabelRemainQty);
				prepackRecord.getPrepackRecordVer().setActualAddLabelQty(actualAddLabelQty);
				prepackRecord.getPrepackRecordVer().setActualItemRemainQty(actualItemRemainQty);
				prepackRecord.getPrepackRecordVer().setPrepareBy(prepareBy);
				prepackRecord.getPrepackRecordVer().setPreCheckBy(preCheckBy);
				prepackRecord.getPrepackRecordVer().setLineClearCheckBy(lineClearCheckBy);
				prepackRecord.getPrepackRecordVer().setPrepackBy(prepackBy);
				prepackRecord.getPrepackRecordVer().setFinalCheckBy(finalCheckBy);
				prepackRecord.getPrepackRecordVer().setDiscrepancyRemark(discrepancyRemark);
				prepackRecord.getPrepackRecordVer().setActionTakenRemark(actionTakenRemark);
				prepackRecord.getPrepackRecordVer().setPrepackStatus(PrepackStatus.Printed);
				prepackRecord.getPrepackRecordVer().setRecordStatus(RecordStatus.Active);
				prepackRecord.getPrepackRecordVer().setCancelReason(cancelReason);
				prepackRecord.getPrepackRecordVer().setActionStatus(ActionStatus.Printed);
				
				prepackRecord.getPrepackRecordVer().setContainer(containerList.get(1));
				prepackRecord.getPrepackRecordVer().setEquipment(equipmentList.get(1));
				prepackRecord.getPrepackRecordVer().setPrepackPackSize(drugItemInfo.getPrepackPackSizeList().get(1));
				
				prepackRecordService.updatePrepackRecord(prepackRecord);
								
				prepackRecord = (PrepackRecord)getValue("#{prepackRecord}");
				
				assert prepackRecord.getPrepackRecordVer().getManufCode().equals(manufCode);
				assert prepackRecord.getPrepackRecordVer().getBatchNum().equals(batchNum);
				assert prepackRecord.getPrepackRecordVer().getDispenseDosageUnit().equals(ddu);
				assert prepackRecord.getPrepackRecordVer().getOriginalPackQty().equals(originalPackQty);
				assert prepackRecord.getPrepackRecordVer().getOriginalPackSize().equals(originalPackSize);
				assert prepackRecord.getPrepackRecordVer().getOriginalTotalQty().equals(originalTotalQty);
				assert prepackRecord.getPrepackRecordVer().getPreparationDate().toString().equals(today.toString());
				assert prepackRecord.getPrepackRecordVer().getAssignExpiryMonth() == ExpiryMonth.NullValue;
				assert prepackRecord.getPrepackRecordVer().getAssignExpiryWeek() == ExpiryWeek.NullValue;
				assert prepackRecord.getPrepackRecordVer().getAssignExpiryDate().toString().equals(today.toString());
				assert prepackRecord.getPrepackRecordVer().getSampleCheckQty().equals(sampleCheckQty);
				assert prepackRecord.getPrepackRecordVer().getProvisionPrepackQty().equals(provisionPrepackQty);
				assert prepackRecord.getPrepackRecordVer().getProvisionPrepackLabelQty().equals(provisionPrepackLabelQty);
				assert prepackRecord.getPrepackRecordVer().getProvisionRemainQty().equals(provisionRemainQty);
				assert prepackRecord.getPrepackRecordVer().getLabelSize() == LabelSize.DppLabel2Size4x2;
				assert prepackRecord.getPrepackRecordVer().getActualPrepackDate().toString().equals(today.toString());
				assert prepackRecord.getPrepackRecordVer().getActualPrepackQty().equals(actualPrepackQty);
				assert prepackRecord.getPrepackRecordVer().getActualLabelRemainQty().equals(actualLabelRemainQty);
				assert prepackRecord.getPrepackRecordVer().getActualAddLabelQty().equals(actualAddLabelQty);
				assert prepackRecord.getPrepackRecordVer().getActualItemRemainQty().equals(actualItemRemainQty);
				assert prepackRecord.getPrepackRecordVer().getPrepareBy().equals(prepareBy);
				assert prepackRecord.getPrepackRecordVer().getPreCheckBy().equals(preCheckBy);
				assert prepackRecord.getPrepackRecordVer().getLineClearCheckBy().equals(lineClearCheckBy);
				assert prepackRecord.getPrepackRecordVer().getPrepackBy().equals(prepackBy);
				assert prepackRecord.getPrepackRecordVer().getFinalCheckBy().equals(finalCheckBy);
				assert prepackRecord.getPrepackRecordVer().getDiscrepancyRemark().equals(discrepancyRemark);
				assert prepackRecord.getPrepackRecordVer().getActionTakenRemark().equals(actionTakenRemark);
				assert prepackRecord.getPrepackRecordVer().getPrepackStatus() == PrepackStatus.Printed;
				assert prepackRecord.getPrepackRecordVer().getRecordStatus() == RecordStatus.Active;
				assert prepackRecord.getPrepackRecordVer().getCancelReason().equals(cancelReason);
				assert prepackRecord.getPrepackRecordVer().getActionStatus().equals(ActionStatus.Printed);
				assert prepackRecord.getPrepackRecordVer().getContainer().getId().equals(containerList.get(1).getId());
				assert prepackRecord.getPrepackRecordVer().getEquipment().getId().equals(equipmentList.get(1).getId());
				assert prepackRecord.getPrepackRecordVer().getPrepackPackSize().getId().equals(drugItemInfo.getPrepackPackSizeList().get(1).getId());
				
				// Cancel pre-pack record
				prepackRecordService.cancelPrepackRecord(prepackRecord);
				
				prepackRecord = (PrepackRecord)getValue("#{prepackRecord}");
				
				assert prepackRecord.getPrepackRecordVer().getActionStatus() == ActionStatus.Cancelled;
				assert prepackRecord.getPrepackRecordVer().getPrepackStatus() == PrepackStatus.Cancelled;
			}
		}.run();
	}

}