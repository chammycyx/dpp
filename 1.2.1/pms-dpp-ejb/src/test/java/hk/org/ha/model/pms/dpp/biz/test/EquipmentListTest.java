package hk.org.ha.model.pms.dpp.biz.test;

import hk.org.ha.model.pms.dpp.biz.EquipmentListServiceLocal;
import hk.org.ha.model.pms.dpp.persistence.Equipment;
import hk.org.ha.model.pms.dpp.udt.RecordStatus;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class EquipmentListTest extends SeamTest {
	
	private final String equipmentName = "TESTING EQUIPMENT NAME";
	private final String newEquipmentName = "!T@E#S$T%I^N&G* !E@Q#U$I%P^M&E*N(T) !N@A#M$E%";
	
	@Test
	public void testEquipmentListComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
				invokeMethod("#{postLogonService.postLogon}");
			}
		}.run();
		
		// Retrieve, Insert, Update, Remove
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				// Initialize service
				EquipmentListServiceLocal equipmentListService = (EquipmentListServiceLocal)getValue("#{equipmentListService}");
				
				// Retrieve existing equipment list
				equipmentListService.retrieveEquipmentList();
				List<Equipment> equipmentList = (List<Equipment>)getValue("#{equipmentList}");
				assert equipmentList != null;
				int totalAmount = equipmentList.size();
				
				// Insert new equipment
				Equipment newEquipment = new Equipment();
				newEquipment.setEquipmentName(equipmentName);
				newEquipment.setRecordStatus(RecordStatus.Active);
				equipmentList.add(newEquipment);
				
				// Check 1. Id generated after update 2. total record amount added 3. equipment name
				// equipment list will be retrieved during update method 
				assert newEquipment.getId() == null;
				equipmentListService.updateEquipmentList(equipmentList);
				assert newEquipment.getId() != null;
				long newId = newEquipment.getId().longValue();
				equipmentList = (List<Equipment>)getValue("#{equipmentList}");
				assert equipmentList.size() == totalAmount + 1;
				newEquipment = null;
				for(Equipment equipment : equipmentList) {
					if(equipment.getId().longValue() == newId) {
						newEquipment = equipment;
						break;
					}
				}
				assert newEquipment.getEquipmentName().equals(equipmentName);
				
				// Update
				newEquipment.setEquipmentName(newEquipmentName);
				equipmentListService.updateEquipmentList(equipmentList);
				equipmentList = (List<Equipment>)getValue("#{equipmentList}");
				for(Equipment equipment : equipmentList) {
					if(equipment.getId().longValue() == newId) {
						newEquipment = equipment;
						break;
					}
				}
				assert !newEquipment.getEquipmentName().equals(equipmentName);
				assert newEquipment.getEquipmentName().equals(newEquipmentName);
				
				// Remove
				newEquipment.setRecordStatus(RecordStatus.Deleted);
				equipmentListService.updateEquipmentList(equipmentList);
				equipmentList = (List<Equipment>)getValue("#{equipmentList}");
				assert equipmentList.size() == totalAmount;
				for(Equipment equipment : equipmentList) {
					if(equipment.getId().longValue() == newId) {
						assert false;
						break;
					}
				}
				
				// Integration
				Equipment updateEquipment = new Equipment();
				Equipment deleteEquipment = new Equipment();
				Equipment insertEquipment = new Equipment();
				updateEquipment.setEquipmentName(equipmentName);
				updateEquipment.setRecordStatus(RecordStatus.Active);
				deleteEquipment.setEquipmentName(newEquipmentName);
				deleteEquipment.setRecordStatus(RecordStatus.Active);
				insertEquipment.setEquipmentName(newEquipmentName);
				insertEquipment.setRecordStatus(RecordStatus.Active);
				equipmentList.add(updateEquipment);
				equipmentList.add(deleteEquipment);
				
				equipmentListService.updateEquipmentList(equipmentList);
				
				long updateId = updateEquipment.getId().longValue();
				long deleteId = deleteEquipment.getId().longValue();
				
				equipmentList = (List<Equipment>)getValue("#{equipmentList}");
				totalAmount = equipmentList.size();
				for(Equipment equipment : equipmentList) {
					if(equipment.getId().longValue() == updateId) {
						equipment.setEquipmentName(newEquipmentName);
						continue;
					}
					if(equipment.getId().longValue() == deleteId) {
						equipment.setRecordStatus(RecordStatus.Deleted);
					}
				}
				equipmentList.add(insertEquipment);
				
				equipmentListService.updateEquipmentList(equipmentList);
				
				equipmentList = (List<Equipment>)getValue("#{equipmentList}");
				
				assert equipmentList.size() == totalAmount;
				assert insertEquipment.getId() != null;
				updateEquipment = null;
				for(Equipment equipment : equipmentList) {
					if(equipment.getId().longValue() == updateId) {
						updateEquipment = equipment;
						continue;
					}
					if(equipment.getId().longValue() == deleteId) {
						assert false;
					}
				}
				assert updateEquipment.getEquipmentName().equals(newEquipmentName);
				
			}
		}.run();
	}

}
