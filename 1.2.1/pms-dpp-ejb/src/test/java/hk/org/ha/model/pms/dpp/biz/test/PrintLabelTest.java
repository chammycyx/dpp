package hk.org.ha.model.pms.dpp.biz.test;

import hk.org.ha.model.pms.dpp.biz.PrintLabelServiceLocal;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PrintLabelTest extends SeamTest {
	
	@Test
	public void testPrintLabelComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
				invokeMethod("#{accessControl.retrieveUserAccessControl}");
			}
		}.run();
		
		new ComponentTest() {
			protected void testComponents() throws Exception {
				PrintLabelServiceLocal printLabelServiceLocal = (PrintLabelServiceLocal)getValue("#{printLabelService}");

				printLabelServiceLocal.retrievePrepackRecord(1004L);
			}
		}.run();
	}

}
