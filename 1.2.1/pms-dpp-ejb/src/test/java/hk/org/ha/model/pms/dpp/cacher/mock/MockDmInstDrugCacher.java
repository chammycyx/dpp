package hk.org.ha.model.pms.dpp.cacher.mock;

import java.util.List;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmInstDrug;
import hk.org.ha.model.pms.dpp.cacher.DmInstDrugCacherInf;

@AutoCreate
@Name("dmInstDrugCacher")
@Scope(ScopeType.APPLICATION)
@Install(precedence=Install.MOCK)
public class MockDmInstDrugCacher extends BaseCacher implements DmInstDrugCacherInf {
	
	public void clearAll() {
	}
	
	public DmInstDrug getDrugByItemCode(String itemCode) {
		InnerDmInstDrug dmInstDrug = new InnerDmInstDrug();
		return dmInstDrug;
	}
	
	public List<DmInstDrug> getDrugListByItemCode(String prefixItemCode) {
		return null;
	}
	
	@Override
	public List<DmInstDrug> getDrugList(String prefixItemCode) {
		return null;
	}
	
	@SuppressWarnings("serial")
	public static class InnerDmDrug extends DmDrug{
		
		public String getFullDrugDesc(){			
			return this.getItemCode();
		}
	}
	
	@SuppressWarnings("serial")
	public static class InnerDmInstDrug extends DmInstDrug{
		
		public String getInstSuspend() {			
			return "N";
		}
		
		public String getInstAvailability() {			
			return "Y";
		}
	}
}
