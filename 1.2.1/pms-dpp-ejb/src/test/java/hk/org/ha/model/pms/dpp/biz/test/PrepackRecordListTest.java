package hk.org.ha.model.pms.dpp.biz.test;

import java.math.BigDecimal;
import java.util.List;

import hk.org.ha.model.pms.dpp.biz.PrepackRecordListServiceLocal;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;
import hk.org.ha.model.pms.dpp.vo.PrepackRecordSearchCriteria;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PrepackRecordListTest extends SeamTest {
	
	@Test
	public void testPrintLogComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
				invokeMethod("#{postLogonService.postLogon}");
			}
		}.run();
		
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				List<PrepackRecord> prepackRecordList;
				PrepackRecordSearchCriteria prepackRecordSearchCriteria;

				PrepackRecordListServiceLocal prepackRecordListServiceLocal = 
						(PrepackRecordListServiceLocal)getValue("#{prepackRecordListService}");
				
				// Test BatchNum
				prepackRecordSearchCriteria = new PrepackRecordSearchCriteria();
				prepackRecordSearchCriteria.setInstitutionCode("PMH");
				prepackRecordSearchCriteria.setBatchNum("E000123");
				
				prepackRecordListServiceLocal.retrievePrepackRecordListBySearchCriteria(prepackRecordSearchCriteria);
				
				prepackRecordList = (List<PrepackRecord>)getValue("#{prepackRecordList}");
				
				assert prepackRecordList != null;
				
				assert prepackRecordList.get(0).getId().equals(1000L);
				
				// Test Search ALL
				prepackRecordSearchCriteria = new PrepackRecordSearchCriteria();
				prepackRecordSearchCriteria.setInstitutionCode("PMH");
				
				prepackRecordListServiceLocal.retrievePrepackRecordListBySearchCriteria(prepackRecordSearchCriteria);
				
				prepackRecordList = (List<PrepackRecord>)getValue("#{prepackRecordList}");
				
				assert prepackRecordList != null;
				
				assert prepackRecordList.size() > 0;
				
				// Test item code
				prepackRecordSearchCriteria = new PrepackRecordSearchCriteria();
				prepackRecordSearchCriteria.setInstitutionCode("PMH");
				prepackRecordSearchCriteria.setItemCode("PARA01");
				
				prepackRecordListServiceLocal.retrievePrepackRecordListBySearchCriteria(prepackRecordSearchCriteria);
				
				prepackRecordList = (List<PrepackRecord>)getValue("#{prepackRecordList}");
				
				assert prepackRecordList != null;
				
				for(PrepackRecord prepackRecord : prepackRecordList) {
					assert prepackRecord.getPrepackRecordVer().getDrugItemInfoVer().getDrugItemInfo().getItemCode().equals("PARA01");
				}
				
				// Test Lot number
				prepackRecordSearchCriteria = new PrepackRecordSearchCriteria();
				prepackRecordSearchCriteria.setInstitutionCode("PMH");
				prepackRecordSearchCriteria.setLotNum("110830001");
				
				prepackRecordListServiceLocal.retrievePrepackRecordListBySearchCriteria(prepackRecordSearchCriteria);
				
				prepackRecordList = (List<PrepackRecord>)getValue("#{prepackRecordList}");
				
				assert prepackRecordList != null;
				
				assert prepackRecordList.get(0).getLotNum().equals("110830001");

				// Test pack size and pack unit
				prepackRecordSearchCriteria = new PrepackRecordSearchCriteria();
				prepackRecordSearchCriteria.setInstitutionCode("PMH");
				prepackRecordSearchCriteria.setPackSize(new BigDecimal(8));
				prepackRecordSearchCriteria.setPackUnit("TABLET(S)");
				
				prepackRecordListServiceLocal.retrievePrepackRecordListBySearchCriteria(prepackRecordSearchCriteria);
				
				prepackRecordList = (List<PrepackRecord>)getValue("#{prepackRecordList}");
				
				assert prepackRecordList != null;
				
				assert prepackRecordList.get(0).getPrepackRecordVer().getPrepackPackSize().getPackSize().equals(new BigDecimal(8));
				
				assert prepackRecordList.get(0).getPrepackRecordVer().getPrepackPackSize().getPackUnit().equals("TABLET(S)");
			}
		}.run();
	}

}
