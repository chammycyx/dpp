package hk.org.ha.model.pms.dpp.biz.test;

import java.util.List;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dpp.biz.DmDrugListServiceLocal;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class DmDrugListTest extends SeamTest {
	
	@Test
	public void testDmDrugListComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
			}
		}.run();
		
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				
				List<DmDrug> dmDrugList;
				
				DmDrugListServiceLocal dmDrugListServiceLocal = (DmDrugListServiceLocal)getValue("#{dmDrugListService}");
				
				dmDrugListServiceLocal.retrieveDmDrugListLikeItemCode("PARA");
				
				dmDrugList = (List<DmDrug>)getValue("#{dmDrugList}");
				
				assert dmDrugList.get(0).getItemCode().equals("PARA01");
				assert dmDrugList.get(1).getItemCode().equals("PARA02");
				assert dmDrugList.get(2).getItemCode().equals("PARA03");
				assert dmDrugList.get(3).getItemCode().equals("PARA04");
				
				
				dmDrugListServiceLocal.retrieveDmDrugListLikeItemCode("ACEB01");
				
				dmDrugList = (List<DmDrug>)getValue("#{dmDrugList}");
				
				assert dmDrugList.get(0).getItemCode().equals("ACEB01");
			}
		}.run();
	}

}