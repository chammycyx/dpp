package hk.org.ha.model.pms.dpp.biz.test;

import hk.org.ha.model.pms.dpp.biz.DprWorksheetServiceLocal;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class DprWorksheetTest extends SeamTest {
	
	@Test
	public void testDprWorksheetComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
				invokeMethod("#{accessControl.retrieveUserAccessControl}");
			}
		}.run();
		
		new ComponentTest() {
			protected void testComponents() throws Exception {
				DprWorksheetServiceLocal dprWorksheetServiceLocal = (DprWorksheetServiceLocal)getValue("#{dprWorksheetService}");
				dprWorksheetServiceLocal.prepareDprWorksheet(1000L);			
			}
		}.run();
	}

}
