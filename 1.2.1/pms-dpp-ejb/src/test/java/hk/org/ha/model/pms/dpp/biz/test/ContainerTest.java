package hk.org.ha.model.pms.dpp.biz.test;

import hk.org.ha.model.pms.dpp.biz.ContainerServiceLocal;
import hk.org.ha.model.pms.dpp.persistence.Container;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class ContainerTest extends SeamTest {
	
	@Test
	public void testContainerComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
				invokeMethod("#{postLogonService.postLogon}");
			}
		}.run();
		
		new ComponentTest() {
			protected void testComponents() throws Exception {
				Container container;
				
				ContainerServiceLocal containerServiceLocal = (ContainerServiceLocal)getValue("#{containerService}");
				
				containerServiceLocal.retrieveContainerByPrepackRecordVerId(1000L);
				
				container = (Container)getValue("#{container}");
				
				assert  container != null;
			}
		}.run();
	}

}
