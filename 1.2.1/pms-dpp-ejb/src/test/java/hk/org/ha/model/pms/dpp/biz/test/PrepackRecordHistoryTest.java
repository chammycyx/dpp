package hk.org.ha.model.pms.dpp.biz.test;

import java.util.List;

import hk.org.ha.model.pms.dpp.biz.PrepackRecordHistoryServiceLocal;
import hk.org.ha.model.pms.dpp.vo.PrepackHistory;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PrepackRecordHistoryTest extends SeamTest {
	
	@Test
	public void testPrepackRecordHistoryComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
				invokeMethod("#{postLogonService.postLogon}");
			}
		}.run();
		
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				
				List<PrepackHistory> prepackHistoryList;
				
				PrepackRecordHistoryServiceLocal prepackRecordHistoryServiceLocal = (PrepackRecordHistoryServiceLocal)getValue("#{prepackRecordHistoryService}");
				
				prepackRecordHistoryServiceLocal.retrievePrepackRecordHistory(1003L);
				
				prepackHistoryList = (List<PrepackHistory>)getValue("#{prepackHistoryList}");
				
				assert prepackHistoryList != null;
				
				if (prepackHistoryList.size() < 1) {
					assert false;
				}
			}
		}.run();
	}

}
