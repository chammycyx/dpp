package hk.org.ha.model.pms.dpp.biz.test;

import java.text.SimpleDateFormat;
import java.util.Date;

import hk.org.ha.model.pms.dpp.biz.EnquiryRptServiceLocal;
import hk.org.ha.model.pms.dpp.vo.EnquiryRptCriteria;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class EnquiryRptTest extends SeamTest {
	
	@Test
	public void testEnquiryRptComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
				invokeMethod("#{postLogonService.postLogon}");
			}
		}.run();
		
		// Retrieve
		new ComponentTest() {
			protected void testComponents() throws Exception {
				// Initialize service
				EnquiryRptServiceLocal enquiryRptService = (EnquiryRptServiceLocal)getValue("#{enquiryRptService}");
				
				EnquiryRptCriteria enquiryRptCriteria = new EnquiryRptCriteria();
				
				String dateString = "20110829";
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				Date startDate = sdf.parse(dateString);
				dateString = "20111023";
				Date endDate = sdf.parse(dateString);
				enquiryRptCriteria.setPreparationStartDate(startDate);
				enquiryRptCriteria.setPreparationEndDate(endDate);
				
				enquiryRptService.retrieveEnquiryRptList(enquiryRptCriteria);
				
				assert enquiryRptService.getEnquiryRptList().size() == 5;
				
				enquiryRptCriteria.setItemCode("PARA01");
				enquiryRptService.retrieveEnquiryRptList(enquiryRptCriteria);
				
				assert enquiryRptService.getEnquiryRptList().size() == 2;
				
			}
			
		}.run();
		
	}

}
