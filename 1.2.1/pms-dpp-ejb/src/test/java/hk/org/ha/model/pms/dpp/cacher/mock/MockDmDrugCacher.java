package hk.org.ha.model.pms.dpp.cacher.mock;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmDrugProperty;
import hk.org.ha.model.pms.dpp.cacher.DmDrugCacherInf;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Name("dmDrugCacher")
@Scope(ScopeType.APPLICATION)
@Install(precedence=Install.MOCK)
public class MockDmDrugCacher extends BaseCacher implements DmDrugCacherInf {
	
	//private List<DmDrug> dmDrugList;
	
	private List<DmDrug> tempDmDrugList() {
		List<DmDrug> dmDrugList = new ArrayList<DmDrug>();
		
		InnerDmDrug dmDrug = new InnerDmDrug();
		
		dmDrug.setItemCode("PARA01");
		dmDrug.setBaseUnit("TABLET(S)");
		dmDrugList.add(dmDrug);
		
		dmDrug = new InnerDmDrug();
		dmDrug.setItemCode("PARA02");
		dmDrug.setBaseUnit("TABLET(S)");
		dmDrugList.add(dmDrug);
		
		dmDrug = new InnerDmDrug();
		dmDrug.setItemCode("PARA03");
		dmDrug.setBaseUnit("TABLET(S)");
		dmDrugList.add(dmDrug);
		
		dmDrug = new InnerDmDrug();
		dmDrug.setItemCode("PARA04");
		dmDrug.setBaseUnit("TABLET(S)");
		dmDrugList.add(dmDrug);
		
		dmDrug = new InnerDmDrug();
		dmDrug.setItemCode("PARA05");
		dmDrug.setBaseUnit("TABLET(S)");
		dmDrugList.add(dmDrug);
		
		dmDrug = new InnerDmDrug();
		dmDrug.setItemCode("PARA06");
		dmDrug.setBaseUnit("TABLET(S)");
		dmDrugList.add(dmDrug);
		
		dmDrug = new InnerDmDrug();
		dmDrug.setItemCode("PARA07");
		dmDrug.setBaseUnit("TABLET(S)");
		dmDrugList.add(dmDrug);
		
		dmDrug = new InnerDmDrug();
		dmDrug.setItemCode("PARA08");
		dmDrug.setBaseUnit("TABLET(S)");
		dmDrugList.add(dmDrug);
		
		dmDrug = new InnerDmDrug();
		dmDrug.setItemCode("PARA09");
		dmDrug.setBaseUnit("TABLET(S)");
		dmDrugList.add(dmDrug);
		
		dmDrug = new InnerDmDrug();
		dmDrug.setItemCode("ACEB01");
		dmDrug.setBaseUnit("BOTTLE(S)");
		dmDrugList.add(dmDrug);
		
		return dmDrugList;
	}
	
	public DmDrug getDrugByItemCode(String itemCode) {
		List<DmDrug> dmDrugList = tempDmDrugList();
		
		for(DmDrug dmDrug : dmDrugList) {
			if(dmDrug.getItemCode().equals(itemCode)) {
				return dmDrug;
			}
		}
		return null;
	}
	
	public List<DmDrug> getDrugListByItemCode(String prefixItemCode) {
		List<DmDrug> dmDrugList = tempDmDrugList();
		
		List<DmDrug> rtnDmDrugList = new ArrayList<DmDrug>();
		
		for(DmDrug dmDrug : dmDrugList) {
			if(dmDrug.getItemCode().startsWith(prefixItemCode)) {
				rtnDmDrugList.add(dmDrug);
			}
		}
		
		return rtnDmDrugList;
	}
	
	public List<DmDrug> getDrugList(String prefixItemCode) {
		return null;
	}
	
	@SuppressWarnings("serial")
	public static class InnerDmDrug extends DmDrug{
		@Override
		public String getFullDrugDesc(){			
			return this.getItemCode();
		}
		
		@Override
		public String getFormCode(){			
			return "";
		}
		
		@Override
		public DmDrugProperty getDmDrugProperty(){			
			return new InnerDmDrugProperty();
		}
		
		public static class InnerDmDrugProperty extends DmDrugProperty {
			@Override
			public String getDisplayname() {
				return "";
			}
			@Override
			public String getSaltProperty() {
				return "";
			}
		}
	}
}
