package hk.org.ha.model.pms.dpp.biz.test;

import hk.org.ha.model.pms.dpp.biz.EquipmentServiceLocal;
import hk.org.ha.model.pms.dpp.persistence.Equipment;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class EquipmentTest extends SeamTest {
	
	@Test
	public void testEquipmentComponent() throws Exception {
		
		// Login
		new ComponentTest() {
			protected void testComponents() throws Exception {
				assert getValue("#{identity.loggedIn}").equals(false);
				setValue("#{identity.username}", "admin");
				setValue("#{identity.password}", "admin");
				invokeMethod("#{identity.login}");
				assert getValue("#{identity.loggedIn}").equals(true);
				invokeMethod("#{postLogonService.postLogon}");
			}
		}.run();
		
		new ComponentTest() {
			protected void testComponents() throws Exception {
				Equipment equipment;
				
				EquipmentServiceLocal equipmentServiceLocal = (EquipmentServiceLocal)getValue("#{equipmentService}");
				
				equipmentServiceLocal.retrieveEquipmentByPrepackRecordVerId(1000L);
				
				equipment = (Equipment)getValue("#{equipment}");
				
				assert  equipment != null;
			}
		}.run();
	}

}
