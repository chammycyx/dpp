package hk.org.ha.model.pms.dpp.persistence;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dpp.udt.ActionStatus;
import hk.org.ha.model.pms.dpp.udt.ExpiryMonth;
import hk.org.ha.model.pms.dpp.udt.ExpiryWeek;
import hk.org.ha.model.pms.dpp.udt.LabelSize;
import hk.org.ha.model.pms.dpp.udt.PrepackStatus;
import hk.org.ha.model.pms.dpp.udt.RecordStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name="PREPACK_RECORD_VER")
@Customizer(AuditCustomizer.class)
@NamedQueries({
	@NamedQuery(name="PrepackRecordVer.findPrintHistoryByPrepackRecordId", query="select o from PrepackRecordVer o where o.prepackRecord.id = :prepackRecordid order by o.createDate"),
	@NamedQuery(name="PrepackRecordVer.findById", query="select o from PrepackRecordVer o where o.id = :id "),
	@NamedQuery(name="PrepackRecordVer.findByPreparationDate", query="select o from PrepackRecordVer o where o.drugItemInfoVer.drugItemInfo.instCode = :instCode and o.drugItemInfoVer.drugItemInfo.workstoreCode = :workstoreCode and o.prepackRecord.prepackRecordVer = o and o.preparationDate >= :startDate and o.preparationDate < :endDate and o.recordStatus = hk.org.ha.model.pms.dpp.udt.RecordStatus.Active order by o.prepackRecord.lotNum DESC "),
	@NamedQuery(name="PrepackRecordVer.findByPrepackDate", query="select o from PrepackRecordVer o where o.drugItemInfoVer.drugItemInfo.instCode = :instCode and o.drugItemInfoVer.drugItemInfo.workstoreCode = :workstoreCode and o.prepackRecord.prepackRecordVer = o and o.actualPrepackDate >= :startDate and o.actualPrepackDate < :endDate and o.recordStatus = hk.org.ha.model.pms.dpp.udt.RecordStatus.Active order by o.prepackRecord.lotNum DESC "),
	@NamedQuery(name="PrepackRecordVer.findCompletedPrepackRecordByPreparationDate", query="select o from PrepackRecordVer o where o.drugItemInfoVer.drugItemInfo.instCode = :instCode and o.drugItemInfoVer.drugItemInfo.workstoreCode = :workstoreCode and o.prepackRecord.prepackRecordVer = o and o.preparationDate >= :startDate and o.preparationDate < :endDate and o.prepackStatus = hk.org.ha.model.pms.dpp.udt.PrepackStatus.Completed and o.recordStatus = hk.org.ha.model.pms.dpp.udt.RecordStatus.Active order by o.prepackRecord.lotNum DESC "),
	@NamedQuery(name="PrepackRecordVer.findCompletedPrepackRecordByPrepackDate", query="select o from PrepackRecordVer o where o.drugItemInfoVer.drugItemInfo.instCode = :instCode and o.drugItemInfoVer.drugItemInfo.workstoreCode = :workstoreCode and o.prepackRecord.prepackRecordVer = o and o.actualPrepackDate >= :startDate and o.actualPrepackDate < :endDate and o.prepackStatus = hk.org.ha.model.pms.dpp.udt.PrepackStatus.Completed and o.recordStatus = hk.org.ha.model.pms.dpp.udt.RecordStatus.Active order by o.prepackRecord.lotNum DESC "),
	@NamedQuery(name="PrepackRecordVer.findForEnquiryReport", 
			query = "	select 	o from PrepackRecordVer o " +
					"	where 	o.drugItemInfoVer.drugItemInfo.instCode = :instCode " +
					"   and     o.drugItemInfoVer.drugItemInfo.workstoreCode = :workstoreCode " +
					"	and		o.prepackRecord.prepackRecordVer = o " +
					"	and		(	:itemCode is null or " +
					"				o.drugItemInfoVer.drugItemInfo.itemCode = :itemCode " +
					"			)" +
					"	and		(	:packSize is null or " +
					"				o.prepackPackSize.packSize = :packSize " +
					"			)" +
					"	and		(	:packUnit is null or " +
					"				o.prepackPackSize.packUnit = :packUnit " +
					"			)" +
					"	and		(	:assignedLotNum is null or " +
					"				o.prepackRecord.lotNum = :assignedLotNum " +
					"			)" +
					"	and		(	:batchNum is null or " +
					"				o.batchNum = :batchNum " +
					"			)" +
					"	and		(	:preparationStartDate is null or " +
					"				(	o.preparationDate >= :preparationStartDate and " +
					"					o.preparationDate < :preparationEndDate " +
					"				)" +
					"			)" +
					"	and		(	:prepackStartDate is null or " +
					"				(	o.actualPrepackDate >= :prepackStartDate and " +
					"					o.actualPrepackDate < :prepackEndDate " +
					"				)" +
					"			)" +
					"	and		(	:prepackStatusFlag = 'Y' or " +
					"				o.prepackStatus = :prepackStatus " +
					"			)" +
					"	and		(	:manufCode is null or " +
					"				o.manufCode = :manufCode " +
					"			)" +
					"	and		(	:containerName is null or " +
					"				o.container.containerName = :containerName " +
					"			)" +
					"	and		(	:equipmentName is null or " +
					"				o.equipment.equipmentName = :equipmentName " +
					"			)" +
					"	and		o.recordStatus = hk.org.ha.model.pms.dpp.udt.RecordStatus.Active " +
					"	order by o.prepackRecord.lotNum DESC ")
})
public class PrepackRecordVer extends VersionEntity {
	
	private static final long serialVersionUID = -2873370218359280629L;
	
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="prepackRecordVerSeq")
	@SequenceGenerator(name="prepackRecordVerSeq", sequenceName="SQ_PREPACK_RECORD_VER", initialValue=100000000)
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PREPACK_RECORD_ID", referencedColumnName="ID")
	private PrepackRecord prepackRecord;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CONTAINER_ID", referencedColumnName="ID")
	private Container container;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="EQUIPMENT_ID", referencedColumnName="ID")
	private Equipment equipment;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PREPACK_PACK_SIZE_ID", referencedColumnName="ID")
	private PrepackPackSize prepackPackSize;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DRUG_ITEM_INFO_VER_ID", referencedColumnName="ID")
	private DrugItemInfoVer drugItemInfoVer;
	
	@OneToMany(mappedBy="prepackRecordVer", fetch=FetchType.LAZY)
	private List<PrepackDiscrepancy> prepackDiscrepancyList;
	
	@OneToMany(mappedBy="prepackRecordVer", fetch=FetchType.LAZY)
	private List<PrepackActionTaken> prepackActionTakenList;
	
	@Column(name="MANUF_CODE", length=4, nullable=false)
	private String manufCode;
	
	@Column(name="BATCH_NUM", length=20, nullable=false)
	private String batchNum;
	
	@Column(name="EXPIRY_DATE", nullable=false)
	private Date expiryDate;
	
	@Column(name="DISPENSE_DOSAGE_UNIT", length=15, nullable=false)
	private String dispenseDosageUnit;
	
	@Column(name="ORIGINAL_PACK_QTY", nullable=false)
	private Integer originalPackQty;
	
	@Column(name="ORIGINAL_PACK_SIZE", nullable=false)
	private Integer originalPackSize;
	
	@Column(name="ORIGINAL_TOTAL_QTY", nullable=false)
	private Integer originalTotalQty;
	
	@Column(name="PREPARATION_DATE", nullable=false)
	private Date preparationDate;
	
	@Converter(name="PrepackRecordVer.assignExpiryMonth", converterClass=ExpiryMonth.Converter.class)
	@Convert("PrepackRecordVer.assignExpiryMonth")
	@Column(name="ASSIGN_EXPIRY_MONTH", length=3, nullable=false)
	private ExpiryMonth assignExpiryMonth;
	
	@Converter(name="PrepackRecordVer.assignExpiryWeek", converterClass=ExpiryWeek.Converter.class)
	@Convert("PrepackRecordVer.assignExpiryWeek")	
	@Column(name="ASSIGN_EXPIRY_WEEK", length=3, nullable=false)
	private ExpiryWeek assignExpiryWeek;
	
	@Column(name="ASSIGN_EXPIRY_DATE", nullable=false)
	private Date assignExpiryDate;
	
	@Column(name="SAMPLE_CHECK_QTY", nullable=false)
	private Integer sampleCheckQty;
	
	@Column(name="PROVISION_PREPACK_QTY", nullable=false)
	private Integer provisionPrepackQty;
	
	@Column(name="PROVISION_PREPACK_LABEL_QTY", nullable=false)
	private Integer provisionPrepackLabelQty;
	
	@Column(name="PROVISION_REMAIN_QTY", precision=19, scale=4, nullable=false)
	private BigDecimal provisionRemainQty;
	
	@Converter(name="PrepackRecordVer.labelSize", converterClass=LabelSize.Converter.class)
	@Convert("PrepackRecordVer.labelSize")
	@Column(name="LABEL_SIZE", length=1, nullable=false)
	private LabelSize labelSize;
	
	@Column(name="ACTUAL_PREPACK_DATE")
	private Date actualPrepackDate;
	
	@Column(name="ACTUAL_PREPACK_QTY")
	private Integer actualPrepackQty;
	
	@Column(name="ACTUAL_LABEL_REMAIN_QTY")
	private Integer actualLabelRemainQty;
	
	@Column(name="ACTUAL_ADD_LABEL_QTY")
	private Integer actualAddLabelQty;
	
	@Column(name="ACTUAL_ITEM_REMAIN_QTY", precision=19, scale=4)
	private BigDecimal actualItemRemainQty;
	
	@Column(name="PREPARE_BY", length=15, nullable=false)
	private String prepareBy;
	
	@Column(name="PRE_CHECK_BY", length=15)
	private String preCheckBy;
	
	@Column(name="LINE_CLEAR_CHECK_BY", length=15)
	private String lineClearCheckBy;
	
	@Column(name="PREPACK_BY", length=25)
	private String prepackBy;
	
	@Column(name="FINAL_CHECK_BY", length=15)
	private String finalCheckBy;
	
	@Column(name="DISCREPANCY_REMARK", length=160)
	private String discrepancyRemark;
	
	@Column(name="ACTION_TAKEN_REMARK", length=160)
	private String actionTakenRemark;
	
	@Column(name="CANCEL_REASON", length=160)
	private String cancelReason;
	
	@Converter(name="PrepackRecordVer.prepackStatus", converterClass=PrepackStatus.Converter.class)
	@Convert("PrepackRecordVer.prepackStatus")
	@Column(name="PREPACK_STATUS", length=1, nullable=false)
	private PrepackStatus prepackStatus;
	
	@Converter(name="PrepackRecordVer.recordStatus", converterClass=RecordStatus.Converter.class)
	@Convert("PrepackRecordVer.recordStatus")
	@Column(name="RECORD_STATUS", length=1, nullable=false)
	private RecordStatus recordStatus;
	
	@Converter(name="PrepackRecordVer.actionStatus", converterClass=ActionStatus.Converter.class)
	@Convert("PrepackRecordVer.actionStatus")
	@Column(name="ACTION_STATUS", length=1, nullable=false)
	private ActionStatus actionStatus;
	
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setPrepackRecord(PrepackRecord prepackRecord) {
		this.prepackRecord = prepackRecord;
	}

	public PrepackRecord getPrepackRecord() {
		return prepackRecord;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public Container getContainer() {
		return container;
	}

	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	public Equipment getEquipment() {
		return equipment;
	}

	public void setPrepackPackSize(PrepackPackSize prepackPackSize) {
		this.prepackPackSize = prepackPackSize;
	}

	public PrepackPackSize getPrepackPackSize() {
		return prepackPackSize;
	}

	public void setDrugItemInfoVer(DrugItemInfoVer drugItemInfoVer) {
		this.drugItemInfoVer = drugItemInfoVer;
	}

	public DrugItemInfoVer getDrugItemInfoVer() {
		return drugItemInfoVer;
	}

	public void setPrepackDiscrepancyList(List<PrepackDiscrepancy> prepackDiscrepancyList) {
		this.prepackDiscrepancyList = prepackDiscrepancyList;
	}

	public List<PrepackDiscrepancy> getPrepackDiscrepancyList() {
		return prepackDiscrepancyList;
	}

	public void setPrepackActionTakenList(List<PrepackActionTaken> prepackActionTakenList) {
		this.prepackActionTakenList = prepackActionTakenList;
	}

	public List<PrepackActionTaken> getPrepackActionTakenList() {
		return prepackActionTakenList;
	}

	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}

	public String getManufCode() {
		return manufCode;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate == null ? null : new Date(expiryDate.getTime());
	}

	public Date getExpiryDate() {
		return expiryDate == null ? null : new Date(expiryDate.getTime());
	}

	public void setDispenseDosageUnit(String dispenseDosageUnit) {
		this.dispenseDosageUnit = dispenseDosageUnit;
	}

	public String getDispenseDosageUnit() {
		return dispenseDosageUnit;
	}

	public void setOriginalPackQty(Integer originalPackQty) {
		this.originalPackQty = originalPackQty;
	}

	public Integer getOriginalPackQty() {
		return originalPackQty;
	}

	public void setOriginalPackSize(Integer originalPackSize) {
		this.originalPackSize = originalPackSize;
	}

	public Integer getOriginalPackSize() {
		return originalPackSize;
	}

	public void setOriginalTotalQty(Integer originalTotalQty) {
		this.originalTotalQty = originalTotalQty;
	}

	public Integer getOriginalTotalQty() {
		return originalTotalQty;
	}

	public void setPreparationDate(Date preparationDate) {
		this.preparationDate = preparationDate == null ? null : new Date(preparationDate.getTime());
	}

	public Date getPreparationDate() {
		return preparationDate == null ? null : new Date(preparationDate.getTime());
	}

	public void setAssignExpiryMonth(ExpiryMonth assignExpiryMonth) {
		this.assignExpiryMonth = assignExpiryMonth;
	}

	public ExpiryMonth getAssignExpiryMonth() {
		return assignExpiryMonth;
	}

	public void setAssignExpiryWeek(ExpiryWeek assignExpiryWeek) {
		this.assignExpiryWeek = assignExpiryWeek;
	}

	public ExpiryWeek getAssignExpiryWeek() {
		return assignExpiryWeek;
	}

	public void setAssignExpiryDate(Date assignExpiryDate) {
		this.assignExpiryDate = assignExpiryDate == null ? null : new Date(assignExpiryDate.getTime());
	}

	public Date getAssignExpiryDate() {
		return assignExpiryDate == null ? null : new Date(assignExpiryDate.getTime());
	}

	public void setSampleCheckQty(Integer sampleCheckQty) {
		this.sampleCheckQty = sampleCheckQty;
	}

	public Integer getSampleCheckQty() {
		return sampleCheckQty;
	}

	public void setProvisionPrepackQty(Integer provisionPrepackQty) {
		this.provisionPrepackQty = provisionPrepackQty;
	}

	public Integer getProvisionPrepackQty() {
		return provisionPrepackQty;
	}

	public void setProvisionPrepackLabelQty(Integer provisionPrepackLabelQty) {
		this.provisionPrepackLabelQty = provisionPrepackLabelQty;
	}

	public Integer getProvisionPrepackLabelQty() {
		return provisionPrepackLabelQty;
	}

	public void setProvisionRemainQty(BigDecimal provisionRemainQty) {
		this.provisionRemainQty = provisionRemainQty;
	}

	public BigDecimal getProvisionRemainQty() {
		return provisionRemainQty;
	}
	
	public void setLabelSize(LabelSize labelSize) {
		this.labelSize = labelSize;
	}

	public LabelSize getLabelSize() {
		return labelSize;
	}

	public void setActualPrepackDate(Date actualPrepackDate) {
		this.actualPrepackDate = actualPrepackDate == null ? null : new Date(actualPrepackDate.getTime());
	}

	public Date getActualPrepackDate() {
		return actualPrepackDate == null ? null : new Date(actualPrepackDate.getTime());
	}

	public void setActualPrepackQty(Integer actualPrepackQty) {
		this.actualPrepackQty = actualPrepackQty;
	}

	public Integer getActualPrepackQty() {
		return actualPrepackQty;
	}

	public void setActualLabelRemainQty(Integer actualLabelRemainQty) {
		this.actualLabelRemainQty = actualLabelRemainQty;
	}

	public Integer getActualLabelRemainQty() {
		return actualLabelRemainQty;
	}

	public void setActualAddLabelQty(Integer actualAddLabelQty) {
		this.actualAddLabelQty = actualAddLabelQty;
	}

	public Integer getActualAddLabelQty() {
		return actualAddLabelQty;
	}

	public void setActualItemRemainQty(BigDecimal actualItemRemainQty) {
		this.actualItemRemainQty = actualItemRemainQty;
	}

	public BigDecimal getActualItemRemainQty() {
		return actualItemRemainQty;
	}

	public void setPrepareBy(String prepareBy) {
		this.prepareBy = prepareBy;
	}

	public String getPrepareBy() {
		return prepareBy;
	}

	public void setPreCheckBy(String preCheckBy) {
		this.preCheckBy = preCheckBy;
	}

	public String getPreCheckBy() {
		return preCheckBy;
	}

	public void setLineClearCheckBy(String lineClearCheckBy) {
		this.lineClearCheckBy = lineClearCheckBy;
	}

	public String getLineClearCheckBy() {
		return lineClearCheckBy;
	}

	public void setPrepackBy(String prepackBy) {
		this.prepackBy = prepackBy;
	}

	public String getPrepackBy() {
		return prepackBy;
	}

	public void setFinalCheckBy(String finalCheckBy) {
		this.finalCheckBy = finalCheckBy;
	}

	public String getFinalCheckBy() {
		return finalCheckBy;
	}

	public void setDiscrepancyRemark(String discrepancyRemark) {
		this.discrepancyRemark = discrepancyRemark;
	}

	public String getDiscrepancyRemark() {
		return discrepancyRemark;
	}

	public void setActionTakenRemark(String actionTakenRemark) {
		this.actionTakenRemark = actionTakenRemark;
	}

	public String getActionTakenRemark() {
		return actionTakenRemark;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setPrepackStatus(PrepackStatus prepackStatus) {
		this.prepackStatus = prepackStatus;
	}

	public PrepackStatus getPrepackStatus() {
		return prepackStatus;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setActionStatus(ActionStatus actionStatus) {
		this.actionStatus = actionStatus;
	}

	public ActionStatus getActionStatus() {
		return actionStatus;
	}
	
}