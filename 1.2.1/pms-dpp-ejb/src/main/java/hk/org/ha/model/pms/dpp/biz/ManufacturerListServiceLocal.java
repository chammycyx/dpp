package hk.org.ha.model.pms.dpp.biz;

import javax.ejb.Local;

@Local
public interface ManufacturerListServiceLocal {

	void retrieveManufacturerList();
	
	void destroy();
}
