package hk.org.ha.model.pms.biz.security;

import hk.org.ha.fmk.pms.sys.SysProfile;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.cacher.DmDrugCacherInf;
import hk.org.ha.model.pms.dpp.cacher.DmInstDrugCacherInf;
import hk.org.ha.model.pms.dpp.cacher.ManufacturerCacherInf;

import java.io.IOException;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;

/**
 * Session Bean implementation class PostLogonServiceBean
 */
@Stateful
@Name("postLogonService")
@Scope(ScopeType.SESSION)
@RemoteDestination
@MeasureCalls
public class PostLogonServiceBean implements PostLogonServiceLocal {

	@In 
	private AccessControlLocal accessControl;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private DmInstDrugCacherInf dmInstDrugCacher;
	
	@In
	private ManufacturerCacherInf manufacturerCacher;
	
	@Out
	private SysProfile sysProfile;
	
	private boolean enablePreLoadCacher = false;
	
	private boolean isEnablePreLoadCacher() {
		return enablePreLoadCacher;
	}

	public void setEnablePreLoadCacher(boolean enablePreLoadCacher) {
		this.enablePreLoadCacher = enablePreLoadCacher;
	}

	public void postLogon() throws IOException {
		
		sysProfile = (SysProfile) Component.getInstance(SysProfile.class, true);
		
		accessControl.retrieveUserAccessControl();
		
		if (isEnablePreLoadCacher()) {
			dmDrugCacher.load();
			dmInstDrugCacher.load();
			manufacturerCacher.load();
		}
	}
	
	@Remove
	public void destroy() {
		if (sysProfile != null) {
			sysProfile = null;
		}
	}
}
