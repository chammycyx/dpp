package hk.org.ha.model.pms.dpp.biz;

import java.util.ArrayList;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.persistence.Container;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("containerListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ContainerListServiceBean implements ContainerListServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@Out(required=false)
	private List<Container> containerList;
	
	@SuppressWarnings("unchecked")
	public void retrieveContainerList() {
		logger.debug("retrieveContainerList");
		containerList = em.createNamedQuery("Container.findAll")
			.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public void updateContainerList(List<Container> newContainerList) {
		logger.debug("updateContainerList");
		
		List<Long> idList = new ArrayList<Long>();
		for(Container container : newContainerList) {
			if(container.getId() != null) {
				idList.add(container.getId());
			}
		}
		
		
		List<String> containerNameList;
		if(idList.size() > 0) {
			containerNameList = em.createNamedQuery("Container.findContainerNameNotInList")
											.setParameter("id", idList)
											.getResultList();
		} else {
			containerNameList = new ArrayList<String>();
		}
		
		for(Container container : newContainerList) {
			if(!containerNameList.contains(container.getContainerName().toUpperCase())) {
				if(container.getId() == null) {
					em.persist(container);
				} else {
					em.merge(container);
				}
			} else {
				throw new OptimisticLockException();
			}
		}
		
		em.flush();
		retrieveContainerList();
	}
	
	@Remove
	public void destroy() {
		if(containerList != null) {
			containerList = null;
		}
	}

}
