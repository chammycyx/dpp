package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.persistence.Discrepancy;
import hk.org.ha.model.pms.dpp.persistence.PrepackDiscrepancy;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;

import java.io.Serializable;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

import java.util.Comparator;
import java.util.Collections;

@Stateful
@Scope(ScopeType.SESSION)
@Name("discrepancyListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DiscrepancyListServiceBean implements DiscrepancyListServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@Out(required=false)
	private List<Discrepancy> discrepancyList;

	private Comparator<Object> comp = new DiscrepancyComparator();
	
	@SuppressWarnings("unchecked")
	public void retrieveDiscrepancyList() {
		logger.debug("retrieveDiscrepancyList");
		discrepancyList = em.createNamedQuery("Discrepancy.findAllActive")
			.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public void updateDiscrepancyList(List<Discrepancy> newDiscrepancyList) {
		logger.debug("updateDiscrepancyList");
		
		List<Long> idList = new ArrayList<Long>();
		for(Discrepancy discrepancy : newDiscrepancyList) {
			if(discrepancy.getId() != null) {
				idList.add(discrepancy.getId());
			}
		}
		
		List<String> discrepancyNameList;
		if(idList.size() > 0) {
			discrepancyNameList = em.createNamedQuery("Discrepancy.findDiscrepancyNameNotInList")
											.setParameter("id", idList)
											.getResultList();
		} else {
			discrepancyNameList = new ArrayList<String>();
		}
		
		for(Discrepancy discrepancy : newDiscrepancyList) {
			if(!discrepancyNameList.contains(discrepancy.getDiscrepancyName().toUpperCase())) {
				if(discrepancy.getId() == null) {
					em.persist(discrepancy);
				} else {
					em.merge(discrepancy);
				}
			} else {
				throw new OptimisticLockException();
			}
		}
		
		em.flush();
		retrieveDiscrepancyList();
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveDiscrepancyDetailsList(Long prepackRecordVerId) {
		logger.debug("retrieveDiscrepancyDetailsList #0", prepackRecordVerId);
		
		discrepancyList = em.createNamedQuery("Discrepancy.findAllActive")
							.getResultList();
		
		List<PrepackDiscrepancy> prepackDiscrepancyList = em.createNamedQuery("PrepackDiscrepancy.findByPrepackRecordVerId")
															.setParameter("prepackRecordVerId", prepackRecordVerId)
															.setHint(QueryHints.BATCH, "o.prepackRecordVer")
															.getResultList();
		
		for(Discrepancy discrepancy : discrepancyList) {
			discrepancy.setEnable(false);
			for(PrepackDiscrepancy prepackDiscrepancy : prepackDiscrepancyList) {
				if (prepackDiscrepancy.getDiscrepancy().getId().equals(discrepancy.getId())) {
					discrepancy.setEnable(true);
					break;
				}
			}
		}
		
		// Fetch Deleted Prepack Discrepancy Record
		List<PrepackDiscrepancy> deletedPrepackDiscrepancyList = em.createNamedQuery("PrepackDiscrepancy.findDeletedDiscrepancyByPrepackRecordVerId")
			.setParameter("prepackRecordVerId", prepackRecordVerId)
			.setHint(QueryHints.BATCH, "o.prepackRecordVer")
			.getResultList();
		
		for(PrepackDiscrepancy prepackDiscrepancy : deletedPrepackDiscrepancyList) {
			Discrepancy deletedDiscrepancy = prepackDiscrepancy.getDiscrepancy();
			deletedDiscrepancy.setEnable(true);
			discrepancyList.add(deletedDiscrepancy);
		}
		
		// Sort for current and deleted Prepack Discrepancy Record 
	    Collections.sort(discrepancyList, comp);
	}
	
	@SuppressWarnings("unchecked")
	public void updateDiscrepancyDetailsList(Long prepackRecordId, List<Discrepancy> updateDiscrepancyList) {
		logger.debug("updateDiscrepancyDetailsList");
		
		// Get latest prepackRecordVerId
		List<PrepackRecord> prepackRecordList = em.createNamedQuery("PrepackRecord.findById")
												  .setParameter("id", prepackRecordId)
												  .getResultList();
		
		Long prepackRecordVerId = prepackRecordList.get(0).getPrepackRecordVer().getId();
		
		List<PrepackDiscrepancy> prepackDiscrepancyList = em.createNamedQuery("PrepackDiscrepancy.findByPrepackRecordVerId")
															.setParameter("prepackRecordVerId", prepackRecordVerId)
															.setHint(QueryHints.BATCH, "o.prepackRecordVer")
															.getResultList();
		
		for(Discrepancy updateDiscrepancy : updateDiscrepancyList) {
			Boolean recordExist = false;
			for (PrepackDiscrepancy prepackDiscrepancy : prepackDiscrepancyList) {
				if (prepackDiscrepancy.getDiscrepancy().getId().equals(updateDiscrepancy.getId())) {
					if ( !updateDiscrepancy.isEnable() ) {
						em.remove(prepackDiscrepancyList.get(0));
					}
					recordExist = true;
					break;
				}
			}
			if (!recordExist) {
				if ( updateDiscrepancy.isEnable() ) {
					PrepackDiscrepancy newPrepackDiscrepancy = new PrepackDiscrepancy();
					newPrepackDiscrepancy.setPrepackRecordVer(prepackRecordList.get(0).getPrepackRecordVer());
					newPrepackDiscrepancy.setDiscrepancy(updateDiscrepancy);
					
					em.persist(newPrepackDiscrepancy);
				}
			}
		}
		
		retrieveDiscrepancyDetailsList(prepackRecordVerId);
	}
	
	// Comparator for sorting current and deleted Prepack Discrepancy Record
	public static class DiscrepancyComparator implements Comparator<Object>, Serializable {
		
		private static final long serialVersionUID = -4688567167586398901L;
		
	    public int compare(Object obj1, Object obj2){     
	        String discrepancyName1 = ((Discrepancy)obj1).getDiscrepancyName();        
	        String discrepancyName2 = ((Discrepancy)obj2).getDiscrepancyName();
	       
	        return discrepancyName1.compareTo(discrepancyName2);
	    }
	}
	
	@Remove
	public void destroy() {
		if(discrepancyList != null) {
			discrepancyList = null;
		}
	}

}
