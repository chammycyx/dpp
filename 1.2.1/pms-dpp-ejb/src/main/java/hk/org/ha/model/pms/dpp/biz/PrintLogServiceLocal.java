package hk.org.ha.model.pms.dpp.biz;

import javax.ejb.Local;

@Local
public interface PrintLogServiceLocal {
	
	void createPrintLogForSampleLabel(Long id, Integer qty);
	
	void createPrintLogForLabel(Long id, Integer qty);
	
	void retrievePrintLogByPrepackRecordId(Long id);
	
	void destroy();
	
}
