package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.persistence.Container;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecordVer;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("containerService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ContainerServiceBean implements ContainerServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;

	@Out(required=false)
	private Container container;

	public void retrieveContainerByPrepackRecordVerId(Long id) {
		logger.debug("retrieveContainerByPrepackRecordVerId");
		
		PrepackRecordVer pVer = (PrepackRecordVer) em.createNamedQuery("PrepackRecordVer.findById")
								.setParameter("id", id)
								.getResultList().get(0);
		
		container = pVer.getContainer();
	}
	
	@Remove
	public void destroy() {
		if(container != null) {
			container = null;
		}
	}

}
