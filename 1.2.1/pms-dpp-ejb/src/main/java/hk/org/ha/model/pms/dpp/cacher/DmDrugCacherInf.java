package hk.org.ha.model.pms.dpp.cacher;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;

import java.util.List;

public interface DmDrugCacherInf extends BaseCacherInf {
  
	DmDrug getDrugByItemCode(String itemCode);
		
	List<DmDrug> getDrugListByItemCode(String prefixItemCode);
	
	List<DmDrug> getDrugList(String prefixItemCode);
}
