package hk.org.ha.model.pms.dpp.vo;

import java.io.Serializable;
import java.util.Date;

public class Manufacturer implements Serializable {
	
	private static final long serialVersionUID = -4688567166586398900L;

	private String companyCode;
    
    private String companyName;
    
    private String suspendFlag;
        
    private Date uploadDate;
        
    private String officePhone;   
    
    private String fax;
    
    private String email;
    
    private String address1;
    
    private String address2;
    
    private String address3;

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setSuspendFlag(String suspendFlag) {
		this.suspendFlag = suspendFlag;
	}

	public String getSuspendFlag() {
		return suspendFlag;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate == null ? null : new Date(uploadDate.getTime());
	}

	public Date getUploadDate() {
		return uploadDate == null ? null : new Date(uploadDate.getTime());
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getFax() {
		return fax;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getAddress3() {
		return address3;
	}

}
