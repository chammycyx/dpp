package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;

import javax.ejb.Local;

@Local
public interface DprWorksheetServiceLocal {
	
	String checkPrepackItemVersion(PrepackRecord prepackRecord);
	
	void generateDprWorksheet();
	
	void prepareDprWorksheet(Long id);
	
	void destroy();

}
