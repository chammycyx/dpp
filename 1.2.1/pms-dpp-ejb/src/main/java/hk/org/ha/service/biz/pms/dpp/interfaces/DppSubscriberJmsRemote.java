package hk.org.ha.service.biz.pms.dpp.interfaces;

import org.jboss.seam.annotations.async.Asynchronous;
import org.osoa.sca.annotations.OneWay;

public interface DppSubscriberJmsRemote {
	
	@OneWay
	@Asynchronous
	void initCache();
	
	@OneWay
	@Asynchronous
	void initJmsServiceProxy();
	
}
