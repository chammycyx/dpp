package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;

import javax.ejb.Local;

@Local
public interface PrepackRecordServiceLocal {

	void addPrepackRecord();
	
	void createPrepackRecord(PrepackRecord newPrepackRecord);
	
	void retrievePrepackRecordById(Long prepackRecordId);
	
	void updatePrepackRecord(PrepackRecord newPrepackRecord);
	
	void cancelPrepackRecord(PrepackRecord prepackRecordCancel);

	void retrievePrepackRecordByNotNo(String lotNo);
	
	void retrievePrepackRecordVersion(Long id);
	
	void destroy();
}
