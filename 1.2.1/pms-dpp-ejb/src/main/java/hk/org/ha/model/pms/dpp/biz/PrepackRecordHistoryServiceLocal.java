package hk.org.ha.model.pms.dpp.biz;

import javax.ejb.Local;

@Local
public interface PrepackRecordHistoryServiceLocal {

	void retrievePrepackRecordHistory(Long prepackRecordId);
		
	void destroy();
}
