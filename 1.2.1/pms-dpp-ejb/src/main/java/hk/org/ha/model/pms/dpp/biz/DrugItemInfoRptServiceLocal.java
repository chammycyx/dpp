package hk.org.ha.model.pms.dpp.biz;
import hk.org.ha.model.pms.dpp.vo.DrugItemInfoData;

import java.util.List;

import javax.ejb.Local;

@Local
public interface DrugItemInfoRptServiceLocal {
	List<DrugItemInfoData> getDrugItemInfoDataList();
	void retrieveDrugItemInfoRpt();
	void destroy();
}
