package hk.org.ha.model.pms.dpp.biz;

import java.util.List;

import javax.ejb.Local;

import hk.org.ha.model.pms.dpp.vo.DrugPrepackDailyRptCriteria;
import hk.org.ha.model.pms.dpp.vo.DrugPrepackDailyRptData;

@Local
public interface DrugPrepackDailyRptServiceLocal {
	
	void retrieveDrugPrepackDailyRptList(DrugPrepackDailyRptCriteria drugPrepackDailyRptCriteria);
	
	List<DrugPrepackDailyRptData> getDrugPrepackDailyRptList();
	
	void destroy();

}
