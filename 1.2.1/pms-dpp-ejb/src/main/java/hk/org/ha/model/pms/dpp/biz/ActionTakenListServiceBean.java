package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.persistence.ActionTaken;
import hk.org.ha.model.pms.dpp.persistence.PrepackActionTaken;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;

import java.io.Serializable;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("actionTakenListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ActionTakenListServiceBean implements ActionTakenListServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@Out(required=false)
	private List<ActionTaken> actionTakenList;
	
	private Comparator<Object> comp = new ActionTakenComparator();
	
	@SuppressWarnings("unchecked")
	public void retrieveActionTakenList() {
		logger.debug("retrieveActionTakenList");
		actionTakenList = em.createNamedQuery("ActionTaken.findAllActive")
			.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public void updateActionTakenList(List<ActionTaken> newActionTakenList) {
		logger.debug("updateActionTakenList");
		
		List<Long> idList = new ArrayList<Long>();
		for(ActionTaken actionTaken : newActionTakenList) {
			if(actionTaken.getId() != null) {
				idList.add(actionTaken.getId());
			}
		}
		
		List<String> actionTakenNameList;
		if(idList.size() > 0) {
			actionTakenNameList = em.createNamedQuery("ActionTaken.findActionTakenNameNotInList")
											.setParameter("id", idList)
											.getResultList();
		} else {
			actionTakenNameList = new ArrayList<String>();
		}
		
		for(ActionTaken actionTaken : newActionTakenList) {
			if(!actionTakenNameList.contains(actionTaken.getActionTakenName().toUpperCase())) {
				if(actionTaken.getId() == null) {
					em.persist(actionTaken);
				} else {
					em.merge(actionTaken);
				}
			} else {
				throw new OptimisticLockException();
			}
		}
		
		em.flush();
		retrieveActionTakenList();
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveActionTakenDetailsList(Long prepackRecordVerId) {
		logger.debug("retrieveActionTakenDetailsList #0", prepackRecordVerId);
		
		actionTakenList = em.createNamedQuery("ActionTaken.findAllActive")
		.getResultList();

		List<PrepackActionTaken> prepackActionTakenList = em.createNamedQuery("PrepackActionTaken.findByPrepackRecordVerId")
										.setParameter("prepackRecordVerId", prepackRecordVerId)
										.setHint(QueryHints.BATCH, "o.prepackRecordVer")
										.getResultList();

		for(ActionTaken actionTaken : actionTakenList) {
			actionTaken.setEnable(false);
			for(PrepackActionTaken prepackActionTaken : prepackActionTakenList) {
				if (prepackActionTaken.getActionTaken().getId().equals(actionTaken.getId())) {
					actionTaken.setEnable(true);
					break;
				}
			}
		}

		// Fetch Deleted Prepack ActionTaken Record
		List<PrepackActionTaken> deletedPrepackActionTakenList = em.createNamedQuery("PrepackActionTaken.findDeletedActionTakenByPrepackRecordVerId")
			.setParameter("prepackRecordVerId", prepackRecordVerId)
			.setHint(QueryHints.BATCH, "o.prepackRecordVer")
			.getResultList();
		
		for(PrepackActionTaken prepackActionTaken : deletedPrepackActionTakenList) {
			ActionTaken deletedActionTaken = prepackActionTaken.getActionTaken();
			deletedActionTaken.setEnable(true);
			actionTakenList.add(deletedActionTaken);
		}
		
		// Sort for current and deleted Prepack Action Taken Record 
	    Collections.sort(actionTakenList, comp);
	}
	
	@SuppressWarnings("unchecked")
	public void updateActionTakenDetailsList(Long prepackRecordId, List<ActionTaken> updateActionTakenList) {
		logger.debug("updateActionTakenDetailsList #0", prepackRecordId);
		
		// Get latest prepackRecordVerId
		List<PrepackRecord> prepackRecordList = em.createNamedQuery("PrepackRecord.findById")
												  .setParameter("id", prepackRecordId)
												  .getResultList();
		
		Long prepackRecordVerId = prepackRecordList.get(0).getPrepackRecordVer().getId();
		
		List<PrepackActionTaken> prepackActionTakenList = em.createNamedQuery("PrepackActionTaken.findByPrepackRecordVerId")
															.setParameter("prepackRecordVerId", prepackRecordVerId)
															.setHint(QueryHints.BATCH, "o.prepackRecordVer")
															.getResultList();
		
		for(ActionTaken updateActionTaken : updateActionTakenList) {
			Boolean recordExist = false;
			for (PrepackActionTaken prepackActionTaken : prepackActionTakenList) {
				if (prepackActionTaken.getActionTaken().getId().equals(updateActionTaken.getId())) {
					if ( !updateActionTaken.isEnable() ) {
						em.remove(prepackActionTakenList.get(0));
					}
					recordExist = true;
					break;
				}
			}
			if (!recordExist) {
				if ( updateActionTaken.isEnable() ) {
					PrepackActionTaken newPrepackActionTaken = new PrepackActionTaken();
					newPrepackActionTaken.setPrepackRecordVer(prepackRecordList.get(0).getPrepackRecordVer());
					newPrepackActionTaken.setActionTaken(updateActionTaken);
					
					em.persist(newPrepackActionTaken);
				}
			}
		}
		
		retrieveActionTakenDetailsList(prepackRecordVerId);
	}
	
	// Comparator for sorting current and deleted Prepack Action Taken Record
	public static class ActionTakenComparator implements Comparator<Object>, Serializable {
		
		private static final long serialVersionUID = -4688567466586398902L;
		
	    public int compare(Object obj1, Object obj2){     
	        String actionTakenName1 = ((ActionTaken)obj1).getActionTakenName();        
	        String actionTakenName2 = ((ActionTaken)obj2).getActionTakenName();
	       
	        return actionTakenName1.compareTo(actionTakenName2);
	    }
	}
	
	@Remove
	public void destroy() {
		if(actionTakenList != null) {
			actionTakenList = null;
		}
	}

}
