package hk.org.ha.model.pms.biz.printing;

import hk.org.ha.model.pms.dpp.exception.PrintingException;

//import hk.org.ha.model.pms.vo.printing.PrinterSelect;

public interface SilentPrintSenderInf {

	void setPrinterCacheTimeout(long printerCacheTimeout);
	void setPrinterCachePurgePollInternal(long printerCachePurgePollInternal);
	void setPrinterHostName(String printerHostName);
	String getPrinterUrl();
	void sendToPrinter(byte[] ba, int printQty, String printDocType) throws PrintingException;
	
}
