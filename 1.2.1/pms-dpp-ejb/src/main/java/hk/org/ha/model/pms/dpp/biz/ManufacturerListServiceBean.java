package hk.org.ha.model.pms.dpp.biz;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.cacher.ManufacturerCacherInf;
import hk.org.ha.model.pms.dpp.vo.Manufacturer;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("manufacturerListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ManufacturerListServiceBean implements ManufacturerListServiceLocal {
	
	@Logger
	private Log logger;
	
	@In
	private ManufacturerCacherInf manufacturerCacher;
	
	@Out(required=false)
	private List<Manufacturer> manufacturerList;
	
	public void retrieveManufacturerList() {
		logger.info("retrieveManufacturerList");
		
		manufacturerList = manufacturerCacher.getFullManufList();
		
		logger.info("manufacturerList size #0", manufacturerList.size());
	}
	
	@Remove
	public void destroy() {
		if (manufacturerList != null) {
			manufacturerList = null;
		}
	}

}
