package hk.org.ha.model.pms.biz.security;

import hk.org.ha.fmk.pms.web.MeasureCalls;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.Name;

@Stateless
@Name("authenticator")
@MeasureCalls
public class AuthenticatorBean implements AuthenticatorLocal {

	public boolean authenticate() {
		return true;
	}
}
