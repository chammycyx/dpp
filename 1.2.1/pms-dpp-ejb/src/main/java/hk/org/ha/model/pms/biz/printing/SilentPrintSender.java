package hk.org.ha.model.pms.biz.printing;

import hk.org.ha.fmk.pms.jee.InitialContext;
import hk.org.ha.fmk.pms.jms.MessageCreator;
import hk.org.ha.fmk.pms.jms.MessageProducer;
import hk.org.ha.fmk.pms.jms.MessageProducerInf;
import hk.org.ha.fmk.pms.util.DefaultTimeoutMap;
import hk.org.ha.fmk.pms.util.TimeoutMap;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.exception.PrintingException;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.naming.NamingException;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;

import commonj.timers.TimerManager;

@AutoCreate
@Name("silentPrintSender")
@Scope(ScopeType.SESSION)
@MeasureCalls
public class SilentPrintSender implements SilentPrintSenderInf, Serializable {
		
	private static final long serialVersionUID = 973242801927147462L;

	private static final String DEF_PRINTER_HOSTNAME = "localhost";
		
	private static final String SILENT_PRINT_MESSAGE_PRODUCER = "silentPrintMessageProducer";
	private static final String SILENT_PRINT_INITIAL_CONTEXT = "silentPrintInitialContext";

	@Logger
	private Log logger;
		
	private String printerHostName = DEF_PRINTER_HOSTNAME;
	
	private boolean isPrinterAvaliable = true;
		
	private long printerCacheTimeout = 300000L;
	private long printerCachePurgePollInternal = 60000L;
	private int printerPort = 61616;
	private static final String EXCEPTION_CLASS_NAME = "SilentPrintSender";

	@In
	private TimerManager timerManager;
	
	private static TimeoutMap<String, MessageProducerHolder> messageProducerTimeoutMap = null;
	
	private long getPrinterCacheTimeout() {
		return printerCacheTimeout;
	}

	public void setPrinterCacheTimeout(long printerCacheTimeout) {
		this.printerCacheTimeout = printerCacheTimeout;
	}

	private long getPrinterCachePurgePollInternal() {
		return printerCachePurgePollInternal;
	}

	public void setPrinterCachePurgePollInternal(long printerCachePurgePollInternal) {
		this.printerCachePurgePollInternal = printerCachePurgePollInternal;
	}
	
	public int getPrinterPort() {
		return printerPort;
	}

	public void setPrinterPort(int printerPort) {
		this.printerPort = printerPort;
	}
	
	public void setPrinterHostName(String printerHostName) {
		this.printerHostName = printerHostName;
	}

	@Create
	public void init() {
		synchronized (SilentPrintSender.class) {
			if (messageProducerTimeoutMap == null) {
				messageProducerTimeoutMap = 
					new DefaultTimeoutMap<String, MessageProducerHolder>(timerManager, this.getPrinterCachePurgePollInternal()) {
						public void onEviction(String key,
								MessageProducerHolder holder) {							
							try {
								holder.getMessageProducer().destroy();
							} catch (JMSException e) {
								throw new UnsupportedOperationException("unable to init printer, please check the connection.", e);
							}
						}					
					};
			}
		}
	}	

	public String getPrinterUrl() {
		logger.debug("getPrinterUrl #0", printerHostName);
		return "tcp://" + printerHostName + ":" + printerPort;
	}
		
	public void sendToPrinter(byte[] ba, int printQty, String printDocType) throws PrintingException {
		sendToPrinter(ba, this.printerHostName, printQty, false, printDocType);
	}
		
	private void sendToPrinter(final byte[] ba, final String hostName, final int copies, final boolean reverse, final String printDocType) throws PrintingException {

		logger.info("sendToPrinter size:" + ba.length + " host:" + hostName + " copies:" + copies + " reverse:" + reverse + " printDocType:" + printDocType);

		MessageProducerInf silentPrintMessageProducer = createSilentPrintMessageProducer(hostName);
		if (silentPrintMessageProducer == null) {
			return;
		}
		
		final String errorString = "unable to print";		
		try {
			
			silentPrintMessageProducer.send(new MessageCreator() {
				public Message createMessage(Session session) throws JMSException {			
					BytesMessage msg = session.createBytesMessage();
					msg.setIntProperty("PRINT_COPIES", copies);
					msg.setBooleanProperty("PRINT_REVERSE", reverse);
					msg.setStringProperty("PRINT_DOC_TYPE", printDocType);
					msg.writeBytes(ba);
					return msg;
				}
			});
						
		} catch (JMSException e) {
			logger.error(errorString, e);
//			throw new IllegalStateException(errorString);
			try {
				InetAddress ia = InetAddress.getByName(hostName);
				throw new PrintingException(hostName+"/"+ia.getHostAddress()+"/"+ia.getCanonicalHostName());
			} catch (UnknownHostException e1) {
				throw new IllegalStateException("printer " + hostName + " not available!");
			}	
		} catch (NamingException e) {
			logger.error(errorString, e);
			try {
				InetAddress ia = InetAddress.getByName(hostName);
				throw new PrintingException(hostName+"/"+ia.getHostAddress()+"/"+ia.getCanonicalHostName());
			} catch (UnknownHostException e1) {
				throw new IllegalStateException("printer " + hostName + " not available!");
			}	
//			throw new IllegalStateException(errorString);
		}
	}

	private MessageProducerInf createSilentPrintMessageProducer(String hostName) throws PrintingException {
	
		MessageProducerHolder holder = messageProducerTimeoutMap.get(hostName);
	
		if (holder != null) {
			isPrinterAvaliable = true;
			printerHostName = hostName;
			Contexts.getEventContext().set(SILENT_PRINT_MESSAGE_PRODUCER, holder.getMessageProducer());
			Contexts.getEventContext().set(SILENT_PRINT_INITIAL_CONTEXT, holder.getInitialContext());
		} else {
			isPrinterAvaliable = checkPrinterAvaliable(hostName);
			if (!isPrinterAvaliable) {
				logger.warn("printer " + hostName + " not available!");
				InetAddress ia;
				try {
					ia = InetAddress.getByName(hostName);
					throw new PrintingException(hostName+"/"+ia.getHostAddress()+"/"+ia.getCanonicalHostName());
				} catch (UnknownHostException e) {
					throw new IllegalStateException("printer " + hostName + " not available!");
				}		
			} else {
				printerHostName = hostName;						
			}
		}
	
		MessageProducerInf silentPrintMessageProducer = 
			(MessageProducerInf) Component.getInstance(SILENT_PRINT_MESSAGE_PRODUCER);
	
		if (holder == null) {
			if (messageProducerTimeoutMap.get(printerHostName) == null) {
				messageProducerTimeoutMap.put(printerHostName, new MessageProducerHolder(
						(MessageProducer) Contexts.getEventContext().get(SILENT_PRINT_MESSAGE_PRODUCER), 
						(InitialContext) Contexts.getEventContext().get(SILENT_PRINT_INITIAL_CONTEXT)),
						this.getPrinterCacheTimeout());
			}
		}

		Contexts.getEventContext().remove(SILENT_PRINT_MESSAGE_PRODUCER);
		Contexts.getEventContext().remove(SILENT_PRINT_INITIAL_CONTEXT);	
		
		return silentPrintMessageProducer;
	}	
	
	private boolean checkPrinterAvaliable(String hostName) throws PrintingException {
		try {
			InetAddress ia = InetAddress.getByName(hostName);
			try {
				Socket socket = new Socket();
				socket.connect(new InetSocketAddress(ia.getHostAddress(), this.getPrinterPort()), 2000);
				socket.close();			
				return true;
			} catch (IOException e) {
				logger.error("printer station #0/#1/#2 not reachable!", hostName, ia.getHostAddress(), ia.getCanonicalHostName());
				throw new PrintingException(hostName+"/"+ia.getHostAddress()+"/"+ia.getCanonicalHostName());
			}
		} catch (UnknownHostException e) {
			logger.error("printer station #0 not found", hostName);
			throw new PrintingException("Printer station not found");
		}
	}
	
	public static class MessageProducerHolder {
		
		private MessageProducer messageProducer;
		private InitialContext initialContext;

		public MessageProducerHolder(MessageProducer messageProducer,
				InitialContext initialContext) {
			super();
			this.messageProducer = messageProducer;
			this.initialContext = initialContext;
		}
		
		public MessageProducer getMessageProducer() {
			return messageProducer;
		}
		
		public InitialContext getInitialContext() {
			return initialContext;
		}		
	}
}
