package hk.org.ha.model.pms.dpp.biz;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.persistence.DrugItemInfo;
import hk.org.ha.model.pms.dpp.persistence.PrepackPackSize;
import hk.org.ha.model.pms.dpp.udt.RecordStatus;
import hk.org.ha.model.pms.dpp.vo.DrugItemInfoData;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("drugItemInfoRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DrugItemInfoRptServiceBean implements DrugItemInfoRptServiceLocal {
	
	@Logger
	private Log logger;
	
	@In
	private UamInfo uamInfo;
	
	@PersistenceContext
	private EntityManager em;
	
	private DrugItemInfoDataComparator dataComparator = new DrugItemInfoDataComparator();
	
	private List<DrugItemInfoData> drugItemInfoDataList;
	
	public List<DrugItemInfoData> getDrugItemInfoDataList() {
		return 	drugItemInfoDataList;
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveDrugItemInfoRpt() {
		List<DrugItemInfo> drugItemInfoList = em.createQuery("select o from DrugItemInfo o " +
				"where o.instCode = :instCode " +
				"and o.workstoreCode = :workstoreCode")
				.setParameter("instCode", uamInfo.getHospital())
				.setParameter("workstoreCode", uamInfo.getWorkstore())
				.setHint(QueryHints.FETCH, "o.drugItemInfoVer")
				.setHint(QueryHints.BATCH, "o.prepackPackSizeList")
				.getResultList();
		
		drugItemInfoDataList = new ArrayList<DrugItemInfoData>();
		
		DrugItemInfoData data;
		
		for (DrugItemInfo drugItemInfo:drugItemInfoList) {
			drugItemInfo.getPrepackPackSizeList().size();
			
			for (PrepackPackSize prepackPackSize:drugItemInfo.getPrepackPackSizeList()) {
				
				if (prepackPackSize.getRecordStatus() != RecordStatus.Active ) {
					continue;
				}
				
				data = new DrugItemInfoData();
				drugItemInfoDataList.add(data);
				
				data.setItemCode(drugItemInfo.getItemCode());
				data.setItemDesc(drugItemInfo.getDmDrug().getDrugName());
				data.setItemLongDesc(drugItemInfo.getDrugItemInfoVer().getItemLongDesc());
				data.setItemShortDesc(drugItemInfo.getDrugItemInfoVer().getItemShortDesc());
				
				data.setRemark(drugItemInfo.getDrugItemInfoVer().getRemark());
				data.setAlias(drugItemInfo.getDrugItemInfoVer().getAlias());
				
				data.setHqSuspend(drugItemInfo.getDmDrug().getHqSuspend());
				data.setInstSuspend(drugItemInfo.getInstSuspend());
				
				data.setAllowPrepack(drugItemInfo.getDrugItemInfoVer().getPrepackFlag().getDataValue());
				data.setDisplayAliasOnFullSizeLabel(drugItemInfo.getDrugItemInfoVer().getDisplayAliasFlag().getDataValue());
				data.setDisplayAliasOnSideLabel(drugItemInfo.getDrugItemInfoVer().getDisplaySideLabelAliasFlag().getDataValue());
				data.setDisplayShortDescOnSideLabel(drugItemInfo.getDrugItemInfoVer().getDisplaySideLabelShortDescFlag().getDataValue());
				
				data.setPackSize(prepackPackSize.getPackSize());
				data.setPackUnit(prepackPackSize.getPackUnit());
			}
		}
		
		Collections.sort(drugItemInfoDataList,dataComparator);
	}
	
	private static class DrugItemInfoDataComparator implements Comparator<DrugItemInfoData> {

		@Override
		public int compare(DrugItemInfoData d1, DrugItemInfoData d2) {
			return new CompareToBuilder()
			.append(d1.getItemCode(), d2.getItemCode())
			.append(d1.getPackUnit(), d2.getPackUnit())
			.append(d1.getPackSize(), d2.getPackSize())
			.toComparison();
		}

	}

	@Remove
	public void destroy() {	
	}
}
