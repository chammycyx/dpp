package hk.org.ha.model.pms.dpp.vo;

import java.util.Date;

public class AuditRptData {
	
	private Date actionDate;
	private String hospCode;
	private String itemCode;
	private String itemDesc;
	private String action;
	private String actionBy;
	private String remarks;
	private String lotNum;
	
	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate == null ? null : new Date(actionDate.getTime());
	}
	
	public Date getActionDate() {
		return actionDate == null ? null : new Date(actionDate.getTime());
	}
	
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	
	public String getItemCode() {
		return itemCode;
	}
	
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public String getItemDesc() {
		return itemDesc;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	public String getAction() {
		return action;
	}
	
	public void setActionBy(String actionBy) {
		this.actionBy = actionBy;
	}
	
	public String getActionBy() {
		return actionBy;
	}
	
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	public String getRemarks() {
		return remarks;
	}

	public void setLotNum(String lotNum) {
		this.lotNum = lotNum;
	}

	public String getLotNum() {
		return lotNum;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getHospCode() {
		return hospCode;
	}

}
