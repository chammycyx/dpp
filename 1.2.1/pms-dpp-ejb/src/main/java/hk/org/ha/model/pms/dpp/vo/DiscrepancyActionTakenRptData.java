package hk.org.ha.model.pms.dpp.vo;

import java.util.Date;
import java.util.List;

public class DiscrepancyActionTakenRptData {
	
	private Date preparationDate;
	private Date prepackDate;
	private String assignedLotNum;
	private String itemCode;
	private String itemDesc;
	private String manufName;
	private String batchNum;
	private List<String> discrepancyDataList;
	private List<String> actionTakenDataList;
	private String discrepancyRemark;
	private String actionTakenRemark;
	
	public void setPreparationDate(Date preparationDate) {
		this.preparationDate = preparationDate == null ? null : new Date(preparationDate.getTime());
	}
	
	public Date getPreparationDate() {
		return preparationDate == null ? null : new Date(preparationDate.getTime());
	}

	public void setPrepackDate(Date prepackDate) {
		this.prepackDate = prepackDate == null ? null : new Date(prepackDate.getTime());
	}

	public Date getPrepackDate() {
		return prepackDate == null ? null : new Date(prepackDate.getTime());
	}

	public void setAssignedLotNum(String assignedLotNum) {
		this.assignedLotNum = assignedLotNum;
	}

	public String getAssignedLotNum() {
		return assignedLotNum;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public String getItemDesc() {
		return itemDesc;
	}

	public void setManufName(String manufName) {
		this.manufName = manufName;
	}

	public String getManufName() {
		return manufName;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setDiscrepancyDataList(List<String> discrepancyDataList) {
		this.discrepancyDataList = discrepancyDataList;
	}

	public List<String> getDiscrepancyDataList() {
		return discrepancyDataList;
	}

	public void setActionTakenDataList(List<String> actionTakenDataList) {
		this.actionTakenDataList = actionTakenDataList;
	}

	public List<String> getActionTakenDataList() {
		return actionTakenDataList;
	}

	public void setDiscrepancyRemark(String discrepancyRemark) {
		this.discrepancyRemark = discrepancyRemark;
	}

	public String getDiscrepancyRemark() {
		return discrepancyRemark;
	}

	public void setActionTakenRemark(String actionTakenRemark) {
		this.actionTakenRemark = actionTakenRemark;
	}

	public String getActionTakenRemark() {
		return actionTakenRemark;
	}

}
