package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecordVer;
import hk.org.ha.model.pms.dpp.persistence.PrintLog;
import hk.org.ha.model.pms.dpp.udt.ActionStatus;
import hk.org.ha.model.pms.dpp.udt.PrintType;
import hk.org.ha.model.pms.dpp.vo.PrepackHistory;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.granite.tide.annotations.TideEnabled;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@TideEnabled
@Name("prepackRecordHistoryService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PrepackRecordHistoryServiceBean implements PrepackRecordHistoryServiceLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@Out(required=false)
	private List<PrepackHistory> prepackHistoryList;

	@SuppressWarnings("unchecked")
	public void retrievePrepackRecordHistory(Long prepackRecordId) {
		logger.debug("retrievePrepackRecordHistory #0", prepackRecordId);
		
		Query q = em.createNamedQuery("PrepackRecordVer.findPrintHistoryByPrepackRecordId")
			.setParameter("prepackRecordid", prepackRecordId)
			.setHint(QueryHints.BATCH, "o.prepackRecord");
		
		List<PrepackRecordVer> prepackRecordVerList = (List<PrepackRecordVer>) q.getResultList();
		
		List<PrintLog> printLogList = (List<PrintLog>) em.createNamedQuery("PrintLog.findByPrepackRecordId")
										.setHint(QueryHints.FETCH, "o.prepackRecordVer")
										.setParameter("prepackRecordId", prepackRecordId)
										.setHint(QueryHints.BATCH, "o.prepackRecordVer")
										.setHint(QueryHints.BATCH, "o.prepackRecordVer.prepackRecord")
										.getResultList();
		
		prepackHistoryList = new ArrayList<PrepackHistory>();
		
		for (PrepackRecordVer prepackRecordVer : prepackRecordVerList) {
			Boolean hasPrintLog = false;
			Boolean hasSampleLabelLog = false;
			for (PrintLog printLog : printLogList) {
				if (prepackRecordVer.getId().equals(printLog.getPrepackRecordVer().getId())) {
					
					if ( printLog.getPrepackRecordVer().getActionStatus() == ActionStatus.Printed ||
							( printLog.getPrintType() == PrintType.SampleLabel && hasSampleLabelLog)
						) {
						PrepackHistory newPrepackHistory = new PrepackHistory();
						newPrepackHistory.setCreateDate(new java.sql.Date(printLog.getCreateDate().getTime()));
						newPrepackHistory.setUpdateUser(printLog.getUpdateUser());
						newPrepackHistory.setAction(buildPrintLabelString(printLog));
						
						prepackHistoryList.add(newPrepackHistory);
					} else {
						PrepackHistory newPrepackHistory = new PrepackHistory();
						newPrepackHistory.setCreateDate(new java.sql.Date(prepackRecordVer.getCreateDate().getTime()));
						newPrepackHistory.setUpdateUser(prepackRecordVer.getUpdateUser());
						newPrepackHistory.setAction(buildActionStatusString(prepackRecordVer.getActionStatus()));
						
						prepackHistoryList.add(newPrepackHistory);
						
						newPrepackHistory = new PrepackHistory();
						newPrepackHistory.setCreateDate(new java.sql.Date(printLog.getCreateDate().getTime()));
						newPrepackHistory.setUpdateUser(printLog.getUpdateUser());
						newPrepackHistory.setAction(buildPrintLabelString(printLog));
						
						prepackHistoryList.add(newPrepackHistory);
						
						if (printLog.getPrintType() == PrintType.SampleLabel) {
							hasSampleLabelLog = true;
						}
					}
					
					hasPrintLog = true;
				}
			}
			
			if (!hasPrintLog) {
				PrepackHistory newPrepackHistory = new PrepackHistory();
				newPrepackHistory.setCreateDate(new java.sql.Date(prepackRecordVer.getCreateDate().getTime()));
				newPrepackHistory.setUpdateUser(prepackRecordVer.getUpdateUser());
				newPrepackHistory.setAction(buildActionStatusString(prepackRecordVer.getActionStatus()));
				
				prepackHistoryList.add(newPrepackHistory);
			}	
		}
	}
	
	private String buildPrintLabelString(PrintLog printLog) {
		if(printLog.getPrintType() == PrintType.Label) {
			return "Print pre-packing label ( Print Qty: " + printLog.getPrintQty().toString() + " )";
		} else if(printLog.getPrintType() == PrintType.SampleLabel) {
			return "Print sample label";
		}
		return "";
	}
	
	private String buildActionStatusString(ActionStatus actionStatus) {
		if(actionStatus == ActionStatus.New) {
			return "New pre-packing record";
		} else if(actionStatus == ActionStatus.Updated) {
			return "Update pre-packing record";
		} else if(actionStatus == ActionStatus.Cancelled) {
			return "Cancel pre-packing record";
		} else if(actionStatus == ActionStatus.Completed) {
			return "Completed";
		}
		return "";
	}
	
	@Remove
	public void destroy() {
		if (prepackHistoryList != null) {
			prepackHistoryList = null;
		}
	}
}
