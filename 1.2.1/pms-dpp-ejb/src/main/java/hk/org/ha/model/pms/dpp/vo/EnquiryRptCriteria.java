package hk.org.ha.model.pms.dpp.vo;

import hk.org.ha.model.pms.dpp.udt.PrepackStatus;

import java.math.BigDecimal;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class EnquiryRptCriteria {
	
	private String itemCode;
	private BigDecimal packSize;
	private String packUnit;
	private String assignedLotNum;
	private String batchNum;
	private Date preparationStartDate;
	private Date preparationEndDate;
	private Date prepackStartDate;
	private Date prepackEndDate;
	private PrepackStatus prepackStatus;
	private String manufCode;
	private String containerName;
	private String equipmentName;
	
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	
	public String getItemCode() {
		return itemCode;
	}
	
	public void setPackSize(BigDecimal packSize) {
		this.packSize = packSize;
	}
	
	public BigDecimal getPackSize() {
		return packSize;
	}

	public void setPackUnit(String packUnit) {
		this.packUnit = packUnit;
	}

	public String getPackUnit() {
		return packUnit;
	}

	public void setAssignedLotNum(String assignedLotNum) {
		this.assignedLotNum = assignedLotNum;
	}

	public String getAssignedLotNum() {
		return assignedLotNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setPreparationStartDate(Date preparationStartDate) {
		this.preparationStartDate = preparationStartDate == null ? null : new Date(preparationStartDate.getTime());
	}

	public Date getPreparationStartDate() {
		return preparationStartDate == null ? null : new Date(preparationStartDate.getTime());
	}

	public void setPreparationEndDate(Date preparationEndDate) {
		this.preparationEndDate = preparationEndDate == null ? null : new Date(preparationEndDate.getTime());
	}

	public Date getPreparationEndDate() {
		return preparationEndDate == null ? null : new Date(preparationEndDate.getTime());
	}

	public void setPrepackStartDate(Date prepackStartDate) {
		this.prepackStartDate = prepackStartDate == null ? null : new Date(prepackStartDate.getTime());
	}

	public Date getPrepackStartDate() {
		return prepackStartDate == null ? null : new Date(prepackStartDate.getTime());
	}

	public void setPrepackEndDate(Date prepackEndDate) {
		this.prepackEndDate = prepackEndDate == null ? null : new Date(prepackEndDate.getTime());
	}

	public Date getPrepackEndDate() {
		return prepackEndDate == null ? null : new Date(prepackEndDate.getTime());
	}

	public void setPrepackStatus(PrepackStatus prepackStatus) {
		this.prepackStatus = prepackStatus;
	}

	public PrepackStatus getPrepackStatus() {
		return prepackStatus;
	}

	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}

	public String getManufCode() {
		return manufCode;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}

	public String getContainerName() {
		return containerName;
	}

	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}

	public String getEquipmentName() {
		return equipmentName;
	}
}
