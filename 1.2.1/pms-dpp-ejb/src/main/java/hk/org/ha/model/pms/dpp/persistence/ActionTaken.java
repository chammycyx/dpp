package hk.org.ha.model.pms.dpp.persistence;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dpp.udt.RecordStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name="ACTION_TAKEN")
@Customizer(AuditCustomizer.class)
@NamedQueries({
	@NamedQuery(name="ActionTaken.findAllActive", query="select o from ActionTaken o where o.recordStatus = hk.org.ha.model.pms.dpp.udt.RecordStatus.Active order by o.actionTakenName ASC"),
	@NamedQuery(name="ActionTaken.findAll", query="select o from ActionTaken o order by o.actionTakenName ASC"),
	@NamedQuery(name="ActionTaken.findActionTakenNameNotInList", query="select UPPER(o.actionTakenName) from ActionTaken o where o.recordStatus = hk.org.ha.model.pms.dpp.udt.RecordStatus.Active and o.id not in :id ")
})
public class ActionTaken extends VersionEntity {
	
	private static final long serialVersionUID = 7641207961509013221L;
	
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="actionTakenSeq")
	@SequenceGenerator(name="actionTakenSeq", sequenceName="SQ_ACTION_TAKEN", initialValue=100000000)
	private Long id;
	
	@Column(name="ACTION_TAKEN_NAME", length=160, nullable=false)
	private String actionTakenName;
	
	@Converter(name="ActionTaken.recordStatus", converterClass=RecordStatus.Converter.class)
	@Convert("ActionTaken.recordStatus")
	@Column(name="RECORD_STATUS", length=1, nullable=false)
	private RecordStatus recordStatus;

	@Transient
	private boolean enable;
	
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setActionTakenName(String actionTakenName) {
		this.actionTakenName = actionTakenName;
	}

	public String getActionTakenName() {
		return actionTakenName;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public boolean isEnable() {
		return enable;
	}
}
