package hk.org.ha.model.pms.biz.security;

import hk.org.ha.fmk.pms.security.Uam;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.util.ConfigHelper;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.vo.security.SysMenu;
import hk.org.ha.model.pms.vo.security.SysMenuDtl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.security.Identity;

@AutoCreate
@Stateless
@Name("accessControl")
@MeasureCalls
public class AccessControlBean implements AccessControlLocal {
	
	private static final JaxbWrapper<SysMenu> JAXB_WRAPPER = 
		new JaxbWrapper<SysMenu>("hk.org.ha.model.pms.vo.security");
	
	public static final String MENU = "menu.xml";
	public static final String MENU_DISPLAY_NAME = "displayName";
	public static final String MENU_CHILDREN = "children";
	public static final String MENU_URL = "url";
	public static final String MENU_ENABLED = "enabled";

	@In
	private Identity identity;
						
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private UamInfo uamInfo;

	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private List<Map<String, Object>> menuMapList;
				
	public void retrieveUserAccessControl() throws IOException
	{		
		Uam uam = (Uam) Component.getInstance(Uam.class, true);
		if (uam.getUamInfo() == null) {
			// Debug mode
			uamInfo = new UamInfo();
			uamInfo.setUserId("itdadmin");
			uamInfo.setWorkstationId("localhost");
			uamInfo.setAppCode("DPP");
			uamInfo.setUserRole("DPP_PMH.DEBUG");
			uamInfo.setHospital("PMH");
			uamInfo.setWorkstore("WKS");
			uam.setUamInfo(uamInfo);
		} else {
			uamInfo = uam.getUamInfo();
		}
		
		SysMenu sysMenu = JAXB_WRAPPER.unmarshall(ConfigHelper.getResourceAsStream(MENU));		
		this.createAdditionalMenu(sysMenu);
	    menuMapList = convertMenu(sysMenu.getMenu());
	    
	    
	}
	
	private void createAdditionalMenu(SysMenu sysMenu) 
	{		
		SysMenu rootMenu = sysMenu; 

		SysMenuDtl helpMenuItem = new SysMenuDtl();
		helpMenuItem.setDisplayName("Help");
		rootMenu.getMenu().add(helpMenuItem);
		
		SysMenuDtl subMenuItem = new SysMenuDtl();
		subMenuItem = new SysMenuDtl();
		subMenuItem.setDisplayName("Welcome Page");
		subMenuItem.setUrl("hk.org.ha.event.pms.main.show.ShowStartupViewEvent()");		
		helpMenuItem.getMenu().add(subMenuItem);
								
		subMenuItem = new SysMenuDtl();
		subMenuItem.setDisplayName("About");
		subMenuItem.setUrl("hk.org.ha.event.pms.dpp.info.popup.ShowAboutPopupEvent()");		
		helpMenuItem.getMenu().add(subMenuItem);
		
		SysMenuDtl logoffMenuItem = new SysMenuDtl();
		logoffMenuItem.setDisplayName("Logoff");
		rootMenu.getMenu().add(logoffMenuItem);
	}	
	
	private List<Map<String, Object>> convertMenu(List<SysMenuDtl> menu) 
	{
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		
		for (SysMenuDtl sysMenuDtl:menu) 
		{
			Map<String, Object> menuItem = new HashMap<String, Object>();
			boolean accessRight = (StringUtils.contains(uamInfo.getUserRole(), "DEBUG") || 
					sysMenuDtl.getTarget() == null ||
					identity.hasPermission(sysMenuDtl.getTarget(), "read only") ||
					identity.hasPermission(sysMenuDtl.getTarget(), "full"));
			
			if (!"N".equals(sysMenuDtl.getVisibility()) && accessRight) 
			{
				if (sysMenuDtl.getMenu() != null && sysMenuDtl.getMenu().size() > 0) 
				{
					List<Map<String, Object>> children = convertMenu(sysMenuDtl.getMenu());
					if (children.isEmpty()) {
						continue;
					}
					menuItem.put(MENU_CHILDREN, children);		
				}
				
				sysMenuDtl.setEnabled(!"N".equals(sysMenuDtl.getEnable()));
				menuItem.put(MENU_DISPLAY_NAME, sysMenuDtl.getDisplayName());
				menuItem.put(MENU_URL, sysMenuDtl.getUrl());
				menuItem.put(MENU_ENABLED, sysMenuDtl.getEnabled()); 

				resultList.add(menuItem);
			}
		}
		return resultList;
	}			
}