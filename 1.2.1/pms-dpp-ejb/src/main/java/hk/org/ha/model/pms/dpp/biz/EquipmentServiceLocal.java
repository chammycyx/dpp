package hk.org.ha.model.pms.dpp.biz;

import javax.ejb.Local;

@Local
public interface EquipmentServiceLocal {

	void retrieveEquipmentByPrepackRecordVerId(Long id);
	
	void destroy();
	
}
