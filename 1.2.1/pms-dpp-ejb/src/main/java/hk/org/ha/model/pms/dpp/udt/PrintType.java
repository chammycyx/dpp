package hk.org.ha.model.pms.dpp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum PrintType implements StringValuedEnum {
	
	Label("L", "Label"),
	SampleLabel("S", "Sample Label");
	
    private final String dataValue;
    private final String displayValue;
        
    PrintType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<PrintType> {
		
		private static final long serialVersionUID = 1687843174578163820L;

		@Override
    	public Class<PrintType> getEnumClass() {
    		return PrintType.class;
    	}
    }
}
