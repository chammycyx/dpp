package hk.org.ha.model.pms.dpp.vo;

import hk.org.ha.model.pms.dpp.persistence.ActionTaken;
import hk.org.ha.model.pms.dpp.persistence.Discrepancy;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DprWorksheet {
	
	private String instCode;
	
	private String workstoreCode;
	
	private String itemCode;
	
	private String itemDesc;
	
	private Date preparationDate;
	
	private String batchNum;
	
	private Date expiryDate;
	
	private String manufName;
	
	private Integer originalPackQty;
	
	private Integer originalPackSize;
	
	private Integer originalTotalQty;
	
	private String dispenseDosageUnit;
	
	private String equipmentName;
	
	private String containerName;
	
	private String lotNum;
	
	private Date assignExpiryDate;
	
	private BigDecimal packSize;
	
	private String packUnit;
	
	private Integer provisionPrepackQty;
	
	private Integer provisionPrepackLabelQty;
	
	private BigDecimal provisionRemainQty;
	
	private Integer sampleCheckQty;
	
	private String prepareBy;
	
	private List<Discrepancy> discrepancyList;
	
	private List<ActionTaken> actionTakenList;
	
	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}

	public String getInstCode() {
		return instCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public String getItemDesc() {
		return itemDesc;
	}

	public void setPreparationDate(Date preparationDate) {
		this.preparationDate = preparationDate == null ? null : new Date(preparationDate.getTime());
	}

	public Date getPreparationDate() {
		return preparationDate == null ? null : new Date(preparationDate.getTime());
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate == null ? null : new Date(expiryDate.getTime());
	}

	public Date getExpiryDate() {
		return expiryDate == null ? null : new Date(expiryDate.getTime());
	}

	public void setManufName(String manufName) {
		this.manufName = manufName;
	}

	public String getManufName() {
		return manufName;
	}

	public void setOriginalPackQty(Integer originalPackQty) {
		this.originalPackQty = originalPackQty;
	}

	public Integer getOriginalPackQty() {
		return originalPackQty;
	}

	public void setOriginalPackSize(Integer originalPackSize) {
		this.originalPackSize = originalPackSize;
	}

	public Integer getOriginalPackSize() {
		return originalPackSize;
	}

	public void setOriginalTotalQty(Integer originalTotalQty) {
		this.originalTotalQty = originalTotalQty;
	}

	public Integer getOriginalTotalQty() {
		return originalTotalQty;
	}

	public String getDispenseDosageUnit() {
		return dispenseDosageUnit;
	}

	public void setDispenseDosageUnit(String dispenseDosageUnit) {
		this.dispenseDosageUnit = dispenseDosageUnit;
	}

	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}

	public String getEquipmentName() {
		return equipmentName;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}

	public String getContainerName() {
		return containerName;
	}

	public void setLotNum(String lotNum) {
		this.lotNum = lotNum;
	}

	public String getLotNum() {
		return lotNum;
	}

	public void setAssignExpiryDate(Date assignExpiryDate) {
		this.assignExpiryDate = assignExpiryDate == null ? null : new Date(assignExpiryDate.getTime());
	}

	public Date getAssignExpiryDate() {
		return assignExpiryDate == null ? null : new Date(assignExpiryDate.getTime());
	}

	public void setPackSize(BigDecimal packSize) {
		this.packSize = packSize;
	}

	public BigDecimal getPackSize() {
		return packSize;
	}

	public String getPackUnit() {
		return packUnit;
	}

	public void setPackUnit(String packUnit) {
		this.packUnit = packUnit;
	}

	public void setProvisionPrepackQty(Integer provisionPrepackQty) {
		this.provisionPrepackQty = provisionPrepackQty;
	}

	public Integer getProvisionPrepackQty() {
		return provisionPrepackQty;
	}

	public void setProvisionPrepackLabelQty(Integer provisionPrepackLabelQty) {
		this.provisionPrepackLabelQty = provisionPrepackLabelQty;
	}

	public Integer getProvisionPrepackLabelQty() {
		return provisionPrepackLabelQty;
	}

	public void setProvisionRemainQty(BigDecimal provisionRemainQty) {
		this.provisionRemainQty = provisionRemainQty;
	}

	public BigDecimal getProvisionRemainQty() {
		return provisionRemainQty;
	}

	public void setSampleCheckQty(Integer sampleCheckQty) {
		this.sampleCheckQty = sampleCheckQty;
	}

	public Integer getSampleCheckQty() {
		return sampleCheckQty;
	}

	public String getPrepareBy() {
		return prepareBy;
	}

	public void setPrepareBy(String prepareBy) {
		this.prepareBy = prepareBy;
	}
	
	public void setDiscrepancyList(List<Discrepancy> discrepancyList) {
		this.discrepancyList = discrepancyList;
	}

	public List<Discrepancy> getDiscrepancyList() {
		return discrepancyList;
	}

	public void setActionTakenList(List<ActionTaken> actionTakenList) {
		this.actionTakenList = actionTakenList;
	}

	public List<ActionTaken> getActionTakenList() {
		return actionTakenList;
	}
	
	public String getPackSizeAsString()
	{
		return packSize.toString() + " " + packUnit;
	}
	
	public String getProvisionRemainQtyAsString()
	{
		return provisionRemainQty.toString();
	}	
}
