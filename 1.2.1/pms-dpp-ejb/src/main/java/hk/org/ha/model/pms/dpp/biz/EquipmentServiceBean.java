package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.persistence.Equipment;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecordVer;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("equipmentService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class EquipmentServiceBean implements EquipmentServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@Out(required=false)
	private Equipment equipment;
	
	public void retrieveEquipmentByPrepackRecordVerId(Long id) {
		logger.debug("retrieveEquipmentByPrepackRecordVerId");
		
		PrepackRecordVer pVer = (PrepackRecordVer) em.createNamedQuery("PrepackRecordVer.findById")
								.setParameter("id", id)
								.getResultList().get(0);
		
		equipment = pVer.getEquipment();
	}
	
	@Remove
	public void destroy() {
		if(equipment != null) {
			equipment = null;
		}
	}
}
