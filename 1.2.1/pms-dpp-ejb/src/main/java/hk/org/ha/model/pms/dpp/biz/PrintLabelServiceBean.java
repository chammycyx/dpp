package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintingAgentInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dpp.cacher.DmDrugCacherInf;
import hk.org.ha.model.pms.dpp.exception.PrintingException;
import hk.org.ha.model.pms.dpp.persistence.DrugItemInfo;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecordVer;
import hk.org.ha.model.pms.dpp.udt.DisplaySideLabelAliasFlag;
import hk.org.ha.model.pms.dpp.udt.DisplaySideLabelShortDescFlag;
import hk.org.ha.model.pms.dpp.udt.LabelSize;
import hk.org.ha.model.pms.dpp.vo.PrepackLabel;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderJob;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

import com.jamesmurty.utils.XMLBuilder;


@Stateful
@Scope(ScopeType.SESSION)
@Name("printLabelService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PrintLabelServiceBean implements PrintLabelServiceLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private UamInfo uamInfo;

	@In
	private PrintingAgentInf printingAgent;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	private static final String DATE_TIME_FORMAT = "dd-MMM-yyyy";
	private static final String QRCODE_DATE_FORMAT = "dd MMM yyyy";
	
	private List<PrepackLabel> labelList;
	private int labelQty = 1;
	private SimpleDateFormat sdf;
	private String labelNo = "";
	private String labelJrxml = "";
	private String printDocType = "";
	private Map<String, Object> parameters;
	
	private String prepackRecordVerMsg;
	
	public void retrievePrepackRecord(Long id) throws TransformerException, ParserConfigurationException, FactoryConfigurationError {
		logger.debug("retrievePrepackRecord #0", id);
	
		PrepackRecordVer prepackRecordVer = (PrepackRecordVer)em.createNamedQuery("PrepackRecordVer.findById")
												.setParameter("id", id)
												.setHint(QueryHints.FETCH, "o.drugItemInfoVer.drugItemInfo")
												.setHint(QueryHints.FETCH, "o.prepackRecord")
												.setHint(QueryHints.FETCH, "o.prepackPackSize")
												.getResultList().get(0);
		
		// Get latest drugItemInfo
		DrugItemInfo drugItemInfo = (DrugItemInfo)em.createNamedQuery("DrugItemInfo.findById")
										.setParameter("id", prepackRecordVer.getDrugItemInfoVer().getDrugItemInfo().getId())
										.setHint(QueryHints.BATCH, "o.prepackPackSizeList")
										.getResultList().get(0);
		
		labelNo = prepackRecordVer.getLabelSize().getDataValue();
		labelQty = prepackRecordVer.getLabelSize().getLabelQty();
		labelJrxml = prepackRecordVer.getLabelSize().getLabelJrxml();
		printDocType = prepackRecordVer.getLabelSize().getDisplayValue();
		
		labelList = new ArrayList<PrepackLabel>();
		sdf = new SimpleDateFormat(DATE_TIME_FORMAT, Locale.ENGLISH);
		String assignExpiryDate = sdf.format( prepackRecordVer.getAssignExpiryDate() );
		
		String itemLongDesc = drugItemInfo.getDrugItemInfoVer().getItemLongDesc();
		String itemShortDesc = drugItemInfo.getDrugItemInfoVer().getItemShortDesc();
		String itemDisplayName = "";
		String itemCode = prepackRecordVer.getDrugItemInfoVer().getDrugItemInfo().getItemCode();
		String batchNum = prepackRecordVer.getBatchNum();
		String lotNum = prepackRecordVer.getPrepackRecord().getLotNum();
		String instCode =  prepackRecordVer.getDrugItemInfoVer().getDrugItemInfo().getInstCode();
		String workstoreCode = prepackRecordVer.getDrugItemInfoVer().getDrugItemInfo().getWorkstoreCode();
		String packSize = prepackRecordVer.getPrepackPackSize().getPackSize().toString();
		String packUnit = prepackRecordVer.getPrepackPackSize().getPackUnit();
		String remark = drugItemInfo.getDrugItemInfoVer().getRemark();
		String sideLabelDesc = generateSideLabelDesc(prepackRecordVer);
		String qrCode = generateQrCode(prepackRecordVer);
		String alias = "";
		
		if ("longDesc".equals(prepackRecordVer.getLabelSize().getItemDescType())) {
			itemDisplayName = itemLongDesc;
		} else {
			itemDisplayName = itemShortDesc;
		}
				
		if ( drugItemInfo.getDrugItemInfoVer().getDisplayAliasFlag().getDataValue().equals("Y") ) {
			alias = drugItemInfo.getDrugItemInfoVer().getAlias();
		}
				
		PrepackLabel newLabel = new PrepackLabel();
		
		newLabel.setAlias(alias);
		newLabel.setQrCode(qrCode);
		newLabel.setBatchNum( batchNum );
		newLabel.setExpiryDate(assignExpiryDate);
		newLabel.setInstCode( instCode );
		newLabel.setWorkstoreCode( workstoreCode );
		newLabel.setItemCode( itemCode );
		newLabel.setItemDisplayName( itemDisplayName );
		newLabel.setLotNum( lotNum );
		newLabel.setPackSize( packSize );
		newLabel.setPackUnit( packUnit );
		newLabel.setRemark( remark );
		newLabel.setSideLabelDesc( sideLabelDesc );
		
		labelList.add(newLabel);
	}
	
	private String generateQrCode( PrepackRecordVer prepackRecordVer ) throws TransformerException, ParserConfigurationException, FactoryConfigurationError {
		logger.debug("generateQrCode #0", prepackRecordVer.getId());
		
		sdf = new SimpleDateFormat(QRCODE_DATE_FORMAT, Locale.ENGLISH);
		DmDrug dmDrug = dmDrugCacher.getDrugByItemCode(prepackRecordVer.getDrugItemInfoVer().getDrugItemInfo().getItemCode());
		
		// Handle case if salt property return null from DMS
		String saltProperty = "";
		if(dmDrug.getDmDrugProperty().getSaltProperty()!=null) {
			saltProperty = dmDrug.getDmDrugProperty().getSaltProperty();
		}
		
		String qrCode = XMLBuilder.create("DRUG")
			.e("A").t(dmDrug.getDmDrugProperty().getDisplayname()).up()
			.e("B").t(saltProperty).up()
			.e("C").t(dmDrug.getFullDrugDesc()).up()
			.e("D").t(dmDrug.getFormCode()).up()
			.e("E").up()
			.e("F").t(dmDrug.getItemCode() ).up()
			.e("G").up()
			.e("H").t(prepackRecordVer.getBatchNum()).up()
			.e("I").t(sdf.format(prepackRecordVer.getAssignExpiryDate())).up()
			.e("J").t(prepackRecordVer.getDrugItemInfoVer().getDrugItemInfo().getInstCode() + prepackRecordVer.getPrepackRecord().getLotNum()).up()
			.asString();
		
		logger.debug("QR Code: #0", qrCode);
		
		return qrCode;
	}
	
	private String generateSideLabelDesc(PrepackRecordVer prepackRecordVer)
	{
		if (prepackRecordVer.getLabelSize() == LabelSize.DppWithSideLabel1Size4x1p33 || 
			prepackRecordVer.getLabelSize() == LabelSize.DppWithSideLabel1Size4x2 || 
			prepackRecordVer.getLabelSize() == LabelSize.DppWithSideLabel2Size4x1p33 ||
			prepackRecordVer.getLabelSize() == LabelSize.DppWithSideLabel2Size4x2)
		{
			String sideLabelDesc = "";
			if (prepackRecordVer.getPrepackPackSize().getDrugItemInfo().getDrugItemInfoVer().getDisplaySideLabelShortDescFlag() == DisplaySideLabelShortDescFlag.Yes)
			{
				sideLabelDesc += prepackRecordVer.getPrepackPackSize().getDrugItemInfo().getDrugItemInfoVer().getItemShortDesc();
			}
			
			if (prepackRecordVer.getPrepackPackSize().getDrugItemInfo().getDrugItemInfoVer().getDisplaySideLabelAliasFlag() == DisplaySideLabelAliasFlag.Yes)
			{
				if (!StringUtils.isBlank(sideLabelDesc))
				{
					sideLabelDesc += " [" + prepackRecordVer.getPrepackPackSize().getDrugItemInfo().getDrugItemInfoVer().getAlias() + "]";
				}
				else
				{
					sideLabelDesc += prepackRecordVer.getPrepackPackSize().getDrugItemInfo().getDrugItemInfoVer().getAlias();
				}
			}
						
			return sideLabelDesc;
		}
		
		return null;
	}
	
	public void generateLabel() {
		logger.debug("generateLabel #0", labelJrxml);
		
		parameters = new HashMap<String, Object>();
		parameters.put("SUBREPORT_DIR", "report/");
		parameters.put("IS_SAMPLE", false);
		parameters.put("LABEL_NO", Integer.parseInt(labelNo));
	
		String contentId = printingAgent.renderImage(new RenderJob(
				labelJrxml, 
				new PrintOption(),  
				parameters, 
				labelList));
		
		printingAgent.redirect(contentId);
		
	}
	
	public String print(PrepackRecord prepackRecord, int printQty, boolean isSample) throws PrintingException {
		logger.debug("print #0 #1 #2", printQty, isSample, labelNo);
		
		prepackRecordVerMsg = checkPrepackItemVersion(prepackRecord);
		if (prepackRecordVerMsg != null)
		{
			return prepackRecordVerMsg;
		}
		
		if(labelList == null) {
			throw new IllegalStateException("unable to print");
		}
		
		parameters.put("IS_SAMPLE", isSample);
		parameters.put("LABEL_NO", Integer.parseInt(labelNo));
		
		String contentId = printingAgent.render(new RenderJob(
				labelJrxml, 
				new PrintOption(),  
				parameters, 
				labelList));
		
		printingAgent.print(contentId, printQty/labelQty, uamInfo.getWorkstationId(), printDocType);
		
		if(printQty%labelQty > 0) {
			parameters.put("LABEL_NO", printQty%labelQty);
			
			contentId = printingAgent.render(new RenderJob(
					labelJrxml, 
					new PrintOption(),  
					parameters, 
					labelList));
			
			printingAgent.print(contentId, 1, uamInfo.getWorkstationId(), printDocType);
		}
		
		return prepackRecordVerMsg;
	}
	
	private String checkPrepackItemVersion(PrepackRecord prepackRecord)
	{
		PrepackRecord latestPrepackRecord = (PrepackRecord) em.createNamedQuery("PrepackRecord.findById")
			.setParameter("id", prepackRecord.getId())
			.setHint(QueryHints.BATCH, "o.prepackRecordVer")
			.setHint(QueryHints.BATCH, "o.prepackRecordVer.prepackPackSize")
			.setHint(QueryHints.BATCH, "o.prepackRecordVer.prepackPackSize.drugItemInfo")
			.setHint(QueryHints.BATCH, "o.prepackRecordVer.prepackPackSize.drugItemInfo.drugItemInfoVer")
			.getResultList().get(0);

		if (!latestPrepackRecord.getVersion().equals(prepackRecord.getVersion()))
		{
			return "0082";
		}
		else if (prepackRecord.getPrepackRecordVer().getDrugItemInfoVer().getDrugItemInfo() != null &&
				 !latestPrepackRecord.getPrepackRecordVer().getDrugItemInfoVer().getDrugItemInfo().getVersion().equals(
				 prepackRecord.getPrepackRecordVer().getDrugItemInfoVer().getDrugItemInfo().getVersion()))
		{
			return "0082";
		}
		else if ((latestPrepackRecord.getPrepackRecordVer().getLabelSize() == LabelSize.DppWithSideLabel1Size4x1p33 || 
				 latestPrepackRecord.getPrepackRecordVer().getLabelSize() == LabelSize.DppWithSideLabel1Size4x2 ||
				 latestPrepackRecord.getPrepackRecordVer().getLabelSize() == LabelSize.DppWithSideLabel2Size4x1p33 ||
				 latestPrepackRecord.getPrepackRecordVer().getLabelSize() == LabelSize.DppWithSideLabel2Size4x2) && 
				 (latestPrepackRecord.getPrepackRecordVer().getPrepackPackSize().getDrugItemInfo().getDrugItemInfoVer().getDisplaySideLabelAliasFlag() == DisplaySideLabelAliasFlag.No && 
				 latestPrepackRecord.getPrepackRecordVer().getPrepackPackSize().getDrugItemInfo().getDrugItemInfoVer().getDisplaySideLabelShortDescFlag() == DisplaySideLabelShortDescFlag.No))
		{
			return "0081";
		}
		
		return null;
	}
		
    @Remove
	public void destroy(){
    	labelList = null;
    	parameters = null;
	}
}
