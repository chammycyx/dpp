package hk.org.ha.model.pms.dpp.vo;

import java.sql.Date;

public class PrepackHistory {
	
	private String action;
	private String updateUser;
	private Date createDate;
	
	public void setAction(String action) {
		this.action = action;
	}
	
	public String getAction() {
		return action;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setCreateDate(java.sql.Date createDate) {
		this.createDate = createDate == null ? null : new Date(createDate.getTime());
	}

	public java.sql.Date getCreateDate() {
		return createDate == null ? null : new Date(createDate.getTime());
	}
}
