package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.model.pms.dpp.persistence.Discrepancy;

import java.util.List;

import javax.ejb.Local;

@Local
public interface DiscrepancyListServiceLocal {
	
	void retrieveDiscrepancyList();
	
	void updateDiscrepancyList(List<Discrepancy> newDiscrepancyList);
	
	void retrieveDiscrepancyDetailsList(Long prepackRecordVerId);
	
	void updateDiscrepancyDetailsList(Long prepackRecordId, List<Discrepancy> updateDiscrepancyList);
	
	void destroy();
	
}
