package hk.org.ha.model.pms.dpp.persistence;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name="PREPACK_RECORD")
@Customizer(AuditCustomizer.class)
@NamedQueries({
	@NamedQuery(name="PrepackRecord.findById", query="select o from PrepackRecord o where o.id = :id"),
	@NamedQuery(name="PrepackRecord.findByLotNum", query="select o from PrepackRecord o where o.lotNum = :lotNum and o.prepackRecordVer.drugItemInfoVer.drugItemInfo.instCode = :instCode and o.prepackRecordVer.drugItemInfoVer.drugItemInfo.workstoreCode = :workstoreCode")
})
public class PrepackRecord extends VersionEntity {
	
	private static final long serialVersionUID = 5308585641114861942L;
	
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="prepackRecordSeq")
	@SequenceGenerator(name="prepackRecordSeq", sequenceName="SQ_PREPACK_RECORD", initialValue=100000000)
	private Long id;
	
	@OneToOne(cascade=CascadeType.PERSIST, fetch=FetchType.EAGER)
	@JoinColumn(name="PREPACK_RECORD_VER_ID", referencedColumnName="ID", nullable=false)
	private PrepackRecordVer prepackRecordVer = new PrepackRecordVer();
	
	@Column(name="LOT_NUM", length=20, nullable=false)
	private String lotNum;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setPrepackRecordVer(PrepackRecordVer prepackRecordVer) {
		this.prepackRecordVer = prepackRecordVer;
	}

	public PrepackRecordVer getPrepackRecordVer() {
		return prepackRecordVer;
	}

	public void addPrepackRecordVer(PrepackRecordVer prepackRecordVer) {
		prepackRecordVer.setPrepackRecord(this);
	}
	
	public void setLotNum(String lotNum) {
		this.lotNum = lotNum;
	}

	public String getLotNum() {
		return lotNum;
	}
	
}
