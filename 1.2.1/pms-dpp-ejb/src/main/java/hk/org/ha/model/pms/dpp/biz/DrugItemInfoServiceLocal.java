package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.model.pms.dpp.persistence.DrugItemInfo;

import javax.ejb.Local;

@Local
public interface DrugItemInfoServiceLocal {

	void retrieveDrugItemInfo(String itemCode);
	
	void updateDrugItemInfo(DrugItemInfo newDrugItemInfo);
	
	void retrieveDrugItemInfoForEnquiry(String itemCode);
	
	void resetDrugItemInfoForEnquiry();
	
	void resetDrugItemInfo();
	
	void destroy();
}
