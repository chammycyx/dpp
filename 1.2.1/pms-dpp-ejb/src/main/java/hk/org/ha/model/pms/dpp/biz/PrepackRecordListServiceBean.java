package hk.org.ha.model.pms.dpp.biz;

import java.math.BigDecimal;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;
import hk.org.ha.model.pms.dpp.vo.PrepackRecordSearchCriteria;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("prepackRecordListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PrepackRecordListServiceBean implements
		PrepackRecordListServiceLocal {

	private static final String INSTITUTION_CODE = "institutionCode";
	private static final String WORKSTORE_CODE = "workstoreCode";
	private static final String LOT_NUM = "lotNum";
	private static final String PACK_SIZE = "packSize";
	private static final String PACK_UNIT = "packUnit";
	private static final String BATCH_NUM = "batchNum";
	private static final String FROM_PREPARATION_DATE = "fromPreparationDate";
	private static final String TO_PREPARATION_DATE = "toPreparationDate";
	private static final String FROM_PREPACK_DATE = "fromPrepackDate";
	private static final String TO_PREPACK_DATE = "toPrepackDate";
	private static final String PREPACK_STATUS = "prepackStatus";
	private static final String PREPACK_STATUS_DATA = "prepackStatusData";
	private static final String ITEM_CODE = "itemCode";
	

	private static final String O_PREPACK_RECORD_VER = "o.prepackRecordVer";
	private static final String O_PREPACK_RECORD_VER_DRUG_ITEM_INFO_VER = "o.prepackRecordVer.drugItemInfoVer";
	private static final String O_PREPACK_RECORD_VER_DRUG_ITEM_INFO_VER_DRUG_ITEM_INFO = "o.prepackRecordVer.drugItemInfoVer.drugItemInfo";

	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<PrepackRecord> prepackRecordList;
	
	public void retrievePrepackRecordList(String lotNum) {
		logger.debug("retrievePrepackRecordList #0", lotNum);
	}
	
	@SuppressWarnings("unchecked")
	public void retrievePrepackRecordListBySearchCriteria(PrepackRecordSearchCriteria prepackRecordSearchCriteria) {
		logger.debug("retrievePrepackRecordListByCriteria #0", prepackRecordSearchCriteria);
		
		StringBuffer enquiryDetailSql = constructSQLStringForSearchCriteria(prepackRecordSearchCriteria);
		
		Query q = em.createQuery(enquiryDetailSql.toString());
		
		q = q.setParameter(INSTITUTION_CODE, prepackRecordSearchCriteria.getInstitutionCode());
		
		q = q.setParameter(WORKSTORE_CODE, prepackRecordSearchCriteria.getWorkstoreCode());
		
		if (prepackRecordSearchCriteria.getLotNum() != null) {
			q = q.setParameter(LOT_NUM, prepackRecordSearchCriteria.getLotNum());
		}
		if (prepackRecordSearchCriteria.getPackSize() != null && prepackRecordSearchCriteria.getPackSize().compareTo(BigDecimal.ZERO) > 0) {
			q = q.setParameter(PACK_SIZE, prepackRecordSearchCriteria.getPackSize());
		}
		if (prepackRecordSearchCriteria.getPackUnit() != null) {
			q = q.setParameter(PACK_UNIT, prepackRecordSearchCriteria.getPackUnit());
		}
		if (prepackRecordSearchCriteria.getBatchNum() != null) {
			q = q.setParameter(BATCH_NUM, prepackRecordSearchCriteria.getBatchNum());
		}
		if (prepackRecordSearchCriteria.getFromPreparationDate() != null) {
			q = q.setParameter(FROM_PREPARATION_DATE, prepackRecordSearchCriteria.getFromPreparationDate(), TemporalType.DATE);
		}
		if (prepackRecordSearchCriteria.getToPreparationDate() != null) {
			q = q.setParameter(TO_PREPARATION_DATE, prepackRecordSearchCriteria.getToPreparationDate());
		}
		if (prepackRecordSearchCriteria.getFromPrepackDate() != null) {
			q = q.setParameter(FROM_PREPACK_DATE, prepackRecordSearchCriteria.getFromPrepackDate(), TemporalType.DATE);
		}
		if (prepackRecordSearchCriteria.getToPrepackDate() != null) {
			q = q.setParameter(TO_PREPACK_DATE, prepackRecordSearchCriteria.getToPrepackDate(), TemporalType.DATE);
		}
		if (prepackRecordSearchCriteria.getPrepackStatus() != null) {
			q = q.setParameter(PREPACK_STATUS, prepackRecordSearchCriteria.getPrepackStatus());
			q = q.setParameter(PREPACK_STATUS_DATA, prepackRecordSearchCriteria.getPrepackStatus().getDataValue());
		}
		if (prepackRecordSearchCriteria.getItemCode() != null) {
			q = q.setParameter(ITEM_CODE, prepackRecordSearchCriteria.getItemCode());
		}
		
		q = q.setHint(QueryHints.FETCH, O_PREPACK_RECORD_VER)
		.setHint(QueryHints.FETCH, O_PREPACK_RECORD_VER_DRUG_ITEM_INFO_VER)
		.setHint(QueryHints.FETCH, O_PREPACK_RECORD_VER_DRUG_ITEM_INFO_VER_DRUG_ITEM_INFO);

		prepackRecordList = q.getResultList();
		
		if(prepackRecordList.size()==0)
		{
			prepackRecordList = null;
		}
		else
		{
			for( PrepackRecord prepackRecordFind:prepackRecordList ) {
				prepackRecordFind.getPrepackRecordVer();
				prepackRecordFind.getPrepackRecordVer().getPrepackRecord();
				prepackRecordFind.getPrepackRecordVer().getContainer();
				prepackRecordFind.getPrepackRecordVer().getEquipment();
				prepackRecordFind.getPrepackRecordVer().getDrugItemInfoVer();
				prepackRecordFind.getPrepackRecordVer().getDrugItemInfoVer().getDrugItemInfo();
				prepackRecordFind.getPrepackRecordVer().getPrepackPackSize();
			}
		}
	}
	
	private StringBuffer constructSQLStringForSearchCriteria(PrepackRecordSearchCriteria prepackRecordSearchCriteria){
		logger.debug("constructSQLStringForEnquiryDetailCriteria #0", prepackRecordSearchCriteria );
		
		StringBuffer enquiryDetailSql = new StringBuffer();
		enquiryDetailSql.append("select o from PrepackRecord o ");
		enquiryDetailSql.append("where (o.prepackRecordVer.drugItemInfoVer.drugItemInfo.instCode = :"
					+ INSTITUTION_CODE + " or :" + INSTITUTION_CODE + " is null) ");
		enquiryDetailSql.append("and (o.prepackRecordVer.drugItemInfoVer.drugItemInfo.workstoreCode = :"
				+ WORKSTORE_CODE + " or :" + WORKSTORE_CODE + " is null) ");
		
		if (prepackRecordSearchCriteria.getItemCode() != null) {
			enquiryDetailSql.append("and (o.prepackRecordVer.drugItemInfoVer.drugItemInfo.itemCode = :" 
					+ ITEM_CODE + " or :" + ITEM_CODE + " is null) ");
		}
		if (prepackRecordSearchCriteria.getLotNum() != null) {
			logger.info("lotNumSearchString = " + prepackRecordSearchCriteria.getLotNum());
			enquiryDetailSql.append("and (o.lotNum like :" + LOT_NUM + " or :" + LOT_NUM + " is null) ");
		}
		if (prepackRecordSearchCriteria.getPackSize() != null && prepackRecordSearchCriteria.getPackSize().compareTo(BigDecimal.ZERO) > 0) {
			enquiryDetailSql.append("and (o.prepackRecordVer.prepackPackSize.packSize = :" 
					+ PACK_SIZE + " or :" + PACK_SIZE + " is null) ");
		}	
		
		if (prepackRecordSearchCriteria.getPackUnit() != null) {
			enquiryDetailSql.append("and (o.prepackRecordVer.prepackPackSize.packUnit = :" 
					+ PACK_UNIT + " or :" + PACK_UNIT + " is null) ");
		}	
		
		if (prepackRecordSearchCriteria.getBatchNum() != null) {
			enquiryDetailSql.append("and (o.prepackRecordVer.batchNum = :" 
					+ BATCH_NUM + " or :" + BATCH_NUM + " is null) ");
		}	
		if (prepackRecordSearchCriteria.getFromPreparationDate() != null) {
			enquiryDetailSql.append("and (o.prepackRecordVer.preparationDate >= :" 
					+ FROM_PREPARATION_DATE + " or :" + FROM_PREPARATION_DATE + " is null) ");
		}
		if (prepackRecordSearchCriteria.getToPreparationDate() != null) {
			enquiryDetailSql.append("and (o.prepackRecordVer.preparationDate < :" +
					TO_PREPARATION_DATE + " or :" + TO_PREPARATION_DATE + " is null) ");
		}
		if (prepackRecordSearchCriteria.getFromPrepackDate() != null) {
			enquiryDetailSql.append("and (o.prepackRecordVer.actualPrepackDate >= :" +
					FROM_PREPACK_DATE + " or :" + FROM_PREPACK_DATE + " is null) ");
		}
		if (prepackRecordSearchCriteria.getToPrepackDate() != null) {
			enquiryDetailSql.append("and (o.prepackRecordVer.actualPrepackDate < :" +
					TO_PREPACK_DATE + " or :" + TO_PREPACK_DATE + " is null) ");
		}
		if (prepackRecordSearchCriteria.getPrepackStatus() != null) {
			enquiryDetailSql.append("and (o.prepackRecordVer.prepackStatus = :"
					+ PREPACK_STATUS + " or :" + PREPACK_STATUS_DATA + " is null) ");
		}
		
		enquiryDetailSql.append(" order by o.lotNum desc "); 
		
		return enquiryDetailSql;
	}
	
	@Remove
	public void destroy() {
		if (prepackRecordList != null) {
			prepackRecordList = null;
		}
	}
}
