package hk.org.ha.model.pms.dpp.cacher;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.model.pms.asa.exception.phs.PhsException;
import hk.org.ha.model.pms.asa.vo.dqa.DqaCompany;
import hk.org.ha.model.pms.dpp.vo.Manufacturer;
import hk.org.ha.service.pms.asa.interfaces.phs.PhsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;

@AutoCreate
@Name("manufacturerCacher")
@Scope(ScopeType.APPLICATION)
public class ManufacturerCacher extends BaseCacher implements ManufacturerCacherInf{
	
	@Logger
	private Log logger;
	
	@In
	private PhsServiceJmsRemote phsServiceProxy;
	
	private InnerCacher cacher;
         
	public void load() {
		getFullManufList();
	}
	
	public void clear() {
		synchronized (this) {
			cacher = null;
		}
	}
	
	public List<Manufacturer> getFullManufList() {
		return (List<Manufacturer>) this.getCacher().getAll();
	}
	
	public Manufacturer getManufacturerByManufCode(String manufCode) {
		return (Manufacturer)this.getCacher().get(manufCode);
	}

	private InnerCacher getCacher() {
		synchronized (this) {
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, Manufacturer> {
		
        public InnerCacher(int expireTime) {
              super(expireTime);
        }
		
		public Manufacturer create(String companyCode) {	
			this.getAll();
			return this.internalGet(companyCode);
		}

		public Collection<Manufacturer> createAll() {
			try {
				List<Manufacturer> manufList = new ArrayList<Manufacturer>();
				List<DqaCompany> companyList = phsServiceProxy.retrieveCompanyForDqa();
				
				for(DqaCompany company : companyList) {
					if(company.getManufacturerFlag().equals("Y")) {
						Manufacturer manufacturer = new Manufacturer();
						manufacturer.setCompanyCode(company.getCompanyCode());
						manufacturer.setCompanyName(company.getCompanyName());
						manufacturer.setEmail(company.getEmail());
						manufacturer.setFax(company.getFax());
						manufacturer.setOfficePhone(company.getOfficePhone());
						manufacturer.setAddress1(company.getAddress1());
						manufacturer.setAddress2(company.getAddress2());
						manufacturer.setAddress3(company.getAddress3());
						manufacturer.setSuspendFlag(company.getSuspendFlag());
						manufacturer.setUploadDate(company.getUploadDate());
						
						manufList.add(manufacturer);
					}
				}
				
				return manufList;
			} catch (PhsException e) {
				logger.debug(e.toString());
				return null;
			}
		}

		public String retrieveKey(Manufacturer manufacturer) {
			return manufacturer.getCompanyCode();
		}		
	}
	
	public static ManufacturerCacherInf instance()
	{
		  if (!Contexts.isApplicationContextActive())
	      {
	         throw new IllegalStateException("No active application scope");
	      }
	      return (ManufacturerCacherInf) Component.getInstance("manufacturerCacher", ScopeType.APPLICATION);
    }	
}
