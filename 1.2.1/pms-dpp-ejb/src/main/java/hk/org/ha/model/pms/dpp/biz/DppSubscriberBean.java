package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.fmk.pms.remote.JmsServiceProxy;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.cacher.DmDrugCacher;
import hk.org.ha.model.pms.dpp.cacher.DmInstDrugCacher;
import hk.org.ha.model.pms.dpp.cacher.ManufacturerCacher;

import java.util.Random;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.core.Events;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("dppSubscriber")
@MeasureCalls
public class DppSubscriberBean implements DppSubscriberLocal {
	
	@Logger
	private Log logger;
	
	@In
	private DmDrugCacher dmDrugCacher;
	
	@In
	private DmInstDrugCacher dmInstDrugCacher;
	
	@In
	private ManufacturerCacher manufacturerCacher;
	
	private Random random = new Random();
	
	private int randomSleepTime = 3*60*1000;

	public int getRandomSleepTime() {
		return randomSleepTime;
	}

	public void setRandomSleepTime(int randomSleepTime) {
		this.randomSleepTime = randomSleepTime;
	}
	
	public void initCache() 
	{
		dmDrugCacher.clear();
		dmInstDrugCacher.clearAll();
		manufacturerCacher.clear();
		
		int sleepTime = random.nextInt(randomSleepTime);		
		logger.info("initCache : start, random sleep for #0", sleepTime);		

		try {
			Thread.sleep(sleepTime);
		} catch (InterruptedException e) {
		}
		
		long startTime = System.currentTimeMillis();
		initDmDrugCache();
		initManufCache();
		logger.info("initCache : time used #0", System.currentTimeMillis() - startTime);		
	}
	
	private void initDmDrugCache()
	{
		long startTime = System.currentTimeMillis();
		
		dmDrugCacher.load();
		
		logger.info("initDmDrugCache : time used #0", System.currentTimeMillis() - startTime);
	}
	
	private void initManufCache()
	{
		long startTime = System.currentTimeMillis();
		
		manufacturerCacher.load();
		
		logger.info("initManufCache : time used #0", System.currentTimeMillis() - startTime);
	}

	public void initJmsServiceProxy() {
		JmsServiceProxy.REINITING.get().set(true);
		try {
			Events.instance().raiseEvent(JmsServiceProxy.EVENT_REINIT_PROXY);
		} finally {
			JmsServiceProxy.REINITING.get().set(false);
		}
	}	
	
}
