package hk.org.ha.model.pms.persistence;

import hk.org.ha.fmk.pms.entity.CreateDate;
import hk.org.ha.fmk.pms.entity.CreateUser;
import hk.org.ha.fmk.pms.entity.UpdateDate;
import hk.org.ha.fmk.pms.entity.UpdateUser;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
@SuppressWarnings("serial")
public abstract class BaseEntity implements Serializable {

	@UpdateDate
	@Column(name = "UPDATE_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@CreateDate
	@Column(name = "CREATE_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@UpdateUser
	@Column(name = "UPDATE_USER", length = 12, nullable = false)
	private String updateUser;

	@CreateUser
	@Column(name = "CREATE_USER", length = 12, nullable = false)
	private String createUser;

	public Date getUpdateDate() {
		return (updateDate != null) ? new Date(updateDate.getTime()) : null;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = new Date(updateDate.getTime());
	}

	public Date getCreateDate() {
		return (createDate != null) ? new Date(createDate.getTime()) : null;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = new Date(createDate.getTime());
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
}
