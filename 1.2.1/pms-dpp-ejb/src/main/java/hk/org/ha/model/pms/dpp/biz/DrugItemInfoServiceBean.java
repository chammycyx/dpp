package hk.org.ha.model.pms.dpp.biz;

import java.util.List;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmInstDrug;
import hk.org.ha.model.pms.dpp.cacher.DmDrugCacherInf;
import hk.org.ha.model.pms.dpp.cacher.DmInstDrugCacherInf;
import hk.org.ha.model.pms.dpp.persistence.DrugItemInfo;
import hk.org.ha.model.pms.dpp.persistence.DrugItemInfoVer;
import hk.org.ha.model.pms.dpp.persistence.PrepackPackSize;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("drugItemInfoService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DrugItemInfoServiceBean implements DrugItemInfoServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private DmInstDrugCacherInf dmInstDrugCacher;
	
	@In
	private UamInfo uamInfo;
	
	@Out(required=false)
	private DrugItemInfo drugItemInfo;
	
	@Out(required=false)
	private DrugItemInfo drugItemInfoForEnquiry;
	
	private static final String UNCHECKED = "unchecked";
	
	private DrugItemInfo buildDummyDrugItemInfoForEnquiry() {
		DrugItemInfo itemInfo = new DrugItemInfo();
		itemInfo.getDrugItemInfoVer().setDrugItemInfo(itemInfo);
		itemInfo.setDmDrug(new DmDrug());
		return itemInfo;
	}
	
	@SuppressWarnings(UNCHECKED)
	private DrugItemInfo getDrugItemInfo(String itemCode) {
		DmDrug dmDrug = dmDrugCacher.getDrugByItemCode(itemCode);
		
		if(dmDrug == null) {
			logger.info("No drug master info. for item code[#0]", itemCode);
			return buildDummyDrugItemInfoForEnquiry();
		} else {
		
			List<DrugItemInfo> drugItemInfoList = em.createNamedQuery("DrugItemInfo.findByInstCodeItemCode")
				.setParameter("instCode", uamInfo.getHospital())
				.setParameter("workstoreCode", uamInfo.getWorkstore())
				.setParameter("itemCode", itemCode)
				.setHint(QueryHints.BATCH, "o.prepackPackSizeList")
				.getResultList();
			
			DrugItemInfo itemInfo;
			
			if(drugItemInfoList.size() == 0) {
				logger.info("No drug item info. for item code[#0]", itemCode);
				itemInfo = new DrugItemInfo();
				itemInfo.setItemCode(itemCode);
				itemInfo.getDrugItemInfoVer().setDrugItemInfo(itemInfo);
				if(itemInfo.getDmDrug() != null) {
					itemInfo.getDrugItemInfoVer().setItemLongDesc(itemInfo.getDmDrug().getFullDrugDesc());
				}
			} else {
				itemInfo = drugItemInfoList.get(0);
				itemInfo.getPrepackPackSizeList().size();
				itemInfo.getDmDrug();
			} 
			
			itemInfo.getInstSuspend();
			
			return itemInfo;
		} 
	}
	
	public void retrieveDrugItemInfo(String itemCode) {
		logger.info("retrieveDrugItemInfo #0", itemCode);
		drugItemInfo = getDrugItemInfo(itemCode);
	}
	
	public void retrieveDrugItemInfoForEnquiry(String itemCode) {
		logger.debug("retrieveDrugItemInfoForEnquiry #0", itemCode);
		drugItemInfoForEnquiry = getDrugItemInfo(itemCode);
	}
	
	public void resetDrugItemInfoForEnquiry() {
		logger.debug("resetDrugItemInfoForEnquiry");
		
		drugItemInfoForEnquiry = null;
	}
	
	public void resetDrugItemInfo() {
		logger.debug("resetDrugItemInfoForEnquiry");
		
		drugItemInfo = null;
	}
	
	public void updateDrugItemInfo(DrugItemInfo newDrugItemInfo) {
		logger.debug("updateDrugItemInfo");
		
		if(newDrugItemInfo.getId() == null) {
			if(em.createNamedQuery("DrugItemInfo.findByInstCodeItemCode")
				.setParameter("instCode", uamInfo.getHospital())
				.setParameter("workstoreCode", uamInfo.getWorkstore())
				.setParameter("itemCode", newDrugItemInfo.getItemCode())
				.getResultList().size() != 0) {
				throw new OptimisticLockException();
			}
			
			newDrugItemInfo.setInstCode(uamInfo.getHospital());
			newDrugItemInfo.setWorkstoreCode(uamInfo.getWorkstore());
			// persist parent and version, 1. terminate relation from version, 2. persist version, then parent, 3. set parent to version
			newDrugItemInfo.getDrugItemInfoVer().setDrugItemInfo(null);
			
			em.persist(newDrugItemInfo.getDrugItemInfoVer());
			em.flush();
			
			em.persist(newDrugItemInfo);
			newDrugItemInfo.getDrugItemInfoVer().setDrugItemInfo(newDrugItemInfo);
			em.flush();
			
		} else {
			DrugItemInfo findDrugItemInfo = (DrugItemInfo)em.createNamedQuery("DrugItemInfo.findById")
												.setParameter("id", newDrugItemInfo.getId())
												.setHint(QueryHints.BATCH, "o.prepackPackSizeList")
												.getResultList().get(0);
			
			if(!findDrugItemInfo.getVersion().equals(newDrugItemInfo.getVersion())) {
				throw new OptimisticLockException();
			}
			
			for(PrepackPackSize prepackPackSize : findDrugItemInfo.getPrepackPackSizeList()) {
				for(PrepackPackSize newPrepackPackSize : newDrugItemInfo.getPrepackPackSizeList()) {
					if(prepackPackSize.getId().equals(newPrepackPackSize.getId())) {
						prepackPackSize.setPackSize(newPrepackPackSize.getPackSize());
						prepackPackSize.setPackUnit(newPrepackPackSize.getPackUnit());
						prepackPackSize.setRecordStatus(newPrepackPackSize.getRecordStatus());
					}
				}
			}
			
			for(PrepackPackSize newPrepackPackSize : newDrugItemInfo.getPrepackPackSizeList()) {
				if(newPrepackPackSize.getId() == null) {
					findDrugItemInfo.addPrepackPackSize(newPrepackPackSize);
				}
			}
			
			DrugItemInfoVer newDrugItemInfoVer = new DrugItemInfoVer();
			newDrugItemInfoVer.setItemLongDesc(newDrugItemInfo.getDrugItemInfoVer().getItemLongDesc());
			newDrugItemInfoVer.setItemShortDesc(newDrugItemInfo.getDrugItemInfoVer().getItemShortDesc());
			newDrugItemInfoVer.setRemark(newDrugItemInfo.getDrugItemInfoVer().getRemark());
			newDrugItemInfoVer.setAlias(newDrugItemInfo.getDrugItemInfoVer().getAlias());
			newDrugItemInfoVer.setPrepackFlag(newDrugItemInfo.getDrugItemInfoVer().getPrepackFlag());
			newDrugItemInfoVer.setDisplayAliasFlag(newDrugItemInfo.getDrugItemInfoVer().getDisplayAliasFlag());
			newDrugItemInfoVer.setDisplaySideLabelAliasFlag(newDrugItemInfo.getDrugItemInfoVer().getDisplaySideLabelAliasFlag());
			newDrugItemInfoVer.setDisplaySideLabelShortDescFlag(newDrugItemInfo.getDrugItemInfoVer().getDisplaySideLabelShortDescFlag());
			
			newDrugItemInfoVer.setDrugItemInfo(findDrugItemInfo);
			findDrugItemInfo.addDrugItemInfoVer(newDrugItemInfoVer);
			findDrugItemInfo.setDrugItemInfoVer(newDrugItemInfoVer);
			
			em.merge(findDrugItemInfo);
			em.flush();
		}
	}
	
	@Remove
	public void destroy() {
		if(drugItemInfo != null) {
			drugItemInfo = null;
		}
		if(drugItemInfoForEnquiry != null) {
			drugItemInfoForEnquiry = null;
		}
	}
	
}
