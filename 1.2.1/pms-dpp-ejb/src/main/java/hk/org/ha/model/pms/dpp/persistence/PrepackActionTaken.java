package hk.org.ha.model.pms.dpp.persistence;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name="PREPACK_ACTION_TAKEN")
@Customizer(AuditCustomizer.class)
@NamedQueries({
	@NamedQuery(name="PrepackActionTaken.findByPrepackRecordVerId", query="select o from PrepackActionTaken o where o.prepackRecordVer.id = :prepackRecordVerId"),
	@NamedQuery(name="PrepackActionTaken.findDeletedActionTakenByPrepackRecordVerId", query="select o from PrepackActionTaken o where o.prepackRecordVer.id = :prepackRecordVerId and o.actionTaken.recordStatus = hk.org.ha.model.pms.dpp.udt.RecordStatus.Deleted")
})
public class PrepackActionTaken extends VersionEntity {
	
	private static final long serialVersionUID = 3735022139054779195L;
	
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="prepackActionTakenSeq")
	@SequenceGenerator(name="prepackActionTakenSeq", sequenceName="SQ_PREPACK_ACTION_TAKEN", initialValue=100000000)
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PREPACK_RECORD_VER_ID", referencedColumnName="ID")
	private PrepackRecordVer prepackRecordVer;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ACTION_TAKEN_ID", referencedColumnName="ID")
	private ActionTaken actionTaken;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setPrepackRecordVer(PrepackRecordVer prepackRecordVer) {
		this.prepackRecordVer = prepackRecordVer;
	}

	public PrepackRecordVer getPrepackRecordVer() {
		return prepackRecordVer;
	}

	public void setActionTaken(ActionTaken actionTaken) {
		this.actionTaken = actionTaken;
	}

	public ActionTaken getActionTaken() {
		return actionTaken;
	}

}
