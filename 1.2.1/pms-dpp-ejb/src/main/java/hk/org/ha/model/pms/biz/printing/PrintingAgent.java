package hk.org.ha.model.pms.biz.printing;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.exception.PrintingException;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.printing.PrintType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderJob;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.beanutils.PropertyUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.document.DocumentData;
import org.jboss.seam.document.DocumentData.DocumentType;
import org.jboss.seam.log.Log;

@AutoCreate
@Name("printingAgent")
@Scope(ScopeType.SESSION)
@MeasureCalls
public class PrintingAgent implements PrintingAgentInf {
	
	private static final long serialVersionUID = -5605926936921434L;

	private static final String SUBREPORT_DIR_KEY = "SUBREPORT_DIR";

	private static final String REPORT_DIR = "report/";
	
	@Logger
	private Log logger;
	
	@In
	private ReportProvider<JRDataSource> reportProvider;
	
	@In
	private SilentPrintSenderInf silentPrintSender;
	
	private DocumentData keepData;

	public String render(RenderJob job) {
		
		String template = this.prepareTemplate(job);

		this.setDocTypeIfEmpty(job, ReportProvider.PDF);

		return reportProvider.generateReport(
				template, 
				job.getParameters(), 
				job.getPrintOption().getDocType(),
				new JRBeanCollectionDataSource(job.getObjectList()));
	}
	
	public String renderImage(RenderJob job) {
		
		String template = this.prepareTemplate(job);

		this.setDocTypeIfEmpty(job, ReportProvider.PNG);

		return reportProvider.generateReport(
				template, 
				job.getParameters(), 
				job.getPrintOption().getDocType(),
				new JRBeanCollectionDataSource(job.getObjectList()));
	}
	
	private String prepareTemplate(RenderJob job) {
		
		this.reflectSetPrintOption(job.getObjectList(), job.getPrintOption());
		
		if (!job.getParameters().containsKey(SUBREPORT_DIR_KEY)) {
			job.getParameters().put(SUBREPORT_DIR_KEY, REPORT_DIR + job.getDocName() +"/");
		}

		StringBuilder sb = new StringBuilder();
		sb.append(REPORT_DIR);
		sb.append(job.getDocName());
		if (job.getPrintOption().getPrintType() != PrintType.Normal) {
			sb.append(job.getPrintOption().getPrintType().toString());
		}
		if (job.getPrintOption().getPrintLang() != PrintLang.Eng) {
			sb.append(job.getPrintOption().getPrintLang().toString());
		}
		sb.append(".jasper");	
		
		return sb.toString();
	}
	
	private void setDocTypeIfEmpty(RenderJob job, DocumentType docType) {
		if (job.getPrintOption().getDocType() == null) {
			job.getPrintOption().setDocType(docType);
		}
	}
	
	public void keep(String contentId) {
		keepData = reportProvider.getDocumentStore().getDocumentData(contentId);
	}
	
	public void redirect(String contentId) {
		reportProvider.redirectReport(contentId);
	}
	
	public void print(int printQty, String printerHostName, String printDocType) throws PrintingException {
		if (keepData != null) {
			if (keepData.getDocumentType() == ReportProvider.PDF) {
				this.print(keepData, printQty, printerHostName, printDocType);
			} else {
				throw new UnsupportedOperationException("unable to print non-PDF document!");
			}
		} else {
			throw new UnsupportedOperationException("please keep your content first!");
		}
	}

	public void print(String contentId, int printQty, String printerHostName, String printDocType) throws PrintingException {
		DocumentData data = reportProvider.getDocumentStore().getDocumentData(contentId);
		if (data.getDocumentType() == ReportProvider.PDF) {
			this.print(data, printQty, printerHostName, printDocType);
		} else {
			throw new UnsupportedOperationException("unable to print non-PDF document!");
		}
	}
	
	private void print(DocumentData data, int printQty, String printerHostName, String printDocType) throws PrintingException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			data.writeDataToStream(baos);		
			synchronized (silentPrintSender) { 
				if(printerHostName != null) {
					silentPrintSender.setPrinterHostName(printerHostName);
				}
				silentPrintSender.sendToPrinter(baos.toByteArray(), printQty, printDocType);
			}
		} catch (IOException e) {
			logger.error("unexpected error", e);
			throw new IllegalStateException("unable to print");
		}
	}
	
	private void reflectSetPrintOption(List<?> objectList, PrintOption printOption) {
		String methodName = "printOption";
		if (!objectList.isEmpty()) {
			if (PropertyUtils.isWriteable(objectList.get(0), methodName)) {			
				for (Object o : objectList) {
					try {
						PropertyUtils.setProperty(o, methodName, printOption);
					} catch (IllegalAccessException e) {
					} catch (InvocationTargetException e) {
					} catch (NoSuchMethodException e) {
					}
				}
			}
		}
	}
	
}
