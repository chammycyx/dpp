package hk.org.ha.model.pms.dpp.biz;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.cacher.DmDrugCacherInf;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecordVer;
import hk.org.ha.model.pms.dpp.udt.ReportDateType;
import hk.org.ha.model.pms.dpp.vo.DrugPrepackDailyRptCriteria;
import hk.org.ha.model.pms.dpp.vo.DrugPrepackDailyRptData;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("drugPrepackDailyRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DrugPrepackDailyRptServiceBean implements DrugPrepackDailyRptServiceLocal {
		
	@Logger
	private Log logger;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private UamInfo uamInfo;

	@PersistenceContext
	private EntityManager em;
	
	private List<DrugPrepackDailyRptData> drugPrepackDailyRptList;
	
	@SuppressWarnings("unchecked")
	public void retrieveDrugPrepackDailyRptList(DrugPrepackDailyRptCriteria drugPrepackDailyRptCriteria) {
		logger.debug("retrieveDrugPrepackDailyRptList");
		
		String namedQuery = "PrepackRecordVer.findByPreparationDate";
		if(drugPrepackDailyRptCriteria.getReportDateType() == ReportDateType.PrepackDate) {
			namedQuery = "PrepackRecordVer.findByPrepackDate";
		}
		
		//Add 1 day to End Date
		Calendar c = Calendar.getInstance(); 
		c.setTime(drugPrepackDailyRptCriteria.getEndDate());
		c.add(Calendar.DATE, 1);  
		drugPrepackDailyRptCriteria.setEndDate(c.getTime());
		
		List<PrepackRecordVer> prepackRecordVerList = em.createNamedQuery(namedQuery)
			.setParameter("instCode", uamInfo.getHospital())
			.setParameter("workstoreCode", uamInfo.getWorkstore())
			.setParameter("startDate", drugPrepackDailyRptCriteria.getStartDate())
			.setParameter("endDate", drugPrepackDailyRptCriteria.getEndDate())
			.setHint(QueryHints.FETCH, "o.prepackRecord")
			.setHint(QueryHints.FETCH, "o.drugItemInfoVer.drugItemInfo")
			.setHint(QueryHints.FETCH, "o.prepackPackSize")
			.getResultList();
		
		drugPrepackDailyRptList = new ArrayList<DrugPrepackDailyRptData>();
		
		for(PrepackRecordVer prepackRecordVer : prepackRecordVerList) {
			DrugPrepackDailyRptData data = new DrugPrepackDailyRptData();
			data.setPreparationDate(prepackRecordVer.getPreparationDate());
			data.setPrepackDate(prepackRecordVer.getActualPrepackDate());
			data.setAssignedLotNum(uamInfo.getHospital() + "-" + prepackRecordVer.getPrepackRecord().getLotNum());
			data.setPrepackStatus(prepackRecordVer.getPrepackStatus().getDisplayValue());
			data.setItemCode(prepackRecordVer.getDrugItemInfoVer().getDrugItemInfo().getItemCode());
			data.setItemDesc(dmDrugCacher.getDrugByItemCode(prepackRecordVer.getDrugItemInfoVer().getDrugItemInfo().getItemCode()).getFullDrugDesc());
			data.setOriginalPackQty(prepackRecordVer.getOriginalPackQty());
			data.setOriginalPackSize(prepackRecordVer.getOriginalPackSize());
			data.setOriginalPackUnit(prepackRecordVer.getDispenseDosageUnit());
			data.setOriginalTotalQty(prepackRecordVer.getOriginalTotalQty());
			data.setPrepackPackSize(prepackRecordVer.getPrepackPackSize().getPackSize());
			data.setPrepackPackUnit(prepackRecordVer.getPrepackPackSize().getPackUnit());
			data.setActualPrepackQty(prepackRecordVer.getActualPrepackQty());
			
			drugPrepackDailyRptList.add(data);
		}
		
		logger.debug("drugPrepackDailyRptList report list size : #0", drugPrepackDailyRptList.size());
		
	}

	public List<DrugPrepackDailyRptData> getDrugPrepackDailyRptList() {
		return drugPrepackDailyRptList;
	}
	
	@Remove
	public void destroy(){
		if(drugPrepackDailyRptList != null) {
			drugPrepackDailyRptList = null;
		}
	}
}
