package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.model.pms.dpp.vo.PrepackRecordSearchCriteria;

import javax.ejb.Local;

@Local
public interface PrepackRecordListServiceLocal {

	void retrievePrepackRecordList(String lotNum);
	
	void retrievePrepackRecordListBySearchCriteria(PrepackRecordSearchCriteria prepackRecordCriteria);
	
	void destroy();
}
