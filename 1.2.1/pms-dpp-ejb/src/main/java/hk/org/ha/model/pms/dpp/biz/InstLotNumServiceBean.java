package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.persistence.InstLotNum;

import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@Stateless
@Scope(ScopeType.STATELESS)
@Name("instLotNumService")
@RemoteDestination
@MeasureCalls
public class InstLotNumServiceBean implements InstLotNumServiceLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Integer newInstLotNum(String instCode) {
		logger.debug("newInstLotNum");
		
		Date todayStart = new java.util.Date();
		
		Integer rtnLotNum = Integer.valueOf(1);
		
		Query q = em.createNamedQuery("InstLotNum.findLotNumByInstCode")
					.setParameter("instCode", instCode)
					.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock);
		
		List<InstLotNum> instLotNumList = q.getResultList();
		
		if (instLotNumList.size() == 0) {			
			InstLotNum newInstLotNum = new InstLotNum();
			newInstLotNum.setInstCode(instCode);
			newInstLotNum.setLotNum(rtnLotNum);
			newInstLotNum.setIssueDate(new java.sql.Date(todayStart.getTime()));
			em.persist(newInstLotNum);
			em.flush();
			
		} else {
			InstLotNum instLotNum = instLotNumList.get(0);
			
			if (instLotNum.getIssueDate().toString().equals( (new java.sql.Date(todayStart.getTime())).toString())) {
				rtnLotNum = instLotNum.getLotNum() + 1;
			} else {
				instLotNum.setIssueDate(new java.sql.Date(todayStart.getTime()));
			}
			
			// Update InstLotNum Table
			instLotNum.setLotNum(rtnLotNum);
			em.persist(instLotNum);
			em.flush();
		}
		
		return rtnLotNum;
	}
	
	@Remove
	public void destroy() {
	}

}
