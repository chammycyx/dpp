package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.model.pms.uam.udt.AuthorizeStatus;
import hk.org.ha.model.pms.uam.vo.AuthenticateCriteria;
import hk.org.ha.service.biz.pms.uam.interfaces.UamServiceJmsRemote;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;

@Stateful
@Scope(ScopeType.SESSION)
@Name("userAuthenticationService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class UserAuthenticationServiceBean implements UserAuthenticationServiceLocal {
	
	@Logger
	private Log logger;

	@In
    private UamInfo uamInfo;
   
    @In
    private UamServiceJmsRemote uamServiceProxy;
    
    @SuppressWarnings("unused")
	@In
	private Identity identity;
    
    @Out(required=false)
	private Boolean isVaildUser;
    
    @Out(required=false)
	private Boolean accessRight;
    
	public void retrieveUserStatus(String corpId) {
		logger.debug("retrievePrintLogByPrepackRecordVerId #0", corpId);
		
		AuthenticateCriteria authenticateCriteria = new AuthenticateCriteria();
		authenticateCriteria.setProfileCode(uamInfo.getUserRole().split("\\.")[0]);
		authenticateCriteria.setUserName(corpId);
		AuthorizeStatus authStatus = uamServiceProxy.authenticate(authenticateCriteria);
		isVaildUser = false;
		if (authStatus == AuthorizeStatus.Succeed) {
			isVaildUser = true;
		} else {
			isVaildUser = false;
		}
		logger.debug("AuthorizeStatus #0", isVaildUser);
	}
	
	public void retrieveTargetAccessRight(String target) {
		logger.debug("retrievePrintLogByPrepackRecordVerId #0", target);
		accessRight = identity.hasPermission(target, "full");
		logger.debug("retrievePrintLogByPrepackRecordVerId - accessRight #0", accessRight);
	}
	
	@Remove
	public void destroy() {
	}
}
