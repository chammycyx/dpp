package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.model.pms.dpp.persistence.ActionTaken;

import java.util.List;

import javax.ejb.Local;

@Local
public interface ActionTakenListServiceLocal {
	
	void retrieveActionTakenList();
	
	void updateActionTakenList(List<ActionTaken> newActionTakenList);
	
	void retrieveActionTakenDetailsList(Long prepackRecordVerId);
	
	void updateActionTakenDetailsList(Long prepackRecordId, List<ActionTaken> updateActionTakenList);
	
	void destroy();
	
}
