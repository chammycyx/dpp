package hk.org.ha.model.pms.dpp.persistence;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dpp.udt.PrintType;
import hk.org.ha.model.pms.dpp.udt.RecordStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name="PRINT_LOG")
@Customizer(AuditCustomizer.class)
@NamedQueries({
	@NamedQuery(name="PrintLog.findByPrepackRecordId", query="select o from PrintLog o where o.prepackRecordVer.prepackRecord.id = :prepackRecordId order by o.createDate")
})
public class PrintLog extends VersionEntity {
	
	private static final long serialVersionUID = -8626144561324661138L;
	
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="printLogSeq")
	@SequenceGenerator(name="printLogSeq", sequenceName="SQ_PRINT_LOG", initialValue=100000000)
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PREPACK_RECORD_VER_ID", referencedColumnName="ID")
	private PrepackRecordVer prepackRecordVer;
	
	@Column(name="PRINT_QTY", nullable=false)
	private Integer printQty;
	
	@Converter(name="PrintLog.printType", converterClass=PrintType.Converter.class)
	@Convert("PrintLog.printType")
	@Column(name="PRINT_TYPE", length=1, nullable=false)
	private PrintType printType;
	
	@Converter(name="PrepackPackSize.recordStatus", converterClass=RecordStatus.Converter.class)
	@Convert("PrepackPackSize.recordStatus")
	@Column(name="RECORD_STATUS", length=1, nullable=false)
	private RecordStatus recordStatus;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setPrepackRecordVer(PrepackRecordVer prepackRecordVer) {
		this.prepackRecordVer = prepackRecordVer;
	}

	public PrepackRecordVer getPrepackRecordVer() {
		return prepackRecordVer;
	}

	public void setPrintQty(Integer printQty) {
		this.printQty = printQty;
	}

	public Integer getPrintQty() {
		return printQty;
	}

	public void setPrintType(PrintType printType) {
		this.printType = printType;
	}

	public PrintType getPrintType() {
		return printType;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}
	
}
