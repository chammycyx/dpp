package hk.org.ha.model.pms.dpp.exception;

import java.util.Map;

import org.granite.messaging.service.ExceptionConverter;
import org.granite.messaging.service.ServiceException;

public class PrintingExceptionConverter implements ExceptionConverter {

	public static final String PRINTING_EXCEPTION = "PrintingException";
	
	@Override
	public boolean accepts(Throwable t, Throwable finalException) {
		return t.getClass().equals(PrintingException.class);
	}

	@Override
	public ServiceException convert(Throwable t, String detail, Map<String, Object> extendedData) {
		ServiceException se = new ServiceException(PRINTING_EXCEPTION, t.getMessage(), detail, t);
		PrintingException printingException = (PrintingException) t;
		extendedData.put("message", printingException.getMessage());
		se.getExtendedData().putAll(extendedData);
		return se;
	}
}
