package hk.org.ha.model.pms.dpp.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback=true)
public class PrintingException extends Exception {

	private static final long serialVersionUID = 3890770006518527878L;

	private String message;
		
	public PrintingException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}