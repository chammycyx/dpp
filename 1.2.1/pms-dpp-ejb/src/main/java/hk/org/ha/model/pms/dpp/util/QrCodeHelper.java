package hk.org.ha.model.pms.dpp.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public final class QrCodeHelper {
      
      private QrCodeHelper() {};
      
      public static InputStream createQrCodeImageStream(String s, int width,
                  int height) throws IOException, WriterException {
            BitMatrix bm = new QRCodeWriter().encode(s, BarcodeFormat.QR_CODE,
                        width, height);
            BufferedImage bi = MatrixToImageWriter.toBufferedImage(bm);
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageOutputStream ios = ImageIO.createImageOutputStream(os);
            ImageIO.write(bi, "png", ios);
            return new ByteArrayInputStream(os.toByteArray());
      }
}
