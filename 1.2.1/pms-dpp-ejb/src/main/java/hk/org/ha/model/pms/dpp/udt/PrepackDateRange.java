package hk.org.ha.model.pms.dpp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum PrepackDateRange implements StringValuedEnum {
	
	SevenDays("7D", "Last 7 Days"),
	FourteenDays("14D", "Last 14 Days"),
	TwentyEightDays("28D", "Last 28 Days"),
	ThreeMonths("3M", "Last 3 Months"),
	SixMonths("6M", "Last 6 Months"),
	OneYear("1Y", "Last 1 Year"),
	Others("Others", "Others");
	
	private final String dataValue;
    private final String displayValue;
        
    PrepackDateRange(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<PrepackDateRange> {
		
		private static final long serialVersionUID = 5136223189472974495L;

		@Override
    	public Class<PrepackDateRange> getEnumClass() {
    		return PrepackDateRange.class;
    	}
    }
}