package hk.org.ha.model.pms.biz.security;

import java.io.IOException;

import javax.ejb.Local;

@Local
public interface AccessControlLocal
{
	void retrieveUserAccessControl() throws IOException;	
}
