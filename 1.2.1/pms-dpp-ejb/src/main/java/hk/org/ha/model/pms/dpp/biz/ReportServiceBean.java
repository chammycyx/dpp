package hk.org.ha.model.pms.dpp.biz;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;

import hk.org.ha.fmk.pms.web.MeasureCalls;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.context.FacesContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.document.ByteArrayDocumentData;
import org.jboss.seam.document.DocumentData;
import org.jboss.seam.document.DocumentStore;
import org.jboss.seam.faces.Renderer;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("reportService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ReportServiceBean implements ReportServiceLocal, Serializable {

	private static final long serialVersionUID = 2673718947440942840L;

	@Logger
	private Log logger;
	
	@In(create=true)
	private Renderer renderer;
	
	@Out
	private String reportWindowsProperties;

	private String reportKey; 
	
	private String reportPath;
	
	private String reportFileName;
	
	public void setReport(String reportPath, String reportKey, String reportFileName){
		this.reportKey = reportKey;
		this.reportPath = reportPath;
		this.reportFileName = reportFileName;
	}
	
	public void setReportWindowsProperties(String reportWindowsProperties) {
		this.reportWindowsProperties = reportWindowsProperties;
	}
	
	public void generateExcelReport() throws IOException{
		logger.debug("generateExcelReport");
		renderer.render(reportPath);		
		DocumentData docData = (DocumentData) Contexts.getEventContext().get(reportKey);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		docData.writeDataToStream(baos);
	
		ByteArrayDocumentData data = new ByteArrayDocumentData(
				reportFileName,
				new DocumentData.DocumentType("xls", "application/msexcel"),
				baos.toByteArray());

		DocumentStore documentStore = DocumentStore.instance();
		String contentId = documentStore.newId();
		documentStore.saveData(contentId, data);
		String url = documentStore.preferredUrlForContent(
				reportFileName,
				data.getDocumentType().getExtension(),
				contentId);   		
		
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect(url);
		} catch (IOException e) {
			logger.error("Unable to redirect", e);
		}
	}
	
	@Remove
	public void destroy(){
		if (reportWindowsProperties != null){			
			reportWindowsProperties = null;
		}
		
	}
	
}
