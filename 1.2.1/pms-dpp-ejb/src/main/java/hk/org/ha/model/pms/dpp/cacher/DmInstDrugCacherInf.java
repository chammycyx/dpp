package hk.org.ha.model.pms.dpp.cacher;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmInstDrug;

import java.util.List;

public interface DmInstDrugCacherInf extends BaseCacherInf {
  
	DmInstDrug getDrugByItemCode(String itemCode);
		
	List<DmInstDrug> getDrugListByItemCode(String prefixItemCode);
	
	List<DmInstDrug> getDrugList(String prefixItemCode);
	
	void clearAll();
}
