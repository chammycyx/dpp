package hk.org.ha.model.pms.dpp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ActionStatus implements StringValuedEnum {
	
	New("N", "New"),
	Printed("P", "Printed"),
	Completed("C", "Completed"),
	Cancelled("L", "Cancelled"),
	Updated("U", "Updated");
	
    private final String dataValue;
    private final String displayValue;
        
    ActionStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<ActionStatus> {

		private static final long serialVersionUID = -7968478287058011303L;

		@Override
    	public Class<ActionStatus> getEnumClass() {
    		return ActionStatus.class;
    	}
    }
}
