package hk.org.ha.model.pms.dpp.biz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintingAgentInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dpp.cacher.DmDrugCacherInf;
import hk.org.ha.model.pms.dpp.cacher.ManufacturerCacherInf;
import hk.org.ha.model.pms.dpp.persistence.ActionTaken;
import hk.org.ha.model.pms.dpp.persistence.Discrepancy;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecordVer;
import hk.org.ha.model.pms.dpp.udt.DisplaySideLabelAliasFlag;
import hk.org.ha.model.pms.dpp.udt.DisplaySideLabelShortDescFlag;
import hk.org.ha.model.pms.dpp.udt.LabelSize;
import hk.org.ha.model.pms.dpp.vo.DprWorksheet;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderJob;

@Stateful
@Scope(ScopeType.SESSION)
@Name("dprWorksheetService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DprWorksheetServiceBean implements DprWorksheetServiceLocal {
	
	@In
	private PrintingAgentInf printingAgent;
	
	@In
	private UamInfo uamInfo;
	
	@In
	private ManufacturerCacherInf manufacturerCacher;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@PersistenceContext
	private EntityManager em;
	
	private List<DprWorksheet> dprWorksheetList;
	
	public String checkPrepackItemVersion(PrepackRecord prepackRecord)
	{
		PrepackRecord latestPrepackRecord = (PrepackRecord) em.createNamedQuery("PrepackRecord.findById")
				.setParameter("id", prepackRecord.getId())
				.setHint(QueryHints.BATCH, "o.prepackRecordVer")
				.setHint(QueryHints.BATCH, "o.prepackRecordVer.prepackPackSize")
				.setHint(QueryHints.BATCH, "o.prepackRecordVer.prepackPackSize.drugItemInfo")
				.setHint(QueryHints.BATCH, "o.prepackRecordVer.prepackPackSize.drugItemInfo.drugItemInfoVer")
				.getResultList().get(0);
		
		if (!latestPrepackRecord.getVersion().equals(prepackRecord.getVersion()))
		{
			return "0082";
		}
		else if (prepackRecord.getPrepackRecordVer().getDrugItemInfoVer().getDrugItemInfo() != null && 
				 !latestPrepackRecord.getPrepackRecordVer().getDrugItemInfoVer().getDrugItemInfo().getVersion().equals(
				 prepackRecord.getPrepackRecordVer().getDrugItemInfoVer().getDrugItemInfo().getVersion()))
		{
			return "0082";
		}
		else if ((latestPrepackRecord.getPrepackRecordVer().getLabelSize() == LabelSize.DppWithSideLabel1Size4x1p33 || 
				 latestPrepackRecord.getPrepackRecordVer().getLabelSize() == LabelSize.DppWithSideLabel1Size4x2 ||
				 latestPrepackRecord.getPrepackRecordVer().getLabelSize() == LabelSize.DppWithSideLabel2Size4x1p33 ||
				 latestPrepackRecord.getPrepackRecordVer().getLabelSize() == LabelSize.DppWithSideLabel2Size4x2) && 
				 (latestPrepackRecord.getPrepackRecordVer().getPrepackPackSize().getDrugItemInfo().getDrugItemInfoVer().getDisplaySideLabelAliasFlag() == DisplaySideLabelAliasFlag.No && 
				 latestPrepackRecord.getPrepackRecordVer().getPrepackPackSize().getDrugItemInfo().getDrugItemInfoVer().getDisplaySideLabelShortDescFlag() == DisplaySideLabelShortDescFlag.No))
		{
			return "0081";
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public void prepareDprWorksheet(Long id) {
		dprWorksheetList = new ArrayList<DprWorksheet>();
		DprWorksheet dprWorksheet = new DprWorksheet();
		List<Discrepancy> discrepancyList = em.createNamedQuery("Discrepancy.findAllActive").getResultList();
		List<ActionTaken> actionTakenList = em.createNamedQuery("ActionTaken.findAllActive").getResultList();
		PrepackRecordVer prepackRecordVer = (PrepackRecordVer)em.createNamedQuery("PrepackRecordVer.findById")
												.setParameter("id", id)
												.setHint(QueryHints.FETCH, "o.drugItemInfoVer.drugItemInfo")
												.setHint(QueryHints.FETCH, "o.equipment")
												.setHint(QueryHints.FETCH, "o.container")
												.setHint(QueryHints.FETCH, "o.prepackRecord")
												.setHint(QueryHints.FETCH, "o.prepackPackSize")
												.getResultList().get(0);
		dprWorksheet.setDiscrepancyList(discrepancyList);
		dprWorksheet.setActionTakenList(actionTakenList);
		dprWorksheet.setItemCode(prepackRecordVer.getDrugItemInfoVer().getDrugItemInfo().getItemCode());
		dprWorksheet.setPreparationDate(prepackRecordVer.getPreparationDate());
		dprWorksheet.setBatchNum(prepackRecordVer.getBatchNum());
		dprWorksheet.setExpiryDate(prepackRecordVer.getExpiryDate());
		dprWorksheet.setManufName(manufacturerCacher.getManufacturerByManufCode(prepackRecordVer.getManufCode()).getCompanyName());
		dprWorksheet.setEquipmentName(prepackRecordVer.getEquipment().getEquipmentName());
		dprWorksheet.setContainerName(prepackRecordVer.getContainer().getContainerName());
		dprWorksheet.setLotNum(prepackRecordVer.getPrepackRecord().getLotNum());
		dprWorksheet.setAssignExpiryDate(prepackRecordVer.getAssignExpiryDate());
		dprWorksheet.setProvisionPrepackLabelQty(prepackRecordVer.getProvisionPrepackLabelQty());
		dprWorksheet.setProvisionPrepackQty(prepackRecordVer.getProvisionPrepackQty());
		dprWorksheet.setProvisionRemainQty(prepackRecordVer.getProvisionRemainQty());
		dprWorksheet.setSampleCheckQty(prepackRecordVer.getSampleCheckQty());
		dprWorksheet.setPrepareBy(prepackRecordVer.getPrepareBy());
		dprWorksheet.setPackSize(prepackRecordVer.getPrepackPackSize().getPackSize());
		dprWorksheet.setPackUnit(prepackRecordVer.getPrepackPackSize().getPackUnit());
		dprWorksheet.setOriginalPackQty(prepackRecordVer.getOriginalPackQty());
		dprWorksheet.setOriginalPackSize(prepackRecordVer.getOriginalPackSize());
		dprWorksheet.setOriginalTotalQty(prepackRecordVer.getOriginalTotalQty());
		dprWorksheet.setDispenseDosageUnit(prepackRecordVer.getDispenseDosageUnit());
		dprWorksheet.setInstCode(uamInfo.getHospital());
		dprWorksheet.setWorkstoreCode(uamInfo.getWorkstore());
		
		DmDrug dmDrug = dmDrugCacher.getDrugByItemCode(dprWorksheet.getItemCode());
		dprWorksheet.setItemDesc(dmDrug.getFullDrugDesc());
		
		dprWorksheetList.add(dprWorksheet);
	}
	
	public void generateDprWorksheet() {
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		String contentId = printingAgent.render(new RenderJob(
				"DprWorksheet", 
				new PrintOption(),  
				parameters, 
				dprWorksheetList));
		
		printingAgent.redirect(contentId);
		
	}
	
	@Remove
	public void destroy() {
		
	}
}
