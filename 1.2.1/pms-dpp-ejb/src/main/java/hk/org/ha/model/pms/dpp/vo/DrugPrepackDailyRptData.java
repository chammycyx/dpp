package hk.org.ha.model.pms.dpp.vo;

import java.math.BigDecimal;
import java.util.Date;

public class DrugPrepackDailyRptData {
	
	private Date preparationDate;
	private Date prepackDate;
	private String assignedLotNum;
	private String prepackStatus;
	private String itemCode;
	private String itemDesc;
	private Integer originalPackQty;
	private Integer originalPackSize;
	private String originalPackUnit;
	private Integer originalTotalQty;
	private BigDecimal prepackPackSize;
	private String prepackPackUnit;
	private Integer actualPrepackQty;
	
	public void setPreparationDate(Date preparationDate) {
		this.preparationDate = preparationDate == null ? null : new Date(preparationDate.getTime());
	}
	
	public Date getPreparationDate() {
		return preparationDate == null ? null : new Date(preparationDate.getTime());
	}
	
	public void setPrepackDate(Date prepackDate) {
		this.prepackDate = prepackDate == null ? null : new Date(prepackDate.getTime());
	}
	
	public Date getPrepackDate() {
		return prepackDate == null ? null : new Date(prepackDate.getTime());
	}
	
	public void setAssignedLotNum(String assignedLotNum) {
		this.assignedLotNum = assignedLotNum;
	}
	
	public String getAssignedLotNum() {
		return assignedLotNum;
	}
	
	public void setPrepackStatus(String prepackStatus) {
		this.prepackStatus = prepackStatus;
	}

	public String getPrepackStatus() {
		return prepackStatus;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	
	public String getItemCode() {
		return itemCode;
	}
	
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	
	public String getItemDesc() {
		return itemDesc;
	}
	
	public void setOriginalPackQty(Integer originalPackQty) {
		this.originalPackQty = originalPackQty;
	}
	
	public Integer getOriginalPackQty() {
		return originalPackQty;
	}
	
	public void setOriginalPackSize(Integer originalPackSize) {
		this.originalPackSize = originalPackSize;
	}
	
	public Integer getOriginalPackSize() {
		return originalPackSize;
	}
	
	public void setOriginalPackUnit(String originalPackUnit) {
		this.originalPackUnit = originalPackUnit;
	}

	public String getOriginalPackUnit() {
		return originalPackUnit;
	}

	public void setOriginalTotalQty(Integer originalTotalQty) {
		this.originalTotalQty = originalTotalQty;
	}
	
	public Integer getOriginalTotalQty() {
		return originalTotalQty;
	}
	
	public void setPrepackPackSize(BigDecimal prepackPackSize) {
		this.prepackPackSize = prepackPackSize;
	}
	
	public BigDecimal getPrepackPackSize() {
		return prepackPackSize;
	}
	
	public void setPrepackPackUnit(String prepackPackUnit) {
		this.prepackPackUnit = prepackPackUnit;
	}
	
	public String getPrepackPackUnit() {
		return prepackPackUnit;
	}

	public void setActualPrepackQty(Integer actualPrepackQty) {
		this.actualPrepackQty = actualPrepackQty;
	}

	public Integer getActualPrepackQty() {
		return actualPrepackQty;
	}

}
