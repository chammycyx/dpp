package hk.org.ha.model.pms.biz.sys;

import hk.org.ha.fmk.pms.sys.SysMsgResolver;

import javax.ejb.Local;

@Local
public interface SystemMessageServiceLocal extends SysMsgResolver{
	
	void retrieveSystemMessage(String messageCode, Object ...params);
}