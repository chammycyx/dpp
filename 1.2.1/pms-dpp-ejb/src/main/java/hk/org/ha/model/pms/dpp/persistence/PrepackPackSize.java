package hk.org.ha.model.pms.dpp.persistence;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dpp.udt.RecordStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name="PREPACK_PACK_SIZE")
@Customizer(AuditCustomizer.class)
@NamedQueries({
	@NamedQuery(name="PrepackPackSize.findByDrugItemInfoId", query="select o from PrepackPackSize o where o.drugItemInfo = :drugItemInfo and o.recordStatus = hk.org.ha.model.pms.dpp.udt.RecordStatus.Active order by o.packUnit, o.packSize ASC"),
	@NamedQuery(name="PrepackPackSize.findByUpdateDate", query="select o from PrepackPackSize o where o.updateDate >= :startDate and o.updateDate < :endDate ")
})
public class PrepackPackSize extends VersionEntity {
	
	private static final long serialVersionUID = -7329850446977980531L;
	
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="prepackPackSizeSeq")
	@SequenceGenerator(name="prepackPackSizeSeq", sequenceName="SQ_PREPACK_PACK_SIZE", initialValue=100000000)
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DRUG_ITEM_INFO_ID", referencedColumnName="ID")
	private DrugItemInfo drugItemInfo;
	
	@Column(name="PACK_SIZE", precision=19, scale=4, nullable=false)
	private BigDecimal packSize;
	
	@Column(name="PACK_UNIT", length=15, nullable=false)
	private String packUnit;
	
	@Converter(name="PrepackPackSize.recordStatus", converterClass=RecordStatus.Converter.class)
	@Convert("PrepackPackSize.recordStatus")
	@Column(name="RECORD_STATUS", length=1, nullable=false)
	private RecordStatus recordStatus;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setDrugItemInfo(DrugItemInfo drugItemInfo) {
		this.drugItemInfo = drugItemInfo;
	}

	public DrugItemInfo getDrugItemInfo() {
		return drugItemInfo;
	}

	public void setPackSize(BigDecimal packSize) {
		this.packSize = packSize;
	}

	public BigDecimal getPackSize() {
		return packSize;
	}

	public void setPackUnit(String packUnit) {
		this.packUnit = packUnit;
	}

	public String getPackUnit() {
		return packUnit;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}
	
}
