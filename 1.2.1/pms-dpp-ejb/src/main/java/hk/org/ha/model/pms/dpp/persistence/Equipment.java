package hk.org.ha.model.pms.dpp.persistence;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Entity;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dpp.udt.RecordStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name="EQUIPMENT")
@Customizer(AuditCustomizer.class)
@NamedQueries({
	@NamedQuery(name="Equipment.findAll", query="select o from Equipment o where o.recordStatus = hk.org.ha.model.pms.dpp.udt.RecordStatus.Active order by o.equipmentName ASC "),
	@NamedQuery(name="Equipment.findEquipmentNameNotInList", query="select UPPER(o.equipmentName) from Equipment o where o.recordStatus = hk.org.ha.model.pms.dpp.udt.RecordStatus.Active and o.id not in :id ")
})
public class Equipment extends VersionEntity {

	private static final long serialVersionUID = -7196941280343254402L;

	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="equipmentSeq")
	@SequenceGenerator(name="equipmentSeq", sequenceName="SQ_EQUIPMENT", initialValue=100000000)
	private Long id;
	
	@Column(name="EQUIPMENT_NAME", length=60, nullable=false)
	private String equipmentName;
	
	@Converter(name="Equipment.recordStatus", converterClass=RecordStatus.Converter.class)
	@Convert("Equipment.recordStatus")
	@Column(name="RECORD_STATUS", length=1, nullable=false)
	private RecordStatus recordStatus;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}

	public String getEquipmentName() {
		return equipmentName;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

}
