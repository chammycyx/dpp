package hk.org.ha.model.pms.dpp.biz;

import javax.ejb.Local;

@Local
public interface InstLotNumServiceLocal {
	
	Integer newInstLotNum(String instCode);
	void destroy();
	
}
