package hk.org.ha.model.pms.dpp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum LabelSize implements StringValuedEnum {
	
	DppLabel1Size4x2("1", "Full Size Label (4 x 2)", 1, "longDesc", "DppLabel1Size4x2"),
	DppLabel2Size4x2("2", "1/2 Size Label (4 x 2)", 2, "shortDesc", "DppLabel2Size4x2"),
	DppLabel3Size4x2("3", "1/3 Size Label (4 x 2)", 3, "shortDesc", "DppLabel3Size4x2"),
	DppWithSideLabel1Size4x2("6", "Full Size Label with Side Label (4 x 2)", 1, "longDesc", "DppWithSideLabel1Size4x2"),
	DppWithSideLabel2Size4x2("7", "1/2 Size Label with Side Label (4 x 2)", 1, "shortDesc", "DppWithSideLabel2Size4x2"),
	DppLabel1Size4x1p33("4", "Full Size Label (4 x 1.33)", 1, "longDesc", "DppLabel1Size4x1p33"),
	DppLabel2Size4x1p33("5", "1/2 Size Label (4 x 1.33)", 2, "shortDesc", "DppLabel2Size4x1p33"),
	DppWithSideLabel1Size4x1p33("8", "Full Size Label with Side Label (4 x 1.33)", 1, "longDesc", "DppWithSideLabel1Size4x1p33"),
	DppWithSideLabel2Size4x1p33("9", "1/2 Size Label with Side Label (4 x 1.33)", 1, "shortDesc", "DppWithSideLabel2Size4x1p33");
	
    private final String dataValue;
    private final String displayValue;
    private final int labelQty;
    private final String itemDescType;
    private final String labelJrxml;

	LabelSize(final String dataValue, final String displayValue, final int labelQty, final String itemDescType, final String labelJrxml){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
        this.labelQty = labelQty;
        this.itemDescType=itemDescType;
        this.labelJrxml = labelJrxml;
    }        
    
	@Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
    public int getLabelQty() {
		return labelQty;
	}
    
	public String getItemDescType() {
		return itemDescType;
	}
	
    public String getLabelJrxml() {
		return labelJrxml;
	}
	
	public static class Converter extends StringValuedEnumConverter<LabelSize> {
		
		private static final long serialVersionUID = 1081125290550871192L;

		@Override
    	public Class<LabelSize> getEnumClass() {
    		return LabelSize.class;
    	}
    }
}
