package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.service.biz.pms.dpp.interfaces.DppSubscriberJmsRemote;

import javax.ejb.Local;

@Local
public interface DppSubscriberLocal extends DppSubscriberJmsRemote {
}
