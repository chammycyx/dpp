package hk.org.ha.model.pms.persistence.sys;

import java.util.Date;

import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@NamedQueries({
@NamedQuery(name = "SystemMessage.findAll", query = "select o from SystemMessage o order by o.messageCode"),
@NamedQuery(name = "SystemMessage.findByMessageCode", query = "select o from SystemMessage o where o.messageCode = :messageCode order by o.messageCode")
})
@Table(name = "SYSTEM_MESSAGE")
public class SystemMessage extends VersionEntity {

	private static final long serialVersionUID = 5164572339457405381L;

	@Id
    @Column(name="MESSAGE_CODE", length = 5, nullable = false)
    private String messageCode;

	@Column(name="APPLICATION_ID", nullable=false)
    private Integer applicationId;
		
    @Column(name="FUNCTION_ID", nullable=false)
    private Integer functionId;
    
    @Column(name="OPERATION_ID")
    private Integer operationId;
    
    @Column(name="SEVERITY_CODE", length = 1, nullable=false)
    private String severityCode;
	
    @Column(name="MAIN_MSG", length = 300, nullable = false)
    private String mainMsg;
    
    @Column(name="LOCALE", length = 5, nullable = false)
    private String locale;
    
    @Column(name="SUPPL_MSG")
    private String supplMsg;
    
    @Column(name="DETAIL")
    private String detail;
    
    @Column(name="BTN_YES_CAPTION", length = 30)
    private String btnYesCaption;
    
    @Column(name="BTN_YES_SIZE", length = 1)
    private String btnYesSize;
    
    @Column(name="BTN_YES_SHORTCUT", length = 1)
    private String btnYesShortcut;
    
    @Column(name="BTN_NO_CAPTION", length = 30)
    private String btnNoCaption;
    
    @Column(name="BTN_NO_SIZE", length = 1)
    private String btnNoSize;
    
    @Column(name="BTN_NO_SHORTCUT", length = 1)
    private String btnNoShortcut;
    
    @Column(name="BTN_CANCEL_CAPTION", length = 30)
    private String btnCancelCaption;
    
    @Column(name="BTN_CANCEL_SIZE", length = 1)
    private String btnCancelSize;
    
    @Column(name="BTN_CANCEL_SHORTCUT", length = 1)
    private String btnCancelShortcut;
    
    @Column(name="EFFECTIVE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date effective;
    
    @Column(name="EXPIRY")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiry;  
    
    @Transient
    private String displayDesc; 
    
	public SystemMessage() {
    }
    
    public SystemMessage(String messageCode){    	
    	this.messageCode = messageCode;
    }
    
    public SystemMessage(String messageCode, String mainMsg){    	
    	this.messageCode = messageCode;
    	this.mainMsg = mainMsg;
    }

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMainMsg() {		
		return mainMsg;
	}

	public void setMainMsg(String mainMsg) {
		this.mainMsg = mainMsg;
	}
	
	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getFunctionId() {
		return functionId;
	}

	public void setFunctionId(Integer functionId) {
		this.functionId = functionId;
	}

	public Integer getOperationId() {
		return operationId;
	}

	public void setOperationId(Integer operationId) {
		this.operationId = operationId;
	}

	public String getSeverityCode() {
		return severityCode;
	}

	public void setSeverityCode(String severityCode) {
		this.severityCode = severityCode;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}


	public void setSupplMsg(String supplMsg) {
		this.supplMsg = supplMsg;
	}

	public String getSupplMsg() {
		return supplMsg;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getDetail() {
		return detail;
	}

	public String getBtnYesCaption() {
		return btnYesCaption;
	}

	public void setBtnYesCaption(String btnYesCaption) {
		this.btnYesCaption = btnYesCaption;
	}

	public String getBtnYesSize() {
		return btnYesSize;
	}

	public void setBtnYesSize(String btnYesSize) {
		this.btnYesSize = btnYesSize;
	}

	public String getBtnYesShortcut() {
		return btnYesShortcut;
	}

	public void setBtnYesShortcut(String btnYesShortcut) {
		this.btnYesShortcut = btnYesShortcut;
	}

	public String getBtnNoCaption() {
		return btnNoCaption;
	}

	public void setBtnNoCaption(String btnNoCaption) {
		this.btnNoCaption = btnNoCaption;
	}

	public String getBtnNoSize() {
		return btnNoSize;
	}

	public void setBtnNoSize(String btnNoSize) {
		this.btnNoSize = btnNoSize;
	}

	public String getBtnNoShortcut() {
		return btnNoShortcut;
	}

	public void setBtnNoShortcut(String btnNoShortcut) {
		this.btnNoShortcut = btnNoShortcut;
	}

	public String getBtnCancelCaption() {
		return btnCancelCaption;
	}

	public void setBtnCancelCaption(String btnCancelCaption) {
		this.btnCancelCaption = btnCancelCaption;
	}

	public String getBtnCancelSize() {
		return btnCancelSize;
	}

	public void setBtnCancelSize(String btnCancelSize) {
		this.btnCancelSize = btnCancelSize;
	}

	public String getBtnCancelShortcut() {
		return btnCancelShortcut;
	}

	public void setBtnCancelShortcut(String btnCancelShortcut) {
		this.btnCancelShortcut = btnCancelShortcut;
	}

	public Date getEffective() {
		return effective;
	}

	public void setEffective(Date effective) {
		this.effective = effective;
	}

	public Date getExpiry() {
		return expiry;
	}

	public void setExpiry(Date expiry) {
		this.expiry = expiry;
	}

	@Transient
	public String getDisplayDesc() {
		return displayDesc;
	}
	@Transient
	public void setDisplayDesc(String displayDesc) {
		this.displayDesc = displayDesc;
	}
}
