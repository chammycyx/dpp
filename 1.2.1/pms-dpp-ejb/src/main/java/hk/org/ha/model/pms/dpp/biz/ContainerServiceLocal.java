package hk.org.ha.model.pms.dpp.biz;

import javax.ejb.Local;

@Local
public interface ContainerServiceLocal {
	
	void retrieveContainerByPrepackRecordVerId(Long id);
	
	void destroy();
	
}
