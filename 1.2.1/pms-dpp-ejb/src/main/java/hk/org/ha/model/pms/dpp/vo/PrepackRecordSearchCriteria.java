package hk.org.ha.model.pms.dpp.vo;

import hk.org.ha.model.pms.dpp.udt.PrepackStatus;

import java.math.BigDecimal;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PrepackRecordSearchCriteria {
	
	private String institutionCode;
	private String workstoreCode;
	private String itemCode;
	private BigDecimal packSize;
	private String packUnit;
	private String lotNum;
	private String batchNum;
	private Date fromPreparationDate;
	private Date toPreparationDate;
	private Date fromPrepackDate;
	private Date toPrepackDate;
	private PrepackStatus prepackStatus;
	
	public String getInstitutionCode() {
		return institutionCode;
	}
	
	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}
	
	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public String getItemCode() {
		return itemCode;
	}
	
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public BigDecimal getPackSize() {
		return packSize;
	}

	public void setPackSize(BigDecimal packSize) {
		this.packSize = packSize;
	}

	public String getPackUnit() {
		return packUnit;
	}
	
	public void setPackUnit(String packUnit) {
		this.packUnit = packUnit;
	}
	
	public String getLotNum() {
		return lotNum;
	}

	public void setLotNum(String lotNum) {
		this.lotNum = lotNum;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public Date getFromPreparationDate() {
		return fromPreparationDate == null ? null : new Date(fromPreparationDate.getTime());
	}

	public void setFromPreparationDate(Date fromPreparationDate) {
		this.fromPreparationDate = fromPreparationDate == null ? null : new Date(fromPreparationDate.getTime());
	}

	public Date getToPreparationDate() {
		return toPreparationDate == null ? null : new Date(toPreparationDate.getTime());
	}

	public void setToPreparationDate(Date toPreparationDate) {
		this.toPreparationDate = toPreparationDate == null ? null : new Date(toPreparationDate.getTime());
	}

	public Date getFromPrepackDate() {
		return fromPrepackDate == null ? null : new Date(fromPrepackDate.getTime());
	}

	public void setFromPrepackDate(Date fromPrepackDate) {
		this.fromPrepackDate = fromPrepackDate == null ? null : new Date(fromPrepackDate.getTime());
	}

	public Date getToPrepackDate() {
		return toPrepackDate == null ? null : new Date(toPrepackDate.getTime());
	}

	public void setToPrepackDate(Date toPrepackDate) {
		this.toPrepackDate = toPrepackDate == null ? null : new Date(toPrepackDate.getTime());
	}

	public PrepackStatus getPrepackStatus() {
		return prepackStatus;
	}

	public void setPrepackStatus(PrepackStatus prepackStatus) {
		this.prepackStatus = prepackStatus;
	}

}
