package hk.org.ha.model.pms.dpp.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.eclipse.persistence.annotations.Customizer;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmInstDrug;
import hk.org.ha.model.pms.dpp.cacher.DmDrugCacher;
import hk.org.ha.model.pms.dpp.cacher.DmInstDrugCacher;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name="DRUG_ITEM_INFO", uniqueConstraints=@UniqueConstraint(columnNames={"ITEM_CODE","INST_CODE"}))
@Customizer(AuditCustomizer.class)
@NamedQueries({
	@NamedQuery(name="DrugItemInfo.findById", query="select o from DrugItemInfo o where o.id = :id "),
	@NamedQuery(name="DrugItemInfo.findByInstCodeItemCode", query="select o from DrugItemInfo o where o.instCode = :instCode and o.workstoreCode = :workstoreCode and o.itemCode = :itemCode ")
})
public class DrugItemInfo extends VersionEntity {
	
	private static final long serialVersionUID = -7151399107838809522L;

	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="drugItemInfoSeq")
	@SequenceGenerator(name="drugItemInfoSeq", sequenceName="SQ_DRUG_ITEM_INFO", initialValue=100000000)
	private Long id;
	
	@OneToMany(cascade=CascadeType.PERSIST, mappedBy="drugItemInfo", fetch=FetchType.LAZY)
	private List<PrepackPackSize> prepackPackSizeList = new ArrayList<PrepackPackSize>();
	
	@OneToOne(cascade=CascadeType.PERSIST, fetch=FetchType.EAGER)
	@JoinColumn(name="DRUG_ITEM_INFO_VER_ID", referencedColumnName="ID", nullable=false)
	private DrugItemInfoVer drugItemInfoVer = new DrugItemInfoVer();
	
	@OneToMany(mappedBy="drugItemInfo", fetch=FetchType.LAZY)
	private List<DrugItemInfoVer> drugItemInfoVerList = new ArrayList<DrugItemInfoVer>();
	
	@Column(name="INST_CODE", length=3, nullable=false)
	private String instCode;
	
	@Column(name="WORKSTORE_CODE", length=4, nullable=false)
	private String workstoreCode;
	
	@Column(name="ITEM_CODE", length=6, nullable=false)
	private String itemCode;
	
	@Transient
	private DmDrug dmDrug;
	//Due to front-end granite problems, DmDrug and DmInstDrug(DmInstDrug.getDmDrug) can't be co-existed
	
	@Transient
	private String instSuspend;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setPrepackPackSizeList(List<PrepackPackSize> prepackPackSizeList) {
		this.prepackPackSizeList = prepackPackSizeList;
	}

	public List<PrepackPackSize> getPrepackPackSizeList() {
		return prepackPackSizeList;
	}
	
	public void addPrepackPackSize(PrepackPackSize prepackPackSize) {
		prepackPackSize.setDrugItemInfo(this);
		this.getPrepackPackSizeList().add(prepackPackSize);
	}
	
	public void removePrepackPackSize(PrepackPackSize prepackPackSize) {
		this.getPrepackPackSizeList().remove(prepackPackSize);
	}

	public void setDrugItemInfoVer(DrugItemInfoVer drugItemInfoVer) {
		this.drugItemInfoVer = drugItemInfoVer;
	}

	public DrugItemInfoVer getDrugItemInfoVer() {
		return drugItemInfoVer;
	}

	public void setDrugItemInfoVerList(List<DrugItemInfoVer> drugItemInfoVerList) {
		this.drugItemInfoVerList = drugItemInfoVerList;
	}

	public List<DrugItemInfoVer> getDrugItemInfoVerList() {
		return drugItemInfoVerList;
	}
	
	public void addDrugItemInfoVer(DrugItemInfoVer drugItemInfoVer) {
		drugItemInfoVer.setDrugItemInfo(this);
		this.getDrugItemInfoVerList().add(drugItemInfoVer);
	}
	
	public void removeDrugItemInfoVer(DrugItemInfoVer drugItemInfoVer) {
		this.getDrugItemInfoVerList().remove(drugItemInfoVer);
	}

	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}

	public String getInstCode() {
		return instCode;
	}

	public void setWorkstoreCode(String workStoreCode) {
		this.workstoreCode = workStoreCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setDmDrug(DmDrug dmDrug) {
		this.dmDrug = dmDrug;
	}

	public DmDrug getDmDrug() {
		if (this.dmDrug == null) {
			this.dmDrug = DmDrugCacher.instance().getDrugByItemCode(this.itemCode);
		}
		return dmDrug;
	}

	public void setInstSuspend(String instSuspend) {
		this.instSuspend = instSuspend; 
	}

	public String getInstSuspend() {
		if (instSuspend == null) {
			DmInstDrug dmInstDrug = DmInstDrugCacher.instance().getDrugByItemCode(itemCode);
			if(dmInstDrug == null || "Y".equals(dmInstDrug.getInstSuspend())) {
				setInstSuspend("Y");
			} else {
				setInstSuspend("N");
			}
		}
		return instSuspend;
	}
}
