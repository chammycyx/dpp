package hk.org.ha.model.pms.dpp.biz;

import javax.ejb.Local;

@Local
public interface PrepackPackSizeListServiceLocal {

	void retrievePrepackPackSizeListByDrugItemInfoId(Long drugItemInfoId);
	
	void destroy();
}
