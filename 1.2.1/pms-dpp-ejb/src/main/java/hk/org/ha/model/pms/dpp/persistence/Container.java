package hk.org.ha.model.pms.dpp.persistence;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Entity;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dpp.udt.RecordStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name="CONTAINER")
@Customizer(AuditCustomizer.class)
@NamedQueries({
	@NamedQuery(name="Container.findAll", query="select o from Container o where o.recordStatus = hk.org.ha.model.pms.dpp.udt.RecordStatus.Active order by o.containerName ASC"),
	@NamedQuery(name="Container.findContainerNameNotInList", query="select UPPER(o.containerName) from Container o where o.recordStatus = hk.org.ha.model.pms.dpp.udt.RecordStatus.Active and o.id not in :id ")
})
public class Container extends VersionEntity {

	private static final long serialVersionUID = 6699172809761372978L;
	
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="containerSeq")
	@SequenceGenerator(name="containerSeq", sequenceName="SQ_CONTAINER", initialValue=100000000)
	private Long id;
	
	@Column(name="CONTAINER_NAME", length=60, nullable=false)
	private String containerName;
	
	@Converter(name="Container.recordStatus", converterClass=RecordStatus.Converter.class)
	@Convert("Container.recordStatus")
	@Column(name="RECORD_STATUS", length=1, nullable=false)
	private RecordStatus recordStatus;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}

	public String getContainerName() {
		return containerName;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

}
