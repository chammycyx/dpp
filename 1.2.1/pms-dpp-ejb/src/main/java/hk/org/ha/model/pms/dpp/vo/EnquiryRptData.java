package hk.org.ha.model.pms.dpp.vo;

import java.math.BigDecimal;
import java.util.Date;

public class EnquiryRptData {
	
	private String itemCode;
	private String assignedLotNum;
	private Date preparationDate;
	private String prepackStatus;
	private String manufName;
	private String batchNum;
	private Date originalExpiryDate;
	private Integer originalPackQty;
	private Integer originalPackSize;
	private String dispenseDosageUnit;
	private Integer originalTotalQty;
	private String equipmentName;
	private String containerName;
	private Date assignedExpriyDate;
	private BigDecimal prepackPackSize;
	private String prepackPackUnit;
	private Integer sampleCheckQty;
	private Integer provisionalPrepackQty;
	private Integer provisionalPrepackLabelQty;
	private BigDecimal provisionRemainQty;
	private Date actualPrepackDate;
	private Integer actualPrepackQty;
	private Integer actualLabelRemainQty;
	private Integer actualAddLabelQty;
	private BigDecimal actualItemRemainQty;
	private String prepareBy;
	private String preCheckBy;
	private String lineClearCheckBy;
	private String prepackBy;
	private String finalCheckBy;
	private String cancelReason;
	
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	
	public String getItemCode() {
		return itemCode;
	}

	public void setAssignedLotNum(String assignedLotNum) {
		this.assignedLotNum = assignedLotNum;
	}

	public String getAssignedLotNum() {
		return assignedLotNum;
	}

	public void setPreparationDate(Date preparationDate) {
		this.preparationDate = preparationDate == null ? null : new Date(preparationDate.getTime());
	}

	public Date getPreparationDate() {
		return preparationDate == null ? null : new Date(preparationDate.getTime());
	}

	public void setPrepackStatus(String prepackStatus) {
		this.prepackStatus = prepackStatus;
	}

	public String getPrepackStatus() {
		return prepackStatus;
	}

	public void setManufName(String manufName) {
		this.manufName = manufName;
	}

	public String getManufName() {
		return manufName;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setOriginalExpiryDate(Date originalExpiryDate) {
		this.originalExpiryDate = originalExpiryDate == null ? null : new Date(originalExpiryDate.getTime());
	}

	public Date getOriginalExpiryDate() {
		return originalExpiryDate == null ? null : new Date(originalExpiryDate.getTime());
	}

	public void setOriginalPackQty(Integer originalPackQty) {
		this.originalPackQty = originalPackQty;
	}

	public Integer getOriginalPackQty() {
		return originalPackQty;
	}

	public void setOriginalPackSize(Integer originalPackSize) {
		this.originalPackSize = originalPackSize;
	}

	public Integer getOriginalPackSize() {
		return originalPackSize;
	}

	public void setDispenseDosageUnit(String dispenseDosageUnit) {
		this.dispenseDosageUnit = dispenseDosageUnit;
	}

	public String getDispenseDosageUnit() {
		return dispenseDosageUnit;
	}

	public void setOriginalTotalQty(Integer originalTotalQty) {
		this.originalTotalQty = originalTotalQty;
	}

	public Integer getOriginalTotalQty() {
		return originalTotalQty;
	}

	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}

	public String getEquipmentName() {
		return equipmentName;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}

	public String getContainerName() {
		return containerName;
	}

	public void setAssignedExpriyDate(Date assignedExpriyDate) {
		this.assignedExpriyDate = assignedExpriyDate == null ? null : new Date(assignedExpriyDate.getTime());
	}

	public Date getAssignedExpriyDate() {
		return assignedExpriyDate == null ? null : new Date(assignedExpriyDate.getTime());
	}

	public void setPrepackPackSize(BigDecimal prepackPackSize) {
		this.prepackPackSize = prepackPackSize;
	}

	public BigDecimal getPrepackPackSize() {
		return prepackPackSize;
	}

	public void setPrepackPackUnit(String prepackPackUnit) {
		this.prepackPackUnit = prepackPackUnit;
	}

	public String getPrepackPackUnit() {
		return prepackPackUnit;
	}

	public void setSampleCheckQty(Integer sampleCheckQty) {
		this.sampleCheckQty = sampleCheckQty;
	}

	public Integer getSampleCheckQty() {
		return sampleCheckQty;
	}

	public void setProvisionalPrepackQty(Integer provisionalPrepackQty) {
		this.provisionalPrepackQty = provisionalPrepackQty;
	}

	public Integer getProvisionalPrepackQty() {
		return provisionalPrepackQty;
	}

	public void setProvisionalPrepackLabelQty(Integer provisionalPrepackLabelQty) {
		this.provisionalPrepackLabelQty = provisionalPrepackLabelQty;
	}

	public Integer getProvisionalPrepackLabelQty() {
		return provisionalPrepackLabelQty;
	}

	public void setProvisionRemainQty(BigDecimal provisionRemainQty) {
		this.provisionRemainQty = provisionRemainQty;
	}

	public BigDecimal getProvisionRemainQty() {
		return provisionRemainQty;
	}

	public void setActualPrepackDate(Date actualPrepackDate) {
		this.actualPrepackDate = actualPrepackDate == null ? null : new Date(actualPrepackDate.getTime());
	}

	public Date getActualPrepackDate() {
		return actualPrepackDate == null ? null : new Date(actualPrepackDate.getTime());
	}

	public void setActualPrepackQty(Integer actualPrepackQty) {
		this.actualPrepackQty = actualPrepackQty;
	}

	public Integer getActualPrepackQty() {
		return actualPrepackQty;
	}

	public void setActualLabelRemainQty(Integer actualLabelRemainQty) {
		this.actualLabelRemainQty = actualLabelRemainQty;
	}

	public Integer getActualLabelRemainQty() {
		return actualLabelRemainQty;
	}

	public void setActualAddLabelQty(Integer actualAddLabelQty) {
		this.actualAddLabelQty = actualAddLabelQty;
	}

	public Integer getActualAddLabelQty() {
		return actualAddLabelQty;
	}

	public void setActualItemRemainQty(BigDecimal actualItemRemainQty) {
		this.actualItemRemainQty = actualItemRemainQty;
	}

	public BigDecimal getActualItemRemainQty() {
		return actualItemRemainQty;
	}

	public void setPrepareBy(String prepareBy) {
		this.prepareBy = prepareBy;
	}

	public String getPrepareBy() {
		return prepareBy;
	}

	public void setPreCheckBy(String preCheckBy) {
		this.preCheckBy = preCheckBy;
	}

	public String getPreCheckBy() {
		return preCheckBy;
	}

	public void setLineClearCheckBy(String lineClearCheckBy) {
		this.lineClearCheckBy = lineClearCheckBy;
	}

	public String getLineClearCheckBy() {
		return lineClearCheckBy;
	}

	public void setPrepackBy(String prepackBy) {
		this.prepackBy = prepackBy;
	}

	public String getPrepackBy() {
		return prepackBy;
	}

	public void setFinalCheckBy(String finalCheckBy) {
		this.finalCheckBy = finalCheckBy;
	}

	public String getFinalCheckBy() {
		return finalCheckBy;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public String getCancelReason() {
		return cancelReason;
	}

}
