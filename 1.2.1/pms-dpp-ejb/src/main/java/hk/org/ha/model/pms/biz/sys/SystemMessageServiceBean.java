package hk.org.ha.model.pms.biz.sys;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.sys.SystemMessage;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.core.Interpolator;

/**
 * Session Bean implementation class SystemMessageService
 */
@Stateless
@Name("systemMessageService")
@RemoteDestination
@MeasureCalls
public class SystemMessageServiceBean implements SystemMessageServiceLocal {

	@PersistenceContext
	private EntityManager em;

	@Out(required = false, scope = ScopeType.SESSION)
	private SystemMessage systemMessage;

	public void retrieveSystemMessage(String messageCode,
			Object... params) {
		systemMessage = em.find(SystemMessage.class, messageCode);
		if (systemMessage != null) {
			systemMessage.setDisplayDesc(Interpolator.instance().interpolate(
					convertHtmlFormat(), params));
		}
	}

	private String convertHtmlFormat() {
		StringBuffer resultString = new StringBuffer();

		if (systemMessage != null) {
			resultString.append("<font size='16'><b>");
			resultString.append(systemMessage.getMainMsg());
			resultString.append("</b></font><br><br>");
			if (systemMessage.getSupplMsg() != null
					&& !systemMessage.getSupplMsg().equals("")) {
				resultString.append(systemMessage.getSupplMsg());
			}
		}
		return resultString.toString();
	}

	public String resolve(String messageCode, Object... params) {
		SystemMessage tempSystemMessage = em.find(SystemMessage.class, messageCode);
		return Interpolator.instance().interpolate(tempSystemMessage.getMainMsg(),
				params);
	}

	public String retrieveMessageDesc(String messageCode) {
		SystemMessage tempSystemMessage = em.find(SystemMessage.class, messageCode);
		return tempSystemMessage.getMainMsg();
	}
}