package hk.org.ha.model.pms.dpp.biz;
import javax.ejb.Local;

@Local
public interface DmDrugListServiceLocal {
	
	void retrieveDmDrugListLikeItemCode(String prefixItemCode);
	
	void destroy();

}
