package hk.org.ha.model.pms.dpp.biz;

import java.util.ArrayList;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.persistence.Equipment;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("equipmentListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class EquipmentListServiceBean implements EquipmentListServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@Out(required=false)
	private List<Equipment> equipmentList;
	
	@SuppressWarnings("unchecked")
	public void retrieveEquipmentList() {
		logger.debug("retrieveEquipmentList");
		equipmentList = em.createNamedQuery("Equipment.findAll")
			.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public void updateEquipmentList(List<Equipment> newEquipmentList) {
		logger.debug("updateEquipmentList");
		
		List<Long> idList = new ArrayList<Long>();
		for(Equipment equipment : newEquipmentList) {
			if(equipment.getId() != null) {
				idList.add(equipment.getId());
			}
		}
		
		List<String> equipmentNameList;
		if(idList.size() > 0) {
			equipmentNameList = em.createNamedQuery("Equipment.findEquipmentNameNotInList")
											.setParameter("id", idList)
											.getResultList();
		} else {
			equipmentNameList = new ArrayList<String>();
		}
		
		for(Equipment equipment : newEquipmentList) {
			if(!equipmentNameList.contains(equipment.getEquipmentName().toUpperCase())) {
				if(equipment.getId() == null) {
					// check if there is duplicated equipment
					em.persist(equipment);
				} else {
					em.merge(equipment);
				}
			} else {
				throw new OptimisticLockException();
			}
		}
		
		em.flush();
		retrieveEquipmentList();
	}

	@Remove
	public void destroy() {
		if(equipmentList != null) {
			equipmentList = null;
		}
	}

}
