package hk.org.ha.model.pms.dpp.biz;

import javax.ejb.Local;

@Local
public interface ManufacturerServiceLocal {

	void retrieveManufacturerByManufCode(String manufCode);
	
	void destroy();
}
