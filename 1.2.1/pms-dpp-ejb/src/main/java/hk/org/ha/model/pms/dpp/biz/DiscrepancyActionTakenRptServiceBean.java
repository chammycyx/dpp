package hk.org.ha.model.pms.dpp.biz;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.cacher.DmDrugCacherInf;
import hk.org.ha.model.pms.dpp.cacher.ManufacturerCacherInf;
import hk.org.ha.model.pms.dpp.persistence.ActionTaken;
import hk.org.ha.model.pms.dpp.persistence.Discrepancy;
import hk.org.ha.model.pms.dpp.persistence.PrepackActionTaken;
import hk.org.ha.model.pms.dpp.persistence.PrepackDiscrepancy;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecordVer;
import hk.org.ha.model.pms.dpp.udt.RecordStatus;
import hk.org.ha.model.pms.dpp.udt.ReportDateType;
import hk.org.ha.model.pms.dpp.vo.DiscrepancyActionTakenRptCriteria;
import hk.org.ha.model.pms.dpp.vo.DiscrepancyActionTakenRptData;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("discrepancyActionTakenRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DiscrepancyActionTakenRptServiceBean implements DiscrepancyActionTakenRptServiceLocal {
		
	@Logger
	private Log logger;
	
	@In
	private ManufacturerCacherInf manufacturerCacher;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private UamInfo uamInfo;

	@PersistenceContext
	private EntityManager em;
	
	private List<DiscrepancyActionTakenRptData> discrepancyActionTakenRptList;
	
	private List<String> discrepancyList;
	
	private List<String> actionTakenList;
	
	private final static String deleteIndicator = " *";
	
	@SuppressWarnings("unchecked")
	public void retrieveDiscrepancyActionTakenRptList(DiscrepancyActionTakenRptCriteria discrepancyActionTakenRptCriteria) {
		logger.debug("retrieveDiscrepancyActionTakenRptList");
		
		//Add 1 day to End Date
		Calendar c = Calendar.getInstance(); 
		c.setTime(discrepancyActionTakenRptCriteria.getEndDate());
		c.add(Calendar.DATE, 1);  
		discrepancyActionTakenRptCriteria.setEndDate(c.getTime());
		
		String namedQuery = "PrepackRecordVer.findCompletedPrepackRecordByPreparationDate";
		if(discrepancyActionTakenRptCriteria.getReportDateType() == ReportDateType.PrepackDate) {
			namedQuery = "PrepackRecordVer.findCompletedPrepackRecordByPrepackDate";
		}
		
		List<PrepackRecordVer> prepackRecordVerList = em.createNamedQuery(namedQuery)
			.setParameter("instCode", uamInfo.getHospital())
			.setParameter("workstoreCode", uamInfo.getWorkstore())
			.setParameter("startDate", discrepancyActionTakenRptCriteria.getStartDate())
			.setParameter("endDate", discrepancyActionTakenRptCriteria.getEndDate())
			.setHint(QueryHints.FETCH, "o.prepackRecord")
			.setHint(QueryHints.FETCH, "o.drugItemInfoVer.drugItemInfo")
			.setHint(QueryHints.BATCH, "o.prepackDiscrepancyList")
			.setHint(QueryHints.BATCH, "o.prepackActionTakenList")
			.getResultList();
		
		List<Discrepancy> tmpDiscrepancyList = em.createNamedQuery("Discrepancy.findAll").getResultList();
		discrepancyList = new ArrayList<String>();
		List<Long> discrepancyIdList = new ArrayList<Long>();
		String discrepancyName;
		for(Discrepancy discrepancy : tmpDiscrepancyList) {
			discrepancyName = discrepancy.getDiscrepancyName();
			if(discrepancy.getRecordStatus() == RecordStatus.Deleted) {
				discrepancyName += deleteIndicator;
			}
			discrepancyList.add(discrepancyName);
			discrepancyIdList.add(discrepancy.getId());
		}
		
		List<ActionTaken> tmpActionTakenList = em.createNamedQuery("ActionTaken.findAll").getResultList();
		actionTakenList = new ArrayList<String>();
		List<Long> actionTakenIdList = new ArrayList<Long>();
		String actionTakenName;
		for(ActionTaken actionTaken : tmpActionTakenList) {
			actionTakenName = actionTaken.getActionTakenName();
			if (actionTaken.getRecordStatus() == RecordStatus.Deleted) {
				actionTakenName += deleteIndicator;
			}
			actionTakenList.add(actionTakenName);
			actionTakenIdList.add(actionTaken.getId());
		}
		
		discrepancyActionTakenRptList = new ArrayList<DiscrepancyActionTakenRptData>();
		
		for(PrepackRecordVer prepackRecordVer : prepackRecordVerList) {
			// If the record contain no discrepancy and action taken, abort to export
			if( prepackRecordVer.getPrepackDiscrepancyList().size() == 0 && prepackRecordVer.getPrepackActionTakenList().size() == 0 &&
					StringUtils.isBlank(prepackRecordVer.getDiscrepancyRemark()) && StringUtils.isBlank(prepackRecordVer.getActionTakenRemark())) {
				continue;
			}
			
			DiscrepancyActionTakenRptData data = new DiscrepancyActionTakenRptData();
			data.setPreparationDate(prepackRecordVer.getPreparationDate());
			data.setPrepackDate(prepackRecordVer.getActualPrepackDate());
			data.setAssignedLotNum(uamInfo.getHospital() + "-" + prepackRecordVer.getPrepackRecord().getLotNum());
			data.setItemCode(prepackRecordVer.getDrugItemInfoVer().getDrugItemInfo().getItemCode());
			data.setItemDesc(dmDrugCacher.getDrugByItemCode(prepackRecordVer.getDrugItemInfoVer().getDrugItemInfo().getItemCode()).getFullDrugDesc());
			data.setManufName(manufacturerCacher.getManufacturerByManufCode(prepackRecordVer.getManufCode()).getCompanyName());
			data.setBatchNum(prepackRecordVer.getBatchNum());
			data.setDiscrepancyRemark(prepackRecordVer.getDiscrepancyRemark());
			data.setActionTakenRemark(prepackRecordVer.getActionTakenRemark());
			
			Set<Long> dataDiscrepancyNameHS = new HashSet<Long>();
			for(PrepackDiscrepancy prepackDiscrepancy : prepackRecordVer.getPrepackDiscrepancyList()) {
				dataDiscrepancyNameHS.add(prepackDiscrepancy.getDiscrepancy().getId());
			}
			
			List<String> discrepancyDataList = new ArrayList<String>();
			for(Long id : discrepancyIdList) {
				if(dataDiscrepancyNameHS.contains(id)) {
					discrepancyDataList.add("1");
				} else {
					discrepancyDataList.add("");
				}
			}
			data.setDiscrepancyDataList(discrepancyDataList);
			
			Set<Long> dataActionTakenNameHS = new HashSet<Long>();
			for(PrepackActionTaken prepackActionTaken : prepackRecordVer.getPrepackActionTakenList()) {
				dataActionTakenNameHS.add(prepackActionTaken.getActionTaken().getId());
			}
			
			List<String> actionTakenDataList = new ArrayList<String>();
			for(Long id : actionTakenIdList) {
				if(dataActionTakenNameHS.contains(id)) {
					actionTakenDataList.add("1");
				} else {
					actionTakenDataList.add("");
				}
			}
			data.setActionTakenDataList(actionTakenDataList);
			
			discrepancyActionTakenRptList.add(data);
		}
		
		// Remove the whole column if all record return false
		for(int i=discrepancyList.size()-1;i>=0;i--) {
			Boolean isAllEmpty = true; 
			for(DiscrepancyActionTakenRptData data : discrepancyActionTakenRptList) {
				if("1".equals(data.getDiscrepancyDataList().get(i))) {
					isAllEmpty = false;
					break;
				}
			}
			
			if(isAllEmpty) {
				discrepancyList.remove(i);
				for(DiscrepancyActionTakenRptData data : discrepancyActionTakenRptList) {
					data.getDiscrepancyDataList().remove(i);
				}
			}
		}
		
		for(int i=actionTakenList.size()-1;i>=0;i--) {
			Boolean isAllEmpty = true; 
			for(DiscrepancyActionTakenRptData data : discrepancyActionTakenRptList) {
				if("1".equals(data.getActionTakenDataList().get(i))) {
					isAllEmpty = false;
					break;
				}
			}
			
			if(isAllEmpty) {
				actionTakenList.remove(i);
				for(DiscrepancyActionTakenRptData data : discrepancyActionTakenRptList) {
					data.getActionTakenDataList().remove(i);
				}
			}
		}
		
		logger.debug("discrepancyActionTakenRptList report list size : #0", discrepancyActionTakenRptList.size());
		
	}

	public List<DiscrepancyActionTakenRptData> getDiscrepancyActionTakenRptList() {
		return discrepancyActionTakenRptList;
	}
	
	public List<String> getDiscrepancyList() {
		return discrepancyList;
	}
	
	public List<String> getActionTakenList() {
		return actionTakenList;
	}
	
	@Remove
	public void destroy(){
		if(discrepancyActionTakenRptList != null) {
			discrepancyActionTakenRptList = null;
		}
		if(discrepancyList != null) {
			discrepancyList = null;
		}
		if(actionTakenList != null) {
			actionTakenList = null;
		}
	}
}
