package hk.org.ha.model.pms.dpp.vo;

import java.math.BigDecimal;

public class DrugItemInfoData {
	private String itemCode;
	private String itemDesc;
	private String itemLongDesc;
	private String itemShortDesc;
	private String remark;
	private String alias;
	private String hqSuspend;
	private String instSuspend;
	private String allowPrepack;
	private String displayAliasOnFullSizeLabel;
	private String displayAliasOnSideLabel;
	private String displayShortDescOnSideLabel;
	private BigDecimal packSize;
	private String packUnit;
	
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getItemDesc() {
		return itemDesc;
	}
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	public String getItemLongDesc() {
		return itemLongDesc;
	}
	public void setItemLongDesc(String itemLongDesc) {
		this.itemLongDesc = itemLongDesc;
	}
	public String getItemShortDesc() {
		return itemShortDesc;
	}
	public void setItemShortDesc(String itemShortDesc) {
		this.itemShortDesc = itemShortDesc;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getHqSuspend() {
		return hqSuspend;
	}
	public void setHqSuspend(String hqSuspend) {
		this.hqSuspend = hqSuspend;
	}
	public String getInstSuspend() {
		return instSuspend;
	}
	public void setInstSuspend(String instSuspend) {
		this.instSuspend = instSuspend;
	}
	public String getAllowPrepack() {
		return allowPrepack;
	}
	public void setAllowPrepack(String allowPrepack) {
		this.allowPrepack = allowPrepack;
	}
	public String getDisplayAliasOnFullSizeLabel() {
		return displayAliasOnFullSizeLabel;
	}
	public void setDisplayAliasOnFullSizeLabel(String displayAliasOnFullSizeLabel) {
		this.displayAliasOnFullSizeLabel = displayAliasOnFullSizeLabel;
	}
	public String getDisplayAliasOnSideLabel() {
		return displayAliasOnSideLabel;
	}
	public void setDisplayAliasOnSideLabel(String displayAliasOnSideLabel) {
		this.displayAliasOnSideLabel = displayAliasOnSideLabel;
	}
	public String getDisplayShortDescOnSideLabel() {
		return displayShortDescOnSideLabel;
	}
	public void setDisplayShortDescOnSideLabel(String displayShortDescOnSideLabel) {
		this.displayShortDescOnSideLabel = displayShortDescOnSideLabel;
	}
	public BigDecimal getPackSize() {
		return packSize;
	}
	public void setPackSize(BigDecimal packSize) {
		this.packSize = packSize;
	}
	public String getPackUnit() {
		return packUnit;
	}
	public void setPackUnit(String packUnit) {
		this.packUnit = packUnit;
	}
}
