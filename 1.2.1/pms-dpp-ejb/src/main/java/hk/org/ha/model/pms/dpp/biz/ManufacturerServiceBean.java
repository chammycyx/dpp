package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.cacher.ManufacturerCacherInf;
import hk.org.ha.model.pms.dpp.vo.Manufacturer;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("manufacturerService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ManufacturerServiceBean implements ManufacturerServiceLocal {
	
	@Logger
	private Log logger;
	
	@In
	private ManufacturerCacherInf manufacturerCacher;
	
	@Out(required=false)
	private Manufacturer manufacturer;

	
	public void retrieveManufacturerByManufCode(String manufCode) {
		logger.info("retrieveManufacturerByManufCode");
		
		manufacturer = manufacturerCacher.getManufacturerByManufCode(manufCode);
	}
	
	@Remove
	public void destroy() {
		if (manufacturer != null) {
			manufacturer = null;
		}
	}

}
