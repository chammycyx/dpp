package hk.org.ha.model.pms.dpp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ExpiryMonth implements StringValuedEnum {
	
	NullValue("N", "Null value"),
	ZeroMonths("0", "0"),
	OneMonths("1", "1"),
	TwoMonths("2", "2"),
	ThreeMonths("3", "3"),
	FourMonths("4", "4"),
	FiveMonths("5", "5"),
	SixMonths("6", "6"),
	SevenMonths("7", "7"),
	EightMonths("8", "8"),
	NightMonths("9", "9"),
	TenMonths("10", "10"),
	ElevenMonths("11", "11"),
	TwelveMonths("12", "12"),
	ThirteenMonths("13", "13"),
	FourteenMonths("14", "14"),
	FifteenMonths("15", "15"),
	SixteenMonths("16", "16"),
	SeventeenMonths("17", "17"),
	EighteenMonths("18", "18"),
	NineteenMonths("19", "19"),
	TwentyMonths("20", "20"),
	TwentyOneMonths("21", "21"),
	TwentyTwoMonths("22", "22"),
	TwentyThreeMonths("23", "23"),
	TwentyFourMonths("24", "24");
	
    private final String dataValue;
    private final String displayValue;
        
    ExpiryMonth(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
	public static class Converter extends StringValuedEnumConverter<ExpiryMonth> {
		
		private static final long serialVersionUID = 5136223189472974436L;

		@Override
    	public Class<ExpiryMonth> getEnumClass() {
    		return ExpiryMonth.class;
    	}
    }
}