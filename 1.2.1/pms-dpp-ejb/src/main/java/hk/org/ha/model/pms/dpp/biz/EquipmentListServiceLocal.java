package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.model.pms.dpp.persistence.Equipment;

import java.util.List;

import javax.ejb.Local;

@Local
public interface EquipmentListServiceLocal {
	
	void retrieveEquipmentList();
	
	void updateEquipmentList(List<Equipment> newEquipmentList);
	
	void destroy();
	
}
