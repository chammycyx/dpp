package hk.org.ha.model.pms.dpp.persistence;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name="INST_LOT_NUM")
@Customizer(AuditCustomizer.class)

@NamedQueries({
	@NamedQuery(name="InstLotNum.findLotNumByInstCode", query="select o from InstLotNum o where o.instCode = :instCode")
})

public class InstLotNum extends VersionEntity {
	
	private static final long serialVersionUID = 7631267961529013224L;
	
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="instLotNumSeq")
	@SequenceGenerator(name="instLotNumSeq", sequenceName="SQ_INST_LOT_NUM", initialValue=100000000)
	private Long id;
	
	@Column(name="INST_CODE", length=3, nullable=false, unique=true)
	private String instCode;
	
	@Column(name="LOT_NUM", nullable=false)
	private Integer lotNum;
	
	@Column(name="ISSUE_DATE", nullable=false)
	private Date issueDate;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}

	public String getInstCode() {
		return instCode;
	}
	
	public void setLotNum(Integer lotNum) {
		this.lotNum = lotNum;
	}

	public Integer getLotNum() {
		return lotNum;
	}
	
	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate == null ? null : new Date(issueDate.getTime());
	}

	public Date getIssueDate() {
		return issueDate == null ? null : new Date(issueDate.getTime());
	}
	
}
