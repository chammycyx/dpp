package hk.org.ha.model.pms.udt.printing;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PrintStatus implements StringValuedEnum {

	Print("P", "Print"),
	Preview("V", "Preview"),
	Reprint("R", "Re-print");	
	
    private final String dataValue;
    private final String displayValue;

    PrintStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    } 
    
    public static PrintStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PrintStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<PrintStatus> {

		private static final long serialVersionUID = -4725106820503119408L;

		@Override
    	public Class<PrintStatus> getEnumClass() {
    		return PrintStatus.class;
    	}
    }
}