package hk.org.ha.model.pms.dpp.biz;

import java.util.List;

import javax.ejb.Local;

import hk.org.ha.model.pms.dpp.vo.EnquiryRptCriteria;
import hk.org.ha.model.pms.dpp.vo.EnquiryRptData;

@Local
public interface EnquiryRptServiceLocal {
	
	void retrieveEnquiryRptList(EnquiryRptCriteria enquiryRptCriteria);
	
	List<EnquiryRptData> getEnquiryRptList();
	
	void destroy();

}
