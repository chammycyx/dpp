package hk.org.ha.model.pms.dpp.biz;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.cacher.ManufacturerCacherInf;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecordVer;
import hk.org.ha.model.pms.dpp.vo.EnquiryRptCriteria;
import hk.org.ha.model.pms.dpp.vo.EnquiryRptData;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("enquiryRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class EnquiryRptServiceBean implements EnquiryRptServiceLocal {
	
	@Logger
	private Log logger;
	
	@In
	private UamInfo uamInfo;
	
	@In
	private ManufacturerCacherInf manufacturerCacher;

	@PersistenceContext
	private EntityManager em;
	
	private List<EnquiryRptData> enquiryRptList;
	
	@SuppressWarnings("unchecked")
	public void retrieveEnquiryRptList(EnquiryRptCriteria enquiryRptCriteria) {
		logger.debug("retrieveEnquiryRptList");
		
		//Add 1 day to End Date
		Calendar c = Calendar.getInstance(); 
		if(enquiryRptCriteria.getPrepackEndDate() != null) {
			c.setTime(enquiryRptCriteria.getPrepackEndDate());
			c.add(Calendar.DATE, 1);  
			enquiryRptCriteria.setPrepackEndDate(c.getTime());
		}
		
		if(enquiryRptCriteria.getPreparationEndDate() != null) {
			c.setTime(enquiryRptCriteria.getPreparationEndDate());
			c.add(Calendar.DATE, 1);  
			enquiryRptCriteria.setPreparationEndDate(c.getTime());
		}
		
		List<PrepackRecordVer> prepackRecordVerList = em.createNamedQuery("PrepackRecordVer.findForEnquiryReport")
			.setParameter("instCode", uamInfo.getHospital())
			.setParameter("workstoreCode", uamInfo.getWorkstore())
			.setParameter("itemCode", enquiryRptCriteria.getItemCode())
			.setParameter("packSize", enquiryRptCriteria.getPackSize())
			.setParameter("packUnit", enquiryRptCriteria.getPackUnit())
			.setParameter("assignedLotNum", enquiryRptCriteria.getAssignedLotNum())
			.setParameter("batchNum", enquiryRptCriteria.getBatchNum())
			.setParameter("preparationStartDate", enquiryRptCriteria.getPreparationStartDate())
			.setParameter("preparationEndDate", enquiryRptCriteria.getPreparationEndDate())
			.setParameter("prepackStartDate", enquiryRptCriteria.getPrepackStartDate())
			.setParameter("prepackEndDate", enquiryRptCriteria.getPrepackEndDate())
			.setParameter("prepackStatus", enquiryRptCriteria.getPrepackStatus())
			.setParameter("prepackStatusFlag", (enquiryRptCriteria.getPrepackStatus()==null?"Y":"N"))
			.setParameter("manufCode", enquiryRptCriteria.getManufCode())
			.setParameter("containerName", enquiryRptCriteria.getContainerName())
			.setParameter("equipmentName", enquiryRptCriteria.getEquipmentName())
			.setHint(QueryHints.FETCH, "o.drugItemInfoVer.drugItemInfo")
			.setHint(QueryHints.FETCH, "o.prepackPackSize")
			.setHint(QueryHints.FETCH, "o.prepackRecord")
			.setHint(QueryHints.FETCH, "o.container")
			.setHint(QueryHints.FETCH, "o.equipment")
			.getResultList();
		
		enquiryRptList = new ArrayList<EnquiryRptData>();
		
		for(PrepackRecordVer prepackRecordVer : prepackRecordVerList) {
			EnquiryRptData data = new EnquiryRptData();
			data.setItemCode(prepackRecordVer.getDrugItemInfoVer().getDrugItemInfo().getItemCode());
			data.setAssignedLotNum(uamInfo.getHospital() + "-" + prepackRecordVer.getPrepackRecord().getLotNum());
			data.setPreparationDate(prepackRecordVer.getPreparationDate());
			data.setPrepackStatus(prepackRecordVer.getPrepackStatus().getDisplayValue());
			data.setManufName(manufacturerCacher.getManufacturerByManufCode(prepackRecordVer.getManufCode()).getCompanyName());
			data.setBatchNum(prepackRecordVer.getBatchNum());
			data.setOriginalExpiryDate(prepackRecordVer.getExpiryDate());
			data.setOriginalPackQty(prepackRecordVer.getOriginalPackQty());
			data.setOriginalPackSize(prepackRecordVer.getOriginalPackSize());
			data.setDispenseDosageUnit(prepackRecordVer.getDispenseDosageUnit());
			data.setOriginalTotalQty(prepackRecordVer.getOriginalTotalQty());
			data.setEquipmentName(prepackRecordVer.getEquipment().getEquipmentName());
			data.setContainerName(prepackRecordVer.getContainer().getContainerName());
			data.setAssignedExpriyDate(prepackRecordVer.getAssignExpiryDate());
			data.setPrepackPackSize(prepackRecordVer.getPrepackPackSize().getPackSize());
			data.setPrepackPackUnit(prepackRecordVer.getPrepackPackSize().getPackUnit());
			data.setSampleCheckQty(prepackRecordVer.getSampleCheckQty());
			data.setProvisionalPrepackQty(prepackRecordVer.getProvisionPrepackQty());
			data.setProvisionalPrepackLabelQty(prepackRecordVer.getProvisionPrepackLabelQty());
			data.setProvisionRemainQty(prepackRecordVer.getProvisionRemainQty());
			data.setActualPrepackDate(prepackRecordVer.getActualPrepackDate());
			data.setActualPrepackQty(prepackRecordVer.getActualPrepackQty());
			data.setActualLabelRemainQty(prepackRecordVer.getActualLabelRemainQty());
			data.setActualAddLabelQty(prepackRecordVer.getActualAddLabelQty());
			data.setActualItemRemainQty(prepackRecordVer.getActualItemRemainQty());
			data.setPrepareBy(prepackRecordVer.getPrepareBy());
			data.setPreCheckBy(prepackRecordVer.getPreCheckBy());
			data.setLineClearCheckBy(prepackRecordVer.getLineClearCheckBy());
			data.setPrepackBy(prepackRecordVer.getPrepackBy());
			data.setFinalCheckBy(prepackRecordVer.getFinalCheckBy());
			data.setCancelReason(prepackRecordVer.getCancelReason());
			
			enquiryRptList.add(data);
		}
		
		logger.debug("enquiryRptList report list size : #0", enquiryRptList.size());
	}
	
	public List<EnquiryRptData> getEnquiryRptList() {
		return enquiryRptList;
	}
	
	@Remove
	public void destroy() {
		if(enquiryRptList != null) {
			enquiryRptList = null;
		}
	}
}
