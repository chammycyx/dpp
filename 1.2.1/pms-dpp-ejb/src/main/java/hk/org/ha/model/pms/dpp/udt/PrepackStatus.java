package hk.org.ha.model.pms.dpp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum PrepackStatus implements StringValuedEnum {
	
	New("N", "New"),
	Printed("P", "Printed"),
	Completed("C", "Completed"),
	Cancelled("L", "Cancelled");
	
    private final String dataValue;
    private final String displayValue;
        
    PrepackStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<PrepackStatus> {
		
		private static final long serialVersionUID = 1081125290550871192L;

		@Override
    	public Class<PrepackStatus> getEnumClass() {
    		return PrepackStatus.class;
    	}
    }
}
