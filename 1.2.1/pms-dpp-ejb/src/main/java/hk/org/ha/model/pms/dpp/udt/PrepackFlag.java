package hk.org.ha.model.pms.dpp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum PrepackFlag implements StringValuedEnum {
	
	Yes("Y", "Yes"),
	No("N", "No");	
	
    private final String dataValue;
    private final String displayValue;
        
    PrepackFlag(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<PrepackFlag> {
		
		private static final long serialVersionUID = -269303139870703878L;

		@Override
    	public Class<PrepackFlag> getEnumClass() {
    		return PrepackFlag.class;
    	}
    }
}
