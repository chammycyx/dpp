package hk.org.ha.model.pms.dpp.persistence;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dpp.udt.RecordStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name="DISCREPANCY")
@Customizer(AuditCustomizer.class)
@NamedQueries({
	@NamedQuery(name="Discrepancy.findAllActive", query="select o from Discrepancy o where o.recordStatus = hk.org.ha.model.pms.dpp.udt.RecordStatus.Active order by o.discrepancyName ASC"),
	@NamedQuery(name="Discrepancy.findAll", query="select o from Discrepancy o order by o.discrepancyName ASC"),
	@NamedQuery(name="Discrepancy.findDiscrepancyNameNotInList", query="select UPPER(o.discrepancyName) from Discrepancy o where o.recordStatus = hk.org.ha.model.pms.dpp.udt.RecordStatus.Active and o.id not in :id ")
})
public class Discrepancy extends VersionEntity {

	private static final long serialVersionUID = 8075596519887256730L;

	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="discrepancySeq")
	@SequenceGenerator(name="discrepancySeq", sequenceName="SQ_DISCREPANCY", initialValue=100000000)
	private Long id;
	
	@Column(name="DISCREPANCY_NAME", length=60, nullable=false)
	private String discrepancyName;
	
	@Converter(name="Discrepancy.recordStatus", converterClass=RecordStatus.Converter.class)
	@Convert("Discrepancy.recordStatus")
	@Column(name="RECORD_STATUS", length=1, nullable=false)
	private RecordStatus recordStatus;

	@Transient
	private boolean enable;
	
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setDiscrepancyName(String discrepancyName) {
		this.discrepancyName = discrepancyName;
	}

	public String getDiscrepancyName() {
		return discrepancyName;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public boolean isEnable() {
		return enable;
	}
}
