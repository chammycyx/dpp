package hk.org.ha.model.pms.dpp.vo;

import hk.org.ha.model.pms.dpp.persistence.Discrepancy;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DiscrepancyDetails {
	
	private Boolean enable;
	
	private Discrepancy discrepancy;

	
	public void setEnable(Boolean enable) {
		this.enable = enable;
	}

	public Boolean getEnable() {
		return enable;
	}

	public void setDiscrepancyList(Discrepancy discrepancy) {
		this.discrepancy = discrepancy;
	}

	public Discrepancy getDiscrepancy() {
		return discrepancy;
	}
}
