package hk.org.ha.model.pms.dpp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ExpiryWeek implements StringValuedEnum {
	
	NullValue("N", "Null value"),
	ZeroWeeks("0", "0"),
	OneWeeks("1", "1"),
	TwoWeeks("2", "2"),
	ThreeWeeks("3", "3"),
	FourWeeks("4", "4");
	
    private final String dataValue;
    private final String displayValue;
        
    ExpiryWeek(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<ExpiryWeek> {
		
		private static final long serialVersionUID = 5136223189472974421L;

		@Override
    	public Class<ExpiryWeek> getEnumClass() {
    		return ExpiryWeek.class;
    	}
    }
}