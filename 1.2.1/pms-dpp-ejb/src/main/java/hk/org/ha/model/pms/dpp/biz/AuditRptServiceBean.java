package hk.org.ha.model.pms.dpp.biz;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.cacher.DmDrugCacherInf;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecordVer;
import hk.org.ha.model.pms.dpp.persistence.PrintLog;
import hk.org.ha.model.pms.dpp.udt.ActionStatus;
import hk.org.ha.model.pms.dpp.udt.PrintType;
import hk.org.ha.model.pms.dpp.vo.AuditRptCriteria;
import hk.org.ha.model.pms.dpp.vo.AuditRptData;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("auditRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class AuditRptServiceBean implements AuditRptServiceLocal {
	
	@Logger
	private Log logger;
	
	@In
	private UamInfo uamInfo;
	
	@In
	private DmDrugCacherInf dmDrugCacher;

	@PersistenceContext
	private EntityManager em;
	
	private List<AuditRptData> auditRptList;
	
	private static AuditRptDataComparator auditRptDataComparator = new AuditRptDataComparator();
	
	@SuppressWarnings("unchecked")
	public void retrieveAuditRptList(AuditRptCriteria auditRptCriteria) {
		logger.debug("retrieveAuditRptList");
		
		//Add 1 day to End Date
		Calendar c = Calendar.getInstance(); 
		c.setTime(auditRptCriteria.getEndDate());
		c.add(Calendar.DATE, 1);  
		auditRptCriteria.setEndDate(c.getTime());
		
		List<PrepackRecordVer> prepackRecordVerList = em.createQuery("select o from PrepackRecordVer o " +
				"where o.drugItemInfoVer.drugItemInfo.instCode = :instCode " +
				"and o.drugItemInfoVer.drugItemInfo.workstoreCode = :workstoreCode " +
				"and o.createDate >= :startDate " +
				"and o.createDate < :endDate " +
				"and o.recordStatus = hk.org.ha.model.pms.dpp.udt.RecordStatus.Active " +
				"and o.actionStatus <> :actionStatus " +
				"order by o.createDate ASC ")
			.setParameter("instCode", uamInfo.getHospital())
			.setParameter("workstoreCode", uamInfo.getWorkstore())
			.setParameter("startDate", new Timestamp(auditRptCriteria.getStartDate().getTime()))
			.setParameter("endDate", new Timestamp(auditRptCriteria.getEndDate().getTime()))
			.setParameter("actionStatus", ActionStatus.Printed)
			.setHint(QueryHints.FETCH, "o.drugItemInfoVer.drugItemInfo")
			.setHint(QueryHints.FETCH, "o.prepackRecord")
			.getResultList();
		
		List<PrintLog> printLogList = em.createQuery("select o from PrintLog o " +
				"where o.prepackRecordVer.drugItemInfoVer.drugItemInfo.instCode = :instCode " +
				"and o.prepackRecordVer.drugItemInfoVer.drugItemInfo.workstoreCode = :workstoreCode " +
				"and o.createDate >= :startDate and " +
				"o.createDate < :endDate " +
				"and o.recordStatus = hk.org.ha.model.pms.dpp.udt.RecordStatus.Active ")
			.setParameter("instCode", uamInfo.getHospital())
			.setParameter("workstoreCode", uamInfo.getWorkstore())
			.setParameter("startDate", new Timestamp(auditRptCriteria.getStartDate().getTime()))
			.setParameter("endDate", new Timestamp(auditRptCriteria.getEndDate().getTime()))
			.setHint(QueryHints.FETCH, "o.prepackRecordVer.drugItemInfoVer.drugItemInfo")
			.setHint(QueryHints.FETCH, "o.prepackRecordVer.prepackRecord")
			.getResultList();
		
		auditRptList = new ArrayList<AuditRptData>();
		
		for(PrepackRecordVer prepackRecordVer : prepackRecordVerList) {
			if(prepackRecordVer.getActionStatus() == ActionStatus.Printed) {
				continue;
			}
			
			AuditRptData data = new AuditRptData();
			data.setHospCode(uamInfo.getHospital());
			data.setActionDate(prepackRecordVer.getCreateDate());
			data.setActionBy(prepackRecordVer.getCreateUser());
			data.setItemCode(prepackRecordVer.getDrugItemInfoVer().getDrugItemInfo().getItemCode());
			data.setItemDesc(dmDrugCacher.getDrugByItemCode(prepackRecordVer.getDrugItemInfoVer().getDrugItemInfo().getItemCode()).getFullDrugDesc());
			data.setLotNum(prepackRecordVer.getPrepackRecord().getLotNum());
			data.setRemarks("Assigned Lot No.: " + uamInfo.getHospital() + "-" + prepackRecordVer.getPrepackRecord().getLotNum() + (StringUtils.isBlank(prepackRecordVer.getCancelReason())?"":", Cancel Reason: " + prepackRecordVer.getCancelReason()));
			
			if(prepackRecordVer.getActionStatus() == ActionStatus.New) {
				data.setAction("New pre-packing record");
			} else if(prepackRecordVer.getActionStatus() == ActionStatus.Updated) {
				data.setAction("Update pre-packing record");
			} else if(prepackRecordVer.getActionStatus() == ActionStatus.Cancelled) {
				data.setAction("Cancel pre-packing record");
			} else if(prepackRecordVer.getActionStatus() == ActionStatus.Completed) {
				data.setAction("Completed");
			}
				
			auditRptList.add(data);
		}
		
		for(PrintLog printLog : printLogList) {
			AuditRptData data = new AuditRptData();
			data.setHospCode(uamInfo.getHospital());
			data.setActionDate(printLog.getCreateDate());
			data.setActionBy(printLog.getCreateUser());
			data.setItemCode(printLog.getPrepackRecordVer().getDrugItemInfoVer().getDrugItemInfo().getItemCode());
			data.setItemDesc(dmDrugCacher.getDrugByItemCode(printLog.getPrepackRecordVer().getDrugItemInfoVer().getDrugItemInfo().getItemCode()).getFullDrugDesc());
			data.setLotNum(printLog.getPrepackRecordVer().getPrepackRecord().getLotNum());
			data.setRemarks("Assigned Lot No.: " + uamInfo.getHospital() + "-" + printLog.getPrepackRecordVer().getPrepackRecord().getLotNum() + ", Print Qty: " + printLog.getPrintQty().toString());
			
			if(printLog.getPrintType() == PrintType.Label) {
				data.setAction("Print pre-packing label");
			} else if(printLog.getPrintType() == PrintType.SampleLabel) {
				data.setAction("Print sample label");
			}
			
			auditRptList.add(data);
		}
		
		// Sort list, primary Assigned Lot No. , secondary Action Date
		Collections.sort(auditRptList, auditRptDataComparator);
		logger.debug("auditRptList report list size : #0", auditRptList.size());
	}
	
	public List<AuditRptData> getAuditRptList() {
		return auditRptList;
	}
	
	@Remove
	public void destroy() {
		if(auditRptList != null) {
			auditRptList = null;
		}
	}
	
	private static class AuditRptDataComparator implements Comparator<AuditRptData> {

		//Descending order by hosp. code & assign lot number
		public int compare(AuditRptData d1, AuditRptData d2) {
			return new CompareToBuilder()
			.append(d2.getHospCode(), d1.getHospCode())
			.append(d2.getLotNum(), d1.getLotNum())
			.append(d2.getActionDate(), d1.getActionDate())
			.toComparison();
		}
	}
}
