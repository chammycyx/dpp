package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.model.pms.dpp.persistence.Container;

import java.util.List;

import javax.ejb.Local;

@Local
public interface ContainerListServiceLocal {
	
	void retrieveContainerList();
	
	void updateContainerList(List<Container> newContainerList);
	
	void destroy();
	
}
