package hk.org.ha.model.pms.biz.printing;

import hk.org.ha.model.pms.dpp.exception.PrintingException;
import hk.org.ha.model.pms.vo.printing.RenderJob;

import java.io.Serializable;

public interface PrintingAgentInf extends Serializable {
	
	String render(RenderJob job);
	
	String renderImage(RenderJob job);
	
	void keep(String contentId);
	
	void redirect(String contentId);
	
	void print(int printQty, String printerHostName, String printDocType) throws PrintingException;
	
	void print(String contentId, int printQty, String printerHostName, String printDocType) throws PrintingException;
}
