package hk.org.ha.model.pms.dpp.cacher;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.model.pms.dms.persistence.DmInstDrug;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsDppServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmInstDrugCacher")
@Scope(ScopeType.APPLICATION)
public class DmInstDrugCacher extends BaseCacher implements DmInstDrugCacherInf {
	
	@In
	private DmsDppServiceJmsRemote dmsDppServiceProxy;
	
	@In(required=false)
	private UamInfo uamInfo;
	
	private Map<String, InnerCacher> cacherMap = new HashMap<String, InnerCacher>();
         
	public void load() {
		this.getCacher(uamInfo.getHospital()).getAll();
	}
	
	public void clear() {
		synchronized (this) {
			cacherMap.remove(uamInfo.getHospital());
		}
	}

	public void clearAll() {
		synchronized (this) {
			cacherMap.clear();   
		}
	}
	
	public DmInstDrug getDrugByItemCode(String itemCode) {
		return (DmInstDrug) this.getCacher(uamInfo.getHospital()).get(itemCode);
	}
		
	public List<DmInstDrug> getDrugListByItemCode(String prefixItemCode) {
		List<DmInstDrug> ret = new ArrayList<DmInstDrug>();
		for (DmInstDrug dmInstDrug : getDrugList(uamInfo.getHospital())) {
			if (dmInstDrug.getDmDrug().getItemCode().startsWith(prefixItemCode)) {
				ret.add(dmInstDrug);
			}
		}
		return ret;
	}
	
	public List<DmInstDrug> getDrugList(String instCode) {
		return (List<DmInstDrug>) this.getCacher(instCode).getAll();
	}

	private InnerCacher getCacher(String instCode) {
		synchronized (this) {
			InnerCacher cacher = cacherMap.get(instCode);
			
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime(), instCode);
				cacherMap.put(instCode, cacher);
			}
			
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, DmInstDrug>
	{		
		private String instCode = null;
		
        public InnerCacher(int expireTime, String instCode) {
              super(expireTime);
              this.instCode = instCode;
        }
		
		public DmInstDrug create(String itemCode) {	
			this.getAll();
			return this.internalGet(itemCode);
		}

		public Collection<DmInstDrug> createAll() {
			return dmsDppServiceProxy.retrieveDmInstDrugListByInstCode(instCode);
		}

		public String retrieveKey(DmInstDrug dmInstDrug) {
			return dmInstDrug.getDmDrug().getItemCode();
		}		
	}
	
	public static DmInstDrugCacherInf instance()
	{
		  if (!Contexts.isApplicationContextActive())
	      {
	         throw new IllegalStateException("No active application scope");
	      }
	      return (DmInstDrugCacherInf) Component.getInstance("dmInstDrugCacher", ScopeType.APPLICATION);
    }	
}
