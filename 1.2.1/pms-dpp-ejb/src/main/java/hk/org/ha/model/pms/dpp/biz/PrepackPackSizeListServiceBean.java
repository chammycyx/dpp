package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.persistence.DrugItemInfo;
import hk.org.ha.model.pms.dpp.persistence.PrepackPackSize;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("prepackPackSizeListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PrepackPackSizeListServiceBean implements PrepackPackSizeListServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@Out(required=false)
	private List<PrepackPackSize> prepackPackSizeList;
	
	@SuppressWarnings("unchecked")
	public void retrievePrepackPackSizeListByDrugItemInfoId(Long drugItemInfoId) {
		logger.debug("retrievePrepackPackSizeListByDrugItemInfoId #0", drugItemInfoId);
		
		DrugItemInfo drugItemInfo = em.find(DrugItemInfo.class, drugItemInfoId);
		prepackPackSizeList = em.createNamedQuery("PrepackPackSize.findByDrugItemInfoId")
			.setParameter("drugItemInfo", drugItemInfo)
			.getResultList();
	}
	
	@Remove
	public void destroy() {
		if(prepackPackSizeList != null) {
			prepackPackSizeList = null;
		}
	}

}
