package hk.org.ha.model.pms.dpp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ReportDateType implements StringValuedEnum {
	
	PreparationDate("PreparationDate", "Preparation Date"),
	PrepackDate("PrepackDate", "Prepack Date");
	
	private final String dataValue;
    private final String displayValue;
        
    ReportDateType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }
    
    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }
    
	public static class Converter extends StringValuedEnumConverter<ReportDateType> {
		
		private static final long serialVersionUID = -2536053116714849140L;

		@Override
    	public Class<ReportDateType> getEnumClass() {
    		return ReportDateType.class;
    	}
    }
}
