package hk.org.ha.model.pms.dpp.cacher;

import hk.org.ha.fmk.pms.cache.BaseCacherInf;
import hk.org.ha.model.pms.dpp.vo.Manufacturer;

import java.util.List;

public interface ManufacturerCacherInf extends BaseCacherInf {
	List<Manufacturer> getFullManufList();
	Manufacturer getManufacturerByManufCode(String manufCode);
}
