package hk.org.ha.model.pms.test;

import hk.org.ha.fmk.pms.test.DBUnitOracleSeamTest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.ext.oracle.Oracle10DataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Observer;
import org.jboss.seam.core.Events;
import org.jboss.seam.log.Log;

/**
 * Imports some data into the database with the help of DBUnit. This allows us
 * to use the same dataset files as in unit testing, but in the regular
 * application startup during development. 
 * 
 * @author Tommy Chong
 */
@Stateless
@Name("dbunitImporter")
@Install(value = false, precedence = Install.MOCK, classDependencies = "org.dbunit.operation.DatabaseOperation")
public class DBUnitImporter extends DBUnitOracleSeamTest implements
		DBUnitImporterLocal {

	public static final String IMPORT_COMPLETE_EVENT = "DBUnitImporter.importComplete";

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;	

	private List<String> datasets = new ArrayList<String>();
	
	private boolean prepared = false;
	
	public List<String> getDatasets() {
		return datasets;
	}

	public void setDatasets(List<String> datasets) {
		this.datasets = datasets;
	}
	
	@Override
	protected void editConfig(DatabaseConfig config) {
		super.editConfig(config);		
		Oracle10DataTypeFactory dataTypeFactory = new Oracle10DataTypeFactory();
		config.setProperty(DatabaseConfig.FEATURE_SKIP_ORACLE_RECYCLEBIN_TABLES, true);
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, dataTypeFactory);
	}

	@Override
	protected void prepareDBUnitOperations() {
		if (datasets == null) {
			return;
		}

		if (!prepared) {
			for (String dataset : datasets) {
				log.info("Adding DBUnit dataset to import: " + dataset);
				beforeTestOperations.add(new DataSetOperation(dataset,
						DatabaseOperation.CLEAN_INSERT));
			}
			prepared = true;
		}
	}

	// Do it when the application starts (but after everything
	// else has been loaded, esp. the persistence unit)
	@Observer("org.jboss.seam.postInitialization")
	public void startImport() {

		// magic code to kick up the eclipselink
		// and generate the table schema into database
		log.info("Generate Schema into Database...");
		em.createNativeQuery("select 1");

		log.info("Importing DBUnit datasets...");
		prepareDBUnitOperations();
		prepareDataBeforeTest();
		Events.instance().raiseEvent(IMPORT_COMPLETE_EVENT);

		prepared = false;
	}
}
