package hk.org.ha.model.pms.dpp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum DisplaySideLabelShortDescFlag implements StringValuedEnum {
	
	Yes("Y", "Yes"),
	No("N", "No");	
	
    private final String dataValue;
    private final String displayValue;
        
    DisplaySideLabelShortDescFlag(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<DisplaySideLabelShortDescFlag> {
		
		private static final long serialVersionUID = 5136223189472974490L;

		@Override
    	public Class<DisplaySideLabelShortDescFlag> getEnumClass() {
    		return DisplaySideLabelShortDescFlag.class;
    	}
    }
}
