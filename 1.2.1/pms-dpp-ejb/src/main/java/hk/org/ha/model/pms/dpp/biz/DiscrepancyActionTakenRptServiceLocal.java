package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.model.pms.dpp.vo.DiscrepancyActionTakenRptCriteria;
import hk.org.ha.model.pms.dpp.vo.DiscrepancyActionTakenRptData;

import java.util.List;

import javax.ejb.Local;

@Local
public interface DiscrepancyActionTakenRptServiceLocal {
	
	void retrieveDiscrepancyActionTakenRptList(DiscrepancyActionTakenRptCriteria discrepancyActionTakenRptCriteria);
	
	List<DiscrepancyActionTakenRptData> getDiscrepancyActionTakenRptList();
	
	List<String> getDiscrepancyList();
	
	List<String> getActionTakenList();
	
	void destroy();

}
