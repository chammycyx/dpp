package hk.org.ha.model.pms.dpp.vo;

import hk.org.ha.model.pms.dpp.udt.ReportDateType;

import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DiscrepancyActionTakenRptCriteria {

	private Date startDate;
	private Date endDate;
	private ReportDateType reportDateType;
	
	public void setStartDate(Date startDate) {
		this.startDate = startDate == null ? null : new Date(startDate.getTime());
	}
	
	public Date getStartDate() {
		return startDate == null ? null : new Date(startDate.getTime());
	}
	
	public void setEndDate(Date endDate) {
		this.endDate = endDate == null ? null : new Date(endDate.getTime());
	}
	
	public Date getEndDate() {
		return endDate == null ? null : new Date(endDate.getTime());
	}

	public void setReportDateType(ReportDateType reportDateType) {
		this.reportDateType = reportDateType;
	}

	public ReportDateType getReportDateType() {
		return reportDateType;
	}
	
}
