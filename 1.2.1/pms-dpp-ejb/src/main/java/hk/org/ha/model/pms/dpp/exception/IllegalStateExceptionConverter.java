package hk.org.ha.model.pms.dpp.exception;

import java.util.Map;

import org.granite.messaging.service.ExceptionConverter;
import org.granite.messaging.service.ServiceException;

public class IllegalStateExceptionConverter implements ExceptionConverter {
	
	public static final String ILLEGAL_STATE = "Persistence.IllegalState";
	
	public boolean accepts(Throwable t, Throwable finalException) {
		return t.getClass().equals(java.lang.IllegalStateException.class);
	}
	
	public ServiceException convert(Throwable t, String detail, Map<String, Object> extendedData) {
		ServiceException se = new ServiceException(ILLEGAL_STATE, t.getMessage(), detail, t);
		se.getExtendedData().putAll(extendedData);
		return se;
	}

}
