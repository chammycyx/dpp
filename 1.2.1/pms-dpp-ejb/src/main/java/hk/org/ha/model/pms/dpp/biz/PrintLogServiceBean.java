package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecordVer;
import hk.org.ha.model.pms.dpp.persistence.PrintLog;
import hk.org.ha.model.pms.dpp.udt.ActionStatus;
import hk.org.ha.model.pms.dpp.udt.PrepackStatus;
import hk.org.ha.model.pms.dpp.udt.PrintType;
import hk.org.ha.model.pms.dpp.udt.RecordStatus;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("printLogService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PrintLogServiceBean implements PrintLogServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@Out(required=false)
	private List<PrintLog> printLogList;
	
	public void createPrintLogForSampleLabel(Long id, Integer qty) {
		logger.debug("createPrintLogForSampleLabel #0", id);

		PrepackRecordVer prepackRecordVer = em.find(PrepackRecordVer.class, id);
		
		PrintLog printLog = new PrintLog();
		
		printLog.setPrepackRecordVer(prepackRecordVer);
		printLog.setPrintQty(qty);
		printLog.setPrintType(PrintType.SampleLabel);
		printLog.setRecordStatus(RecordStatus.Active);
		
		em.persist(printLog);
		em.flush();
	}
	
	public void createPrintLogForLabel(Long id, Integer qty) {
		logger.debug("createPrintLogForLabel #0 #1", id, qty);
		
		PrepackRecord prepackRecordFind = em.find(PrepackRecord.class, id);
		
		// Add a new PrepackRecordVer
		PrepackRecordVer newPrepackRecordVer = new PrepackRecordVer();
		
		newPrepackRecordVer.setManufCode(prepackRecordFind.getPrepackRecordVer().getManufCode());
		newPrepackRecordVer.setBatchNum(prepackRecordFind.getPrepackRecordVer().getBatchNum());
		newPrepackRecordVer.setExpiryDate(prepackRecordFind.getPrepackRecordVer().getExpiryDate());
		newPrepackRecordVer.setDispenseDosageUnit(prepackRecordFind.getPrepackRecordVer().getDispenseDosageUnit());
		newPrepackRecordVer.setOriginalPackQty(prepackRecordFind.getPrepackRecordVer().getOriginalPackQty());
		newPrepackRecordVer.setOriginalPackSize(prepackRecordFind.getPrepackRecordVer().getOriginalPackSize());
		newPrepackRecordVer.setOriginalTotalQty(prepackRecordFind.getPrepackRecordVer().getOriginalTotalQty());
		newPrepackRecordVer.setPreparationDate(prepackRecordFind.getPrepackRecordVer().getPreparationDate());
		newPrepackRecordVer.setAssignExpiryMonth(prepackRecordFind.getPrepackRecordVer().getAssignExpiryMonth());
		newPrepackRecordVer.setAssignExpiryWeek(prepackRecordFind.getPrepackRecordVer().getAssignExpiryWeek());
		newPrepackRecordVer.setAssignExpiryDate(prepackRecordFind.getPrepackRecordVer().getAssignExpiryDate());
		newPrepackRecordVer.setSampleCheckQty(prepackRecordFind.getPrepackRecordVer().getSampleCheckQty());
		newPrepackRecordVer.setProvisionPrepackQty(prepackRecordFind.getPrepackRecordVer().getProvisionPrepackQty());
		newPrepackRecordVer.setProvisionPrepackLabelQty(prepackRecordFind.getPrepackRecordVer().getProvisionPrepackLabelQty());
		newPrepackRecordVer.setProvisionRemainQty(prepackRecordFind.getPrepackRecordVer().getProvisionRemainQty());
		newPrepackRecordVer.setLabelSize(prepackRecordFind.getPrepackRecordVer().getLabelSize());
		newPrepackRecordVer.setPrepackPackSize(prepackRecordFind.getPrepackRecordVer().getPrepackPackSize());
		newPrepackRecordVer.setActualPrepackDate(prepackRecordFind.getPrepackRecordVer().getActualPrepackDate());
		newPrepackRecordVer.setActualPrepackQty(prepackRecordFind.getPrepackRecordVer().getActualPrepackQty());
		newPrepackRecordVer.setActualLabelRemainQty(prepackRecordFind.getPrepackRecordVer().getActualLabelRemainQty());
		newPrepackRecordVer.setActualAddLabelQty(prepackRecordFind.getPrepackRecordVer().getActualAddLabelQty());
		newPrepackRecordVer.setActualItemRemainQty(prepackRecordFind.getPrepackRecordVer().getActualItemRemainQty());
		newPrepackRecordVer.setPrepareBy(prepackRecordFind.getPrepackRecordVer().getPrepareBy());
		newPrepackRecordVer.setPreCheckBy(prepackRecordFind.getPrepackRecordVer().getPreCheckBy());
		newPrepackRecordVer.setLineClearCheckBy(prepackRecordFind.getPrepackRecordVer().getLineClearCheckBy());
		newPrepackRecordVer.setPrepackBy(prepackRecordFind.getPrepackRecordVer().getPrepackBy());
		newPrepackRecordVer.setFinalCheckBy(prepackRecordFind.getPrepackRecordVer().getFinalCheckBy());
		newPrepackRecordVer.setDiscrepancyRemark(prepackRecordFind.getPrepackRecordVer().getDiscrepancyRemark());
		newPrepackRecordVer.setActionTakenRemark(prepackRecordFind.getPrepackRecordVer().getActionTakenRemark());
		newPrepackRecordVer.setRecordStatus(prepackRecordFind.getPrepackRecordVer().getRecordStatus());
		newPrepackRecordVer.setCancelReason(prepackRecordFind.getPrepackRecordVer().getCancelReason());
		
		newPrepackRecordVer.setPrepackStatus(PrepackStatus.Printed);
		newPrepackRecordVer.setActionStatus(ActionStatus.Printed);
		
		// TODO: lazy get
		newPrepackRecordVer.setContainer(prepackRecordFind.getPrepackRecordVer().getContainer());
		newPrepackRecordVer.setEquipment(prepackRecordFind.getPrepackRecordVer().getEquipment());
		newPrepackRecordVer.setPrepackPackSize(prepackRecordFind.getPrepackRecordVer().getPrepackPackSize());
		newPrepackRecordVer.setDrugItemInfoVer(prepackRecordFind.getPrepackRecordVer().getDrugItemInfoVer());

		newPrepackRecordVer.setPrepackRecord(prepackRecordFind);	
		prepackRecordFind.addPrepackRecordVer(newPrepackRecordVer);
		prepackRecordFind.setPrepackRecordVer(newPrepackRecordVer);
		
		PrepackRecord savedPrepackRecord = em.merge(prepackRecordFind);
		
		em.flush();
		
		// Add Print Log
		PrintLog printLog = new PrintLog();
		
		printLog.setPrepackRecordVer(savedPrepackRecord.getPrepackRecordVer());
		printLog.setPrintQty(qty);
		printLog.setPrintType(PrintType.Label);
		printLog.setRecordStatus(RecordStatus.Active);
		
		em.persist(printLog);
		em.flush();
	}
	
	@SuppressWarnings("unchecked")
	public void retrievePrintLogByPrepackRecordId(Long id) {
		logger.debug("retrievePrintLogByPrepackRecordVerId #0", id);
		
		printLogList = em.createNamedQuery("PrintLog.findByPrepackRecordId")
			.setParameter("prepackRecordId", id)
			.setHint(QueryHints.BATCH, "o.prepackRecordVer")
			.setHint(QueryHints.BATCH, "o.prepackRecordVer.prepackRecord")
			.getResultList();
	}
	
	@Remove
	public void destroy() {
		if(printLogList != null) {
			printLogList = null;
		}
	}
}
