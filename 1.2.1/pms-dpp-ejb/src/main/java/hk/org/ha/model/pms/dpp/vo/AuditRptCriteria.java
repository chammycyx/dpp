package hk.org.ha.model.pms.dpp.vo;

import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class AuditRptCriteria {
	
	private Date startDate;
	private Date endDate;
	
	public void setStartDate(Date startDate) {
		this.startDate = startDate == null ? null : new Date(startDate.getTime());
	}
	
	public Date getStartDate() {
		return startDate == null ? null : new Date(startDate.getTime());
	}
	
	public void setEndDate(Date endDate) {
		this.endDate = endDate == null ? null : new Date(endDate.getTime());
	}
	
	public Date getEndDate() {
		return endDate == null ? null : new Date(endDate.getTime());
	}
	
}
