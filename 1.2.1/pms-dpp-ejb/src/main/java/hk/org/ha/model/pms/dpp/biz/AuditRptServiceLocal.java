package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.model.pms.dpp.vo.AuditRptCriteria;
import hk.org.ha.model.pms.dpp.vo.AuditRptData;

import java.util.List;

import javax.ejb.Local;

@Local
public interface AuditRptServiceLocal {
	
	void retrieveAuditRptList(AuditRptCriteria auditRptCriteria);
	
	List<AuditRptData> getAuditRptList();
	
	void destroy();

}
