package hk.org.ha.model.pms.vo.printing;

import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.printing.PrintStatus;
import hk.org.ha.model.pms.udt.printing.PrintType;

import org.jboss.seam.document.DocumentData.DocumentType;

public class PrintOption {
	
	private PrintType printType;
	private PrintLang printLang;	
	private DocumentType docType;
	private PrintStatus printStatus;
	
	public PrintOption() {
		this(PrintType.Normal, PrintLang.Eng);
	}

	public PrintOption(PrintType printType, PrintLang printLang) {		
		this(printType, printLang, PrintStatus.Print);
	}

	public PrintOption(PrintType printType, PrintLang printLang, PrintStatus printStatus) {		
		super();
		this.printType = printType;
		this.printLang = printLang;
		this.printStatus = printStatus;
	}

	public PrintType getPrintType() {
		return printType;
	}

	public void setPrintType(PrintType printType) {
		this.printType = printType;
	}

	public PrintLang getPrintLang() {
		return printLang;
	}

	public void setPrintLang(PrintLang printLang) {
		this.printLang = printLang;
	}

	public DocumentType getDocType() {
		return docType;
	}

	public void setDocType(DocumentType docType) {
		this.docType = docType;
	}

	public PrintStatus getPrintStatus() {
		return printStatus;
	}

	public void setPrintStatus(PrintStatus printStatus) {
		this.printStatus = printStatus;
	}
}