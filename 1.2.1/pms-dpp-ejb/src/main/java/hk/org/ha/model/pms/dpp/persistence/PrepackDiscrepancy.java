package hk.org.ha.model.pms.dpp.persistence;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name="PREPACK_DISCREPANCY")
@Customizer(AuditCustomizer.class)
@NamedQueries({
	@NamedQuery(name="PrepackDiscrepancy.findByPrepackRecordVerId", query="select o from PrepackDiscrepancy o where o.prepackRecordVer.id = :prepackRecordVerId"),
	@NamedQuery(name="PrepackDiscrepancy.findDeletedDiscrepancyByPrepackRecordVerId", query="select o from PrepackDiscrepancy o where o.prepackRecordVer.id = :prepackRecordVerId and o.discrepancy.recordStatus = hk.org.ha.model.pms.dpp.udt.RecordStatus.Deleted")
})
public class PrepackDiscrepancy extends VersionEntity {
	
	private static final long serialVersionUID = 1232118255823402661L;
	
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="prepackDiscrepancySeq")
	@SequenceGenerator(name="prepackDiscrepancySeq", sequenceName="SQ_PREPACK_DISCREPANCY", initialValue=100000000)
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PREPACK_RECORD_VER_ID", referencedColumnName="ID")
	private PrepackRecordVer prepackRecordVer;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DISCREPANCY_ID", referencedColumnName="ID")
	private Discrepancy discrepancy;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setPrepackRecordVer(PrepackRecordVer prepackRecordVer) {
		this.prepackRecordVer = prepackRecordVer;
	}

	public PrepackRecordVer getPrepackRecordVer() {
		return prepackRecordVer;
	}

	public void setDiscrepancy(Discrepancy discrepancy) {
		this.discrepancy = discrepancy;
	}

	public Discrepancy getDiscrepancy() {
		return discrepancy;
	}

}
