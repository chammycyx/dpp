package hk.org.ha.model.pms.udt.printing;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PrintLang implements StringValuedEnum {

	Eng("en", "English"),
	Chi("zh_TW", "Chinese");
	
    private final String dataValue;
    private final String displayValue;

    PrintLang(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    
    public static PrintLang dataValueOf(String dataValue) {
    	return StringValuedEnumReflect.getObjectFromValue(PrintLang.class, dataValue);
    }
    
	public static class Converter extends StringValuedEnumConverter<PrintLang> {

		private static final long serialVersionUID = -4725106820503119408L;

		@Override
    	public Class<PrintLang> getEnumClass() {
    		return PrintLang.class;
    	}
    }
}



