package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmInstDrug;
import hk.org.ha.model.pms.dpp.cacher.DmDrugCacherInf;
import hk.org.ha.model.pms.dpp.cacher.DmInstDrugCacherInf;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("dmDrugListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DmDrugListServiceBean implements DmDrugListServiceLocal {

	@Logger
	private Log logger;

	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private DmInstDrugCacherInf dmInstDrugCacher;

	@Out(required=false)
	private List<DmDrug> dmDrugList;
	
	public void retrieveDmDrugListLikeItemCode(String prefixItemCode)
	{
		logger.debug("retrieveDmDrugListLikeItemCode #0 ", prefixItemCode);
		
		List<DmDrug> tmpDmDrugList = dmDrugCacher.getDrugListByItemCode(prefixItemCode);	
		dmDrugList = new ArrayList<DmDrug>();
		
		for(DmDrug dmDrug : tmpDmDrugList) {
			DmInstDrug dmInstDrug = dmInstDrugCacher.getDrugByItemCode(dmDrug.getItemCode());
			if(dmInstDrug != null && dmInstDrug.getInstAvailability().equals("N")) {
				continue;
			}
			dmDrugList.add(dmDrug);
		}

		logger.info("dmDrugList size #0", dmDrugList.size());
	}

	@Remove
	public void destroy() 
	{
		if (dmDrugList != null) {
			dmDrugList = null;
		}
	}
}
