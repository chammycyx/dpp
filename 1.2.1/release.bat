@echo off

set releaseVersion=

if "%2" neq "" set releaseVersion=-DreleaseVersion=%2

if "%1" equ "test" goto TEST
if "%1" equ "tag" goto TAG

echo Usage: release (test [Version] ^| tag [Version])

goto EOF

:TEST
mvn release:prepare -DdryRun=true %releaseVersion%
	
goto EOF

:TAG
mvn release:clean release:prepare release:perform %releaseVersion%

:EOF
