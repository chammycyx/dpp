<?xml version="1.0" encoding="UTF-8"?>
<story:Storyboard xmlns:story="http://wireframesketcher.com/1.0/model/story">
  <panels x="75" y="375">
    <screen href="ActionTaken.screen#/"/>
  </panels>
  <panels x="975" y="75">
    <screen href="Container.screen#/"/>
  </panels>
  <panels x="675" y="75">
    <screen href="Discrepancy.screen#/"/>
  </panels>
  <panels x="375" y="75">
    <screen href="DrugItemInfo.screen#/"/>
  </panels>
  <panels x="75" y="75">
    <screen href="Equipment.screen#/"/>
  </panels>
</story:Storyboard>
