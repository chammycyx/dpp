<?xml version="1.0" encoding="UTF-8"?>
<story:Storyboard xmlns:story="http://wireframesketcher.com/1.0/model/story">
  <panels x="375" y="75">
    <screen href="PrepackRecordEntry1_OriginalProduct.screen#/"/>
  </panels>
  <panels x="75" y="75">
    <screen href="PrepackRecordEntry2_PrepackItem.screen#/"/>
  </panels>
  <panels x="975" y="75">
    <screen href="Popup_PrintDPPWorksheet.screen#/"/>
  </panels>
  <panels x="675" y="75">
    <screen href="Popup_PrintPrepackLabel.screen#/"/>
  </panels>
  <panels x="75" y="675">
    <screen href="PrepackRecordEnquiry0_Enquiry.screen#/"/>
  </panels>
  <panels x="375" y="975">
    <screen href="Popup_CancelPrepackRecord.screen#/"/>
  </panels>
  <panels x="975" y="375">
    <screen href="PrepackRecordEnquiry1_OriginalProduct.screen#/"/>
  </panels>
  <panels x="675" y="375">
    <screen href="PrepackRecordEnquiry2_PrepackItem.screen#/"/>
  </panels>
  <panels x="375" y="375">
    <screen href="PrepackRecordEnquiry3_ActualPrepackInfo.screen#/"/>
  </panels>
  <panels x="75" y="375">
    <screen href="PrepackRecordEnquiry4_History.screen#/"/>
  </panels>
  <panels x="675" y="675">
    <screen href="ActualPrepackInfo_Enquiry.screen#/"/>
  </panels>
  <panels x="375" y="675">
    <screen href="ActualPrepackInfo_Entry.screen#/"/>
  </panels>
  <panels x="75" y="975">
    <screen href="DrugPrepackRecordWorksheet.screen#/"/>
  </panels>
  <panels x="975" y="675">
    <screen href="PrepackLabel.screen#/"/>
  </panels>
</story:Storyboard>
