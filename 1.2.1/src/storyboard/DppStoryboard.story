<?xml version="1.0" encoding="UTF-8"?>
<story:Storyboard xmlns:story="http://wireframesketcher.com/1.0/model/story">
  <panels x="75" y="375">
    <screen href="maintenance/ActionTaken.screen#/"/>
  </panels>
  <panels x="975" y="75">
    <screen href="maintenance/Container.screen#/"/>
  </panels>
  <panels x="675" y="75">
    <screen href="maintenance/Discrepancy.screen#/"/>
  </panels>
  <panels x="75" y="75">
    <screen href="maintenance/Equipment.screen#/"/>
  </panels>
  <panels x="375" y="75">
    <screen href="maintenance/DrugItemInfo.screen#/"/>
  </panels>
  <panels x="675" y="375">
    <screen href="prepack/PrepackRecordEntry1_OriginalProduct.screen#/"/>
  </panels>
  <panels x="375" y="375">
    <screen href="prepack/PrepackRecordEntry2_PrepackItem.screen#/"/>
  </panels>
  <panels x="75" y="675">
    <screen href="prepack/Popup_PrintDPPWorksheet.screen#/"/>
  </panels>
  <panels x="975" y="375">
    <screen href="prepack/Popup_PrintPrepackLabel.screen#/"/>
  </panels>
  <panels x="675" y="675">
    <screen href="prepack/ActualPrepackInfo_Enquiry.screen#/"/>
  </panels>
  <panels x="375" y="675">
    <screen href="prepack/ActualPrepackInfo_Entry.screen#/"/>
  </panels>
  <panels x="975" y="975">
    <screen href="prepack/PrepackRecordEnquiry0_Enquiry.screen#/"/>
  </panels>
  <panels x="675" y="1275">
    <screen href="prepack/Popup_CancelPrepackRecord.screen#/"/>
  </panels>
  <panels x="675" y="975">
    <screen href="prepack/PrepackRecordEnquiry1_OriginalProduct.screen#/"/>
  </panels>
  <panels x="375" y="975">
    <screen href="prepack/PrepackRecordEnquiry2_PrepackItem.screen#/"/>
  </panels>
  <panels x="75" y="975">
    <screen href="prepack/PrepackRecordEnquiry3_ActualPrepackInfo.screen#/"/>
  </panels>
  <panels x="975" y="675">
    <screen href="prepack/PrepackRecordEnquiry4_History.screen#/"/>
  </panels>
  <panels x="375" y="1275">
    <screen href="prepack/DrugPrepackRecordWorksheet.screen#/"/>
  </panels>
  <panels x="75" y="1275">
    <screen href="prepack/PrepackLabel.screen#/"/>
  </panels>
  <panels x="375" y="1575">
    <screen href="report/DrugPrepackDailyLogReport.screen#/"/>
  </panels>
  <panels x="75" y="1575">
    <screen href="report/DiscrepancyActionTakenReport.screen#/"/>
  </panels>
  <panels x="975" y="1275">
    <screen href="report/AuditReport.screen#/"/>
  </panels>
  <panels x="675" y="1575">
    <screen href="report/EnquiryReport.screen#/"/>
  </panels>
</story:Storyboard>
