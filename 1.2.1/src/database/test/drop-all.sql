ALTER TABLE PREPACK_PACK_SIZE DROP CONSTRAINT PREPACKPACKSIZEDRUGITEMINFO_ID
ALTER TABLE PREPACK_RECORD_VER DROP CONSTRAINT PRPCKRECORDVERPRPACKPACKSIZEID
ALTER TABLE PREPACK_RECORD_VER DROP CONSTRAINT PREPACK_RECORD_VEREQUIPMENT_ID
ALTER TABLE PREPACK_RECORD_VER DROP CONSTRAINT PRPCKRECORDVERDRGITEMINFOVERID
ALTER TABLE PREPACK_RECORD_VER DROP CONSTRAINT PRPACKRECORDVERPREPACKRECORDID
ALTER TABLE PREPACK_RECORD_VER DROP CONSTRAINT PREPACK_RECORD_VERCONTAINER_ID
ALTER TABLE DRUG_ITEM_INFO_VER DROP CONSTRAINT DRUGITEMINFOVERDRUGITEMINFO_ID
ALTER TABLE PREPACK_ACTION_TAKEN DROP CONSTRAINT PRPCKCTIONTAKENPRPCKRCORDVERID
ALTER TABLE PREPACK_ACTION_TAKEN DROP CONSTRAINT PREPACKACTIONTAKENCTIONTAKENID
ALTER TABLE PRINT_LOG DROP CONSTRAINT PRINT_LOGPREPACK_RECORD_VER_ID
ALTER TABLE PREPACK_RECORD DROP CONSTRAINT PRPACKRECORDPREPACKRECORDVERID
ALTER TABLE PREPACK_DISCREPANCY DROP CONSTRAINT PREPACKDISCREPANCYDSCREPANCYID
ALTER TABLE PREPACK_DISCREPANCY DROP CONSTRAINT PRPCKDSCREPANCYPRPCKRCORDVERID
ALTER TABLE DRUG_ITEM_INFO DROP CONSTRAINT DRUGITEMINFODRUGITEMINFOVER_ID
ALTER TABLE DRUG_ITEM_INFO DROP CONSTRAINT UNQ_DRUG_ITEM_INFO_0
DROP TABLE PREPACK_PACK_SIZE
DROP TABLE PREPACK_RECORD_VER
DROP TABLE DRUG_ITEM_INFO_VER
DROP TABLE CONTAINER
DROP TABLE PREPACK_ACTION_TAKEN
DROP TABLE EQUIPMENT
DROP TABLE PRINT_LOG
DROP TABLE INST_LOT_NUM
DROP TABLE ACTION_TAKEN
DROP TABLE AUDIT_LOG
DROP TABLE SYSTEM_MESSAGE
DROP TABLE PREPACK_RECORD
DROP TABLE PREPACK_DISCREPANCY
DROP TABLE DISCREPANCY
DROP TABLE DRUG_ITEM_INFO
DROP SEQUENCE SQ_EQUIPMENT
DROP SEQUENCE SQ_DISCREPANCY
DROP SEQUENCE SQ_PREPACK_DISCREPANCY
DROP SEQUENCE SQ_PREPACK_RECORD_VER
DROP SEQUENCE SQ_PREPACK_RECORD
DROP SEQUENCE SQ_PRINT_LOG
DROP SEQUENCE SQ_DRUG_ITEM_INFO_VER
DROP SEQUENCE SQ_CONTAINER
DROP SEQUENCE SQ_PREPACK_PACK_SIZE
DROP SEQUENCE SQ_PREPACK_ACTION_TAKEN
DROP SEQUENCE SQ_DRUG_ITEM_INFO
DROP SEQUENCE SQ_ACTION_TAKEN
DROP SEQUENCE SQ_INST_LOT_NUM
DROP SEQUENCE SQ_AUDIT_LOG
