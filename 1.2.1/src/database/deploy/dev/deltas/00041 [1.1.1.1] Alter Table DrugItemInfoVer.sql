alter table DRUG_ITEM_INFO_VER add DISPLAY_SIDE_LABEL_ALIAS_FLAG varchar2(1);
comment on column DRUG_ITEM_INFO_VER.DISPLAY_SIDE_LABEL_ALIAS_FLAG is 'Display side label alias flag';
alter table DRUG_ITEM_INFO_VER add DISPLAY_SIDE_SHORT_DESC_FLAG varchar2(1); 
comment on column DRUG_ITEM_INFO_VER.DISPLAY_SIDE_SHORT_DESC_FLAG is 'Display side label short desc flag';

update DRUG_ITEM_INFO_VER set DISPLAY_SIDE_LABEL_ALIAS_FLAG = 'N', DISPLAY_SIDE_SHORT_DESC_FLAG = 'N';

alter table DRUG_ITEM_INFO_VER modify DISPLAY_SIDE_LABEL_ALIAS_FLAG varchar2(1) not null;
alter table DRUG_ITEM_INFO_VER modify DISPLAY_SIDE_SHORT_DESC_FLAG varchar2(1) not null;

insert into system_message values ( '0079', 1008, 1000, 0, 'W', 'Please input an alias', 'en_US', '' ,'', '', NULL, NULL, '',  NULL, NULL, '', NULL, NULL, NULL, NULL, 1,  sysdate, 'itd', sysdate, 'itd');
insert into system_message values ( '0080', 1008, 1000, 0, 'W', 'Please input a short description', 'en_US', '' ,'', '', NULL, NULL, '',  NULL, NULL, '', NULL, NULL, NULL, NULL, 1,  sysdate, 'itd', sysdate, 'itd');
insert into system_message values ( '0081', 1008, 2000, 0, 'W', 'This record is not allowed to print because the item display name of the side label is empty', 'en_US', 'Please choose to display the alias, short description or both on the side label in the Drug Item Information Maintenance' ,'', '', NULL, NULL, '',  NULL, NULL, '', NULL, NULL, NULL, NULL, 1,  sysdate, 'itd', sysdate, 'itd');
insert into system_message values ( '0082', 1008, 2000, 0, 'W', 'The corresponding drug item information or chosen pre-packing record has been updated just then, please retrieve the pre-packing record again before worksheet or label printing to ensure data integrity', 'en_US', '' ,'', '', NULL, NULL, '',  NULL, NULL, '', NULL, NULL, NULL, NULL, 1,  sysdate, 'itd', sysdate, 'itd');

--//@UNDO
alter table DRUG_ITEM_INFO_VER drop column DISPLAY_SIDE_LABEL_ALIAS_FLAG;
alter table DRUG_ITEM_INFO_VER drop column DISPLAY_SIDE_SHORT_DESC_FLAG;

delete system_message where message_code in ('0079', '0080', '0081', '0082');
--//
