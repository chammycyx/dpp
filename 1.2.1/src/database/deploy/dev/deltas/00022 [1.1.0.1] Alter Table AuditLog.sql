update AUDIT_LOG set LOG_TYPE = 'T' where LOG_TYPE = 'FromTimingFilter';
update AUDIT_LOG set LOG_TYPE = 'E' where LOG_TYPE = 'FromCustomizer';
update AUDIT_LOG set LOG_TYPE = 'M' where LOG_TYPE = 'FromMessageCode';
update AUDIT_LOG set LOG_TYPE = 'N' where LOG_TYPE = 'Manual';

alter table AUDIT_LOG modify LOG_TYPE varchar2(1);
alter table AUDIT_LOG modify MESSAGE varchar2(3000);

alter table AUDIT_LOG add PACKAGE_NAME varchar2(100) null;
alter table AUDIT_LOG add EXCEPTION_PACKAGE_NAME varchar2(100) null;
alter table AUDIT_LOG add EXCEPTION_CLASS_NAME varchar2(100) null;
alter table AUDIT_LOG add EXCEPTION_MESSAGE varchar2(1000) null;
alter table AUDIT_LOG add CAUSE_PACKAGE_NAME varchar2(100) null;
alter table AUDIT_LOG add CAUSE_CLASS_NAME varchar2(100) null;
alter table AUDIT_LOG add CAUSE_MESSAGE varchar2(1000) null;


--//@UNDO
alter table AUDIT_LOG drop column PACKAGE_NAME;
alter table AUDIT_LOG drop column EXCEPTION_PACKAGE_NAME;
alter table AUDIT_LOG drop column EXCEPTION_CLASS_NAME;
alter table AUDIT_LOG drop column EXCEPTION_MESSAGE;
alter table AUDIT_LOG drop column CAUSE_PACKAGE_NAME;
alter table AUDIT_LOG drop column CAUSE_CLASS_NAME;
alter table AUDIT_LOG drop column CAUSE_MESSAGE;

alter table AUDIT_LOG modify MESSAGE varchar2(1000);
alter table AUDIT_LOG modify LOG_TYPE varchar2(50);

update AUDIT_LOG set LOG_TYPE = 'FromTimingFilter' where LOG_TYPE = 'T';
update AUDIT_LOG set LOG_TYPE = 'FromCustomizer' where LOG_TYPE = 'E';
update AUDIT_LOG set LOG_TYPE = 'FromMessageCode' where LOG_TYPE = 'M';
update AUDIT_LOG set LOG_TYPE = 'Manual' where LOG_TYPE = 'N';
--//
