update SYSTEM_MESSAGE set MAIN_MSG = 'Please input an original product use before' where MESSAGE_CODE = '0038';
update SYSTEM_MESSAGE set MAIN_MSG = 'Original product use before should be later than today' where MESSAGE_CODE = '0064';

insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, VERSION, UPDATE_DATE, UPDATE_USER, CREATE_DATE, CREATE_USER) values ( '0078', 1008, 2000, 0, 'W', '#0', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 1,  sysdate, 'itd', sysdate, 'itd');

--//@UNDO
update SYSTEM_MESSAGE set MAIN_MSG = 'Please input an original product expiry date' where MESSAGE_CODE = '0038';
update SYSTEM_MESSAGE set MAIN_MSG = 'Original product expiry date should be later than today' where MESSAGE_CODE = '0064';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0078';
--//
