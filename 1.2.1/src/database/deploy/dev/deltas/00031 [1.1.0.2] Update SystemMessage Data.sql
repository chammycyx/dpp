update SYSTEM_MESSAGE set MAIN_MSG = 'Please input an original pack unit' where MESSAGE_CODE = '0016';
update SYSTEM_MESSAGE set MAIN_MSG = 'Same user for prepared by and pre-checked by is not allowed' where MESSAGE_CODE = '0045';
update SYSTEM_MESSAGE set MAIN_MSG = 'Same user for pre-packed by and final checked by is not allowed' where MESSAGE_CODE = '0046';
update SYSTEM_MESSAGE set MAIN_MSG = 'Invalid Corp ID for pre-checked by' where MESSAGE_CODE = '0058';
update SYSTEM_MESSAGE set MAIN_MSG = 'Invalid Corp ID for line clearance checked by' where MESSAGE_CODE = '0059';
update SYSTEM_MESSAGE set MAIN_MSG = 'Invalid Corp ID for final checked by' where MESSAGE_CODE = '0060';
update SYSTEM_MESSAGE set MAIN_MSG = 'Print quantity cannot be more than the provisional pre-pack label quantity' where MESSAGE_CODE = '0062';
update SYSTEM_MESSAGE set MAIN_MSG = 'Total original product quantity cannot be less than the pre-pack pack size' where MESSAGE_CODE = '0067';
update SYSTEM_MESSAGE set MAIN_MSG = 'Cannot reach the printer station #0' where MESSAGE_CODE = '0078';

--//@UNDO
update SYSTEM_MESSAGE set MAIN_MSG = 'Pack unit should not be blank' where MESSAGE_CODE = '0016';
update SYSTEM_MESSAGE set MAIN_MSG = 'Same user for Prepared By and Pre-checked By is not allowed' where MESSAGE_CODE = '0045';
update SYSTEM_MESSAGE set MAIN_MSG = 'Same user for Pre-packed By and Final Checked By is not allowed' where MESSAGE_CODE = '0046';
update SYSTEM_MESSAGE set MAIN_MSG = 'Invalid CUID for pre-checked by' where MESSAGE_CODE = '0058';
update SYSTEM_MESSAGE set MAIN_MSG = 'Invalid CUID for line clearance checked by' where MESSAGE_CODE = '0059';
update SYSTEM_MESSAGE set MAIN_MSG = 'Invalid CUID for final checked by' where MESSAGE_CODE = '0060';
update SYSTEM_MESSAGE set MAIN_MSG = 'Print quantity cannot be more than the provisional pre-pack label qty' where MESSAGE_CODE = '0062';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please input pack size' where MESSAGE_CODE = '0067';
update SYSTEM_MESSAGE set MAIN_MSG = '#0' where MESSAGE_CODE = '0078';
--//
