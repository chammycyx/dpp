create table SYSTEM_LOG (
	ID number(19) not null,
	HOSP_CODE varchar2(3) null,
	WORKSTORE_CODE varchar2(4) null,
	WORKSTATION_ID varchar2(100) null,
	USER_ID varchar2(20) null,
	APPLICATION_ID number(10) null,
	REQUEST_URI varchar2(500) null,
	TRAN_ID varchar2(50) null,
	ENTITY_NAME varchar2(100) null,
	MESSAGE_ID varchar2(5) null,
	MESSAGE varchar2(3000) not null,
	DETAIL_MESSAGE clob null,
	CREATE_DATE timestamp not null,
	PACKAGE_NAME varchar2(100) null,
	CLASS_NAME varchar2(100) null,
	METHOD_NAME varchar2(100) null,
	LINE_NUMBER varchar2(20) null,
	FILE_NAME varchar2(100) null,
	ELAPSE_TIME number(10) null,
	LOG_TYPE varchar2(1) not null,
	HOST_NAME varchar2(50) null,
	EXCEPTION_PACKAGE_NAME varchar2(100) null,
	EXCEPTION_CLASS_NAME varchar2(100) null,
	EXCEPTION_MESSAGE varchar2(1000) null,
	CAUSE_PACKAGE_NAME varchar2(100) null,
	CAUSE_CLASS_NAME varchar2(100) null,
	CAUSE_MESSAGE varchar2(1000) null,	
	constraint PK_SYSTEM_LOG primary key (ID) using index tablespace DPP_SLOG_INDX_01)
partition by range(CREATE_DATE) (
	partition P_DPP_SLOG_DATA_2012 values less than (MAXVALUE) tablespace P_DPP_SLOG_DATA_2012);

create index I_SYSTEM_LOG_01 on SYSTEM_LOG (CREATE_DATE) local;

create sequence SQ_SYSTEM_LOG start with 10000000000000;

comment on table  SYSTEM_LOG is 'System logs written by audit logger';
comment on column SYSTEM_LOG.ID is 'System log id';
comment on column SYSTEM_LOG.HOSP_CODE is 'Hospital code';
comment on column SYSTEM_LOG.WORKSTORE_CODE is 'Workstore code';
comment on column SYSTEM_LOG.WORKSTATION_ID is 'Workstation id';
comment on column SYSTEM_LOG.USER_ID is 'Actioned user';
comment on column SYSTEM_LOG.APPLICATION_ID is 'Application id';
comment on column SYSTEM_LOG.REQUEST_URI is 'Access URL';
comment on column SYSTEM_LOG.TRAN_ID is 'EclipseLink customizer - unit of work';
comment on column SYSTEM_LOG.ENTITY_NAME is 'Java entity name';
comment on column SYSTEM_LOG.MESSAGE_ID is 'System message code';
comment on column SYSTEM_LOG.MESSAGE is 'Log message';
comment on column SYSTEM_LOG.DETAIL_MESSAGE is 'Log message more than 1000 characters';
comment on column SYSTEM_LOG.CREATE_DATE is 'Created date';
comment on column SYSTEM_LOG.CLASS_NAME is 'Java class name';
comment on column SYSTEM_LOG.METHOD_NAME is 'Java method name';
comment on column SYSTEM_LOG.LINE_NUMBER is 'Line number of Java source code';
comment on column SYSTEM_LOG.PACKAGE_NAME is 'Java package name';
comment on column SYSTEM_LOG.FILE_NAME is 'Java file name';
comment on column SYSTEM_LOG.ELAPSE_TIME is 'Elapsed time';
comment on column SYSTEM_LOG.LOG_TYPE is 'Log type';
comment on column SYSTEM_LOG.HOST_NAME is 'Workstation name';
comment on column SYSTEM_LOG.EXCEPTION_PACKAGE_NAME is 'Exception package name';
comment on column SYSTEM_LOG.EXCEPTION_CLASS_NAME is 'Exception class name';
comment on column SYSTEM_LOG.EXCEPTION_MESSAGE is 'Exception message';
comment on column SYSTEM_LOG.CAUSE_PACKAGE_NAME is 'Cause package name';
comment on column SYSTEM_LOG.CAUSE_CLASS_NAME is 'Cause class name';
comment on column SYSTEM_LOG.CAUSE_MESSAGE is 'Cause message';


--//@UNDO
drop sequence SQ_SYSTEM_LOG;
drop index I_SYSTEM_LOG_01;

drop table SYSTEM_LOG;
--//