<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009"
	xmlns:s="library://ns.adobe.com/flex/spark"
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:prepack="hk.org.ha.view.pms.dpp.prepack.*"
	width="100%" height="100%"
	>
	
	<fx:Metadata>
		[Name("prepackRecordAddView")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.dpp.AddPrepackRecordEvent;
			import hk.org.ha.event.pms.dpp.ChangePrepackRecordAddViewTabStateEvent;
			import hk.org.ha.event.pms.dpp.CheckPrepackItemVersionEvent;
			import hk.org.ha.event.pms.dpp.CreatePrepackRecordEvent;
			import hk.org.ha.event.pms.dpp.RefreshDrugItemInfoEvent;
			import hk.org.ha.event.pms.dpp.ResetDrugItemInfoEvent;
			import hk.org.ha.event.pms.dpp.RetrieveContainerListEvent;
			import hk.org.ha.event.pms.dpp.RetrieveDmDrugListEvent;
			import hk.org.ha.event.pms.dpp.RetrieveDrugItemInfoEvent;
			import hk.org.ha.event.pms.dpp.RetrieveEquipmentListEvent;
			import hk.org.ha.event.pms.dpp.show.ShowItemCodeLookupPopupEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.fmk.pms.security.UamInfo;
			import hk.org.ha.model.pms.dms.persistence.DmDrug;
			import hk.org.ha.model.pms.dms.persistence.DmFormDosageUnitMapping;
			import hk.org.ha.model.pms.dms.persistence.DmFormDosageUnitMappingPK;
			import hk.org.ha.model.pms.dpp.persistence.DrugItemInfo;
			import hk.org.ha.model.pms.dpp.persistence.DrugItemInfoVer;
			import hk.org.ha.model.pms.dpp.persistence.PrepackPackSize;
			import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;
			import hk.org.ha.model.pms.dpp.persistence.PrepackRecordVer;
			import hk.org.ha.model.pms.dpp.udt.DisplaySideLabelAliasFlag;
			import hk.org.ha.model.pms.dpp.udt.DisplaySideLabelShortDescFlag;
			import hk.org.ha.model.pms.dpp.udt.ExpiryMonth;
			import hk.org.ha.model.pms.dpp.udt.ExpiryWeek;
			import hk.org.ha.model.pms.dpp.udt.LabelSize;
			import hk.org.ha.model.pms.dpp.udt.PrepackFlag;
			import hk.org.ha.model.pms.dpp.udt.PrepackStatus;
			import hk.org.ha.model.pms.dpp.udt.RecordStatus;
			import hk.org.ha.view.pms.dpp.prepack.popup.PrepackItemPrintPopup;
			
			import mx.collections.ArrayCollection;
			import mx.collections.Sort;
			import mx.collections.SortField;
			import mx.events.IndexChangedEvent;
			import mx.managers.PopUpManager;
			import mx.utils.StringUtil;
			
			import spark.events.TextOperationEvent;
			
			[In] [Bindable]
			public var uamInfo:UamInfo;
			
			[Bindable]
			public var drugItemInfo:DrugItemInfo;
			
			[In] [Bindable]
			public var dmDrugList:ArrayCollection;
			
			[Bindable]
			public var dmDrug:DmDrug;
			
			[Bindable]
			public var prepackRecord:PrepackRecord;
			
			[Bindable]
			public var discrepancyList:ArrayCollection;
			
			[Bindable]
			public var actionTakenList:ArrayCollection;
			
			[Bindable]
			public var prepackRecordVerList:ArrayCollection;
			
			[Bindable]
			public var prepackPackSizeList:ArrayCollection;
			
			[Bindable]
			public var equipmentList:ArrayCollection;
			
			[Bindable]
			public var containerList:ArrayCollection;
			
			private var prepackPackSize:PrepackPackSize;
			
			private var screenName:String = "prepackRecordAddView";
			
			[Bindable]
			private var isEditable:Boolean = true;
			
			[Bindable]
			public var packUnitList:ArrayCollection;
			
			public var prepackItemPrintPopup:PrepackItemPrintPopup = null;
			
			public var selectedTabIndex:int = 0;
			
			private var latestPrepackSizeList:ArrayCollection;
			
			private function prepackingRecordTab_createComplete():void {
				prepackingRecordTab.selectedIndex = selectedTabIndex;
			}
			
			public function enableEdit(value:Boolean):void {
				isEditable = value;
			}
			
			private function initData():void {
				dispatchEvent(new AddPrepackRecordEvent());
				dispatchEvent(new RetrieveEquipmentListEvent());
				dispatchEvent(new RetrieveContainerListEvent());
			}
			
			private function resetBindedVariables():void {
				dispatchEvent(new ResetDrugItemInfoEvent());
				prepackPackSizeList = new ArrayCollection();
				prepackRecordOriginalProductView.expiryDate.text = "";
				prepackRecordOriginalProductView.expiryDate.errorString = "";
			}
			
			public override function onShowLater():void {
				if ( prepackingRecordTab.getChildByName("prepackRecordActualPrepackInfoView") != null ) {
					prepackingRecordTab.removeChild(prepackingRecordTab.getChildByName("prepackRecordActualPrepackInfoView"));
				}
				if ( prepackingRecordTab.getChildByName("prepackRecordHistoryView") != null ) {
					prepackingRecordTab.removeChild(prepackingRecordTab.getChildByName("prepackRecordHistoryView"));
				}
				clearItem();
			}
			
			private function enableEditableForComponents():void {
				prepackRecordOriginalProductView.enableEdit(true);
				prepackRecordItemView.enableEdit(true);
			}
			
			private function showSystemMessage(errorCode:String, okFunc:Function=null, yesFunc:Function=null):void {
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				
				if(okFunc != null) {
					msgProp.setOkCancelButton = true;
					msgProp.okHandler = okFunc;
				} else if(yesFunc != null) {
					msgProp.setYesNoButton = true;
					msgProp.yesHandler = yesFunc;
				} else {
					msgProp.setOkButtonOnly = true;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			protected function txtItemCode_changeHandler(evt:TextOperationEvent):void {
				var length:int = (evt.currentTarget as TextInput).text.length;
				if(length > 0) {
					if (length == 6) {
						btnItemCodeLookup.setFocus();
						dispatchEvent(new RetrieveDrugItemInfoEvent((evt.currentTarget as TextInput).text, new RefreshDrugItemInfoEvent(screenName)));
					} else {
						this.currentState = "allowLookup";
					}	
				} else {
					this.currentState = "disallowLookup";
				}
			}
			
			protected function btnItemCodeLookup_clickHandler(evt:MouseEvent):void
			{
				var itemCode:String = txtItemCode.text;
				dispatchEvent(new RetrieveDmDrugListEvent(itemCode, new ShowItemCodeLookupPopupEvent(screenName)));
			}
			
			private function dtfPreparationDate_dateChangeHandler():void {
				prepackRecordItemView.updateAssignedExpiryDate();
			}
			
			public function clearItem():void {	
				this.currentState = "clear";
				resetForm(pPrepackRecord);
				resetBindedVariables();
				initData();
				enableEdit(true);
				enableEditableForComponents();
				prepackingRecordTab.selectedIndex = 0;
				txtItemCode.setFocus();
				prepackRecordItemView.resetAndEdit();
			}
			
			public function clearPackUnitList():void {
				packUnitList = null;
			}

			public function saveItem():void {
				if (!validatePrepackRecord()) {
					return;
				}
				
				// System shall be able to pop up an alert message when users click the "Save" or "Print" button 
				// if system does not allow users to print Drug Pre-packing record worksheet and pre-packing label.
				if (prepackRecord.prepackRecordVer.labelSize == LabelSize.DppLabel1Size4x2 ||
					prepackRecord.prepackRecordVer.labelSize == LabelSize.DppLabel1Size4x1p33) {
					// Large size label
					if (drugItemInfo.drugItemInfoVer.itemLongDesc == null ||
						StringUtil.trim(drugItemInfo.drugItemInfoVer.itemLongDesc).length == 0) {
						showSystemMessage("0073");
					}
				} else if (prepackRecord.prepackRecordVer.labelSize == LabelSize.DppWithSideLabel1Size4x1p33 ||
						   prepackRecord.prepackRecordVer.labelSize == LabelSize.DppWithSideLabel1Size4x2 ||
						   prepackRecord.prepackRecordVer.labelSize == LabelSize.DppWithSideLabel2Size4x1p33 ||
						   prepackRecord.prepackRecordVer.labelSize == LabelSize.DppWithSideLabel2Size4x2)
				{
					if ((prepackRecord.prepackRecordVer.labelSize == LabelSize.DppWithSideLabel1Size4x2 ||
						prepackRecord.prepackRecordVer.labelSize == LabelSize.DppWithSideLabel1Size4x1p33) &&
						(drugItemInfo.drugItemInfoVer.itemLongDesc == null ||
						StringUtil.trim(drugItemInfo.drugItemInfoVer.itemLongDesc).length == 0))
					{
						showSystemMessage("0073");
					}
					else if ((prepackRecord.prepackRecordVer.labelSize == LabelSize.DppWithSideLabel2Size4x1p33 ||
							 prepackRecord.prepackRecordVer.labelSize == LabelSize.DppWithSideLabel2Size4x2) &&
							 (drugItemInfo.drugItemInfoVer.itemShortDesc == null ||
							 StringUtil.trim(drugItemInfo.drugItemInfoVer.itemShortDesc).length == 0))
					{
						showSystemMessage("0074");
					}					
					else if (drugItemInfo.drugItemInfoVer.displaySideLabelAliasFlag == DisplaySideLabelAliasFlag.No && 
						drugItemInfo.drugItemInfoVer.displaySideLabelShortDescFlag == DisplaySideLabelShortDescFlag.No)
					{
						showSystemMessage("0081");
					}
					else if ((drugItemInfo.drugItemInfoVer.displaySideLabelShortDescFlag == DisplaySideLabelShortDescFlag.Yes && 
							 (drugItemInfo.drugItemInfoVer.itemShortDesc == null || 
							 StringUtil.trim(drugItemInfo.drugItemInfoVer.itemShortDesc).length == 0)) ||
							 (drugItemInfo.drugItemInfoVer.displaySideLabelAliasFlag == DisplaySideLabelAliasFlag.Yes && 
							 (drugItemInfo.drugItemInfoVer.alias == null || 
							 StringUtil.trim(drugItemInfo.drugItemInfoVer.alias).length == 0)))
					{
						showSystemMessage("0081");
					}
				} else {
					// 1/2 size label or 1/3 size label
					if (drugItemInfo.drugItemInfoVer.itemShortDesc == null ||
						StringUtil.trim(drugItemInfo.drugItemInfoVer.itemShortDesc).length == 0) {
						showSystemMessage("0074");
					}
				}
				
				with (prepackRecordItemView) {
					if ( expiryDateGroup.selectedValue == "Original Product Use Before" ) {	
						prepackRecord.prepackRecordVer.assignExpiryMonth = ExpiryMonth.NullValue;
						prepackRecord.prepackRecordVer.assignExpiryWeek = ExpiryWeek.NullValue;
					}
				}
				
				prepackRecord.prepackRecordVer.drugItemInfoVer = drugItemInfo.drugItemInfoVer;
				prepackRecord.prepackRecordVer.prepackStatus = PrepackStatus.New;
				prepackRecord.prepackRecordVer.prepareBy = uamInfo.userId;
				prepackRecord.prepackRecordVer.recordStatus = RecordStatus.Active;
				
				dispatchEvent(new CreatePrepackRecordEvent(prepackRecord));
				
				prepackRecordOriginalProductView.enableEdit(false);
				prepackRecordItemView.enableEdit(false);
				
				enableEdit(false);
			}
			
			public function printItem():void {
				// System shall be able to pop up an alert message when users click the "Save" or "Print" button 
				// if system does not allow users to print Drug Pre-packing record worksheet and pre-packing label.
				if (prepackRecord.prepackRecordVer.labelSize == LabelSize.DppLabel1Size4x2 ||
						prepackRecord.prepackRecordVer.labelSize == LabelSize.DppLabel1Size4x1p33) {
					// Large size label
					if (drugItemInfo.drugItemInfoVer.itemLongDesc == null ||
						drugItemInfo.drugItemInfoVer.itemLongDesc == "") {
						showSystemMessage("0073");
						return;
					}
				} else if (prepackRecord.prepackRecordVer.labelSize == LabelSize.DppWithSideLabel1Size4x1p33 ||
						   prepackRecord.prepackRecordVer.labelSize == LabelSize.DppWithSideLabel1Size4x2 ||
						   prepackRecord.prepackRecordVer.labelSize == LabelSize.DppWithSideLabel2Size4x1p33 ||
						   prepackRecord.prepackRecordVer.labelSize == LabelSize.DppWithSideLabel2Size4x2)
				{
					if ((prepackRecord.prepackRecordVer.labelSize == LabelSize.DppWithSideLabel1Size4x2 ||
						prepackRecord.prepackRecordVer.labelSize == LabelSize.DppWithSideLabel1Size4x1p33) && 
						(drugItemInfo.drugItemInfoVer.itemLongDesc == null ||
						StringUtil.trim(drugItemInfo.drugItemInfoVer.itemLongDesc).length == 0))
					{
						showSystemMessage("0073");
						return;
					}
					else if ((prepackRecord.prepackRecordVer.labelSize == LabelSize.DppWithSideLabel2Size4x1p33 ||
							 prepackRecord.prepackRecordVer.labelSize == LabelSize.DppWithSideLabel2Size4x2) &&
							 (drugItemInfo.drugItemInfoVer.itemShortDesc == null ||
							 StringUtil.trim(drugItemInfo.drugItemInfoVer.itemShortDesc).length == 0))
					{
						showSystemMessage("0074");
						return;
					} 
					else if (drugItemInfo.drugItemInfoVer.displaySideLabelAliasFlag == DisplaySideLabelAliasFlag.No && 
						drugItemInfo.drugItemInfoVer.displaySideLabelShortDescFlag == DisplaySideLabelShortDescFlag.No)
					{
						showSystemMessage("0081");
						return;
					}
					else if ((drugItemInfo.drugItemInfoVer.displaySideLabelShortDescFlag == DisplaySideLabelShortDescFlag.Yes && 
							 (drugItemInfo.drugItemInfoVer.itemShortDesc == null || 
							 StringUtil.trim(drugItemInfo.drugItemInfoVer.itemShortDesc).length == 0)) ||
							 (drugItemInfo.drugItemInfoVer.displaySideLabelAliasFlag == DisplaySideLabelAliasFlag.Yes && 
							 (drugItemInfo.drugItemInfoVer.alias == null || 
							 StringUtil.trim(drugItemInfo.drugItemInfoVer.alias).length == 0)))
					{
						showSystemMessage("0081");
						return;
					}
				} else {
					// 1/2 size label or 1/3 size label
					if (drugItemInfo.drugItemInfoVer.itemShortDesc == null ||
						drugItemInfo.drugItemInfoVer.itemShortDesc == "") {
						showSystemMessage("0074");
						return;
					}
				}
				
				dispatchEvent(new CheckPrepackItemVersionEvent(prepackRecord, screenName)); 
			}

			public function removeLabelPrintPopup():void {
				PopUpManager.removePopUp(prepackItemPrintPopup);
			}
			
			private function validatePrepackRecord():Boolean {
				if (txtItemCode.text == null || txtItemCode.text == "" ||
					drugItemInfo == null || drugItemInfo.itemCode == null ||
					drugItemInfo.itemCode == "" || drugItemInfo.dmDrug == null || 
					StringUtil.trim(drugItemInfo.dmDrug.itemCode) == "") {
					// Please input a vaild item code
					showSystemMessage("0063");
					txtItemCode.setFocus();
					return false;
				}
				
				if (prepackRecord.prepackRecordVer.manufCode == null) {
					showSystemMessage("0021");
					return false;
				}
				
				if (prepackRecord.prepackRecordVer.batchNum == null || 
					StringUtil.trim(prepackRecord.prepackRecordVer.batchNum) == "" ) {
					showSystemMessage("0022");
					return false;
				}
				
				if (prepackRecord.prepackRecordVer.expiryDate == null) {
					showSystemMessage("0038");
					return false;
				}
				
				// System shall not users to save the record if Original Product Use Before <= Today
				var today:Date = new Date();
				if (prepackRecord.prepackRecordVer.expiryDate <= today) {
					showSystemMessage("0064");
					return false;
				}
				
				if (isNaN(prepackRecord.prepackRecordVer.originalPackQty) ||
					prepackRecordOriginalProductView.txtOriginalPackQty.text == "" ||
					Number(prepackRecord.prepackRecordVer.originalPackQty) <= 0) {
					showSystemMessage("0025");
					return false;
				}
				
				if (isNaN(prepackRecord.prepackRecordVer.originalPackSize) ||
					prepackRecordOriginalProductView.txtOriginalPackSize.text == "" ||
					Number(prepackRecord.prepackRecordVer.originalPackSize) <= 0) {
					showSystemMessage("0026");
					return false;
				}
				
				if (prepackRecordOriginalProductView.cbxDdu.selectedIndex < 0) {
					showSystemMessage("0016");
					return false;
				}
									
				if (prepackRecord.prepackRecordVer.preparationDate == null) {
					showSystemMessage("0028");
					return false;
				}
				
				if (prepackRecord.prepackRecordVer.equipment == null) {
					showSystemMessage("0036");
					return false;
				}
				
				if (prepackRecord.prepackRecordVer.container == null) {
					showSystemMessage("0037");
					return false;
				}

				if (prepackRecord.prepackRecordVer.prepackPackSize == null) {
					showSystemMessage("0065");
					return false;
				}
				
				if (prepackRecord.prepackRecordVer.provisionPrepackQty == 0) {
					showSystemMessage("0067");
					return false;
				}
				
				if (prepackRecord.prepackRecordVer.labelSize == null) {
					showSystemMessage("0035");
					return false;
				}
				
				return true;
			}
			
			public function refreshDrugItemInfo():void {
				// 1. Prompt Error message if DM Drug not found
				if (StringUtil.trim(drugItemInfo.dmDrug.itemCode) == "") {
					this.currentState = "disallowLookup";
					showSystemMessage("0015");
					return;
				}
				
				// 2. bind, filter & sort prepackPackSizeList(grid provider) from drugItemInfo
				// Sort by Unit then Pack Size
				var packSizeField:SortField = new SortField();
				var packUnitSortField:SortField = new SortField();
				packSizeField.name = "packSize";
				packSizeField.numeric = true;
				packUnitSortField.name = "packUnit";
				
				var numericDataSort:Sort = new Sort();
				numericDataSort.fields = [packUnitSortField, packSizeField];
				
				if ( drugItemInfo.prepackPackSizeList != null ) {
					drugItemInfo.prepackPackSizeList.sort = numericDataSort;
					drugItemInfo.prepackPackSizeList.refresh();
					
					ctx.prepackPackSizeList = new ArrayCollection();
					for each(var prepackPackSize:PrepackPackSize in drugItemInfo.prepackPackSizeList) {
						if(prepackPackSize.recordStatus == RecordStatus.Active) {
							ctx.prepackPackSizeList.addItem(prepackPackSize);
						}
					}
				}
				
				// 3. packUnit data massage
				if(packUnitList == null) {
					packUnitList = new ArrayCollection();
				}
				
				var dosageUnitField:SortField = new SortField();
				dosageUnitField.name = "dosageUnit";
				
				var dosageUnitDataSort:Sort = new Sort();
				dosageUnitDataSort.fields = [dosageUnitField];
				
				if ( packUnitList != null ) {
					packUnitList.sort = dosageUnitDataSort;
				}
		
				var containsBaseUnit:Boolean = false;
				packUnitList = new ArrayCollection();
				for each(var ddu:DmFormDosageUnitMapping in drugItemInfo.dmDrug.dmForm.dmFormDosageUnitMappingList) {
					if(ddu.compId.dosageUnit == drugItemInfo.dmDrug.baseUnit) {
						containsBaseUnit = true;
					}
					packUnitList.addItem(ddu.compId);
				}

				if(!containsBaseUnit) {
					var baseUnit:Object = new Object();
					baseUnit.dosageUnit = drugItemInfo.dmDrug.baseUnit;
					packUnitList.addItem(baseUnit);
				}

				//remove deleted drugItemInfo.prepackPackSize.packUnit from drugItemInfo.prepackPackSizeList
				latestPrepackSizeList = new ArrayCollection();
				for each (var ppSize:PrepackPackSize in drugItemInfo.prepackPackSizeList)
				{
					if (ppSize.recordStatus == RecordStatus.Active && latestPrepackSizeList.getItemIndex(ppSize.packUnit) == -1)
					{
						latestPrepackSizeList.addItem(ppSize.packUnit);
					}
				}
				
				//filter packUnitList to match maintainence packUint
				packUnitList.filterFunction = packUnitCollectionFilter;
				packUnitList.refresh();
																
				//auto select if only one pack unit is maint
				if (drugItemInfo.prepackPackSizeList.length == 1 || hasOnlyOnePackUnitMaint())
				{
					prepackRecord.prepackRecordVer.dispenseDosageUnit = drugItemInfo.prepackPackSizeList.getItemAt(0).packUnit;
				}
				
				prepackRecordItemView.filterPrePackUnit(prepackRecord == null?null:prepackRecord.prepackRecordVer.dispenseDosageUnit);
				if (drugItemInfo.drugItemInfoVer.prepackFlag == PrepackFlag.No) {
					showSystemMessage("0042");
					
					clearItem();
					this.currentState = "disallowLookup";
					txtItemCode.setFocus();
				}				
				else if (drugItemInfo.instSuspend == "Y" || drugItemInfo.dmDrug.hqSuspend == "Y") {
					// Allow Pre-pack is off for this item
					showSystemMessage("0066");
					 
					clearItem();
					this.currentState = "disallowLookup";
					txtItemCode.setFocus();
				} else {
					this.currentState = "edit";
				}
			}
			
			private function hasOnlyOnePackUnitMaint():Boolean
			{
				if (drugItemInfo.prepackPackSizeList != null && drugItemInfo.prepackPackSizeList.length == 0)
				{
					return false;					
				}
				
				var packUnit:String = drugItemInfo.prepackPackSizeList.getItemAt(0).packUnit;
				
				for (var i:int = 0; i < drugItemInfo.prepackPackSizeList.length; i++)
				{
					if (packUnit != drugItemInfo.prepackPackSizeList.getItemAt(i).packUnit)
					{
						return false;
					}
				}
				
				return true;
			}
			
			private function packUnitCollectionFilter(item:Object):Boolean
			{
				for each (var value:String in latestPrepackSizeList)
				{
					if (item.dosageUnit == value)
					{
						return true;
					}
				}
				
				return false;
			}
			
			public function setEditableTab(tab1:Boolean, tab2:Boolean, tab3:Boolean):void {
				if (prepackRecordOriginalProductView != null) {
					prepackRecordOriginalProductView.enableEdit(tab1);
				}
				if (prepackRecordItemView != null) {
					prepackRecordItemView.enableEdit(tab2);					
				}
				if (prepackRecordActualPrepackInfoView != null) {
					prepackRecordActualPrepackInfoView.enableEdit(tab3);
				}
			}
			
			public var infoViewEditable:Boolean = false;
			
			private function prepackRecordActualPrepackInfoView_creationComplete():void {
				prepackRecordActualPrepackInfoView.enableEdit(infoViewEditable);
			}
			
			public function reset():void {
				resetForm(pPrepackRecord);
				if ( prepackRecordOriginalProductView != null ) {
					prepackRecordOriginalProductView.reset();
				}
				if ( prepackRecordActualPrepackInfoView != null ) {
					prepackRecordActualPrepackInfoView.reset();
				}
				if ( prepackRecordHistoryView != null ) {
					prepackRecordHistoryView.reset();
				}
				if ( prepackRecordItemView != null ) {
					prepackRecordItemView.reset();
				}
				ctx.drugItemInfo = null;
			}
			
			private function tabChangeHandler(evt:IndexChangedEvent):void
			{
				if (evt.newIndex == 1 && !prepackRecordItemView.isPrePackUnitFiltered && 
					(prepackRecordOriginalProductView.cbxDdu.selectedItem != null || 
					prepackRecordOriginalProductView.cbxDdu.selectedItem == undefined))
				{
					prepackRecordItemView.filterPrePackUnit(prepackRecord.prepackRecordVer.dispenseDosageUnit);
				}
			}
			
		]]>
	</fx:Script>
	
	<fc:layout>
		<s:VerticalLayout gap="0"/>
	</fc:layout>
	
	<fc:states>
		<s:State name="clear"/>
		<s:State name="saved"/>
		<s:State name="edit"/>
		<s:State name="suspend"/>
		<s:State name="allowLookup"/>
		<s:State name="disallowLookup"/>
	</fc:states>
	
	<s:VGroup id="pPrepackRecord" width="100%" height="100%" left="10" right="10" top="10" bottom="10" gap="0" paddingLeft="10" paddingRight="10" paddingBottom="10">
		<s:HGroup width="100%" verticalAlign="middle" paddingTop="10" paddingBottom="10">	
			<s:Label text="Item Code" width="120" verticalAlign="middle"/>
			<fc:UppercaseTextInput id="txtItemCode" width="90" maxChars="6" 
								   text="{drugItemInfo.itemCode}" 
								   enabled.clear="{true &amp;&amp; isEditable}" enabled.saved="{false &amp;&amp; isEditable}" enabled.edit="{false &amp;&amp; isEditable}" enabled.suspend="{false &amp;&amp; isEditable}"
								   enabled.allowLookup="{true &amp;&amp; isEditable}" enabled.disallowLookup="{true &amp;&amp; isEditable}" change="txtItemCode_changeHandler(event)"/>
			<fc:LookupButton id="btnItemCodeLookup" enabled="false" click="btnItemCodeLookup_clickHandler(event)" 
							 enabled.clear="false" enabled.saved="false" enabled.edit="false" enabled.suspend="false" enabled.allowLookup="true" enabled.disallowLookup="false"/>
			<s:TextInput id="txtItemDesc" width="440"
						 text="{drugItemInfo.dmDrug.fullDrugDesc}"
						 enabled="false" editable="false"/>
			<!--<mx:Spacer width="16"/>-->
			<s:HGroup width="100%" height="100%" verticalAlign="middle" horizontalAlign="right">
				<s:Label textAlign="left" text="Preparation Date" width="120"/>
				<fc:ExtendedAbbDateField id="dtfPreparationDate" width="120" showABBDateChooser="false"
									   selectedDate="@{prepackRecord.prepackRecordVer.preparationDate}"
									   enabled="false" editable="false"
									   change="dtfPreparationDate_dateChangeHandler()"/>
			</s:HGroup>
		</s:HGroup>
		
		<s:HGroup width="100%" verticalAlign="middle" horizontalAlign="right" paddingBottom="10">
			<s:Label textAlign="left" text="Assigned Lot No." width="120"/>
			<s:TextInput text="@{drugItemInfo.instCode}" enabled="false" editable="false" width="60"/>
			<mx:Spacer width="3"/>
			<s:Label text="-" width="15"/>
			<s:TextInput id="txtAssignedLotNum" text="@{prepackRecord.lotNum}" enabled="false" editable="false" width="100"/>
			<mx:Spacer width="392"/>
			<s:HGroup width="100%" height="100%" verticalAlign="middle" horizontalAlign="right">
				<s:Label textAlign="left" text="Pre-pack Status" width="120"/>
				<s:TextInput id="txtPrepackStatus" enabled="false" editable="false" width="120"
							 text="{prepackRecord.prepackRecordVer.prepackStatus}"/>
			</s:HGroup>
		</s:HGroup>		
		
		<mx:TabNavigator id="prepackingRecordTab" 
						 width="100%" height="100%" top="0" 
						 selectedIndex="0" change="tabChangeHandler(event)"
						 creationComplete="prepackingRecordTab_createComplete()">
			<prepack:PrepackRecordOriginalProductView id="prepackRecordOriginalProductView"
													  label="Original Product"
													  drugItemInfo="{this.drugItemInfo}"
													  dmDrug="{this.dmDrug}"
													  dmDrugList="{this.dmDrugList}"
													  prepackRecord="{this.prepackRecord}"
													  packUnitList="{this.packUnitList}"/>
			<prepack:PrepackRecordItemView id="prepackRecordItemView"
										   label="Pre-pack Item"
										   prepackRecord="{this.prepackRecord}"
										   drugItemInfo="{this.drugItemInfo}"
										   prepackPackSizeList="{this.prepackPackSizeList}"
										   equipmentList="{this.equipmentList}"
										   containerList="{this.containerList}"/>
			<prepack:PrepackRecordActualPrepackInfoView id="prepackRecordActualPrepackInfoView"
														label="Actual Pre-packing Info."
														creationComplete="prepackRecordActualPrepackInfoView_creationComplete();"
														prepackRecord="{this.prepackRecord}"
														drugItemInfo="{this.drugItemInfo}"
														discrepancyList="{this.discrepancyList}"
														actionTakenList="{this.actionTakenList}"
														/>
			<prepack:PrepackRecordHistoryView id="prepackRecordHistoryView"
											  prepackRecordVerList="{this.prepackRecordVerList}"
											  label="History"/>
		</mx:TabNavigator>
	</s:VGroup>	
</fc:ExtendedNavigatorContent>