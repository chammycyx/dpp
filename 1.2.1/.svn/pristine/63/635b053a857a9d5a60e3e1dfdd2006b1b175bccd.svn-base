package hk.org.ha.model.pms.dpp.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dpp.udt.DisplayAliasFlag;
import hk.org.ha.model.pms.dpp.udt.DisplaySideLabelAliasFlag;
import hk.org.ha.model.pms.dpp.udt.DisplaySideLabelShortDescFlag;
import hk.org.ha.model.pms.dpp.udt.PrepackFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name="DRUG_ITEM_INFO_VER")
@Customizer(AuditCustomizer.class)
public class DrugItemInfoVer extends VersionEntity {
	
	private static final long serialVersionUID = 3898873829828289410L;

	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="drugItemInfoVerSeq")
	@SequenceGenerator(name="drugItemInfoVerSeq", sequenceName="SQ_DRUG_ITEM_INFO_VER", initialValue=100000000)
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DRUG_ITEM_INFO_ID", referencedColumnName="ID")
	private DrugItemInfo drugItemInfo;
	
	@Column(name="ITEM_LONG_DESC", length=59)
	private String itemLongDesc;
	
	@Column(name="ITEM_SHORT_DESC", length=40)
	private String itemShortDesc;
	
	@Column(name="REMARK", length=90)
	private String remark;
	
	@Column(name="ALIAS", length=20)
	private String alias;
	
	@Converter(name="DrugItemInfoVer.prepackFlag", converterClass=PrepackFlag.Converter.class)
	@Convert("DrugItemInfoVer.prepackFlag")
	@Column(name="PREPACK_FLAG", length=1, nullable=false)
	private PrepackFlag prepackFlag = PrepackFlag.No;
	
	@Converter(name="DrugItemInfoVer.displayAliasFlag", converterClass=DisplayAliasFlag.Converter.class)
	@Convert("DrugItemInfoVer.displayAliasFlag")
	@Column(name="DISPLAY_ALIAS_FLAG", length=1, nullable=false)
	private DisplayAliasFlag displayAliasFlag = DisplayAliasFlag.No;
	
	@Converter(name="DrugItemInfoVer.displaySideLabelAliasFlag", converterClass=DisplaySideLabelAliasFlag.Converter.class)
	@Convert("DrugItemInfoVer.displaySideLabelAliasFlag")
	@Column(name="DISPLAY_SIDE_LABEL_ALIAS_FLAG", length=1, nullable=false)
	private DisplaySideLabelAliasFlag displaySideLabelAliasFlag = DisplaySideLabelAliasFlag.No;
	
	@Converter(name="DrugItemInfoVer.displaySideLabelShortDescFlag", converterClass=DisplaySideLabelShortDescFlag.Converter.class)
	@Convert("DrugItemInfoVer.displaySideLabelShortDescFlag")
	@Column(name="DISPLAY_SIDE_SHORT_DESC_FLAG", length=1, nullable=false)
	private DisplaySideLabelShortDescFlag displaySideLabelShortDescFlag = DisplaySideLabelShortDescFlag.No;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setDrugItemInfo(DrugItemInfo drugItemInfo) {
		this.drugItemInfo = drugItemInfo;
	}

	public DrugItemInfo getDrugItemInfo() {
		return drugItemInfo;
	}

	public void setItemLongDesc(String itemLongDesc) {
		this.itemLongDesc = itemLongDesc;
	}

	public String getItemLongDesc() {
		return itemLongDesc;
	}

	public void setItemShortDesc(String itemShortDesc) {
		this.itemShortDesc = itemShortDesc;
	}

	public String getItemShortDesc() {
		return itemShortDesc;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRemark() {
		return remark;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getAlias() {
		return alias;
	}

	public void setPrepackFlag(PrepackFlag prepackFlag) {
		this.prepackFlag = prepackFlag;
	}

	public PrepackFlag getPrepackFlag() {
		return prepackFlag;
	}

	public void setDisplayAliasFlag(DisplayAliasFlag displayAliasFlag) {
		this.displayAliasFlag = displayAliasFlag;
	}

	public DisplayAliasFlag getDisplayAliasFlag() {
		return displayAliasFlag;
	}
	
	public DisplaySideLabelAliasFlag getDisplaySideLabelAliasFlag() {
		return displaySideLabelAliasFlag;
	}

	public void setDisplaySideLabelAliasFlag(
			DisplaySideLabelAliasFlag displaySideLabelAliasFlag) {
		this.displaySideLabelAliasFlag = displaySideLabelAliasFlag;
	}

	public DisplaySideLabelShortDescFlag getDisplaySideLabelShortDescFlag() {
		return displaySideLabelShortDescFlag;
	}

	public void setDisplaySideLabelShortDescFlag(
			DisplaySideLabelShortDescFlag displaySideLabelShortDescFlag) {
		this.displaySideLabelShortDescFlag = displaySideLabelShortDescFlag;
	}
}
