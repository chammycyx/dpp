package hk.org.ha.model.pms.dpp.biz;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmInstDrug;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecord;
import hk.org.ha.model.pms.dpp.persistence.PrepackRecordVer;
import hk.org.ha.model.pms.dpp.udt.ActionStatus;
import hk.org.ha.model.pms.dpp.udt.ExpiryMonth;
import hk.org.ha.model.pms.dpp.udt.ExpiryWeek;
import hk.org.ha.model.pms.dpp.udt.PrepackStatus;

import hk.org.ha.model.pms.dpp.biz.InstLotNumServiceLocal;
import hk.org.ha.model.pms.dpp.cacher.DmDrugCacherInf;
import hk.org.ha.model.pms.dpp.cacher.DmInstDrugCacherInf;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("prepackRecordService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PrepackRecordServiceBean implements PrepackRecordServiceLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;

	private static final String LOTNUM_DATE_FORMAT = "yyMMdd";
	
	private SimpleDateFormat sdf;
	
	@Out(required=false)
	private PrepackRecord prepackRecord;
	
	@Out(required=false)
	private String prepackRecordVersion;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private DmInstDrugCacherInf dmInstDrugCacher;
	
	@In
	private UamInfo uamInfo;
	
	@In(create=true)
	private InstLotNumServiceLocal instLotNumService;
	
	public void addPrepackRecord() {
		logger.debug("addPrepackRecord");
		prepackRecord = new PrepackRecord();
		
		Calendar cal = Calendar.getInstance();
		
		PrepackRecordVer prepackRecordVer = prepackRecord.getPrepackRecordVer();
		prepackRecordVer.setAssignExpiryMonth(ExpiryMonth.TwelveMonths);
		prepackRecordVer.setAssignExpiryWeek(ExpiryWeek.ZeroWeeks);
		prepackRecordVer.setPrepackRecord(prepackRecord);
		prepackRecordVer.setContainer(null);
		prepackRecordVer.setEquipment(null);
		prepackRecordVer.setDrugItemInfoVer(null);
		prepackRecordVer.setPrepackPackSize(null);
		prepackRecordVer.setPreparationDate(new java.sql.Date( cal.getTime().getTime() ));
	}
	
	public void createPrepackRecord(PrepackRecord newPrepackRecord) {
		updatePrepackRecord(newPrepackRecord);
	}
	
	public void updatePrepackRecord(PrepackRecord newPrepackRecord) {
		logger.debug("updatePrepackRecord #0", newPrepackRecord);
		
		if(newPrepackRecord.getId() == null) {
			
			Calendar today = Calendar.getInstance();
			// for generate lot num
			Date todayDate = today.getTime();
			// today
			today.set(today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DATE), 0, 0, 0);
			Date todayStart = today.getTime();
			
			sdf = new SimpleDateFormat(LOTNUM_DATE_FORMAT, Locale.ENGLISH);
			String todayLotNumDate = sdf.format( todayDate );
						
			newPrepackRecord.setLotNum(todayLotNumDate + String.format("%03d", instLotNumService.newInstLotNum(uamInfo.getHospital()).intValue()));
			
			// persist parent and version, 1. terminate relation from version, 2. persist version, then parent, 3. set parent to version
			newPrepackRecord.getPrepackRecordVer().setPrepackRecord(null);
			newPrepackRecord.getPrepackRecordVer().setActionStatus(ActionStatus.New);
			
			em.persist(newPrepackRecord.getPrepackRecordVer());
			em.flush();
			
			em.persist(newPrepackRecord);
			newPrepackRecord.getPrepackRecordVer().setPrepackRecord(newPrepackRecord);
			em.flush();

			prepackRecord = newPrepackRecord;

		} else {
			PrepackRecord prepackRecordFind = em.find(PrepackRecord.class, newPrepackRecord.getId());
			
			if(!prepackRecordFind.getVersion().equals(newPrepackRecord.getVersion())) {
				throw new OptimisticLockException();
			}
			
			PrepackRecordVer newPrepackRecordVer = new PrepackRecordVer();
			newPrepackRecordVer.setManufCode(newPrepackRecord.getPrepackRecordVer().getManufCode());
			newPrepackRecordVer.setBatchNum(newPrepackRecord.getPrepackRecordVer().getBatchNum());
			newPrepackRecordVer.setExpiryDate(newPrepackRecord.getPrepackRecordVer().getExpiryDate());
			newPrepackRecordVer.setDispenseDosageUnit(newPrepackRecord.getPrepackRecordVer().getDispenseDosageUnit());
			newPrepackRecordVer.setOriginalPackQty(newPrepackRecord.getPrepackRecordVer().getOriginalPackQty());
			newPrepackRecordVer.setOriginalPackSize(newPrepackRecord.getPrepackRecordVer().getOriginalPackSize());
			newPrepackRecordVer.setOriginalTotalQty(newPrepackRecord.getPrepackRecordVer().getOriginalTotalQty());
			newPrepackRecordVer.setPreparationDate(newPrepackRecord.getPrepackRecordVer().getPreparationDate());
			newPrepackRecordVer.setAssignExpiryMonth(newPrepackRecord.getPrepackRecordVer().getAssignExpiryMonth());
			newPrepackRecordVer.setAssignExpiryWeek(newPrepackRecord.getPrepackRecordVer().getAssignExpiryWeek());
			newPrepackRecordVer.setAssignExpiryDate(newPrepackRecord.getPrepackRecordVer().getAssignExpiryDate());
			newPrepackRecordVer.setSampleCheckQty(newPrepackRecord.getPrepackRecordVer().getSampleCheckQty());
			newPrepackRecordVer.setProvisionPrepackQty(newPrepackRecord.getPrepackRecordVer().getProvisionPrepackQty());
			newPrepackRecordVer.setProvisionPrepackLabelQty(newPrepackRecord.getPrepackRecordVer().getProvisionPrepackLabelQty());
			newPrepackRecordVer.setProvisionRemainQty(newPrepackRecord.getPrepackRecordVer().getProvisionRemainQty());
			newPrepackRecordVer.setLabelSize(newPrepackRecord.getPrepackRecordVer().getLabelSize());
			newPrepackRecordVer.setPrepackPackSize(newPrepackRecord.getPrepackRecordVer().getPrepackPackSize());
			newPrepackRecordVer.setActualPrepackDate(newPrepackRecord.getPrepackRecordVer().getActualPrepackDate());
			newPrepackRecordVer.setActualPrepackQty(newPrepackRecord.getPrepackRecordVer().getActualPrepackQty());
			newPrepackRecordVer.setActualLabelRemainQty(newPrepackRecord.getPrepackRecordVer().getActualLabelRemainQty());
			newPrepackRecordVer.setActualAddLabelQty(newPrepackRecord.getPrepackRecordVer().getActualAddLabelQty());
			newPrepackRecordVer.setActualItemRemainQty(newPrepackRecord.getPrepackRecordVer().getActualItemRemainQty());
			newPrepackRecordVer.setPrepareBy(newPrepackRecord.getPrepackRecordVer().getPrepareBy());
			newPrepackRecordVer.setPreCheckBy(newPrepackRecord.getPrepackRecordVer().getPreCheckBy()==null?null:newPrepackRecord.getPrepackRecordVer().getPreCheckBy().toLowerCase());
			newPrepackRecordVer.setLineClearCheckBy(newPrepackRecord.getPrepackRecordVer().getLineClearCheckBy()==null?null:newPrepackRecord.getPrepackRecordVer().getLineClearCheckBy().toLowerCase());
			newPrepackRecordVer.setPrepackBy(newPrepackRecord.getPrepackRecordVer().getPrepackBy()==null?null:newPrepackRecord.getPrepackRecordVer().getPrepackBy().toLowerCase());
			newPrepackRecordVer.setFinalCheckBy(newPrepackRecord.getPrepackRecordVer().getFinalCheckBy()==null?null:newPrepackRecord.getPrepackRecordVer().getFinalCheckBy().toLowerCase());
			newPrepackRecordVer.setDiscrepancyRemark(newPrepackRecord.getPrepackRecordVer().getDiscrepancyRemark());
			newPrepackRecordVer.setActionTakenRemark(newPrepackRecord.getPrepackRecordVer().getActionTakenRemark());
			newPrepackRecordVer.setPrepackStatus(newPrepackRecord.getPrepackRecordVer().getPrepackStatus());
			newPrepackRecordVer.setRecordStatus(newPrepackRecord.getPrepackRecordVer().getRecordStatus());
			newPrepackRecordVer.setCancelReason(newPrepackRecord.getPrepackRecordVer().getCancelReason());
			newPrepackRecordVer.setActionStatus(newPrepackRecord.getPrepackRecordVer().getActionStatus());
			
			// TODO: lazy get
			newPrepackRecordVer.setContainer(newPrepackRecord.getPrepackRecordVer().getContainer());
			newPrepackRecordVer.setEquipment(newPrepackRecord.getPrepackRecordVer().getEquipment());
			newPrepackRecordVer.setPrepackPackSize(newPrepackRecord.getPrepackRecordVer().getPrepackPackSize());
			newPrepackRecordVer.setDrugItemInfoVer(newPrepackRecord.getPrepackRecordVer().getDrugItemInfoVer());

			newPrepackRecordVer.setPrepackRecord(prepackRecordFind);	
			prepackRecordFind.addPrepackRecordVer(newPrepackRecordVer);
			prepackRecordFind.setPrepackRecordVer(newPrepackRecordVer);
			
			prepackRecord = em.merge(prepackRecordFind);
			
			em.flush();
		}
	}
	
	public void cancelPrepackRecord(PrepackRecord prepackRecordCancel) {
		logger.debug("cancelPrepackRecord #0", prepackRecordCancel);
		if (prepackRecordCancel != null) {
			prepackRecordCancel.getPrepackRecordVer().setPrepackStatus(PrepackStatus.Cancelled);
			prepackRecordCancel.getPrepackRecordVer().setActionStatus(ActionStatus.Cancelled);
			// Action Status
			updatePrepackRecord(prepackRecordCancel);
		}
	}

	public void retrievePrepackRecordById(Long prepackRecordId) {
		logger.debug("retrievePrepackRecordById #0", prepackRecordId);
		
		Query q = em.createNamedQuery("PrepackRecord.findById")
			.setParameter("id", prepackRecordId);
		
		List<PrepackRecord> prepackRecordList = q.getResultList();
		
		if (prepackRecordList.size() == 0) {
			prepackRecord = null;
		} else {
			prepackRecord = prepackRecordList.get(0);
			prepackRecord.getPrepackRecordVer();
			prepackRecord.getPrepackRecordVer().getPrepackRecord();
			prepackRecord.getPrepackRecordVer().getContainer();
			prepackRecord.getPrepackRecordVer().getEquipment();
			prepackRecord.getPrepackRecordVer().getPrepackPackSize();
			prepackRecord.getPrepackRecordVer().getDrugItemInfoVer();
			prepackRecord.getPrepackRecordVer().getDrugItemInfoVer().getDrugItemInfo();
			prepackRecord.getPrepackRecordVer().getDrugItemInfoVer().getDrugItemInfo().getDrugItemInfoVer();
			DmDrug dmDrug = dmDrugCacher.getDrugByItemCode(prepackRecord.getPrepackRecordVer().getDrugItemInfoVer().getDrugItemInfo().getItemCode());
			DmInstDrug dmInstDrug = dmInstDrugCacher.getDrugByItemCode(prepackRecord.getPrepackRecordVer().getDrugItemInfoVer().getDrugItemInfo().getItemCode());
			
			if((dmInstDrug != null && dmInstDrug.getInstSuspend().equals("Y")) || dmInstDrug == null) {
				prepackRecord.getPrepackRecordVer().getDrugItemInfoVer().getDrugItemInfo().setInstSuspend("Y");
			} else {
				prepackRecord.getPrepackRecordVer().getDrugItemInfoVer().getDrugItemInfo().setInstSuspend("N");
			}
			
			// Indicate no DM Drug found
			if(dmDrug == null) {
				dmDrug = new DmDrug();
			}
			
			prepackRecord.getPrepackRecordVer().getDrugItemInfoVer().getDrugItemInfo().setDmDrug(dmDrug);
		}
	}
	
	public void retrievePrepackRecordByNotNo(String lotNo) {
		logger.debug(lotNo);
		
		Query q = em.createNamedQuery("PrepackRecord.findByLotNum")
			.setParameter("lotNum", lotNo)
			.setParameter("workstoreCode", uamInfo.getWorkstore())
			.setParameter("instCode", uamInfo.getHospital());
		
		List<PrepackRecord> prepackRecordList = q.getResultList();
		
		if (prepackRecordList.size() == 0) {
			prepackRecord = null;
		} else {
			prepackRecord = prepackRecordList.get(0);
			prepackRecord.getPrepackRecordVer();
			prepackRecord.getPrepackRecordVer().getPrepackRecord();
			prepackRecord.getPrepackRecordVer().getContainer();
			prepackRecord.getPrepackRecordVer().getEquipment();
			prepackRecord.getPrepackRecordVer().getPrepackPackSize();
			prepackRecord.getPrepackRecordVer().getDrugItemInfoVer();
			prepackRecord.getPrepackRecordVer().getDrugItemInfoVer().getDrugItemInfo();
			prepackRecord.getPrepackRecordVer().getDrugItemInfoVer().getDrugItemInfo().getDrugItemInfoVer();
		}
	}
	
	public void retrievePrepackRecordVersion(Long id) {
		logger.debug(id);
		
		Query q = em.createNamedQuery("PrepackRecord.findById")
					.setParameter("id", id);
		
		prepackRecordVersion = ((PrepackRecord) q.getResultList().get(0)).getVersion().toString();
		
		logger.debug("prepackRecordVersion #0", prepackRecordVersion);
	}
	
	@Remove
	public void destroy() {
		if (prepackRecord != null) {
			prepackRecord = null;
		}

	}

}
