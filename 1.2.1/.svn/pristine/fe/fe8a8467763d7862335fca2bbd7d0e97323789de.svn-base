package hk.org.ha.model.pms.dpp.cacher;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsDppServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmDrugCacher")
@Scope(ScopeType.APPLICATION)
public class DmDrugCacher extends BaseCacher implements DmDrugCacherInf{
	
	@In
	private DmsDppServiceJmsRemote dmsDppServiceProxy;
	
	private Map<String, InnerCacher> cacherMap = new HashMap<String, InnerCacher>();
	
	private static final String CACHER_PREFIX = "ABCDEFGHIJKLMNOPQRSTUVWXYZ?";
         
	public void load() {
		for(int i=0;i<CACHER_PREFIX.length();i++) {
			this.getCacher(Character.toString(CACHER_PREFIX.charAt(i))).getAll();
		}
	}
	
	public void clear() {
		synchronized (this) {
			cacherMap.clear();
		}
	}

	public DmDrug getDrugByItemCode(String itemCode) {
		DmDrug dmDrug = (DmDrug) this.getCacher(this.convertPrefix(itemCode)).get(itemCode);
		if(dmDrug != null) {
			dmDrug.getFullDrugDesc();
		}
		return dmDrug;
	}
		
	public List<DmDrug> getDrugListByItemCode(String prefixItemCode) {
		List<DmDrug> ret = new ArrayList<DmDrug>();
		for (DmDrug dmDrug : getDrugList(this.convertPrefix(prefixItemCode))) {
			if (dmDrug.getItemCode().startsWith(prefixItemCode)) {
				dmDrug.getFullDrugDesc();
				ret.add(dmDrug);
			}
		}
		return ret;
	}
	
	public List<DmDrug> getDrugList(String prefix) {
		return (List<DmDrug>) this.getCacher(prefix).getAll();
	}
	
	private String convertPrefix(String itemCode) {
		char c = itemCode.charAt(0);
		if (c >= 'A' && c <= 'Z') {
			return String.valueOf(c);
		} else {
			return "?";
		}
	}

	private InnerCacher getCacher(String prefix) {
		synchronized (this) {
			InnerCacher cacher = cacherMap.get(prefix);
			
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime(), prefix);
				cacherMap.put(prefix, cacher);
			}
			
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, DmDrug>
	{		
		private String prefix = null;
		
        public InnerCacher(int expireTime, String prefix) {
              super(expireTime);
              this.prefix = prefix;
        }
		
		public DmDrug create(String itemCode) {	
			this.getAll();
			return this.internalGet(itemCode);
		}

		public Collection<DmDrug> createAll() {
			return dmsDppServiceProxy.retrieveDmDrugListByItemCodePrefix(prefix);
		}

		public String retrieveKey(DmDrug dmDrug) {
			return dmDrug.getItemCode();
		}		
	}
	
	public static DmDrugCacherInf instance()
	{
		  if (!Contexts.isApplicationContextActive())
	      {
	         throw new IllegalStateException("No active application scope");
	      }
	      return (DmDrugCacherInf) Component.getInstance("dmDrugCacher", ScopeType.APPLICATION);
    }	
}
