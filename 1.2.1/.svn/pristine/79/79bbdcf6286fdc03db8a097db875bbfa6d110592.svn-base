<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009"
	xmlns:s="library://ns.adobe.com/flex/spark"
	xmlns:mx="library://ns.adobe.com/flex/mx" 
	width="100%" height="100%">
	
	<fx:Metadata>
		[Name("actionTakenMaintView")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.dpp.RetrieveActionTakenListEvent;
			import hk.org.ha.event.pms.dpp.UpdateActionTakenListEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.model.pms.dpp.persistence.ActionTaken;
			import hk.org.ha.model.pms.dpp.udt.RecordStatus;
			
			import mx.collections.ArrayCollection;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.DataGridEvent;
			import mx.managers.PopUpManager;
			
			[In] [Bindable]
			public var actionTakenList:ArrayCollection;
			
			private var actionTaken:ActionTaken;
			
			//use to prevent exception from switching screen
			public var screenActive:Boolean;
			
			public var isChanged:Boolean;
			
			[Bindable]
			private var girdContextMenu:ContextMenu = new ContextMenu();
			private var addMenuItem:ContextMenuItem = new ContextMenuItem("Add ActionTaken");
			private var refreshMenu:ContextMenuItem = new ContextMenuItem("Refresh", true);
			
			public override function onHide():void 
			{
				ctx.actionTakenList = null;
				screenActive = false;
			}

			public override function onShowLater():void {
				screenActive = true;
				
				//disable restore focus
				enableFocusOnShow = false;
				
				// Refresh/auto retrieve list, reset isChange flag
				dispatchEvent(new RetrieveActionTakenListEvent);
				isChanged = false;

				// initialize toolbar
				if (!dppToolbar.hasInited()) {
					dppToolbar.saveYesFunc = saveYesHandler;
					dppToolbar.init();
				}
			}
			
			private function saveYesHandler():void {
				// remove new add deleted record
				for(var i:int=actionTakenList.length-1;i>=0;i--) {
					var actionTaken:ActionTaken = actionTakenList.getItemAt(i) as ActionTaken;
					if(isNaN(actionTaken.id) && actionTaken.recordStatus == RecordStatus.Deleted) {
						actionTakenList.removeItemAt(i);
					}
				}
				
				// apply validation before saving
				if(!validateActionTakenList()) {
					return;
				}
				
				// dispatch update event and reset isChange flag
				dispatchEvent(new UpdateActionTakenListEvent(actionTakenList));
				isChanged = false;
			}
			
			// validation before saving
			private function validateActionTakenList():Boolean {
				// 1. check if name field leave blank
				for each(var actionTaken:ActionTaken in actionTakenList) {
					if(actionTaken.actionTakenName == "") {
						showSystemMessage("0018");
						return false;
					}
				}
				
				// 2. check if any duplicated name
				for(var i:int=0;i<actionTakenList.length-1;i++) {
					if(actionTakenList.getItemAt(i).recordStatus == RecordStatus.Active) {
						for(var j:int=i+1;j<actionTakenList.length;j++) {
							if(actionTakenList.getItemAt(j).recordStatus == RecordStatus.Active && 
								actionTakenList.getItemAt(i).actionTakenName.toLowerCase() == actionTakenList.getItemAt(j).actionTakenName.toLowerCase()) {
								showSystemMessage("0017");
								return false;
							} 
						}
					}
				}
				return true;
			}
			
			private function actionTakenAddMenuRecordFunction(evt:ContextMenuEvent):void {
				// Add blank object into list (e.g. status set to active, name set to blank), and set focus onto name field 
				var actionTaken:ActionTaken = new ActionTaken();
				actionTaken.actionTakenName = "";
				actionTaken.recordStatus = RecordStatus.Active;
				actionTakenList.addItem(actionTaken);
				actionTakenGrid.selectedItem = actionTaken;
				actionTakenGrid.editedItemPosition={rowIndex: actionTakenGrid.selectedIndex, columnIndex: 1};
			}
			
			private function actionTakenRefreshFunction(evt:ContextMenuEvent):void {
				if(isChanged) {
					showSystemMessage("0007", null, refreshConfirmationYesHandler);
				} else {
					//remove List in order to unfocus any textInput
					actionTakenList.removeAll();
					dispatchEvent(new RetrieveActionTakenListEvent);
					isChanged = false;
				}
			}
			
			private function refreshConfirmationYesHandler(evt:MouseEvent):void {
				PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
				//remove List in order to unfocus any textInput
				actionTakenList.removeAll();
				dispatchEvent(new RetrieveActionTakenListEvent);
				isChanged = false;
			}
			
			protected function initContextMenu():void {
				girdContextMenu.hideBuiltInItems();
				girdContextMenu.customItems = [addMenuItem, refreshMenu];
				
				// initialize context menu
				addMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, actionTakenAddMenuRecordFunction);
				refreshMenu.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, actionTakenRefreshFunction);
				
				actionTakenGrid.contextMenu = girdContextMenu;
			}
			
			private function destroyGridEditor(evt:DataGridEvent):void {
				var dataGrid:ExtendedDataGrid = evt.currentTarget as ExtendedDataGrid;
				dataGrid.destroyItemEditor();
			}
			
			private function showSystemMessage(errorCode:String, okFunc:Function=null, yesFunc:Function=null):void {
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				
				if(okFunc != null) {
					msgProp.setOkCancelButton = true;
					msgProp.okHandler = okFunc;
				} else if(yesFunc != null) {
					msgProp.setYesNoButton = true;
					msgProp.yesHandler = yesFunc;
				} else {
					msgProp.setOkButtonOnly = true;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			private function dataGridItemEditBeginningHandler(evt:DataGridEvent):void
			{
				if (!screenActive) {
					evt.preventDefault();
					return;
				}
			}
			
		]]>
	</fx:Script>
	
	<fc:layout>
		<s:VerticalLayout gap="0"/>
	</fc:layout>
	
	<fc:Toolbar id="dppToolbar" width="100%"/>
	
	<s:VGroup width="100%" height="100%" gap="10" paddingTop="10" paddingRight="10" paddingLeft="10" paddingBottom="10">
		<fc:ExtendedDataGrid id="actionTakenGrid" dataProvider="{actionTakenList}" editable="true" verticalScrollPolicy="on" draggableColumns="false"
					 selectable="true" height="100%" width="100%" itemEditBeginning="dataGridItemEditBeginningHandler(event)" 
					 itemEditEnd="destroyGridEditor(event)" sortableColumns="false" contextMenu="{girdContextMenu}" creationComplete="initContextMenu()">
			<fc:columns>
				<mx:DataGridColumn headerText="Delete" dataField="recordStatus" editable="false" sortable="false" width="50" resizable="false">
					<mx:itemRenderer>
						<fx:Component>
							<s:MXDataGridItemRenderer>
								<fx:Script>
									<![CDATA[
										import hk.org.ha.model.pms.dpp.udt.RecordStatus;
									]]>
								</fx:Script>
								<fx:Binding source="{recordStatusChx.selected?RecordStatus.Deleted:RecordStatus.Active}" destination="data.recordStatus" />
								<s:layout>
									<s:HorizontalLayout horizontalAlign="center"/>
								</s:layout>
								<s:CheckBox id="recordStatusChx" selected="{data.recordStatus == RecordStatus.Deleted}" click="outerDocument.isChanged = true"/>
							</s:MXDataGridItemRenderer>
						</fx:Component>
					</mx:itemRenderer>
				</mx:DataGridColumn>
				<mx:DataGridColumn headerWordWrap="true" headerText="Action Taken" dataField="actionTakenName" showDataTips="true" dataTipField="actionTakenName" minWidth="100" width="905">
					<mx:itemEditor>
						<fx:Component>
							<s:MXDataGridItemRenderer>
								<s:TextInput id="actionTakenNameTxt" width="100%" maxChars="160" text="@{data.actionTakenName}" creationComplete="actionTakenNameTxt.setFocus()" change="outerDocument.isChanged = true" />
							</s:MXDataGridItemRenderer>
						</fx:Component>
					</mx:itemEditor>
				</mx:DataGridColumn>
			</fc:columns>
		</fc:ExtendedDataGrid>
	</s:VGroup>
	
</fc:ExtendedNavigatorContent>