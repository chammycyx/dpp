<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="DppLabel2" language="groovy" pageWidth="288" pageHeight="99" columnWidth="288" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0">
	<property name="ireport.zoom" value="2.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="hk.org.ha.model.pms.dpp.util.QrCodeHelper"/>
	<style name="PackUnitStyle">
		<conditionalStyle>
			<conditionExpression><![CDATA[new Boolean($F{packUnit}.length() <= 10)]]></conditionExpression>
			<style fill="Solid" fontName="Arial Narrow" fontSize="7" isBold="true"/>
		</conditionalStyle>
		<conditionalStyle>
			<conditionExpression><![CDATA[new Boolean($F{packUnit}.length() > 10)]]></conditionExpression>
			<style fill="Solid" fontName="Arial Narrow" fontSize="6" isBold="true"/>
		</conditionalStyle>
	</style>
	<parameter name="IS_SAMPLE" class="java.lang.Boolean">
		<defaultValueExpression><![CDATA[false]]></defaultValueExpression>
	</parameter>
	<parameter name="LABEL_NO" class="java.lang.Integer"/>
	<field name="itemCode" class="java.lang.String"/>
	<field name="packSize" class="java.lang.String"/>
	<field name="lotNum" class="java.lang.String"/>
	<field name="batchNum" class="java.lang.String"/>
	<field name="expiryDate" class="java.lang.String"/>
	<field name="instCode" class="java.lang.String"/>
	<field name="itemDisplayName" class="java.lang.String"/>
	<field name="qrCode" class="java.lang.String"/>
	<field name="packUnit" class="java.lang.String"/>
	<field name="sideLabelDesc" class="java.lang.String"/>
	<field name="workstoreCode" class="java.lang.String"/>
	<variable name="concatPackSizeUnitFlag" class="java.lang.Boolean">
		<variableExpression><![CDATA[new Boolean($F{packSize}.length() + $F{packUnit}.length() <= 13)]]></variableExpression>
	</variable>
	<background>
		<band height="99" splitType="Stretch">
			<textField>
				<reportElement x="26" y="83" width="57" height="14" forecolor="#3C3C3C">
					<printWhenExpression><![CDATA[$P{IS_SAMPLE}==true]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font fontName="Times New Roman" size="11" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["SAMPLE"]]></textFieldExpression>
			</textField>
		</band>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="99" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement x="128" y="23" width="52" height="13"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Narrow" size="9" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{itemCode}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="121" y="41" width="55" height="13"/>
				<textElement>
					<font fontName="Arial Narrow" size="11" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{expiryDate}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="113" y="34" width="35" height="11"/>
				<textElement>
					<font fontName="Arial Narrow" size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[Use Before:]]></text>
			</staticText>
			<image scaleImage="Clip" hAlign="Center" vAlign="Middle">
				<reportElement x="48" y="23" width="65" height="65"/>
				<imageExpression class="java.io.InputStream"><![CDATA[QrCodeHelper.createQrCodeImageStream($F{qrCode}, 65, 65)]]></imageExpression>
			</image>
			<textField isBlankWhenNull="true">
				<reportElement x="112" y="76" width="33" height="11"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Narrow" size="7" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{instCode} + " " + $F{workstoreCode}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="144" y="76" width="36" height="11"/>
				<box>
					<leftPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Narrow" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{lotNum}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="79" y="86" width="101" height="11"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Narrow" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{batchNum}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="48" y="3" width="132" height="21"/>
				<textElement>
					<font fontName="Arial Narrow" size="9" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{itemDisplayName}]]></textFieldExpression>
			</textField>
			<elementGroup/>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="3" width="35" height="93"/>
				<box>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="Left">
					<font fontName="Arial Narrow" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{sideLabelDesc}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="113" y="57" width="67" height="11"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Narrow" size="9" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$V{concatPackSizeUnitFlag}?$F{packSize} + " " + $F{packUnit}:$F{packSize}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="113" y="65" width="63" height="10">
					<printWhenExpression><![CDATA[!$V{concatPackSizeUnitFlag}]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial Narrow" size="7" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{packUnit}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
