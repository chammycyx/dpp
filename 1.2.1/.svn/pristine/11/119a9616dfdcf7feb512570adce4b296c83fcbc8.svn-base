package hk.org.ha.model.pms.dpp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum RecordStatus implements StringValuedEnum {
	
	Active("A", "Active"),
	Deleted("D", "Logical Deleted");	
	
    private final String dataValue;
    private final String displayValue;
        
    RecordStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<RecordStatus> {

		private static final long serialVersionUID = -7968478287058011303L;

		@Override
    	public Class<RecordStatus> getEnumClass() {
    		return RecordStatus.class;
    	}
    }
}
