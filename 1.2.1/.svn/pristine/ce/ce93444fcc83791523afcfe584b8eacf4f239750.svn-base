<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009"
	xmlns:s="library://ns.adobe.com/flex/spark"
	xmlns:mx="library://ns.adobe.com/flex/mx" 
	width="100%" height="100%">
	
	<fx:Metadata>
		[Name("equipmentMaintView")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.dpp.RetrieveEquipmentListEvent;
			import hk.org.ha.event.pms.dpp.UpdateEquipmentListEvent;
			import hk.org.ha.fmk.pms.flex.components.core.ExtendedDataGrid;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.model.pms.dpp.persistence.Equipment;
			import hk.org.ha.model.pms.dpp.udt.RecordStatus;
			
			import mx.collections.ArrayCollection;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.DataGridEvent;
			import mx.managers.PopUpManager;
			
			[In] [Bindable]
			public var equipmentList:ArrayCollection;
			
			private var equipment:Equipment;
			
			//use to prevent exception from switching screen
			public var screenActive:Boolean;
			
			public var isChanged:Boolean;
			
			[Bindable]
			private var girdContextMenu:ContextMenu = new ContextMenu();
			private var addMenuItem:ContextMenuItem = new ContextMenuItem("Add Equipment");
			private var refreshMenu:ContextMenuItem = new ContextMenuItem("Refresh", true);
			
			public override function onHide():void 
			{
				ctx.equipmentList = null;
				screenActive = false;
			}
			
			public override function onShowLater():void {
				screenActive = true;
				
				//disable restore focus
				enableFocusOnShow = false;
				
				// Refresh/auto retrieve list, reset isChange flag
				dispatchEvent(new RetrieveEquipmentListEvent);
				isChanged = false;
				
				// initialize toolbar
				if (!dppToolbar.hasInited()) {
					dppToolbar.saveYesFunc = saveYesHandler;
					dppToolbar.init();
				}
			}
			
			private function saveYesHandler():void {
				// remove new add deleted record
				for(var i:int=equipmentList.length-1;i>=0;i--) {
					var equipment:Equipment = equipmentList.getItemAt(i) as Equipment;
					if(isNaN(equipment.id) && equipment.recordStatus == RecordStatus.Deleted) {
						equipmentList.removeItemAt(i);
					}
				}
				
				// apply validation before saving
				if(!validateEquipmentList()) {
					return;
				}
				
				// dispatch update event and reset isChange flag
				dispatchEvent(new UpdateEquipmentListEvent(equipmentList));
				isChanged = false;
			}
			
			// validation before saving
			private function validateEquipmentList():Boolean {
				// 1. check if name field leave blank
				for each(var equipment:Equipment in equipmentList) {
					if(equipment.equipmentName == "") {
						showSystemMessage("0009");
						return false;
					}
				}
				
				// 2. check if any duplicated name
				for(var i:int=0;i<equipmentList.length-1;i++) {
					if(equipmentList.getItemAt(i).recordStatus == RecordStatus.Active) {
						for(var j:int=i+1;j<equipmentList.length;j++) {
							if(equipmentList.getItemAt(j).recordStatus == RecordStatus.Active && 
								equipmentList.getItemAt(i).equipmentName.toLowerCase() == equipmentList.getItemAt(j).equipmentName.toLowerCase()) {
								showSystemMessage("0008");
								return false;
							} 
						}
					}
				}
				return true;
			}
			
			private function equipmentAddMenuRecordFunction(evt:ContextMenuEvent):void {
				// Add blank object into list (e.g. status set to active, name set to blank), and set focus onto name field 
				var equipment:Equipment = new Equipment();
				equipment.equipmentName = "";
				equipment.recordStatus = RecordStatus.Active;
				equipmentList.addItem(equipment);
				equipmentGrid.selectedItem = equipment;
				equipmentGrid.editedItemPosition={rowIndex: equipmentGrid.selectedIndex, columnIndex: 1};
			}
			
			private function equipmentRefreshFunction(evt:ContextMenuEvent):void {
				if(isChanged) {
					showSystemMessage("0007", null, refreshConfirmationYesHandler);
				} else {
					//remove List in order to unfocus any textInput
					equipmentList.removeAll();
					dispatchEvent(new RetrieveEquipmentListEvent);
					isChanged = false;
				}
			}
			
			private function refreshConfirmationYesHandler(evt:MouseEvent):void {
				PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
				//remove List in order to unfocus any textInput
				equipmentList.removeAll();
				dispatchEvent(new RetrieveEquipmentListEvent);
				isChanged = false;
			}
			
			protected function initContextMenu():void {
				// initialize context menu
				addMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, equipmentAddMenuRecordFunction);
				refreshMenu.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, equipmentRefreshFunction);
				
				girdContextMenu.hideBuiltInItems();
				girdContextMenu.customItems = [addMenuItem, refreshMenu];
				
				equipmentGrid.contextMenu = girdContextMenu;
			}
			
			private function destroyGridEditor(evt:DataGridEvent):void {
				var dataGrid:ExtendedDataGrid = evt.currentTarget as ExtendedDataGrid;
				dataGrid.destroyItemEditor();
			}
			
			private function showSystemMessage(errorCode:String, okFunc:Function=null, yesFunc:Function=null):void {
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				
				if(okFunc != null) {
					msgProp.setOkCancelButton = true;
					msgProp.okHandler = okFunc;
				} else if(yesFunc != null) {
					msgProp.setYesNoButton = true;
					msgProp.yesHandler = yesFunc;
				} else {
					msgProp.setOkButtonOnly = true;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			private function dataGridItemEditBeginningHandler(evt:DataGridEvent):void
			{
				if (!screenActive) {
					evt.preventDefault();
					return;
				}
			}
			
		]]>
	</fx:Script>
	
	<fc:layout>
		<s:VerticalLayout gap="0"/>
	</fc:layout>
	
	<fc:Toolbar id="dppToolbar" width="100%"/>
	
	<s:VGroup width="100%" height="100%" gap="10" paddingTop="10" paddingRight="10" paddingLeft="10" paddingBottom="10">
		<fc:ExtendedDataGrid id="equipmentGrid" dataProvider="{equipmentList}" 
							 editable="true" verticalScrollPolicy="on" draggableColumns="false"
							 selectable="true" mouseEnabled="true" height="100%" width="100%" itemEditBeginning="dataGridItemEditBeginningHandler(event)"
							 itemEditEnd="destroyGridEditor(event)" sortableColumns="false" creationComplete="initContextMenu()">
			<fc:columns>
				<mx:DataGridColumn headerText="Delete" dataField="recordStatus" editable="false" sortable="false" width="50" resizable="false">
					<mx:itemRenderer>
						<fx:Component>
							<s:MXDataGridItemRenderer>
								<fx:Script>
									<![CDATA[
										import hk.org.ha.model.pms.dpp.udt.RecordStatus;
									]]>
								</fx:Script>
								<fx:Binding source="{recordStatusChx.selected?RecordStatus.Deleted:RecordStatus.Active}" destination="data.recordStatus" />
								<s:layout>
									<s:HorizontalLayout horizontalAlign="center"/>
								</s:layout>
								<s:CheckBox id="recordStatusChx" selected="{data.recordStatus == RecordStatus.Deleted}" click="outerDocument.isChanged = true"/>
							</s:MXDataGridItemRenderer>
						</fx:Component>
					</mx:itemRenderer>
				</mx:DataGridColumn>
				<mx:DataGridColumn headerWordWrap="true" headerText="Equipment" dataField="equipmentName" minWidth="100" width="905">
					<mx:itemEditor>
						<fx:Component>
							<s:MXDataGridItemRenderer>
								<s:TextInput id="equipmentNameTxt" width="100%" maxChars="60" text="@{data.equipmentName}" creationComplete="equipmentNameTxt.setFocus()" change="outerDocument.isChanged = true" />
							</s:MXDataGridItemRenderer>
						</fx:Component>
					</mx:itemEditor>
				</mx:DataGridColumn>
			</fc:columns>
		</fc:ExtendedDataGrid>
	</s:VGroup>
	
</fc:ExtendedNavigatorContent>